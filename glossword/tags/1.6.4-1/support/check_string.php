<?php
/**
 * $Id: check_string.php,v 1.7 2004/11/14 10:48:28 yrtimd Exp $
 * 
 * Checks whether substring() function is working correctly.
 */
// --------------------------------------------------------
// protects script from wrong virtual host configuration
if (defined('IN_GW'))
{
    die("<!-- leave empty -->");
}
/* Change current directory to access for website files */
if (!@chdir('..'))
{
    exit(print "Can't change directory to `..'");
}
define('IN_GW', TRUE);
error_reporting(E_ALL);

/* Load configuration */
$sys['path_include'] = "inc";
include_once('/etc/glossword/db_config.php');
$sys['is_prepend'] = 0;
include_once($sys['path_include'] . "/config.inc.php");

if (file_exists('gw_install/install_functions.php'))
{
	include_once('gw_install/install_functions.php');
}
else
{
	printf('<br/><b>Error:</b> File %s required.', 'gw_install/install_functions.php');
}


/* --------------------------------------------------------
 * Translation kit
 * -------------------------------------------------------- */
define('GW_T_LANGUAGE', 'il');
include_once($sys['path_include'] . '/class.gwtk.php');
$gv['vars'][GW_T_LANGUAGE] = 'en';
$oL = new gwtk;
$oL->setHomeDir('gw_install/gw_locale');
$oL->setLocale($gv['vars'][GW_T_LANGUAGE].'-utf8');
$oL->getCustom('l_install', $gv['vars'][GW_T_LANGUAGE].'-utf8');



/* */
function gw_check_string($length = 1)
{
	global $arStr, $oFunc, $oL;
	print '<div class="contents u">';
	print '<p>'.sprintf($oL->m('016'), $length).'</p>';
	print '<table cellpadding="2" cellspacing="1" width="75%">';
	$int_length = 0;
	for (reset($arStr); list($k, $v) = each($arStr);)
	{
		print '<tr>';
		$int_length = $oFunc->mb_strlen($oFunc->mb_substr($v, 0, $length));
		$str_status = ($int_length == $length) 
					? sprintf('<span class="green">%s</span>', $oL->m('017')) 
					: sprintf('<span class="red">%s</span>', $oL->m('018'));
		print '<td style="width:10%">'.$oFunc->mb_substr($v, 0, $length) . '</td>';
		print '<td style="width:5%">' . $int_length . '</td>';
		print '<td>' . $str_status . '</td>';
		print '</tr>';
	}
	print '</table>';
	print '</div>';
}
function gw_check_settings()
{
	if (function_exists('mb_get_info'))
	{
		print '<div class="contents u">';
		print '<p><tt>mb_get_info()</tt></p>';
		html_array_to_table(mb_get_info('all'));
		print '</div>';
	}
}

/* */

$sys['path_css'] =  $sys['server_dir'].'/'.$sys['path_tpl'].'/'.$sys['themename'];
$sys['html_title'] = sprintf($oL->m('019'));
$arStr = array('123', 'abc', 'бвг', 'ԱԲԱՍ', '签字仪式');

/* */
gw_html_open();

gw_check_string(1);
gw_check_string(2);
gw_check_settings();

gw_html_close();

?>