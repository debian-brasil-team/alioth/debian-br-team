<?php
/**
 * Glossword installation 1.6
 * by Dmitry N. Shilnikov <dev at glossword dot info>
 * @version $Id: install.php,v 1.13 2004/11/16 13:26:27 yrtimd Exp $
 */
/* Change current directory to access for website files */
if (!@chdir('..'))
{
    exit(print "Can't change directory to `..'");
}
define('IN_GW', TRUE);
define('THIS_SCRIPT', 'install.php');
error_reporting(E_ALL);

/* Load configuration */
$sys['path_include'] = 'inc';
$sys['is_debug']  = 0;
$sys['is_prepend'] = 0;
include_once('/etc/glossword/db_config.php');
include_once($sys['path_include'] . '/config.inc.php');
if (file_exists('gw_install/install_functions.php'))
{
	include_once('gw_install/install_functions.php');
}
else
{
	printf('<br/><b>Error:</b> File %s required.', 'gw_install/install_functions.php');
}
include_once($sys['path_include'] . '/constants.inc.php');
include_once($sys['path_include'] . '/func.sql.inc.php');
include_once($sys['path_include'] . '/class.forms.php');
include_once($sys['path_include'] . '/class.gwtk.php');
include_once($sys['path_gwlib'].'/class.globals.php');
include_once($sys['path_gwlib'].'/class.func.php');
include_once($sys['path_gwlib'].'/class.db.mysql.php');


/* Constants */
define('CRLF', "\r\n");
define('LF', "\\n\\");
$tmp['arPhpVer'] = explode('.', PHP_VERSION);
define('PHP_VERSION_INT', intval(sprintf('%d%02d%02d', $tmp['arPhpVer'][0], $tmp['arPhpVer'][1], $tmp['arPhpVer'][2])));

@header("Expires: " . gmdate("D, d M Y H:i:s") . " GMT");
@header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
@header("Cache-Control: no-cache, must-revalidate");
@header("Pragma: no-cache");

/* */
// On Debian GNU/Linux, GW_DEB_ADMIN_* are found in db_config
//
/* Contact information */
$sys['user_name'] = GW_DEB_ADMIN_NAME;
$sys['user_email'] = GW_DEB_ADMIN_EMAIL;
$sys['user_login'] = GW_DEB_ADMIN_LOGIN;
//$sys['user_pass'] = GW_DEB_ADMIN_PASSWD;
$sys['pass_new'] = GW_DEB_ADMIN_PASSWD;
$sys['is_mod_rewrite']  = 0;
$sys['themename']  = 'gw_silver';
$sys['path_css'] =  '../'.$sys['path_tpl'].'/'.$sys['themename'];
$sys['version'] = '1.6.4';
include_once($sys['path_tpl'].'/'.$sys['themename'].'/theme.inc.php');
/* */
$gv['vars'] = $oGlobals->register(array('step', 'arpost', 'id_dict', GW_T_LANGUAGE));
$oGlobals->do_default($gv['vars']['step'], 1);
$oGlobals->do_default($gv['vars']['arpost']['dbhost'], 'localhost');
$oGlobals->do_default($gv['vars']['arpost']['dbport'], '');
$oGlobals->do_default($gv['vars']['arpost']['dbname'], GW_DB_DATABASE);
$oGlobals->do_default($gv['vars']['arpost']['dbprefix'], GW_TBL_PREFIX);
$oGlobals->do_default($gv['vars']['arpost']['dbuser'], GW_DB_USER);
$oGlobals->do_default($gv['vars']['arpost']['dbpass'], GW_DB_PASSWORD);
/* English by default */
$oGlobals->do_default($gv['vars'][GW_T_LANGUAGE], 'en');
/* --------------------------------------------------------
 * Translation kit
 * -------------------------------------------------------- */
$oL = new gwtk;
$oL->setHomeDir('gw_install/gw_locale');
$oL->setLocale($gv['vars'][GW_T_LANGUAGE].'-utf8');
$oL->getCustom('l_install', $gv['vars'][GW_T_LANGUAGE].'-utf8');
/* */
$sys['html_title'] = sprintf($oL->m('001'), $sys['version']);

function sqlGetQ($keyname)
{
	$args = func_get_args();
	$arSql = array(
		'ins-setting' => "INSERT INTO " . TBL_SETTINGS . " VALUES ('%s', '%s')",
		'TBL_SETTINGS' => "CREATE TABLE " . TBL_SETTINGS . " (
						settings_key varchar(127) NOT NULL default '',
						settings_val mediumtext NOT NULL,
						PRIMARY KEY (settings_key)
						) TYPE=MyISAM
						",
		'TBL_AUTH' => "CREATE TABLE " . TBL_AUTH . " (
						id int(10) unsigned NOT NULL auto_increment,
						user_id varchar(32) NOT NULL default '',
						username varchar(32) NOT NULL default '',
						password varchar(32) NOT NULL default '',
						perms varchar(8) NOT NULL default '00000000',
						PRIMARY KEY (id),
						UNIQUE KEY username (username)
						) TYPE=MyISAM
						",
		'TBL_DICT' => "CREATE TABLE " . TBL_DICT . " (
						id smallint(5) unsigned NOT NULL auto_increment,
						id_tp tinyint(3) unsigned NOT NULL default '1',
						is_active enum('0','1') NOT NULL default '0',
						is_show_az enum('0','1') NOT NULL default '1',
						is_leech enum('0','1') NOT NULL default '0',
						is_htmled enum('0','1') NOT NULL default '0',
						id_user smallint(5) unsigned NOT NULL default '1',
						auth_level tinyint(3) unsigned NOT NULL default '0',
						lang varchar(12) NOT NULL default 'en-utf8',
						date_modified timestamp(14) NOT NULL,
						date_created timestamp(14) NOT NULL,
						uid varchar(8) NOT NULL default '',
						int_terms mediumint(8) unsigned NOT NULL default '0',
						int_bytes int(12) unsigned NOT NULL default '0',
						tablename varchar(32) NOT NULL default '',
						themename varchar(64) NOT NULL default 'gw_silver',
						title varchar(255) binary NOT NULL default '',
						announce varchar(255) binary NOT NULL default '',
						description blob NOT NULL,
						keywords varchar(255) binary NOT NULL default '',
						dict_settings blob NOT NULL,
						PRIMARY KEY (id),
						UNIQUE KEY uid (uid)
						) TYPE=MyISAM
					",
		'TBL_SESS' => "CREATE TABLE " . TBL_SESS . " (
						sid varchar(32) NOT NULL default '',
						name varchar(32) NOT NULL default '',
						val text,
						changed varchar(14) NOT NULL default '',
						ip int(10) unsigned NOT NULL default '0',
						ua varchar(32) NOT NULL default '',
						PRIMARY KEY  (name,sid),
						KEY changed (changed)
						) TYPE=MyISAM
					",
		'TBL_STAT_DICT' => "CREATE TABLE " . TBL_STAT_DICT . " (
						id smallint(5) unsigned NOT NULL auto_increment,
						hits int(10) unsigned NOT NULL default '0',
						PRIMARY KEY  (id)
						) TYPE=MyISAM
					",
		'TBL_TPCS' => "CREATE TABLE " . TBL_TPCS . " (
						id smallint(5) unsigned NOT NULL auto_increment,
						s smallint(5) unsigned NOT NULL default '10',
						p smallint(5) unsigned NOT NULL default '0',
						tpname varchar(255) binary NOT NULL default '',
						PRIMARY KEY  (id)
						) TYPE=MyISAM
					",
		'TBL_USERS' => "CREATE TABLE " . TBL_USERS . " (
						auth_id smallint(5) unsigned NOT NULL auto_increment,
						date_reg timestamp(14) NOT NULL,
						date_login timestamp(14) NOT NULL,
						is_active enum('0','1') NOT NULL default '0',
						user_actkey varchar(16) NOT NULL default '',
						auth_level tinyint(3) unsigned NOT NULL default '4',
						user_name varchar(64) binary NOT NULL default '',
						user_email varchar(64) NOT NULL default '',
						is_showcontact enum('0','1') NOT NULL default '0',
						user_settings BLOB NOT NULL,
						PRIMARY KEY (auth_id),
						UNIQUE KEY user_email (user_email)
					  ) TYPE=MyISAM
					",
		'TBL_USERS_MAP' => "CREATE TABLE " . TBL_USERS_MAP . " (
						id int(10) unsigned NOT NULL auto_increment,
						user_id smallint(5) unsigned NOT NULL default '1',
						dict_id smallint(5) unsigned NOT NULL default '1',
						PRIMARY KEY (id)
						) TYPE=MyISAM
					",
		'TBL_WORDLIST' => "CREATE TABLE " . TBL_WORDLIST . " (
						word_text varchar(100) binary NOT NULL default '',
						word_id int(10) unsigned NOT NULL auto_increment,
						PRIMARY KEY (word_text),
						UNIQUE KEY word_id (word_id)
						) TYPE=MyISAM
					",
		'TBL_WORDMAP' => "CREATE TABLE " . TBL_WORDMAP . " (
						word_id int(10) unsigned NOT NULL default '0',
						term_id int(10) unsigned NOT NULL default '0',
						dict_id smallint(5) unsigned NOT NULL default '0',
						term_match tinyint(2) NOT NULL default '0',
						KEY word_id (word_id),
						KEY term_id (term_id)
						) TYPE=MyISAM
					",
		'TBL_SRCH_RESULTS' => "CREATE TABLE " . TBL_SRCH_RESULTS . " (
						id_srch varchar(32) NOT NULL default '',
						id_d mediumint(5) unsigned NOT NULL default '0',
						srch_date timestamp(14) NOT NULL,
						found int(12) unsigned NOT NULL default '1',
						hits mediumint(5) unsigned NOT NULL default '0',
						q varchar(254) NOT NULL default '',
						srch_settings mediumblob NOT NULL,
						PRIMARY KEY (id_srch)
						) TYPE=MyISAM
					",
	);
	for ($i = 0; $i <= 2; $i++)
	{
		$ar[] = isset($args[$i]) ? $args[$i] : '';
	}
	$sql = isset($arSql[$keyname]) ? $arSql[$keyname] : 'SELECT 1';
	$sql = preg_replace("/(\r|\n|\t)/", ' ', $sql);
	$sql = preg_replace("/( ){2,}/", ' ', trim($sql));
	return sprintf( $sql, $ar[1], $ar[2]);
}
/* */
class gw_install
{
	var $id_dict = 0;
	var $str_step = '';
	var $str_before = '';
	var $ar_status = '';
	var $str_after = '';
	var $cur_funcname = '';
	var $oDb;
	function gw_install()
	{
		global $oDb, $oFunc, $sys, $gv, $theme, $oL;
		$this->oDb = new gwtkDb;
		$this->oForm = new gwForms;
		$this->oFunc =& $oFunc;
		$this->oL =& $oL;
		$this->sys =& $sys;
		$this->gv =& $gv;
		$this->arTheme =& $theme;
	}
	/* */
	function is_locked()
	{
		return file_exists($this->sys['file_lock']);
	}
	function get_html_steps_progress($step)
	{
		$ar = array();
		for ($i = 1; $i <= 4; $i++)
		{
			$ar[$i] = ' '. $this->oL->m('054') . ' ' . $i.'  ';
			if ($step == $i) { $ar[$i] = '<b class="green">'.$ar[$i].'</b>'; }
		}
		return '<span class="gray">'.implode('&#x2192;', $ar).'</span>';
	}
	/* */
	function step()
	{
		if ($this->is_locked())
		{
			$this->ar_status[] = sprintf($this->oL->m('052'), $this->sys['file_lock']);
		}
		else
		{
			$this->{$this->cur_funcname}();
			$this->str_step .= $this->get_html_steps_progress($this->gv['vars']['step']);
		}
	}
	/* */
	function step_1()
	{
		$this->str_before .= $this->oL->m('004');
		$this->str_before .= '<ol>';
		$this->str_before .= '<li>'.$this->oL->m('005').'</li>';
		$this->str_before .= '<li>'.$this->oL->m('075').'</li>';
		$this->str_before .= '<li>'.$this->oL->m('006').'</li>';
		$this->str_before .= '<li>'.$this->oL->m('007').'</li>';
		$this->str_before .= '</ol>';
		$is_continue = 0;
		/* Get PHP version */
		$this->ar_status[1] = $this->oL->m('009');
		if (PHP_VERSION_INT > '40201')
		{
			$this->ar_status[1] .= get_html_item_progress(sprintf($this->oL->m('055'), PHP_VERSION), 1);
			$is_continue = 1;
		}
		else
		{
			$this->ar_status[1] .= get_html_item_progress(sprintf($this->oL->m('056'), PHP_VERSION), 3);
		}
		/* Get permisions */
/* On Debian GNU/Linux, configuration file (db_config.php)
 must be 0644 permissions
		$is_continue = 0;
		$this->ar_status[2] = $this->oL->m('059');
		if ( file_exists('/etc/glossword/db_config.php') && is_writeable('/etc/glossword/db_config.php') )
		{
			$this->ar_status[2] .= get_html_item_progress(sprintf($this->oL->m('060'), '/etc/glossword/db_config.php'), 1);
			$is_continue = 1;
		}
		else
		{
			$this->ar_status[2] .= get_html_item_progress(sprintf($this->oL->m('061'), '/etc/glossword/db_config.php'), 3);
		}
*/
/*

....and check permissions is not necessary

		$is_continue = 0;
		$this->ar_status[3] = $this->oL->m('074');
		if ( file_exists($this->sys['path_temporary']) && is_writeable($this->sys['path_temporary']) )
		{
			$this->ar_status[3] .= get_html_item_progress(sprintf($this->oL->m('060'), $this->sys['path_temporary'].'/'), 1);
			$is_continue = 1;
		}
		else
		{
			$this->ar_status[3] .= get_html_item_progress(sprintf($this->oL->m('061'), $this->sys['path_temporary'].'/'), 3);
		}
*/
		/* Get PHP extensions */
		if ($is_continue)
		{
			$this->ar_status[4] = $this->oL->m('062');
			$arPhpExt = array();
			if (function_exists('get_loaded_extensions'))
			{
				$arPhpExt = get_loaded_extensions();
			}
			if (empty($arPhpExt))
			{
				$this->ar_status[4] .= get_html_item_progress($this->oL->m('069'), 3);
				$is_continue = 0;
			}
		}
		/* For each extension */
		if ($is_continue)
		{
			if (in_array('xml', $arPhpExt))
			{
				$this->ar_status[4] .= get_html_item_progress($this->oL->m('063'), 1);
			}
			else
			{
				$this->ar_status[4] .= get_html_item_progress($this->oL->m('068'), 3);
				$is_continue = 0;
			}
			if (in_array('xslt', $arPhpExt))
			{
				$this->ar_status[4] .= get_html_item_progress($this->oL->m('064'), 1);
			}
			else
			{
				$this->ar_status[4] .= get_html_item_progress($this->oL->m('069'), 2);
			}
			if (in_array('iconv', $arPhpExt))
			{
				$this->ar_status[4] .= get_html_item_progress($this->oL->m('065'), 1);
			}
			else
			{
				$this->ar_status[4] .= get_html_item_progress($this->oL->m('070'), 2);
			}
			if (in_array('mbstring', $arPhpExt))
			{
				$this->ar_status[4] .= get_html_item_progress($this->oL->m('066'), 1);
			}
			else
			{
				$this->ar_status[4] .= get_html_item_progress($this->oL->m('071'), 2);
			}
			if (in_array('afterburner', $arPhpExt))
			{
				$this->ar_status[4] .= get_html_item_progress($this->oL->m('067'), 1);
			}
		}
		$this->ar_status[5] = gw_next_step($is_continue, 'step=2');
	}
	/* */
	function step_2()
	{
		$this->str_before .= sprintf($this->oL->m('076'), 'Glossword');
		$this->str_before .= '<p>'.$this->oL->m('077').'</p>';
		$str_license = $this->oFunc->file_get_contents('support/license.html');
		$str_license = preg_replace("/^(.*)<body>/s", '', $str_license);
		$str_license = preg_replace("/<\/body>(.*)$/s", '', $str_license);
		$this->str_before .= '<div class="iframe">'. $str_license .'</div>';
		$this->str_before .= '<br/>';
		$this->str_before .= $this->get_form(2);
	}
	function step_3()
	{
		$this->str_before .= $this->oL->m('082');
		$this->str_before .= $this->get_form(3);
	}
	function step_4()
	{
		$this->str_before .= '<br/><br/>';
		/* Check database connection */
		$is_continue = 0;
		if (@mysql_connect($this->gv['vars']['arpost']['dbhost'], $this->gv['vars']['arpost']['dbuser'], $this->gv['vars']['arpost']['dbpass']))
		{
			if (@mysql_select_db($this->gv['vars']['arpost']['dbname']))
			{
				$is_continue = 1;
				$this->oDb->host = $this->gv['vars']['arpost']['dbhost'];
				$this->oDb->user = $this->gv['vars']['arpost']['dbuser'];
				$this->oDb->password = $this->gv['vars']['arpost']['dbpass'];
				$this->oDb->database = $this->gv['vars']['arpost']['dbname'];
			}
			else
			{
				$this->str_before .= get_html_item_progress(sprintf($this->oL->m('084'), $this->gv['vars']['arpost']['dbname']), 3);
			}
		}
		else
		{
			$this->str_before .= get_html_item_progress(sprintf($this->oL->m('083'), $this->gv['vars']['arpost']['dbhost'], $this->gv['vars']['arpost']['dbuser']), 3);
		}
		/* Check contact information */
		$this->sys['user_email'] = $this->gv['vars']['arpost']['user_email'];
		$this->sys['user_name'] = $this->gv['vars']['arpost']['user_name'];
		$this->sys['user_login'] = $this->gv['vars']['arpost']['user_login'];
		$this->sys['pass_new'] = $this->gv['vars']['arpost']['pass_new'];
		if ($this->gv['vars']['arpost']['user_name'] == '')
		{
			$this->str_before .= get_html_item_progress($this->oL->m('095'), 3);
			$is_continue = 0;
		}
		if ($this->gv['vars']['arpost']['user_email'] == '')
		{
			$this->str_before .= get_html_item_progress($this->oL->m('096'), 3);
			$is_continue = 0;
		}
		if ($this->gv['vars']['arpost']['user_login'] == '')
		{
			$this->str_before .= get_html_item_progress($this->oL->m('097'), 3);
			$is_continue = 0;
		}
		/*
		if ($this->gv['vars']['arpost']['pass_new'] != $this->gv['vars']['arpost']['pass_confirm'])
		{
			$this->sys['pass_new'] = $this->sys['pass_confirm'] = '';
			$this->str_before .= get_html_item_progress($this->oL->m('098'), 3);
		}*/
		/* Server settings */
		$this->sys['server_proto'] = $this->gv['vars']['arpost']['server_proto'];
		$this->sys['server_host'] = $this->gv['vars']['arpost']['server_host'];
		$this->sys['server_dir'] = $this->gv['vars']['arpost']['server_dir'];
		/* */
		if ($is_continue)
		{
			$this->ar_status[] = sprintf($this->oL->m('085'), $this->gv['vars']['arpost']['dbname']);

			/* Prepare database queries */
			$arQuery = array();
			$this->ar_status[1] = $this->oL->m('086');
			global $arTables;
			for (reset($arTables); list($k, $v) = each($arTables);)
			{
				/* Does table exist? */
				if ($arTableInfo = $this->oDb->table_info(constant($v)) )
				{
					$arQuery[] = 'DELETE FROM '.constant($v);
				}
				else
				{
					$arQuery[] = sqlGetQ($v);
				}
			}
			$arQuery[] = sqlGetQ('ins-setting', 'y_email', gw_text_sql($this->gv['vars']['arpost']['user_email']));
			$arQuery[] = sqlGetQ('ins-setting', 'y_name', gw_text_sql($this->gv['vars']['arpost']['user_name']));
			$arQuery[] = sqlGetQ('ins-setting', 'version', $this->sys['version']);
			$arQuery[] = sqlGetQ('ins-setting', 'site_name', gw_text_sql($this->gv['vars']['arpost']['server_host']));
			$arQuery[] = sqlGetQ('ins-setting', 'keywords', 'glossary, dictionary, glossword');
			$arQuery[] = sqlGetQ('ins-setting', 'locale_name', 'en-utf8');
			$arQuery[] = sqlGetQ('ins-setting', 'page_limit', '20');
			$arQuery[] = sqlGetQ('ins-setting', 'time_refresh', '2');
			$arQuery[] = sqlGetQ('ins-setting', 'time_new', '7');
			$arQuery[] = sqlGetQ('ins-setting', 'time_upd', '14');
			$arQuery[] = sqlGetQ('ins-setting', 'defn_cut', '200');
			$arQuery[] = sqlGetQ('ins-setting', 'srch_len', '1');
			$arQuery[] = sqlGetQ('ins-setting', 'hideurl', '1');
			$arQuery[] = sqlGetQ('ins-setting', 'dp_layout', '0');
			$arQuery[] = sqlGetQ('ins-setting', 'searchformcode', '');
			$arQuery[] = sqlGetQ('ins-setting', 'site_desc', 'Glossary compiler');
			$arQuery[] = sqlGetQ('ins-setting', 'themename', gw_text_sql('gw_silver'));
			/* Topics */
			$arQuery[] = "INSERT INTO " . TBL_TPCS . " VALUES (1, 10, 0, 'Dictionary Listing')";
			$q1 = $q2 = array();
			/* Auth table */
			$q1['id'] = 1;
			$q1['user_id'] = md5($this->sys['user_login']);
			$q1['username'] = gw_text_sql($this->gv['vars']['arpost']['user_login']);
			$q1['password'] = md5($this->gv['vars']['arpost']['pass_new']);
			$q1['perms'] = '11111111';
			$arQuery[] = gw_sql_replace($q1, TBL_AUTH);
			/* Users table */
			$q2['auth_id'] = $q1['id'];
			$q2['date_reg'] = date("YmdHis");
			$q2['date_login'] = '00000000000000';
			$q2['is_showcontact'] = 0;
			$q2['is_active'] = '1';
			$q2['auth_level'] = '16';
			$q2['user_name'] = gw_text_sql($this->gv['vars']['arpost']['user_name']);
			$q2['user_email'] = gw_text_sql($this->gv['vars']['arpost']['user_email']);
			$q2['user_settings'] = serialize(array());
			$arQuery[] = gw_sql_replace($q2, TBL_USERS);
			/* !!! POSTING QUERIES !!! */
			$is_continue = 1; /* Continue by default */
			for (reset($arQuery); list($k, $v) = each($arQuery);)
			{
				if ($this->sys['is_debug'])
				{
					$this->ar_status[] = $v . ';';
				}
				else
				{
					if (!$this->oDb->sqlExec($v)){
						$is_continue = 0;
						$this->ar_status[] = $this->oL->m('103').' '.$v;
					}
				}
			}
			if ($is_continue)
			{
				$this->ar_status[1] .= '... Done';
			}
			else
			{
				$this->ar_status[1] .= ' <span class="red">'.$this->oL->m('102').'</span>';
				return false;
			}
			/* Prepare configuration file */
/* On Debian GNU/Linux, configuration file (db_config.php)
 must be prepared meantime dpkg installation or configuration

			$is_continue = 0;
			$str_file = '<'.'?php';
			$str_file .= CRLF . '/* Database settings for Glossword /';
			$str_file .= CRLF . sprintf("define('GW_DB_HOST', '%s');", $this->gv['vars']['arpost']['dbhost']);
			$str_file .= CRLF . sprintf("define('GW_DB_DATABASE', '`%s`');", $this->gv['vars']['arpost']['dbname']);
			$str_file .= CRLF . sprintf("define('GW_DB_USER', '%s');", $this->gv['vars']['arpost']['dbuser']);
			$str_file .= CRLF . sprintf("define('GW_DB_PASSWORD', '%s');", $this->gv['vars']['arpost']['dbpass']);
			$str_file .= CRLF . sprintf("define('GW_TBL_PREFIX', '%s');", $this->gv['vars']['arpost']['dbprefix']);
			$str_file .= CRLF . '/* Path names for Glossword /';
			$str_file .= CRLF . sprintf("\$sys['server_proto'] = '%s';", $this->gv['vars']['arpost']['server_proto']);
			$str_file .= CRLF . sprintf("\$sys['server_host'] = '%s';", $this->gv['vars']['arpost']['server_host']);
			$str_file .= CRLF . sprintf("\$sys['server_dir'] = '%s';", $this->gv['vars']['arpost']['server_dir']);
			$str_file .= CRLF . sprintf("\$sys['server_url'] = '%s';", $this->gv['vars']['arpost']['server_proto'].$this->gv['vars']['arpost']['server_host'].$this->gv['vars']['arpost']['server_dir']);
			$str_file .= CRLF . '?'.'>';
			$this->ar_status[] = $this->oL->m('087');
			if (!$this->sys['is_debug'])
			{
				$is_continue = $this->oFunc->file_put_contents('/etc/glossword/db_config.php', $str_file, 'w');
			}
			if ($is_continue)
			{
				$this->ar_status[(sizeof($this->ar_status)-1)] .= '... Done';
				$is_continue = 0;
			}
			else
			{
				$this->ar_status[(sizeof($this->ar_status)-1)] .= get_html_item_progress('<b>/etc/glossword/db_config.php</b><pre>'.htmlspecialchars($str_file).'</pre>', 3);
			}
*/
			/* Lock installer */
			$this->ar_status[] = $this->oL->m('088');
			if (!$this->sys['is_debug'])
			{
				$is_continue = $this->oFunc->file_put_contents($this->sys['file_lock'], $this->oL->m('100', $this->sys['file_lock']), 'w');
			}
			if ($is_continue)
			{
				$this->ar_status[3] .= '... Done';
				$is_continue = 0;
			}
			else
			{
				$this->ar_status[(sizeof($this->ar_status)-1)] .= get_html_item_progress('<b>'.$this->sys['file_lock'].'</b><pre>locked</pre>', 3);
			}
			/* Do something more */
#			$this->ar_status[] = $this->oL->m('007');
			/* COMPLETE */
			$this->ar_status[] = '<b class="green">'.$this->oL->m('090').'</b>';
			/* Link to administrative interface */
			$login_url =  $this->gv['vars']['arpost']['server_proto'].$this->gv['vars']['arpost']['server_host'].$this->gv['vars']['arpost']['server_dir'] . '/gw_admin/login.php';
			$this->ar_status[] = '<b style="color:#C00">Login:</b> <b>' . $this->gv['vars']['arpost']['user_login'] . '</b>';
			$this->ar_status[] = '<b style="color:#C00">Password:</b> <b>' . $this->gv['vars']['arpost']['pass_new'] . '</b>';
			$this->ar_status[] = 'Login URL: ' . '<a href="' . $login_url . '" target="_blank">' . $login_url.'</a>';
		}
		else
		{
			/* Back to previous step */
			$this->str_before .= '<br/>';
			$this->gv['vars']['step']--;
			$this->str_before .= $this->get_form($this->gv['vars']['step']);
		}
	}
	/* */
	function get_form($step)
	{
		$tmp = '';
		$this->oForm->set('formbgcolor',       $this->arTheme['color_2']);
		$this->oForm->set('formbordercolor',   $this->arTheme['color_4']);
		$this->oForm->set('formbordercolorL',  $this->arTheme['color_1']);
		$this->oForm->set('formwidth', '75%');
		$this->oForm->set('action', THIS_SCRIPT);
		$this->oForm->set('isButtonCancel', 0);
		$this->oForm->set('align_buttons', 'right');
		switch($step)
		{
			case 2:
				$this->oForm->set('align_buttons', 'center');
				$this->oForm->set('isButtonCancel', 1);
				$this->oForm->set('strNotes', $this->oL->m('078'));
				$this->oForm->set('submitok', $this->oL->m('079') );
				$this->oForm->set('submitcancel', $this->oL->m('080') );
			break;
			case 3:
				/* Default password */
				if (empty($this->sys['pass_new']))
				{
					$this->sys['pass_new'] = $this->oFunc->text_make_uid(8, 0);
				}
				$tmp .= $this->get_form_title($this->oL->m('094'));
				$tmp .= '<table class="gw2TableFieldset" width="100%">';
				$tmp .= '<tr><td style="width:30%"></td><td></td></tr>';
				$tmp .= $this->get_form_tr($this->oL->m('095'), $this->oForm->field('input', 'arpost[user_name]', $this->sys['user_name']));
				$tmp .= $this->get_form_tr($this->oL->m('096'), $this->oForm->field('input', 'arpost[user_email]', $this->sys['user_email']));
				$tmp .= $this->get_form_tr($this->oL->m('097'), $this->oForm->field('input', 'arpost[user_login]', $this->sys['user_login']));
				$tmp .= $this->get_form_tr($this->oL->m('098'), $this->oForm->field('input', 'arpost[pass_new]', $this->sys['pass_new']));
#				$tmp .= $this->get_form_tr($this->oL->m('099'), $this->oForm->field('input', 'arpost[pass_confirm]', $this->sys['pass_new']));
				$tmp .= '</table>';
				$tmp .= $this->get_form_title($this->oL->m('050'));
				$tmp .= '<table class="gw2TableFieldset" width="100%">';
				$tmp .= '<tr><td style="width:30%"></td><td></td></tr>';

				$tmp .= $this->get_form_tr($this->oL->m('081'), $this->oForm->field('select', 'arpost[server_proto]', '', 'http://', array('http://' => 'http://', 'https://'=> 'https://')));
				$tmp .= $this->get_form_tr($this->oL->m('046'), $this->oForm->field('input', 'arpost[server_host]', $this->sys['server_host']));
				$tmp .= $this->get_form_tr($this->oL->m('048'), $this->oL->m('101').'<br/>'.$this->oForm->field('input', 'arpost[server_dir]', $this->sys['server_dir']));
				$tmp .= '</table>';
				$tmp .= $this->get_form_title($this->oL->m('049'));
				$tmp .= '<table class="gw2TableFieldset" width="100%">';
				$tmp .= '<tr><td style="width:30%"></td><td></td></tr>';
				$tmp .= $this->get_form_tr($this->oL->m('040'), $this->oForm->field('input', 'arpost[dbhost]', $this->gv['vars']['arpost']['dbhost']));
#				$tmp .= $this->get_form_tr($this->oL->m('041'), $this->oForm->field('input', 'arpost[server_proto]', $this->gv['vars']['arpost']['dbport']));

				$arDatabases = $this->oDb->get_databases();
				if (!empty($arDatabases))
				{
					$tmp .= $this->get_form_tr($this->oL->m('042'), $this->oForm->field('select', 'arpost[dbname]', $this->gv['vars']['arpost']['dbname'], 'glossword1', $arDatabases));
				}
				else
				{
					$tmp .= $this->get_form_tr($this->oL->m('042'), $this->oForm->field('input', 'arpost[dbname]', $this->gv['vars']['arpost']['dbname']));
				}
				$tmp .= $this->get_form_tr($this->oL->m('043'), $this->oForm->field('input', 'arpost[dbprefix]', $this->gv['vars']['arpost']['dbprefix']));
				$tmp .= $this->get_form_tr($this->oL->m('044'), $this->oForm->field('input', 'arpost[dbuser]', $this->gv['vars']['arpost']['dbuser']));
				$tmp .= $this->get_form_tr($this->oL->m('045'), $this->oForm->field('pass', 'arpost[dbpass]', $this->gv['vars']['arpost']['dbpass']));
				$tmp .= '</table>';
			break;
		}
		$tmp .= $this->oForm->field('hidden', 'step', $step+1);
		return '<div class="center">'.$this->oForm->Output($tmp).'</div>';
	}
	/* */
	function get_form_tr($td1, $td2)
	{
		return sprintf('<tr><td class="td1">%s</td><td class="td2">%s</td></tr>', $td1, $td2);
	}
	/* */
	function get_form_title($t)
	{
		return '<div class="r" style="padding:4px;background:'.$this->arTheme['color_4'].'">'.$t.'</div>';
	}
	/* */
	function output()
	{
		$str_status = '';
		if (!empty($this->ar_status))
		{
			$str_status .= '<ul class="gwstatus"><li>' . implode('</li><li>', $this->ar_status) . '</li></ul>';
		}
		$str = '<div class="contents u">';
		$str .= $this->str_step;
		$str .= $this->str_before;
		$str .= $str_status;
		$str .= $this->str_after;
		$str .= '</div>';
		return $str;
	}
}
/* */

/* */


$oInstall = new gw_install;
$gv['vars']['cur_funcname'] = 'step_'.$gv['vars']['step'];
/* Open step number  */
if (in_array(strtolower($gv['vars']['cur_funcname']), get_class_methods($oInstall)))
{
	$oInstall->cur_funcname = $gv['vars']['cur_funcname'];
	$oInstall->step();
}

gw_html_open();
print $oInstall->output();
gw_html_close();

?>
