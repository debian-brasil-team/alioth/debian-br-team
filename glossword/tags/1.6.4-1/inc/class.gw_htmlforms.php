<?php
/**
 * $Id: class.gw_htmlforms.php,v 1.10 2004/11/14 09:27:12 yrtimd Exp $
 */
/**
 *  Glossword HTML-form class extension
 *  ==============================================
 *  Copyright (c) 2003 Dmitry Shilnikov <dev at glossword.info>
 *  http://glossword.info/dev/
 */
// --------------------------------------------------------
// 
class gw_htmlforms extends gwForms
{
	var $Gsys     = array();
	var $Gtmp     = array('strform' => '', 'str' => '');
	var $L        = array();
	var $objDom   = '';
	var $objDict  = '';
	var $oFieldExt  = array();	
	var $arFields = array();
	var $arEl     = array();
	var $abbr_element = 0;     // [ 0 - abbreviation | 1 - abbr.   ]
	var $trns_element = 0;     // [ 0 - translation  | 1 - transl. ]
	var $abbr_element_web = 1; // [ 0 - abbreviation | 1 - abbr.   ]
	var $trns_element_web = 1; // [ 0 - translation  | 1 - transl. ]
				
	// available functions
	var $arFuncList = array(
					   'make_term' => 1, 'make_trsp' => 1, 'make_defn' => 1,
					   'make_abbr' => 1, 'make_trns' => 1,
					   'make_syn'  => 1, 'make_see'  => 1,
					   'make_usg'  => 1, 'make_src'  => 1, 'make_address' => 1, 'make_phone'  => 1
					  );
	// autostart
	function gw_htmlforms()
	{
		// load trns
		// load abbr
	}
	function tag2field($fieldname, $ar = array())
	{
		$fieldname = strtolower($fieldname);
		$funcname = 'make_' . $fieldname;
		if (isset($this->arFuncList[$funcname]) && $this->arFuncList[$funcname])
		{
			return $this->$funcname($fieldname, $ar);
		}
	}
	//
	function html_editor_make_toolbar($id = 0, $html_add = '', $html_remove = '')
	{
		global $oFunc;
		$this->unsetTag('input');
		$this->setTag('input', 'type', 'button');
		$this->setTag('input', 'class', 'btn');
		
#		$this->setTag('input', 'onmouseover', 'this.className=\'btnO\'');
#		$this->setTag('input', 'onmouseout',  'this.className=\'btn\'');
#		$this->setTag('input', 'onmousedown', 'this.className=\'btnD\'');
#		$this->setTag('input', 'onmouseup',   'this.className=\'btnO\'');

		$tmp['str'] = CRLF . '<div style="overflow:auto; width:500px; float:'.$this->Gsys['css_align_right'].';"><table class="toolbar" cellspacing="0" cellpadding="1" border="0" width="1%"><tr>';
		// [0]type, [1]jsName, [2]caption, [3]text, [4]hint
		$arButtons = array(
			'1||format_bold|| B ||b||ed_bold',
			'1||format_italic|| I ||i||ed_em',
			'2||format_underline|| U ||&lt;span class=&quot;underline&quot;&gt;|&lt;/span&gt;||',
			'1||paragraph|| P ||p||ed_p',
			'1||tt|| TT ||tt||&lt;tt&gt;|&lt;/tt&gt;',
			'spacer||||||||',
			'2||quot1||&quot;|&quot;||&amp;quot;|&amp;quot;||ed_quot',
			'2||quot2||&#171;|&#187;||&amp;#171;|&amp;#187;||ed_elochki',
			'2||xml1||&lt;|&gt;||&lt;|&gt;||tag open',
			'2||xml2||&lt;/|&gt;||&lt;/|&gt;||tag close',
			'spacer||||||||',
			'2||stress||&lt;Ś&gt;||&lt;stress&gt;|&lt;/stress&gt;||&lt;stress&gt;|&lt;/stress&gt;',
			'2||xref||&lt;xref&gt;||&lt;xref link=&quot;|&quot;&gt;&lt;/xref&gt;||&lt;xref link=&quot;|&quot;&gt;&lt;/xref&gt;',            
			'spacer||||||||',
			'3||nbsp||&amp;nbsp;||&amp;#160;||ed_nbsp',
			'3||tm||&#8482;||&amp;#8482;||Trademark',
			'3||euro||&#8364;||&amp;#8364;||Euro Sign',
			'3||copy||&#169;||&amp;#169;||Copyright mark',
			'3||reg||&#174;||&amp;#174;||Registered mark',
			'3||deg||&#176;||&amp;#176;||Degree',
			'3||dots||&#8230;||&amp;#8230;||Dots',
			'spacer||||||||'
		);
		//
		$jsToolbarOff = $jsToolbarSymbOff = $jsToolbarOn = '';
		//
		for (reset($arButtons); list($k, $v) = each($arButtons);)
		{
			$arParam = explode("||", $v);
			$this->setTag('input', 'title', $this->L->m($arParam[4]));

			if ($oFunc->is_num($arParam[0]))
			{
				$vJs = "['" .  $arParam[1] . "_' + id + '']";
				if ($arParam[0] == 1)
				{
					$this->setTag('input', 'onclick', 'sTextTag('.$id.', \''. $arParam[3].'\')');
				}
				else if ($arParam[0] == 2)
				{
					$ar_pairs_src = explode("|", $arParam[3]);
					$this->setTag('input', 'onclick', 'sTextDoubleSymbol('.$id.', \''.$ar_pairs_src[0].'\', \''.$ar_pairs_src[1].'\')');
				}
				else if ($arParam[0] == 3)
				{
					$this->setTag('input', 'onclick', 'sTextSymbol('.$id.', \''.$arParam[3].'\')');
					$jsToolbarSymbOff .= 'document.forms[\'vbform\'].elements'.$vJs.'.disabled = ';
				}
				$tmp['str'] .= '<td>' . $this->field('input', $arParam[1].'_'.$id, $arParam[2]) . '</td>';
				$jsToolbarOff .= 'document.forms[\'vbform\'].elements'.$vJs.'.disabled = ';
				$jsToolbarOn .= 'document.forms[\'vbform\'].elements'.$vJs.'.disabled = ';
			}
			else
			{
				$tmp['str'] .= '<td><img src="gw_admin/images/spacer.gif" width="4" height="22" alt="" /></td>';
			}
		}
		// add/remove definition buttons
		$tmp['str'] .= ($html_add != '') ? ('<td>'.$html_add.'</td>') : '';
		$tmp['str'] .= ($html_remove != '') ? ('<td>'.$html_remove.'</td>') : '';        
		// end of toolbar table
		$tmp['str'] .= '</tr></table></div>';

		$jsToolbarSymbOff .= 'true;';
		$jsToolbarOff .= 'true;';
		$jsToolbarOn .= 'false;';

		// enable-disable toolbar
		$tmp['str'] .= '<script type="text/javascript">/*<![CDATA[*/
			function toolbar_on(id) { '.$jsToolbarOn.' }
			function toolbar_off(id) { '.$jsToolbarOff.' }
			function toolbar_symb_off(id) { '.$jsToolbarSymbOff.' }
			/*]]>*/</script>';
		$tmp['str'] .= CRLF.'<script type="text/javascript">//<![CDATA[ '.CRLF.'toolbar_off(\''.$id.'\'); '.CRLF.'//]]></script>';
		return $tmp['str'];
	}        
	//
	function make_term($fieldname, $ar = array())
	{
		global $oFunc;
		if (!isset($this->arEl[$fieldname]))
		{
			$this->arEl[$fieldname][0]['value'] = '';
		}                   
		//
		$this->unsetTag('input'); // reset settings for <input>
		//
		$tmp['strform'] = '';
		$tmp['strform'] .= '<tr style="vertical-align:top">';
		$tmp['strform'] .= sprintf('<td style="width:15%%;text-align:%s" class="%s">%s:%s</td>',
							   $this->Gsys['css_align_right'], $this->Gtmp['cssTrClass'], $this->L->m($fieldname), ''
						   );
		$tmp['strform'] .= '<td style="width:85%">';
		$tmp['strform'] .= '<table cellspacing="0" cellpadding="1" border="0" width="100%"><tr>';
		//
			$this->setTag('input', 'maxlength', '128');
			$this->setTag('input', 'size', '40');
		$tmp['strform'] .= '<td style="width:86%">' . $this->field('input', 'arPre['.$fieldname.'][0][value]', textcodetoform( $oFunc->mb_substr( $this->objDom->get_content($this->arEl[$fieldname][0]), 0, 128)) ) . '</td>';
		//
			$this->setTag('input', 'maxlength', '3');
			$this->setTag('input', 'size', '2');
		$tmp['strform'] .= '<td style="width:6%">' . $this->field('input', 'arPre['.$fieldname.'][0][attributes][t1]', textcodetoform( $this->objDom->get_attribute('t1', '', $this->arEl[$fieldname]) ) ) . '</td>';
		//
			$this->setTag('input', 'maxlength', '6');
			$this->setTag('input', 'size', '4');
		$tmp['strform'] .= '<td style="width:8%">' . $this->field('input', 'arPre['.$fieldname.'][0][attributes][t2]', textcodetoform( $this->objDom->get_attribute('t2', '', $this->arEl[$fieldname]) ) ) . '</td>';
		$tmp['strform'] .= '</tr></table>';
		$tmp['strform'] .= '</td>';
		$tmp['strform'] .= '</tr>';
		return $tmp['strform'];
	}
	//
	function make_trsp($fieldname, $ar = array())
	{
		global $oFunc;
		if (empty($ar))
		{
			$ar['elK'] = 0;
		}
		if (!isset($this->arEl[$fieldname]))
		{
			$this->arEl[$fieldname] = array();
		}
		$tmp['str'] = $tmp['strform'] = '';
		
		for (reset($this->arEl[$fieldname]); list($elK, $elV) = each($this->arEl[$fieldname]);)
		{
			if (isset($elV['value']))
			{
				$tmp['str'] .= $elV['value'];
				$tmp['str'] .= CRLF;
			}            
		}
		$tmp['intFormHeight'] = $oFunc->getFormHeight( $tmp['str'] );
		$tmp['strform'] .= CRLF . '<tr style="vertical-align:top">';
		$tmp['strform'] .= sprintf('<td style="text-align:%s" class="%s">%s:%s</td>',
							$this->Gsys['css_align_right'], $this->Gtmp['cssTrClass'], $this->L->m($fieldname), ''
						   );
		$tmp['strform'] .= '<td colspan="2">';
		$tmp['strform'] .= $this->make_trsp_ext($fieldname, $ar);
		$tmp['strform'] .= $this->field('textarea',
								'arPre['.$fieldname.']['.$ar['elK'].'][value]',
								textcodetoform($tmp['str'] ),
								(intval($tmp['intFormHeight'] / 2) + 1)
							);
		$tmp['strform'] .= '</td>';
		$tmp['strform'] .= '</tr>';
		return $tmp['strform'];
	}
	//
	function make_trsp_ext($fieldname, $ar)
	{
		$str = '';
		if (isset($this->Gsys['is_field_extensions']) && $this->Gsys['is_field_extensions'])
		{
			$this->setTag('textarea', 'onclick', 'storeCaret(this)');
			$this->setTag('textarea', 'onselect', 'storeCaret(this)');
			$this->setTag('textarea', 'onkeyup', 'storeCaret(this)');

			$oFieldExt = new gw_fields_extension;

			$str .= $oFieldExt->get_js($fieldname, $ar['elK']);
			$str .= $oFieldExt->get_html($fieldname, $ar['elK']);
			
			
		}
		return $str;
	}
	//
	function make_defn($fieldname, $ar = array())
	{
		global $oFunc;
		$tmp['strform'] = $tmp['strBtnRemove'] = $tmp['strHtmlTB'] = $tmp['strBtnAdd'] = '';
		//
		$this->unsetTag('textarea'); // reset settings for <textarea>
		//
		// default value
		//
		if (!isset($this->arEl[$fieldname]) || !is_array($this->arEl[$fieldname][0]))
		{
			$this->arEl[$fieldname] = array(0 => array('value' => ''));
		}
		if (!isset($this->arEl[$fieldname]))
		{
			$this->arEl[$fieldname][0]['value'] = '';
		}
		while (list($elK, $elV) = each($this->arEl[$fieldname]))
		{
			// break table for each definition
			$tmp['strform'] .= '</table>';
			//
			$tmp['strBtnAdd'] = '<input tabindex="7" type="submit" style="width:24px;height:24px" class="submitcancel" name="'.
								'arControl['.$fieldname.']['.GW_A_ADD.']['.$elK.'][0]" title="'.
								$this->L->m('3_add_defn').'" value="+"/>';
			if ( $elK > 0 )
			{
				$tmp['strBtnRemove'] = '<input tabindex="8" type="submit" style="width:24px;height:24px" class="submitdel" name="'.
									   'arControl['.$fieldname.']['.REMOVE.']['.$elK.'][0]" title="'.
									   $this->L->m('3_remove_defn').'" value="&#215;"/>';
			}
			$tmp['strHtmlTB'] = $tmp['strBtnRemove'] . $tmp['strBtnAdd'];
			//
			// enable HTML-editor events
			if ($this->arDictParam['is_htmled'] == 1)
			{
				$this->L->getCustom('html_editor', $this->arDictParam['lang'], 'join');
				
				$tmp['strHtmlTB'] = $this->html_editor_make_toolbar($elK, $tmp['strBtnRemove'], $tmp['strBtnAdd']);
				include('./inc/edcode.js.' . $this->Gsys['phpEx']);
				$this->setTag('textarea', 'onfocus', 'toolbar_on(\''.$elK.'\')');
#				$this->setTag('textarea', 'onblur', 'toolbar_off(\''.$elK.'\')');
				$this->setTag('textarea', 'onclick', 'storeCaret(this)');
				$this->setTag('textarea', 'onselect', 'storeCaret(this)');
				$this->setTag('textarea', 'onkeyup', 'storeCaret(this)');
			}            
			$tmp['strform'] .= CRLF . getFormTitleNav($this->L->m($fieldname), $tmp['strHtmlTB']);
			//
			//
			$tmp['strform'] .= CRLF . '<table cellspacing="1" cellpadding="2" border="0" width="100%">';
			// get definition content
			$tmp['str'] = $elV['value'];
			//
			// Parse subtags, abbr + trns
			//
			$arTmp['elK'] = $elK;
			if ($this->arDictParam['is_abbr'])
			{
				$tmp['strform'] .= $this->tag2field('abbr', $arTmp);
			}
			if ($this->arDictParam['is_trns'])
			{
				$tmp['strform'] .= $this->tag2field('trns', $arTmp);
			}
			//
			$tmp['intFormHeight'] = $oFunc->getFormHeight( $tmp['str'] );
			if ($tmp['intFormHeight'] < 3) { $tmp['intFormHeight'] = 3; }
			$tmp['strform'] .= CRLF .sprintf('<tr style="vertical-align:top"><td style="text-align:%s" class="%s">%s:%s</td>',
								$this->Gsys['css_align_right'], $this->Gtmp['cssTrClass'], $this->L->m($fieldname), ''
								);
			$tmp['strform'] .= '<td style="width:29%" colspan="2">';
			$tmp['strform'] .= $this->field(
									'textarea',
									'arPre['.$fieldname.']['.$elK.'][value]',
									textcodetoform( $tmp['str'] ),
									$tmp['intFormHeight']
								);
			$tmp['strform'] .= '</td><td></td>';
			$tmp['strform'] .= '</tr>';

			//
			// Parse subtags
			//
			for (reset($this->arFields); list($fK, $fV) = each($this->arFields);)
			{
				// not root elements only
				if ((!isset($fV[4]) || !$fV[4]) && $this->arDictParam['is_'.$fV[0]]
					&& ($fV[0] != 'abbr' && $fV[0] != 'trns'))
				{
					$tmp['strform'] .= $this->tag2field($fV[0], $arTmp);
				}
			}

			// set width for colums to help browser render the page,
			// this code also adds a nice visual bottom margin
			$tmp['strform'] .= '<tr><td style="width:14%;height:1px"></td>'.
							   '<td style="width:15%;height:1px"></td>'.
							   '<td style="width:65%;height:1px"></td>'.
							   '<td style="width:1%;height:1px"></td></tr>';
		}
		return $tmp['strform'];
	}
	//
	function load_abbr_trns()
	{
		$this->load_abbr(0);
		$this->load_abbr(1);
		$this->load_trns(0);
		$this->load_trns(1);
	}
	//
	function load_trns($id = 0)
	{
		$tmp['strform'] = '';
		//
		// do auto-fill
		//
		$tmp['arTmp'] = array();        
		$tmp['arTmp']['--'] = $this->L->m('000');
		//
		// Language names
		//
		$arSrc = $this->L->getCustom('d_language', $this->arDictParam['lang'], 'return');
		// Custom overwrites existent
		$arSrc = array_merge_clobber($arSrc, $this->L->getCustom('d_language_custom', $this->arDictParam['lang'], 'return'));
		// convert 2-dimensional array into single array
		while (list($abrK, $abrV) = each($arSrc))
		{
			if (isset($abrV[$id]))
			{
				$tmp['arTmp'][$abrK] = $abrV[$id];
			}
		}
		asort($tmp['arTmp']);
		$this->Set('arTrns'.$id, $tmp['arTmp']);
	}
	//
	function load_abbr($id = 0)
	{
		$tmp['strform'] = '';
		//
		// do auto-fill
		//
		$tmp['arAbbr']['--'] = $this->L->m('000');
		//
		// Custom abbreviations
		//
		$tmp['arTmp'] = array();
		$arSrc = $this->L->getCustom('d_abbr_custom', $this->arDictParam['lang'], 'return');
		// convert 2-dimensional array into single array
		while (list($abrK, $abrV) = each($arSrc))
		{
			if (isset($abrV[$id]))
			{
				$tmp['arTmp'][$abrK] = $abrV[$id];
			}
		}
		asort($tmp['arTmp']);
		$tmp['arAbbr'] = array_merge_clobber($tmp['arAbbr'], $tmp['arTmp']);
		$tmp['arAbbr'] = array_merge_clobber($tmp['arAbbr'], array('--   ' => '------------') );
		//
		// Language-related abbreviation names
		//
		$tmp['arTmp'] = array();
		//
		$arSrc = $this->L->getCustom('d_linguistics', $this->arDictParam['lang'], 'return');
		// convert 2-dimensional array into single array
		while (list($abrK, $abrV) = each($arSrc))
		{
			if (isset($abrV[$id]))
			{
				$tmp['arTmp'][$abrK] = $abrV[$id];
			}
		}
		asort($tmp['arTmp']);
		$tmp['arAbbr'] = array_merge_clobber($tmp['arAbbr'], $tmp['arTmp']);
		$tmp['arAbbr'] = array_merge_clobber($tmp['arAbbr'], array('-- ' => '------------') );
		//
		// Parts of speech
		//
		$tmp['arTmp'] = array();
		$arSrc = $this->L->getCustom('d_speech', $this->arDictParam['lang'], 'return');
		// convert 2-dimensional array into single array
		while (list($abrK, $abrV) = each($arSrc))
		{
			if (isset($abrV[$id]))
			{
				$tmp['arTmp'][$abrK] = $abrV[$id];
			}
		}
		asort($tmp['arTmp']);
		$tmp['arAbbr'] = array_merge_clobber($tmp['arAbbr'], $tmp['arTmp']);
		$tmp['arAbbr'] = array_merge_clobber($tmp['arAbbr'], array('--  ' => '------------') );
		//
		// Natural science names
		//
		$tmp['arTmp'] = array();
		$arSrc = $this->L->getCustom('d_science', $this->arDictParam['lang'], 'return');
		// convert 2-dimensional array into single array
		while (list($abrK, $abrV) = each($arSrc))
		{
			if (isset($abrV[$id]))
			{
				$tmp['arTmp'][$abrK] = $abrV[$id];
			}
		}
		asort($tmp['arTmp']);
		$tmp['arAbbr'] = array_merge_clobber($tmp['arAbbr'], $tmp['arTmp']);
		$tmp['arAbbr'] = array_merge_clobber($tmp['arAbbr'], array('--  ' => '------------') );
		$tmp['arTmp'] = array();
		//
		$this->Set('arAbbr'.$id, $tmp['arAbbr']);
	}
	//
	function make_trns($fieldname, $ar = array())
	{
		return $this->make_abbr($fieldname, $ar, 'trns');
	}
	//
	function make_abbr($fieldname, $ar = array(), $tag = 'abbr')
	{
		$tmp['strform'] = '';

		if ($tag == 'abbr')
		{
			$tmp['cur_element'] = 'arAbbr'.$this->abbr_element;
		}
		elseif ($tag == 'trns')
		{
			$tmp['cur_element'] = 'arTrns'.$this->trns_element;
		}
		$tmp['cur_array'] = $this->$tmp['cur_element'];
		
		$tmp['arEl'] = isset($this->arEl[$fieldname][$ar['elK']]) ? $this->arEl[$fieldname][$ar['elK']] : array();
		//
		$this->unsetTag('input'); // reset settings for <input>
		$this->setTag('input', 'size', '25');
		//
		// do auto fill
		//
		if (empty($tmp['arEl']))
		{
			$tmp['arEl'][0] = array('value' => '', 'attributes' => array('lang' => '--'));
		}
		//
		while (list($chK, $chV) = each($tmp['arEl']))
		{
			//
			$tmp['attributes'] = $this->objDom->get_attribute('lang', '', $chV);
			$tmp['str'] = $this->objDom->get_content( $chV );
			// append new value
			if (!isset($tmp['cur_array'][$tmp['attributes']]))
			{
				if ($tmp['attributes'] == '')
				{
					$tmp['cur_array']['--'] = $this->L->m('000');
				}
				else
				{
					$tmp['cur_array'][$tmp['attributes']] = $tmp['attributes'];
				}
			}
			//
			$tmp['strform'] .= CRLF . sprintf('<tr style="vertical-align:top"><td style="text-align:%s" class="%s">%s:%s</td>',
											$this->Gsys['css_align_right'], $this->Gtmp['cssTrClass'], $this->L->m($fieldname), ''
									  );
			$tmp['strform'] .= '<td>' . $this->field('select', 'arPre['.$fieldname.']['.$ar['elK'].']['.$chK.'][attributes][lang]', $tmp['attributes'], '160', $tmp['cur_array']) . '</td>';

			// enable HTML-editor events
			if ($this->arDictParam['is_htmled'] == 1)
			{
				$this->setTag('input', 'onfocus', 'toolbar_on(\''.$ar['elK'].'\');toolbar_symb_off(\''.$ar['elK'].'\')');
#				$this->setTag('input', 'onblur', 'toolbar_off(\''.$ar['elK'].'\');toolbar_symb_off(\''.$ar['elK'].'\')');
				$this->setTag('input', 'onclick', 'storeCaret(this)');
				$this->setTag('input', 'onselect', 'storeCaret(this)');
			}
			$tmp['strform'] .= '<td>' . $this->field('input', 'arPre['.$fieldname.']['.$ar['elK'].']['.$chK.'][value]', textcodetoform( $tmp['str'] ) ) . '</td>';
			
			$tmp['strform'] .= '<td>';
			// allow to add new element, default
			if ($chK == 0)
			{
				$tmp['strform'] .= '<input type="submit" style="width:24px;height:24px" class="submitok" name="'.
								   'arControl['.$fieldname.']['.GW_A_ADD.']['.$ar['elK'].']['.$chK.']" title="'.
								   $this->L->m('3_add_'.$tag).'" value="+" />';
			}
			else // remove abbreviation
			{
				// use javascript to clean values; much faster that submit reload; submit also works
				$js['CurFieldVal'] = $this->text_field2id('arPre['.$fieldname.']['.$ar['elK'].']['.$chK.'][value]');
				$js['CurFieldAtt'] = $this->text_field2id('arPre['.$fieldname.']['.$ar['elK'].']['.$chK.'][attributes][lang]');
				$js['Onclick'] = 'document.forms[\'vbform\'].elements[\''.$js['CurFieldVal'].'\'].value=\'\'';
				$js['Onclick'] .= ';document.forms[\'vbform\'].elements[\''.$js['CurFieldAtt'].'\'].value=\'--\'';
				$tmp['strform'] .= '<input onclick="'.$js['Onclick'].'" type="button" style="width:24px;height:24px" class="submitdel" name="'.
								   'arControl['.$fieldname.']['.REMOVE.']['.$ar['elK'].']['.$chK.']" title="'.
								   $this->L->m('3_remove_'.$tag).'" value="&#215;" />';
			}
			$tmp['strform'] .= '</td>';
			$tmp['strform'] .= '</tr>';
		}
		return $tmp['strform'];
	}
	//
	function make_usg($fieldname, $ar = array())
	{
		return $this->make_set_array2textarea($fieldname, $ar);
	}
	//
	function make_src($fieldname, $ar = array())
	{
		return $this->make_set_textarea($fieldname, $ar);
	}
	//
	function make_syn($fieldname, $ar = array())
	{
		global $oFunc;
		$tmp['strform'] = $tmp['str'] = '';

#        $tmp['isLinkSyn'] = isset($this->arEl['isLinkSyn']) ? $this->arEl['isLinkSyn'] : 0; // is synonyms linked or not
		$tmp['arEl'] = isset($this->arEl[$fieldname][$ar['elK']]) ? $this->arEl[$fieldname][$ar['elK']] : array();
		//
		// do auto fill
		//
		if (empty($tmp['arEl']))
		{
			$tmp['arEl'][0] = array('value' => '', 'attributes' => array('link' => ''));
		}
		//
		$this->unsetTag('textarea'); // reset settings for <textarea>
		$this->unsetTag('checkbox'); // reset settings for <input>
		$this->setTag('checkbox', 'title', $this->L->m('ed_linksyn'));
		$this->setTag('textarea', 'title', $this->L->m('tip002'));
		//
		while (list($elK, $elV) = each($tmp['arEl']))
		{
			$tmp['str'] .= $elV['value'];
			$tmp['isLinkSyn'] = 0;
			if (isset($elV['attributes']['text']))
			{
				$tmp['str'] .= $elV['attributes']['text'];
			}
			if (isset($elV['attributes']['is_link']))
			{
				$tmp['isLinkSyn'] = 1;
			}            
			$tmp['str'] .= CRLF;
		}
		$tmp['intFormHeight'] = $oFunc->getFormHeight( $tmp['str'] );
		$tmp['strform'] .= CRLF . '<tr style="vertical-align:top">';
		$tmp['strform'] .= sprintf('<td style="text-align:%s" class="%s">%s:%s</td>',
							$this->Gsys['css_align_right'], $this->Gtmp['cssTrClass'], $this->L->m($fieldname), ''
						   );
		$tmp['strform'] .= '<td colspan="2">';
		$tmp['strform'] .= $this->field('textarea',
								'arPre['.$fieldname.']['.$ar['elK'].'][0][value]',
								textcodetoform( $tmp['str'] ),
								(intval($tmp['intFormHeight'] - 1))
							);
		$tmp['strform'] .= '</td>';
		$tmp['strform'] .= '<td>' . $this->field('checkbox', 'arPre['.$fieldname.']['.$ar['elK'].'][0][attributes][is_link]', $tmp['isLinkSyn']) . '</td>';
		$tmp['strform'] .= '</tr>';
		return $tmp['strform'];
	}
	//
	function make_see($fieldname, $ar = array())
	{
		global $oFunc;
		$tmp['strform'] = $tmp['str'] = '';
		$tmp['arEl'] = isset($this->arEl[$fieldname][$ar['elK']]) ? $this->arEl[$fieldname][$ar['elK']] : array();
		//
		// do auto fill
		//
		if (empty($tmp['arEl']))
		{
			$tmp['arEl'][0] = array('value' => '', 'attributes' => array('link' => ''));
		}
		//
		$this->unsetTag('textarea'); // reset settings for <textarea>
		$this->unsetTag('checkbox'); // reset settings for <input>
		$this->setTag('checkbox', 'title', $this->L->m('ed_linksee'));
		$this->setTag('textarea', 'title', $this->L->m('tip002'));
		//
		while (list($elK, $elV) = each($tmp['arEl']))
		{
			$tmp['str'] .= $elV['value'];
			$tmp['isLinkSee'] = 0;
			if (isset($elV['attributes']['text']))
			{
				$tmp['str'] .= $elV['attributes']['text'];
			}
			if (isset($elV['attributes']['is_link']))
			{
				$tmp['isLinkSee'] = 1;
			}
			$tmp['str'] .= CRLF;
		}
		$tmp['intFormHeight'] = $oFunc->getFormHeight( $tmp['str'] );
		$tmp['strform'] .= CRLF . '<tr style="vertical-align:top">';
		$tmp['strform'] .= sprintf('<td style="text-align:%s" class="%s">%s:%s</td>',
							$this->Gsys['css_align_right'], $this->Gtmp['cssTrClass'], $this->L->m($fieldname), ''
						   );
		$tmp['strform'] .= '<td colspan="2">';
		$tmp['strform'] .= $this->field('textarea',
								'arPre['.$fieldname.']['.$ar['elK'].'][0][value]',
								textcodetoform( $tmp['str'] ),
								(intval($tmp['intFormHeight'] - 1))
							);
		$tmp['strform'] .= '</td>';
		$tmp['strform'] .= '<td>' . $this->field('checkbox', 'arPre['.$fieldname.']['.$ar['elK'].'][0][attributes][is_link]', $tmp['isLinkSee']) . '</td>';
		$tmp['strform'] .= '</tr>';
		return $tmp['strform'];
	}
	//
	function make_address($fieldname, $ar = array())
	{
		return $this->make_set_textarea($fieldname, $ar);
	}
	//
	function make_phone($fieldname, $ar = array())
	{
		return $this->make_set_textarea($fieldname, $ar);
	}
	//
	function make_set_textarea($fieldname, $ar = array())
	{
		global $oFunc;
		$tmp['strform'] = $tmp['str'] = '';
		$tmp['arEl'] = isset($this->arEl[$fieldname][$ar['elK']]) ? $this->arEl[$fieldname][$ar['elK']] : array();
		//
		// do auto fill
		//
		if (empty($tmp['arEl']))
		{
			$tmp['arEl'][0] = array('value' => '');
		}
		//
		$this->unsetTag('textarea'); // reset settings for <textarea>
		//
		while (list($elK, $elV) = each($tmp['arEl']))
		{
			$tmp['str'] .= $elV['value'];
			$tmp['str'] .= CRLF;
		}
		$tmp['intFormHeight'] = $oFunc->getFormHeight( $tmp['str'] );
		$tmp['strform'] .= CRLF . '<tr style="vertical-align:top">';
		$tmp['strform'] .= sprintf('<td style="text-align:%s" class="%s">%s:%s</td>',
							$this->Gsys['css_align_right'], $this->Gtmp['cssTrClass'], $this->L->m($fieldname), ''
						   );
		$tmp['strform'] .= '<td colspan="2">';
		$tmp['strform'] .= $this->field('textarea',
								'arPre['.$fieldname.']['.$ar['elK'].'][0][value]',
								textcodetoform( $tmp['str'] ),
								(intval($tmp['intFormHeight'] - 1))
							);
		$tmp['strform'] .= '</td>';
		$tmp['strform'] .= '<td></td>';
		$tmp['strform'] .= '</tr>';
		return $tmp['strform'];
	}
	//
	function make_set_array2textarea($fieldname, $ar = array())
	{
		global $oFunc;
		$this->unsetTag('textarea');
		//
		$tmp['strform'] = $tmp['str'] = '';
		//
		if (!isset($this->arEl[$fieldname]))
		{
			$this->arEl[$fieldname] = array();
		}
		if (!isset($ar['elK']))
		{
			$ar['elK'] = 0;
		}        
		for (reset($this->arEl[$fieldname]); list($elK, $elV) = each($this->arEl[$fieldname]);)
		{
			if (isset($elV['value']) && ($ar['elK'] == $elK))
			{
				$tmp['str'] .= $elV['value'];
				$tmp['str'] .= CRLF;
			}
			elseif (intval($ar['elK']) == intval($elK)) // multiarray
			{
				while (list($k, $v) = each($elV))
				{
					$tmp['str'] .= $v['value'];
					$tmp['str'] .= CRLF;
				}
				#print '<br>' . $ar['elK'] . ' == ' . $elK;
			}            
		}        
				
		//
		$tmp['intFormHeight'] = $oFunc->getFormHeight( $tmp['str'] );
		$tmp['strform'] .= CRLF . '<tr style="vertical-align:top">';
		$tmp['strform'] .= sprintf('<td style="text-align:%s" class="%s">%s:%s</td>',
							$this->Gsys['css_align_right'], $this->Gtmp['cssTrClass'], $this->L->m($fieldname), ''
						   );
		$tmp['strform'] .= '<td colspan="2">';
		//
		// enable HTML-editor events
		if ($this->arDictParam['is_htmled'] == 1)
		{
			$this->setTag('textarea', 'onfocus', 'toolbar_on(\''.$ar['elK'].'\');toolbar_symb_off(\''.$ar['elK'].'\')');
#			$this->setTag('textarea', 'onblur', 'toolbar_on(\''.$ar['elK'].'\');toolbar_symb_off(\''.$ar['elK'].'\')');
			$this->setTag('textarea', 'onclick', 'storeCaret(this)');
			$this->setTag('textarea', 'onselect', 'storeCaret(this)');
		}
		$tmp['strform'] .= $this->field('textarea',
								'arPre['.$fieldname.']['.$ar['elK'].'][value]',
								textcodetoform( $tmp['str'] ),
								(intval($tmp['intFormHeight'] / 2) + 1)
							);
		$tmp['strform'] .= '</td>';
		$tmp['strform'] .= '</tr>';
		return $tmp['strform'];
	}

} // end of class

?>