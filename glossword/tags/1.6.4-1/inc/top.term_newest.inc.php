<?php
if (!defined('IN_GW'))
{
	die('<!-- $Id: top.term_newest.inc.php,v 1.7 2004/11/13 12:30:09 yrtimd Exp $ -->');
}
/**
 *  Glossword - glossary compiler (http://glossword.info/dev/) 
 *  � 2002-2004 Dmitry N. Shilnikov <dev at glossword.info>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  (see `glossword/support/license.html' for details)
 */
// --------------------------------------------------------
/**
 *  Makes the list of last added terms.
 *  This code runs inside function getTop10();
 *  switch keyname: R_TERM_NEWEST
 */
// --------------------------------------------------------
/**
 * Variables:
 * $amount, $arThText,  $arThAlign, $strTopicName, $strData, $curDateMk
 */

	//
	$arSql = $oDb->sqlRun(sprintf($oSqlQ->getQ('top-term-new', $arDictParam['tablename'], $amount)), sprintf("%05d", $dictID));
	//
	$arThText = array();
	// Sets width for colums
	if ($isItemOnly)
	{
		$arThWidth = array('99%');
	}
	else
	{
		$arThWidth = array('70%', '29%');
	}    
	// Sets alignment for colums
	$arThAlign = array($sys['css_align_left'], $sys['css_align_right']);
	$strTopicName = $L->m('recent');
	// for each dictionary
	for (; list($arK, $arV) = each($arSql);)
	{
		$cnt % 2 ? ($bgcolor = $GLOBALS['theme']['color_2']) : ($bgcolor = $GLOBALS['theme']['color_1']);
		$cnt++;
		$strData .= '<tr class="f" style="background:' . $bgcolor . '">';
		$strData .= '<td class="q">' . $cnt . '</td>';
		$url = $oHtml->a( append_url($sys['page_index']
							. '?a=term&d=' . $GLOBALS["dictID"]
							. '&t=' . $arV['id']), strip_tags($arV['term']), '');
		$strData .= '<td class="termpreview">' . $url . '</td>';
		if (!$isItemOnly)
		{
			$strData .= '<td class="q" style="white-space:nowrap">';
			$strData .= (dateExtract($arV['date_created'], '%d') / 1) . dateExtract($arV['date_created'], '&#160;%F&#160;%Y');
			$strData .= '</td>';
		}
		$strData .= '</tr>';
	}
/**
 * end of R_TERM_NEWEST
 */
?>