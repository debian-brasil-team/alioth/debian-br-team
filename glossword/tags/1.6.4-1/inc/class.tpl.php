<?php
/**
 *  $Id: class.tpl.php,v 1.11 2004/06/15 15:27:53 yrtimd Exp $
 */
/**
 *  Glossword - glossary compiler (http://glossword.info/dev/)
 *  � 2002-2004 Dmitry N. Shilnikov <dev at glossword.info>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  (see `glossword/support/license.html' for details)
 */
// --------------------------------------------------------
/**
 *  HTML-template class. Obsoleted by XSLT.
 */
// --------------------------------------------------------


class gwTemplate{

	// Relative path for filenames
	var $root               = ".";
	// Replaces unmatched tags with this text
	var $unknownTags        = ""; // [ keep | clear | newcontent ]
	var $unknownTagsT       = ""; // [ keep | clear | newcontent ]
	var $msgError           = "";
	var $tplPairs           = array();
	var $content            = array();
	var $contentParsed      = "";
	var $isImportDefaultVars = 0;
	var $varXref            = "";
	var $layout             = "";
	var $dplayout           = 0;
	var $is_tpl_show_names  = 0;

	function removeVal($varname, $value)
	{
		unset($this->tplPairs[$varname]);
	} // end func addVal

	function addVal($varname, $value)
	{
		if (!isset($this->tplPairs[$varname]))
		{
			$this->tplPairs[$varname] = $value;
		}
		else
		{
			//
			$this->tplPairs[$varname] = $value;
		}
	} // end func addVal

	/**
	 * Recursive
	 */
	function _tpl_replace($str, $isFirst = 1)
	{
		// match tags in template
		$preg = "/\{([A-Za-z0-9:\-_]*?)\}/si";
		preg_match_all($preg, $str, $a);
		// 23 nov 2002
		// unique templates only
		$a[1] = array_values(array_unique($a[1]));
		$a[1] = array_flip($a[1]);
		//
		$intFound = 0;
		// starting replace
		for (; list($varFound, $varId) = each($a[1]);)
		{
			$isCollect = 1; // set variable as unmatched
			// for mathched tags
			if ( isset($this->tplPairs[$varFound]) )
			{
				$intFound++;
				$str = str_replace('{'.$varFound.'}', $this->tplPairs[$varFound], $str);
			}
			else
			{
				if (($this->unknownTags != 'keep')&&($isFirst))
				{
					$str = str_replace('{'.$varFound.'}', $this->unknownTags, $str);
				}
				elseif (($this->unknownTagsT != 'keep')&&(!$isFirst))
				{
					$str = str_replace('{'.$varFound.'}', $this->unknownTags, $str);
				}
				// collect unmatched tags
				$arNoVar[] = $varFound;
			}
		} // elements found
		if (isset($arNoVar))
		{
#           print "<ol>" . "<li>".implode("<li>", $arNoVar) . "</ol>";
		}
		return $str;
	}

	/**
	 *
	 */
	function parse()
	{
		// autoload theme colors
		if ( isset($GLOBALS["theme"]) && is_array($GLOBALS["theme"]) )
		{
			for (reset($GLOBALS["theme"]); list($k, $v) = each($GLOBALS["theme"]);)
			{
				$this->addVal($k, $v);
			}
		}
		//
		$this->contentParsed = implode("", $this->content);
		$this->contentParsed = $this->_tpl_replace($this->contentParsed);
		// 2nd
		$preg = "/\{([A-Za-z0-9:\-_]+)\}/si";
		preg_match_all($preg, $this->contentParsed, $a);
		if (sizeof($a[0]) > 0)
		{
			$this->contentParsed = $this->_tpl_replace($this->contentParsed, 0);
		}
		// If any error occured exit with error message
		if ($this->msgError != ""){ $this->halt($this->msgError); }

		$this->content = array();
		$this->tplPairs = array();
		return true;
	}

	/**
	* Sets root directory for files. (Not in use)
	*
	* @param    string  Name of the file
	* @return   boolean False if the given file was not found or is empty otherwise true.
	*/
	function setRoot($r)
	{
		if (is_dir($r))
		{
			$this->halt("setRoot: $r is not a directory.");
			return false;
		}
		$this->root = $r;
		return true;
	} // end func setRoot


	function setBlock($handle, $name, $pairs)
	{
		$reg = "/<!--\s+BEGIN $name\s+-->(.*)\n\s*<!--\s+END $name\s+-->/sm";
		preg_match($reg, $this->content[$handle], $m);
		$arRep = array();
		$this->content[$handle] = preg_replace($reg, "{" . "$name}", $this->content[$handle]);
		if(isset($m[1]))
		{
			reset($pairs);
			for (; list($k1, $v1) = each($pairs);)
			{
				if(is_array($v1))
				{
					$arRep[$k1] = $m[1];
					reset($v1);
					for (; list($k2, $v2) = each($v1);)
					{
						$arRep[$k1] = str_replace("{" . "$k2}", $v2, $arRep[$k1]);
				   }
				}
			}
		}

		if( $this->dplayout > 1 )
		{
			$r = new htmlRenderCells();
			$r->ar = $arRep;
			$r->tBorder = 0;
			$r->X = $this->dplayout;
			$r->Y = $GLOBALS['sys']['page_limit'];
			$arRep = array();
			$this->content[$handle] = str_replace("{list_item}", $r->RenderCells(), $this->content[$handle]);
			$this->content[$handle] = str_replace("{search_item}", $r->RenderCells(), $this->content[$handle]);
			$this->content[$handle] = str_replace("{catalog_item}", $r->RenderCells(), $this->content[$handle]);
			$this->content[$handle] = str_replace("{catalog_topic}", $r->RenderCells(), $this->content[$handle]);
			$this->content[$handle] = str_replace("{catalog_dict}", $r->RenderCells(), $this->content[$handle]);
		}

		$this->content[$handle] = str_replace("{" . "$name}", implode("", $arRep), $this->content[$handle]);

		return true;
	}

	function setHandle($handle, $filename = "")
	{
		if (!is_array($handle))
		{
			if ($filename == "")
			{
				$this->halt("setHandle: filename for $handle is empty.");
				return false;
			}
			if ($this->is_tpl_show_names)
			{
				$this->content[$handle] = '<table border="1"><tr><td>'.$filename.'</td></tr><tr><td>'.$this->loadFile($filename).'</tr></td></table>';
			}
			else
			{
				$this->content[$handle] = $this->loadFile($filename);
			}
		}
	}

	/**
	* Loads the specified template file.
	*
	* @param    string  Name of the file
	* @return   string      Content
	*/
	function loadFile($filename)
	{
		global $oFunc;
		return $oFunc->file_get_contents($filename);
	}

	/**
	* empty description
	*
	* @return   boolean Always false
	*/
	function halt($msg)
	{
		die($msg);
		return false;
	}

	/**
	* Outputs parsed page
	*
	* @return   string      parsed content
	*/
	function output()
	{
		if ($this->contentParsed != '') // if parse() was called
		{
			// return parsed template
			return $this->contentParsed;
		}
		else // if parse() was not called
		{
			// return the source of the template
			return implode('', $this->content);
		}
	}

} // end class gwTemplate

?>