<?php
/**
 *  Glossword - glossary compiler (http://glossword.info/dev/) 
 *  � 2002-2004 Dmitry N. Shilnikov <dev at glossword.info>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  (see `glossword/support/license.html' for details)
 */
// --------------------------------------------------------
/**
 * Auth class.
 * @version $Id: lib.auth.php,v 1.12 2004/09/25 11:07:10 yrtimd Exp $
 * Based on PHPlib concepts
 * http://phplib.shonline.de/
 */
// --------------------------------------------------------
/**
 * Session Management for PHP3
 *
 * Copyright (c) 1998-2000 NetUSE AG
 *                    Boris Erdmann, Kristian Koehntopp
 * Copyright (c) 1999-2000 Internet Images srl
 *                    Massimiliano Masserelli
 *
 * Id: auth.inc,v 1.2 2000/07/12 18:22:33 kk Exp
 *
 */
 
		/**
		 * Opens "Login" window.
		 */
		function _gw_login_edit($arV = '', $isFirst = 0, $arBroken = array())
		{
			global $tpl, $form, $arReq, $sess, $user, $post;
			/* no vars defined, 1st time run mode */
			if (empty($arV))
			{
				$arV['user_name'] = '';
				$arV['user_pass'] = '';
				$arV['user_email'] = '';
			}
			$str = _getFormLogin($arV, $isFirst, $arBroken, $arReq);
			return $form->Output($str);
		}

		/* */
		function _getFormLogin($vars, $isFirst = 0, $arBroken = array(), $arReq = array())
		{
			global $sys, ${GW_ACTION}, ${GW_TARGET}, $form, $auth, $L, $url_login, ${GW_SID};
			$form = new gwForms();
			$form->action = $sys['page_login'];
			$form->submitok = $L->m('3_login');
			$form->submitcancel = $L->m('3_cancel');
			$form->isButtonCancel = 0;
			$form->Set('charset', $sys['internal_encoding']);
			$form->strNotes = '<span class="t">'.
						htmlTagsA( $url_login . '?' . GW_TARGET . '=' . USER . '&' .  GW_ACTION . '=lostpass',
						'&gt; '.$L->m('3_password_lost')) .
						'</span>';
			$titlemsg = $L->m('2_login');
			if (${GW_ACTION} == 'lostpass')
			{
				$titlemsg = $L->m('3_password_lost');
				$form->submitok = $L->m('3_password_send');
				$form->strNotes = '<span class="t">'.
							htmlTagsA( $url_login . '?' . GW_TARGET . '=' . USER . '&' .  GW_ACTION . '=login',
							'&gt; '.$L->m('2_login')) .
							'</span>';
			}
			$form->formwidth = "300";
			$form->formbgcolor = $GLOBALS['theme']['color_2'];
			$form->formbordercolor = $GLOBALS['theme']['color_4'];
			$trClass = "t";
			$strForm = "";
			## ----------------------------------------------------
			##
			// reverse array keys <-- values;
			$arReq = array_flip($arReq);
			// mark fields as "REQUIRED" and make error messages
			while(is_array($vars) && list($key, $val) = each($vars) )
			{
				$arReqMsg[$key] = $arBrokenMsg[$key] = "";
				if (isset($arReq[$key])) { $arReqMsg[$key] = '&#160;<span style="color:#E30"><b>*</b></span>'; }
				if (isset($arBroken[$key])) { $arBrokenMsg[$key] = ' <span class="'.$trClass.'" style="color:#E30"><b>' . $L->m('reason_9') .'</b></span>'; }
			} // end of while
			##
			## ----------------------------------------------------
			$strForm .= getFormTitleNav($titlemsg);
			$strForm .= '<table cellspacing="3" cellpadding="0" border="0" width="100%">';
			$strForm .= '<col width="30%"/><col width="70%"/>';
			$strForm .= '<tr valign="top">'.
						'<td class="' . $trClass . '">' . $L->m('login') . ':' . $arReqMsg["user_name"] . '</td>'.
						'<td >' . $arBrokenMsg['user_name'] . $form->field("input", "arPost[user_name]", textcodetoform($vars['user_name']), 16) . '</td>'.
						'</tr>';
			//
			if (${GW_ACTION} == 'lostpass')
			{
				$strForm .= '<tr valign="top">'.
							'<td class="' . $trClass . '">' . $L->m('y_email') . ':' . $arReqMsg["user_email"] . '</td>'.
							'<td>' . $arBrokenMsg['user_email'] . $form->field("input", "arPost[user_email]", textcodetoform($vars['user_email']), 64) . '</td>'.
							'</tr>';
				$strForm .= '</table>';
				$strForm .= $form->field("hidden", GW_ACTION, ${GW_ACTION});
			}
			else
			{
				$strForm .= '<tr valign="top">'.
							'<td class="' . $trClass . '">' . $L->m('password') . ':' . $arReqMsg["user_pass"] . '</td>'.
							'<td >' . $arBrokenMsg['user_pass'] . $form->field("pass", "arPost[user_pass]", textcodetoform($vars['user_pass']), 16) . '</td>'.
							'</tr>';
				$strForm .= '</table>';
				$strForm .= $form->field("hidden", GW_ACTION, "login");
			}
			$strForm .= $form->field("hidden", GW_SID, ${GW_SID});
			return $strForm;
		}
		
// --------------------------------------------------------
class Auth {
  var $classname = "Auth";
  var $persistent_slots = array("auth");
  ## User qualifiable settings.
  /**
   * Max allowed idle time in minutes before reauthentication is necessary.
   * If set to 0, auth never expires.
   */
  var $lifetime = 180;
  /**
   * Refresh interval in minutes. When expires auth data is refreshed
   * from db using auth_refreshlogin() method. Set to 0 to disable refresh.
   */
  var $refresh  = 0;
  /* "log" for login only systems, "reg" for user self registration */
  var $mode     = "log";
  /* Used in uniqid() generation */
  var $magic    = "";
  /* If true, a default auth is created... */
  var $nobody   = false;
  /* The name of a button that can be used to cancel a login form */
  var $cancel_login = "cancel_login";
  ## End of user qualifiable settings.
  var $auth = array();
  var $in;
  var $db;
  ##
  ## Initialization
  ##
  function start() {
	$cl = $this->cancel_login;
	global $sess, $$cl;
	$uid = '';
	## This is for performance, I guess but I'm not sure if it could
	## be safely removed -- negro
	if (!isset($this->in) || !$this->in) {
	  $sess->register("auth");
	  $this->in = true;
	}
	## back compatibility: if d_c is set, create db object
	if(isset($this->database_class)) {
	  $class = $this->database_class;
	  $this->db = new $class;
	}
	# Check current auth state. Should be one of
	#  1) Not logged in (no valid auth info or auth expired)
	#  2) Logged in (valid auth info)
	#  3) Login in progress (if $$cl, revert to state 1)
	if ($this->is_authenticated()) {
	  $uid = $this->auth["uid"];
	  switch ($uid) {
		case "form":
		  # Login in progress
		  if ($$cl) {
			# If $$cl is set, delete all auth info
			# and set state to "Not logged in", so eventually
			# default or automatic authentication may take place
			$this->unauth();
			$state = 1;
		  } else {
			# Set state to "Login in progress"
			$state = 3;
		  }
		  break;
		default:
		  # User is authenticated and auth not expired
		  $state = 2;
		  break;
	  }
	} else {
	  # User is not (yet) authenticated
	  $this->unauth();
	  $state = 1;
	}
	switch ($state) {
	  case 1:
		# No valid auth info or auth is expired
		# Check for user supplied automatic login procedure
		if ( $uid = $this->auth_preauth() ) {
		  $this->auth["uid"] = $uid;
		  $this->auth["exp"] = time() + (60 * $this->lifetime);
		  $this->auth["refresh"] = time() + (60 * $this->refresh);
		  return true;
		}
		# Check for "log" vs. "reg" mode
		switch ($this->mode) {
		  case "yes":
		  case "log":
			if ($this->nobody) {
			  # Authenticate as nobody
			  $this->auth["uid"] = "nobody";
			  # $this->auth["uname"] = "nobody";
			  $this->auth["exp"] = 0x7fffffff;
			  $this->auth["refresh"] = 0x7fffffff;
			  return true;
			} else {
			  # Show the login form
			  $this->auth_loginform();
			  $this->auth["uid"] = "form";
			  $this->auth["exp"] = 0x7fffffff;
			  $this->auth["refresh"] = 0x7fffffff;
			  $sess->freeze();
			  exit;
			}
			break;
		  case "reg":
			# Show the registration form
			$this->auth_registerform();
			$this->auth["uid"] = "form";
			$this->auth["exp"] = 0x7fffffff;
			$this->auth["refresh"] = 0x7fffffff;
			$sess->freeze();
			exit;
			break;
		  default:
			# This should never happen. Complain.
			echo "Error in auth handling: no valid mode specified.\n";
			$sess->freeze();
			exit;
		}
		break;
	  case 2:
		# Valid auth info
		# Refresh expire info
		## DEFAUTH handling: do not update exp for nobody.
		if ($uid != "nobody")
		  $this->auth["exp"] = time() + (60 * $this->lifetime);
		break;
	  case 3:
		# Login in progress, check results and act accordingly
		switch ($this->mode) {
		  case "yes":
		  case "log":
			if ( $uid = $this->auth_validatelogin() ) {
			  $this->auth["uid"] = $uid;
			  $this->auth["exp"] = time() + (60 * $this->lifetime);
			  $this->auth["refresh"] = time() + (60 * $this->refresh);
			  return true;
			} else {
			  $this->auth_loginform();
			  $this->auth["uid"] = "form";
			  $this->auth["exp"] = 0x7fffffff;
			  $this->auth["refresh"] = 0x7fffffff;
			  $sess->freeze();
			  exit;
			}
			break;
		  case "reg":
			if ($uid = $this->auth_doregister()) {
			  $this->auth["uid"] = $uid;
			  $this->auth["exp"] = time() + (60 * $this->lifetime);
			  $this->auth["refresh"] = time() + (60 * $this->refresh);
			  return true;
			} else {
			  $this->auth_registerform();
			  $this->auth["uid"] = "form";
			  $this->auth["exp"] = 0x7fffffff;
			  $this->auth["refresh"] = 0x7fffffff;
			  $sess->freeze();
			  exit;
			}
			break;
		  default:
			# This should never happen. Complain.
			echo "Error in auth handling: no valid mode specified.\n";
			$sess->freeze();
			exit;
			break;
		}
		break;
	  default:
		# This should never happen. Complain.
		echo "Error in auth handling: invalid state reached.\n";
		$sess->freeze();
		exit;
		break;
	}
  }
  function login_if( $t ) {
	if ( $t ) {
	  $this->unauth();  # We have to relogin, so clear current auth info
	  $this->nobody = false; # We are forcing login, so default auth is
							 # disabled
	  $this->start(); # Call authentication code
	}
  }
  function unauth($nobody = false) {
	$this->auth["uid"]   = "";
	$this->auth["perm"]  = "";
	$this->auth["exp"]   = 0;
	## Back compatibility: passing $nobody to this method is
	## deprecated
	if ($nobody) {
	  $this->auth["uid"]   = "nobody";
	  $this->auth["perm"]  = "";
	  $this->auth["exp"]   = 0x7fffffff;
	}
  }
  function logout($nobody = "") {
	global $sess;
	$sess->unregister("auth");
	unset($this->auth["uname"]);
	$this->unauth($nobody == "" ? $this->nobody : $nobody);
  }
  function is_authenticated() {
	if (
	  isset($this->auth["uid"])
		&&
	  (($this->lifetime <= 0) || (time() < $this->auth["exp"]))
	) {
	  # If more than $this->refresh minutes are passed since last check,
	  # perform auth data refreshing. Refresh is only done when current
	  # session is valid (registered, not expired).
	  if (
		($this->refresh > 0)
		 &&
		($this->auth["refresh"])
		 &&
		($this->auth["refresh"] < time())
	  ) {
		if ( $this->auth_refreshlogin() ) {
		  $this->auth["refresh"] = time() + (60 * $this->refresh);
		} else {
		  return false;
		}
	  }
	  return $this->auth["uid"];
	} else {
	  return false;
	}
  }
  ########################################################################
  ##
  ## Helper functions
  ##
  function url() {
	return $GLOBALS["sess"]->self_url();
  }
  function purl() {
	print $GLOBALS["sess"]->self_url();
  }
  ## This method can authenticate a user before the loginform
  ## is being displayed. If it does, it must set a valid uid
  ## (i.e. nobody IS NOT a valid uid) just like auth_validatelogin,
  ## else it shall return false.
  function auth_preauth() { return false; }
  ##
  ## Authentication dummies. Must be overridden by user.
  ##
  function auth_loginform() { ; }
  function auth_validatelogin() { ; }
  function auth_refreshlogin() { ; }
  function auth_registerform() { ; }
  function auth_doregister() { ; }
} // end of Auth
//
//
//
class gwAuth extends Auth {
	var $classname = "gwAuth";
	var $database_table = TBL_AUTH;
	var $database_class = "gwtkDb";
	var $magic    = "�"; // ������ '�'? ����� ����� �� ���������.
	/* Check password */
	function auth_validatelogin()
	{
		$uid = false;
		if (isset($this->auth['user_id']))
		{
			$uid = $this->auth['user_id'];
			unset($this->auth['user_id']);
		}
		// should return user id
		return $uid;
	}
	/* Enter password */
	function auth_loginform()
	{
		global $sess, ${GW_SID}, ${GW_ACTION}, ${GW_TARGET},
			   $sys, $vars, $post, $arReq, $L, $arReq,
			   $arPost, $url_login, $actkey, $oHdr;
		$isShowRedirect = $sys['is_delay_redirect'];
		$isShowQueries = 0;
		$strStatus = '';
		/* Set required fields for html forms */
		$arReq = array('user_name');
		if (${GW_ACTION} == 'lostpass')
		{
			$arReq[] = 'user_email';
		}
		else
		{
			$arReq[] = 'user_pass';
		}
		$url_login = $sys['server_proto'] . $sys['server_host'] . $sys['page_login'];
		$url_admin = $sys['server_proto'] . $sys['server_host'] . $sys['page_admin'];
		
		/* if adminpage loaded without session */
		if (${GW_SID} == '') 
		{
			$actkey = (($actkey != '') ? '?actkey='.$actkey : '');
			$url_login = $url_login . $actkey;
			gwtk_header(append_url( $url_login ), $isShowRedirect);
		}
		/* */
		if (defined('THIS_SCRIPT') && (THIS_SCRIPT == basename($sys['page_admin'])))
		{
			gwtk_header(append_url( $url_login ), $isShowRedirect);
		}


		/* Translation engine */
		$L = new gwtk;
		$L->setHomeDir("locale");
		$L->setLocale($sys['locale_name']);
		$L->getCustom('admin', $sys['locale_name'], 'join');
		$L->getCustom('err', $sys['locale_name'], 'join');
		$L->getCustom('mail', $sys['locale_name'], 'join');
		/* */
		$tpl = new gwTemplate();
		$tpl->addVal( 'LANG',           $L->languagelist("0") );
		$tpl->addVal( 'TEXT_DIRECTION', $L->languagelist("1") );
		$tpl->addVal( 'CHARSET',        $L->languagelist("2") );
		$tpl->addVal( "L_securenote",   $L->m('securenote') );
		$tpl->addVal( "TITLE",          $sys['site_name']);
		$tpl->addVal( "PATH_CSS",       '.' );
		$tplConstruct = array( $sys['path_admin'] . '/' . $sys['path_tpl'] . '/login.html');
		/* not posted */
		if ($post == '')
		{
			$tpl->addVal( 'non::objLogin',  _gw_login_edit() );
		}
		else
		{
			/* check for broken fields */
			if (isset($arReq)) 
			{
				$arBroken = validatePostWalk($arPost, $arReq);
			}
			if (isset($arReq) && sizeof($arBroken) == 0)
			{
				$isPostError = 0;
			}
			else
			{
				$isPostError = 1;
				$tpl->addVal( 'non::objLogin', _gw_login_edit($arPost, 0, $arBroken) );
			}
			/* no errors, get login name */
			if (!$isPostError)
			{
				$isPostError = $isLogin = 0;
				/* Lost password */
				if (${GW_ACTION} == 'lostpass')
				{
					// default error
					$strStatus = '<p>'.$L->m('reason_20').'</p>';
					// Get user_id
					$sql = sprintf('SELECT a.id, u.user_name, u.is_active
									FROM %s as a, %s as u
									WHERE a.username = \'%s\'
									AND u.user_email = \'%s\'
									AND u.auth_id = a.id',
									TBL_AUTH, TBL_USERS,
									gw_addslashes($arPost['user_name']),
									gw_addslashes($arPost['user_email'])
						);
					$this->db->query($sql);
					while($this->db->next_record())
					{
						/* account is inactive */
						if ($this->db->r('is_active') == 0)
						{
							$isLogin = 0;
							$strStatus = $L->m('reason_21');
							break;
						}
						$isLogin = 1;
						$user_id = $this->db->r('id');
						$user_name = $this->db->r("user_name");
					}
					if ($isShowQueries)
					{
						prn_r( preg_replace("/\s{2,}/s", ' ', $sql) );
						prn_r( $this->auth );
					}
					if (!$isLogin)
					{
						$arBroken['user_name'] = true;
						$arBroken['user_email'] = true;
						$tpl->addVal( 'non::objLogin',  _gw_login_edit($arPost, 0, $arBroken) );
					}
					else
					{
						/* new password created */
						$strStatus = '<p>'.$L->m('reason_22').'</p>';
						$actkey = kMakeUid('k', $maxchar = 16, 1);
						$actkey_url = $sys['server_proto'] . $sys['server_host'] . $sys['page_login'] . '?' . 'actkey=' . $actkey;
						$actkey_url = '<a href="'.$actkey_url.'">'.$actkey_url.'</a>';
						$mail_sig = $sys['site_name'] . ' - ' . $sys['site_desc'];
						$arSrc = array('{USERNAME}', '{SITENAME}', '{U_ACTIVATE}');
						$arTrg = array($user_name, $sys['site_name'], $actkey_url);
						$msgBody = str_replace($arSrc, $arTrg, $L->m('mail_newpass'));
						$msgSubj = str_replace($arSrc, $arTrg, $L->m('mail_newpass_subj'));
						/* */
						$tpl_mail = new gwTemplate();
						$tpl_mail->addVal( 'LANG',           $L->languagelist("0") );
						$tpl_mail->addVal( 'TEXT_DIRECTION', $L->languagelist("1") );
						$tpl_mail->addVal( 'CHARSET',        $L->languagelist("2") );
						$tpl_mail->addVal( 'EMAIL_SIG',      $mail_sig );
						$tpl_mail->addVal( 'MSG_NEWPASS',    $msgBody );
						$tpl_mail->addVal( 'SUBJECT',        $msgSubj );
						$tpl_mail->setHandle(0, $sys['path_admin'] . '/' . $sys['path_tpl'] . '/mail_newpass.html');
						$tpl_mail->parse();
						$msgBody = $tpl_mail->output();
						/* */
						if (!kMailTo($arPost['user_email'], $sys['y_email'], $msgSubj, $msgBody, 0))
						{
							$strStatus .= '<p><span class="s"><b>'.sprintf($L->m("reason_17"), $arPost['user_email']).'</b></span></p>';
							/* display activation key */
							$strStatus .= '<p>' . $actkey_url . '</p>';
						}
						/* Disable account and set activation key */
						$sql = 'UPDATE %s
								SET date_reg=date_reg, date_login=date_login, is_active = "0", user_actkey = "%s"
								WHERE auth_id ="%d"';
						$sql = sprintf($sql, TBL_USERS, $actkey, $user_id);
						if($this->db->sqlExec($sql, '', 0))
						{
						page_close();
						}
						/* Set comment to disable Activation Key confirmation */
						$actkey = '';
					} // input data correct
				}
				else
				{
					/* Login */
					/* Set default error */
					$strStatus = '<p>'.$L->m('reason_20').'</p>'; 
					$sql = sprintf("SELECT a.id, a.user_id, a.perms, u.is_active
									FROM %s as a, %s as u
									WHERE STRCMP(a.username, '%s') = 0
									AND STRCMP(a.password, '%s') = 0
									AND u.auth_id = a.id",
									TBL_AUTH,
									TBL_USERS,
									gw_addslashes($arPost['user_name']),
									md5($arPost['user_pass'])
							);
					$this->db->query($sql);
					while($this->db->next_record())
					{
						/* account is inactive */
						if ($this->db->r('is_active') == 0)
						{
							$isLogin = 0;
							$strStatus = $L->m('reason_21');
							break;
						}
						$this->auth["id"]    = $this->db->r("id");
						$this->auth["uid"]   = $this->db->r("user_id");
						$this->auth["perm"]  = $this->db->r("perms");
						$this->auth["uname"] = $arPost['user_name'];
						page_close();
						$isLogin = 1;
					}
					if ($isShowQueries)
					{
						prn_r( preg_replace("/\s{2,}/s", ' ', $sql) );
						prn_r( $this->auth );
					}
					if (!$isLogin)
					{
						$arBroken['user_name'] = true;
						$arBroken['user_pass'] = true;
						$tpl->addVal( 'non::objLogin',  _gw_login_edit($arPost, 0, $arBroken) );
					}
					else
					{
						$strStatus = '';
						$sql = 'UPDATE ' . TBL_USERS .' SET date_reg = date_reg, date_login = ' . date("YmdHis");
						$this->db->query($sql);
						gwtk_header(append_url($url_admin), $isShowRedirect);
					}
				} // lostpassword | login
			} // login check
		} // posted
		/* Activation Key */
		if (preg_match("/^[0-9a-zA-Z]{16}$/", $actkey))
		{
			// 1) get login, email, name from activation key
			// 2) create password
			// 3) create mail message
			// 4) send mail
			// 5) update database
			// 6) TODO status ok | no
			// 7) redirect to login page
			$sql = 'SELECT a.id, a.username as login, u.user_email, u.user_name
					FROM %s as a, %s as u
					WHERE a.id = u.auth_id
					AND u.user_actkey = "%s"
					AND u.is_active = "0"';
			$sql = sprintf($sql, TBL_AUTH, TBL_USERS, $actkey);
			$this->db->query($sql);
			$isActkey = 0;
			while($this->db->next_record())
			{
				// create new password and login
				$password = kMakeUid('', $maxchar = 10, 1);
				$login_url = $sys['server_proto'] . $sys['server_host'] . $sys['page_login'];
				$login_url = '<a href="'.$login_url.'" onclick="window.open(this.href);return false;">'.$login_url.'</a>';
				// Prepare message
				// User name, URL, login, password, message template
				$msgBody = gwCreateMessage($this->db->r("user_name"), $login_url, $this->db->r("login"), $password, 'mail_newuser.html');
				if (!kMailTo($this->db->r("user_email"), $sys['y_email'], '', $msgBody, 0))
				{
					$strStatus .= '<p><span class="s"><b>'.sprintf($L->m("reason_17"), $this->db->r("user_email")).'</b></span></p>';
					// display login and password
					$strStatus .= '<p>'.$L->m('login'). ': <b>' . $this->db->r("login") . '</b><br /> '.
						 $L->m('password'). ': <b>' . $password . '</b></p>';
				}
				$user_id = $this->db->r("id");
				$isActkey = 1;
			}
			if (!$isActkey)
			{
				$strStatus .= '<p><span class="s"><b>' . $L->m("reason_23") . '</b></span></p>';
			}
			else
			{
				// Set new password
				$sql = 'UPDATE %s SET password = "%s" WHERE id = "%d"';
				$sql = sprintf($sql, TBL_AUTH, md5($password), $user_id);
				$this->db->query($sql);
				// Activate account
				$sql = 'UPDATE %s
						SET date_reg=date_reg, date_login=date_login, is_active = "1", user_actkey = "%s"
						WHERE auth_id = "%d"';
				$sql = sprintf($sql, TBL_USERS, '', $user_id);
				$this->db->query($sql);
				$strStatus .= '<p>' . $L->m("reason_24") . '</p>';
			}
		}
		/* Current status */
		if ($strStatus != '')
		{
			$tpl->addVal( 'non::objStatus', $strStatus);
		}
		/* Render page */
		if ( isset($tplConstruct) && is_array($tplConstruct) )
		{
			for (reset($tplConstruct); list($k, $v) = each($tplConstruct); )
			{
				$tpl->setHandle( $k , $v );
			}
		}
		$tpl->parse();
		$oHdr->add('Expires: ' . gmdate("D, d M Y H:i:s") . ' GMT');
		$oHdr->add('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
		$oHdr->add('Cache-Control: no-cache, must-revalidate');
		$oHdr->add('Pragma: no-cache');
		$oHdr->add('Content-Type: text/html; charset='.$L->languagelist('2'));
		$oHdr->output();
		print $tpl->output();
		page_close();
	}
#	else // old session request, no session in url.
#	{
#		gwtk_header(append_url($url_login), $isShowRedirect);
#	}
	/**
	 *
	 */
	function getAuthUserInfo($user_id = '', $username = '', $email = '', $password = '')
	{
		$record = array();
		$sql = '';
		if ($user_id != '') // select by Id
		{
			$sql = sprintf('SELECT a.*, u.is_active
							FROM %s as a, %s as u
							WHERE u.auth_id = a.id
							AND u.auth_id = "%d"',
							TBL_AUTH, TBL_USERS, $user_id
					);
		}
		elseif ($username != '') // select by name
		{
			$sql = sprintf('SELECT a.id
							FROM %s as a, %s as u
							WHERE u.auth_id = a.id
							AND a.username = "%s"',
							TBL_AUTH, TBL_USERS, $username
					);
		}
		elseif ($email != '') // select by name
		{
			$sql = sprintf('SELECT a.id
							FROM %s as a, %s as u
							WHERE u.auth_id = a.id
							AND u.user_email = "%s"',
							TBL_AUTH, TBL_USERS, $email
					);
		}
		elseif ($password != '') // select by password
		{
			$sql = sprintf('SELECT a.id
							FROM %s as a, %s as u
							WHERE u.auth_id = a.id
							AND STRCMP(a.password, "%s") = 0',
							TBL_AUTH, TBL_USERS, md5($password)
					);
		}
		if ($sql != '')
		{
			$this->db->query($sql);
			while($this->db->next_record())
			{
				$record = $this->db->record;
			}
		}
		return $record;
	}
	/**
	 * @param   string  $p  permissions
	 * @param   int     $d  dictionary ID
	 */
	function is_allow($p, $d = '')
	{
		$accessnames = array(
							"topic",
							"dict",
							"export",
							"term",
							"html",
							"sys",
							"user",
							"other"
						);
		for(; list($k, $v) = each($accessnames);)
		{
			if (($p == $v) && ($d != '')) // selected dictionary
			{
				global $user;
				if ((($p == 'dict')||($p == 'export')||($p == 'term')) && (PERMLEVEL > 4)) // Admin, Author
				{
					return (PERMLEVEL > 8) ? 1 : in_array($d, $user->get('dictionaries'));
				}
				elseif (($p == 'term') && (PERMLEVEL > 8)) // Admin
				{
					return 1;
				}
				elseif (($p == 'term') && (PERMLEVEL < 8) && in_array($d, $user->get('dictionaries'))) // Assistant
				{
					return 1;
				}
				else //
				{
					return 0;
				}
			}
			elseif($p == $v)
			{
				return substr($this->auth["perm"], $k, 1);
			}
		}
		return 0;
	}
	/* */
	function have_perm($name, $level)
	{
		$p = array("assistant" => 4, "author" => 8, "admin" => 16);
		for(;list($k, $v) = each($p);)
		{
			if (($k == $name) && ($level >= $p[$k]))
			{
				return 1;
			}
		}
		return 0;
	} //
	/**
	 *
	 */
	function get_accesslevel()
	{
		$accesslevels = array(
							"1" =>  "00000000",
							"2" =>  "00000000",
							"4" =>  "00110000",
							"8" =>  "11110010",
							"16" => "11111111"
						  );
		for(; list($k, $v) = each($accesslevels);)
		{
			if ($this->auth['perm'] == $v)
			{
				return $k;
			}
		}
		return 4;
	} //
}  // end of gwAuth
?>
