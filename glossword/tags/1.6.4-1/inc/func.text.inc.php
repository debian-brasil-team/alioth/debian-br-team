<?php
if (!defined('IN_GW'))
{
	die('<!-- $Id: func.text.inc.php,v 1.17 2004/11/14 09:27:13 yrtimd Exp $ -->');
}
/**
 *  Glossword - glossary compiler (http://glossword.info/dev/)
 *  © 2002-2004 Dmitry N. Shilnikov <dev at glossword.info>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
  * (see `glossword/support/license.html' for details)
 */
/* ------------------------------------------------------- */
/**
 *  Functions for an HTML-code and text operations.
 */
/* ------------------------------------------------------- */

/* Clear value for a specified key, recursive */
function array_clear_key($ar, $key_value)
{
	if (!is_array($ar)) { return $ar; }
	while (list($k, $v) = each($ar))
	{
		if (is_array($v))
		{
			$ar[$k] = array_clear_key($v, $key_value);
		}
		else if (isset($ar[$key_value]))
		{
			$ar[$k] = '';
		}
		else
		{
			$ar[$k] = $v;
		}
	}
	return $ar;
}
	
function gw_text_wildcars($t = '', $mode = 'none')
{
	if ($mode == 'sql')
	{
		if (is_array($t))
		{
			while (list($k, $v) = each($t))
			{
				$t[$k] = str_replace('*', '%', str_replace('?', '_', $v));
			}
		}
		else
		{
			$t = str_replace('*', '%', str_replace('?', '_', $t));
		}
	}
	else
	{
		if (is_array($t))
		{
			while (list($k, $v) = each($t))
			{
				$t[$k] = str_replace('*', '', str_replace('?', '', $v));
			}
		}
		else
		{
			$t = str_replace('*', '', str_replace('?', '', $t));
		}
	}
	return $t;
}


/**
 * Generates random string. Better characters' strength,
 * two groups of illegal symbols excluded.
 *
 * @param    string $first    First character for returned string
 * @param    int    $maxchar  Maximum generated string length
 * @param    int    $isU      Use uppercase characters only, added 30 Oct 2002
 * @return   string Generated text
 */
function kMakeUid($first = '', $maxchar = 8, $isU = 0)
{
	// Exclude bad symbols.
	// 1st bad group: 0, 1, l, I
	// 2nd bad group: a, c, e, o, p, x, A, C, E, H, O, K, M, P, X
	$str = "";
	$charN = '23456789bdfghijkmnqrstuvwyz';
	$charU = 'QWRYUSDFGJLZVN';
	$charN = ($isU) ? $charN . $charU : $charN;
	$len = strlen($charN);
	mt_srand( (double) microtime()*1000000);
	for ($i = 0; $i < $maxchar; $i++)
	{
		$sed = mt_rand(0, $len-1);
		$str .= $charN[$sed];
	}
	$str = $first . substr($str, 0, strlen($str) - strlen($first));
	return $str;
}

/**
 * Merges arrays and clobber any existing key/value pairs
 * kc@hireability.com (12-Oct-2000 11:08) PHP-notes
 * Don't worry, numeric keys will not be renumbered.
 * Additional check-ups added by Dmitry N. Shilnikov, 1st feb 2003
 *
 * @param   array   $a1 First array
 * @param   array   $a2 Second array
 * @return  array   Merged arrays
 */
function array_merge_clobber($a1, $a2)
{
	if (!is_array($a1) || !is_array($a2)) { return false; }
	$arNew = $a1;
	while (list($key, $val) = each($a2))
	{
		if (is_array($val) && isset($arNew[$key]) && is_array($arNew[$key]))
		{
			$arNew[$key] = array_merge_clobber($arNew[$key], $val);
		}
		else
		{
			$arNew[$key] = $val;
		}
	}
	return $arNew;
}


/**
 * Inserts value into array between keys.
 * Note: Works with the first key only if multidimensional array.
 *
 * @param   array   Source array data
 * @param   string  Key
 * @param   string  New value
 * @return  array   Result
 */
function gw_array_insert(&$ar, $k, $v)
{
	if (!is_array($ar)) { return false; }
	//
	// Two ways:
	//
	// 1. array_slice method:
	$ar = array_merge(
			array_slice($ar, 0, ($k+1)),
			array(($k+1) => $v),
			array_slice($ar, ($k+1))
	);
	// 2. Array backup with foreach method
	// # Obsolete
}

/**
 * Converts array into string,
 * splitted with defined character
 *
 * @param   array   $ar Array that needs to be converted
 * @param   string  $a2 Character for delimiter between array elements
 * @return  string  Joined array
 */
function gw_array2str($ar, $delimeter = "\n")
{
	$s = array();
	if (is_array($ar))
	{
		while (list($k, $v) = each($ar))
		{
			$s[] = $v;
		}
	}
	else
	{
		$s = explode($delimeter, $ar);
	}
	// reserved
	// ..
	return implode($delimeter, $s);
}


/**
 * Returns key from array by value
 *
 * @param   array  $ar  array with value and key
 * @param   string $str value
 * @return  string key
 */
function gw_array_value($ar, $str)
{
	if (in_array($str, $ar))
	{
		$ar = array_flip($ar);
		return $ar[$str];
	}
	return $str;
} //
/**
 * Exclude arrays. Target subtracts from Source.
 *
 * @param   array $arA Source array
 * @param   array $arB Target array
 * @return  array Result
 */
function gw_array_exclude($arA, $arB)
{
	if (empty($arB)){ return $arA; } // 15 Dec 2002
	$arC = array_diff($arA, $arB);
	$arC = array_intersect($arC, $arA);
	return $arC;
}

/**
 *
 */
function unhtmlentities($t)
{
##-----------------------------------------------
## 0.003678
#   $trans_tbl = array_flip(get_html_translation_table(HTML_ENTITIES));
#   return strtr($t, $trans_tbl);
##-----------------------------------------------
## 0.002137
	$from = array('&nbsp;', '&amp;', '&quot;', '&lt;', '&gt;', '&deg;', '&copy;', '&eth;', '&thorn;');
	$to =   array(' ',      '&',     '"',      '<',     '>',   '',     '',      '&eth;',    '&thorn;');
	return str_replace($from, $to, $t);
}
/**
 *
 */
function gw_htmlentities($t)
{
	$t = str_replace('<u>', '<span class="underline">', $t);
	$t = str_replace('</u>', '</span>', $t);
	$t = str_replace('<center>', '<div style="text-align:center">', $t);
	$t = str_replace('</center>', '</div>', $t);
	$t = str_replace('<br>', '<br />', $t);
	$arSrc = array(' & '    );
	$arTrg = array(' &amp; ');
	return str_replace($arSrc, $arTrg, $t);
}
/**
 * Splits the text into keywords.
 *
 * @param   string  $t String that needs to be splitted, multibyte
 * @param   int     $min Minimum length for one keyword
 * @param   int     $max Maximum length for one keyword
 * @return  array   Array with keywords
 */
function text2keywords($t, $min = 1, $max = 25, $enc = 'UTF-8')
{
	global $oFunc;
	if ($min == 0){ return array(); }
	$t = $t.' ';
	$pos = $cntLetter = 0;
	$len = strlen($t);
	$strD = '';
	$ar = array();
	$enc = strtoupper($enc);
	while ($pos < $len)
	{
		$charAt = substr($t, $pos, 1);
		$asciiPos = ord($charAt);
		if (($asciiPos >= 240) && ($asciiPos <= 255) && ($enc == 'UTF-8'))
		{
			$char2 = substr($t, $pos, 4);
			$strD .= $char2;
			$cntLetter++;
			$pos += 4;
		}
		else if (($asciiPos >= 224) && ($asciiPos <= 239)  && ($enc == 'UTF-8'))
		{
			$char2 = substr($t, $pos, 3);
			$strD .= $char2;
			$cntLetter++;
			$pos += 3;
		}
		else if (($asciiPos >= 192) && ($asciiPos <= 223)  && ($enc == 'UTF-8'))
		{
			$char2 = substr($t, $pos, 2);
			$strD .= $char2;
			$cntLetter++;
			$pos += 2;
		}
		else
		{
			$strD .= $charAt;
			if ($charAt == ' ')
			{
				// can't drop characters , -, + etc. from UTF-8
				// Warning: UTF-8 and trim($string) may occur problems
				$strD = trim($strD);
				$mb_len = $oFunc->mb_strlen($strD, $enc);
				// 'words' with length <= $min or > $max characters are removed.
				if (($mb_len >= $min)&&($mb_len < $max))
				{
					// unique keywords only
					// $ar[md5($strD)] = $strD;
					$ar[] = $strD;
				}
				$strD = '';
			}
			$cntLetter++;
			$pos++;
		}
	}
	// unique keywords only
	$ar = array_values(array_unique($ar));
	return $ar;
}
/**
 * Prepares text for a TERM UNIQUE field
 */
function text2term_uniq($t, $isStrip = 1)
{
	// add a space to HTML-tags
	$t = str_replace('<i>', '', $t);
	$t = str_replace('<b>', '', $t);
	$t = str_replace('</i>', '', $t);
	$t = str_replace('</b>', '', $t);
	$t = str_replace('><', '> <', $t);
	// removes {TEMPLATES}
	$t = preg_replace("/\{([A-Za-z0-9:\-_]+)\}/", '', $t);
	// remove HTML-tags
	$t = strip_tags($t);
	// remove new lines and tabs
	$t = preg_replace("/(\r\n|\n|\t|\r)/", ' ', $t);
	// remove HTML-entitles
	$t = preg_replace('/&#[x0-9a-fA-F]+;/', ' ', $t); // remove hex values
	$t = preg_replace('/&[a-z]+;/', ' ', $t);  // remove others
	// do uppercase AND normalize uppercased letters
	$t = gw_mb_strtonorm($t);
	//
	$ReplaceMap = array(
		// Chinese high specials
		'！'=>' ','＂'=>' ','＃'=>' ','＄'=>' ','％'=>' ','＆'=>' ','＇'=>' ','（'=>' ','）'=>' ','＊'=>' ','＋'=>' ','，'=>' ','－'=>' ','．'=>' ','／'=>' ','０'=>'0','１'=>'1','２'=>'2','３'=>'3','４'=>'4','５'=>'5','６'=>'6','７'=>'7','８'=>'8','９'=>'9','：'=>' ','；'=>' ','＜'=>' ','＝'=>' ','＞'=>' ','？'=>' ','＠'=>' ','Ａ'=>'A','Ｂ'=>'B','Ｃ'=>'C','Ｄ'=>'D','Ｅ'=>'E','Ｆ'=>'F','Ｇ'=>'G','Ｈ'=>'H','Ｉ'=>'I','Ｊ'=>'J','Ｋ'=>'K','Ｌ'=>'L','Ｍ'=>'M','Ｎ'=>'N','Ｏ'=>'O','Ｐ'=>'P','Ｑ'=>'Q','Ｒ'=>'R','Ｓ'=>'S','Ｔ'=>'T','Ｕ'=>'U','Ｖ'=>'V','Ｗ'=>'W','Ｘ'=>'X','Ｙ'=>'Y','Ｚ'=>'Z','［'=>' ','＼'=>' ','］'=>' ','＾'=>' ','＿'=>' ','｀'=>' ','ａ'=>'a','ｂ'=>'b','ｃ'=>'c','ｄ'=>'d','ｅ'=>'e','ｆ'=>'f','ｇ'=>'g','ｈ'=>'h','ｉ'=>'i','ｊ'=>'j','ｋ'=>'k','ｌ'=>'l','ｍ'=>'m','ｎ'=>'n','ｏ'=>'o','ｐ'=>'p','ｑ'=>'q','ｒ'=>'r','ｓ'=>'s','ｔ'=>'t','ｕ'=>'u','ｖ'=>'v','ｗ'=>'w','ｘ'=>'x','ｙ'=>'y','ｚ'=>'z','｛'=>' ','｜'=>' ','｝'=>' ',
		// Special umlauts
		'̀'=>'','́'=>'','̂'=>'','̃'=>'','̄'=>'','̅'=>'','̆'=>'','̇'=>'','̈'=>'','̉'=>'','̊'=>'','̋'=>'','̌'=>'','̍'=>'','̎'=>'','̏'=>'','̐'=>'','̑'=>'','̒'=>'','̓'=>'','̔'=>'','̕'=>'','̖'=>'','̗'=>'','̘'=>'','̙'=>'','̚'=>'','̛'=>'','̜'=>'','̝'=>'','̞'=>'','̟'=>'','̠'=>'','̡'=>'','̢'=>'','̣'=>'','̤'=>'','̥'=>'','̦'=>'','̧'=>'','̨'=>'','̩'=>'','̪'=>'','̫'=>'','̬'=>'','̭'=>'','̮'=>'',
		// Special characters, but should be always removed
		'.'=>' ', '--'=>' '
	);
	if ($isStrip)
	{
		$ReplaceMap = array_merge($ReplaceMap, array(
			'　'=>' ','、'=>' ','。'=>' ','〃'=>' ','〄'=>' ','々'=>' ','〆'=>' ','〇'=>' ','〈'=>' ','〉'=>' ','《'=>' ','》'=>' ','「'=>' ','」'=>' ','『'=>' ','』'=>' ','【'=>' ','】'=>' ','〒'=>' ','〓'=>' ','〔'=>' ','〕'=>' ','〖'=>' ','〗'=>' ','〘'=>' ','〙'=>' ','〚'=>' ','〛'=>' ','〜'=>' ','〝'=>' ','〞'=>' ',
			'!'=>' ','"'=>' ','#'=>' ','$'=>' ','%'=>' ','&'=>' ','\''=>' ','('=>' ',')'=>' ','+'=>' ',','=>' ','-'=>'','/'=>' ',':'=>' ',';'=>' ','<'=>' ','='=>' ','>'=>' ','@'=>' ','['=>' ','\\'=>' ',']'=>' ','^'=>' ','_'=>'','`'=>' ','{'=>' ','|'=>' ','}'=>' ','~'=>' ',''=>' ','€'=>' ',''=>' ','‚'=>' ','ƒ'=>' ','„'=>' ','…'=>' ','†'=>' ','‡'=>' ','ˆ'=>' ','‰'=>' ','Š'=>' ','‹'=>' ','Œ'=>' ',''=>' ','Ž'=>' ',''=>' ',''=>' ','‘'=>' ','’'=>' ','“'=>' ','”'=>' ','•'=>' ','–'=>' ','—'=>' ','˜'=>' ','™'=>' ','š'=>' ','›'=>' ','œ'=>' ',''=>' ','ž'=>' ','Ÿ'=>' ',' '=>' ','¡'=>' ','¢'=>' ','£'=>' ','¤'=>' ','¥'=>' ','¦'=>' ','§'=>' ','¨'=>' ','©'=>' ','ª'=>' ','«'=>' ','¬'=>' ','­'=>' ','®'=>' ','¯'=>' ','°'=>' ','±'=>' ','²'=>' ','³'=>' ','´'=>' ','µ'=>' ','¶'=>' ','·'=>' ','¸'=>' ','¹'=>' ','º'=>' ','»'=>' '
		));
	}
	$t = str_replace(array_keys($ReplaceMap), array_values($ReplaceMap), $t); // 2nd
	//
	$t = trim($t);
#    print "<br>uniq: $isStrip " . $t;
	//
	return $t;
}
/**
 *
 */
function htmlBlockSmall($title="no title", $content="no content", $classT = "t", $classN = 0, $alignT = "left", $alignN = "left")
{
	global $sys;
	
	$classT = ($classT != "0") ? ( ' class="'.$classT.'"' ) : ( ' class="t"' );
	$classN = ($classN != "0") ? ( ' class="'.$classN.'"' ) : false;
	$alignT = ($alignT != "0") ? ( 'text-align:'.$alignT ) : 'text-align:'.$sys['css_align_left'];
	// 12 december 2002, rtl
	$alignN = ($alignN == 'center') ? $alignN : (($alignN == 'left') ? $sys['css_align_left'] : 'right');
	$str = '';
	$tmp['style_cont_str'] = '';
	if ($alignN != '')
	{
		$tmp['style_cont'] = array('text-align:'.$alignN);
		$tmp['style_cont_str'] = ' style="'.implode(';', $tmp['style_cont']).'"';
	}
	// 25 june 2003
	$tmp['tpl_srch_name'] = 'tpl_block_small.html';
	$oTpl = new gwTemplate();
	$oTpl->unknownTags = 'keep';
	$oTpl->unknownTagsT = 'keep';
	$oTpl->setHandle( 0 , $GLOBALS['sys']['path_tpl']."/".$GLOBALS['sys']['path_theme'].'/'.$tmp['tpl_srch_name'] );

	if (isset($sys['path_www_images']))
	{
		$oTpl->addVal( 'PATH_IMG_WWW', $sys['dirname'] . '/'. $sys['path_www_images'] );
	}
	$oTpl->addVal( 'PATH_IMG', $sys['server_dir'] . '/'. $sys['path_img'] );

	$oTpl->addVal( 'S_HEAD_CLASS', $classT);
	$oTpl->addVal( 'S_HEAD_ALIGN', $alignT);

	$oTpl->addVal( 'S_CONT_CLASS', $classN);
	$oTpl->addVal( 'S_CONT_STYLE', $tmp['style_cont_str']);
	$oTpl->addVal( 'S_CONT', $content);
	$oTpl->addVal( 'S_HEAD', $title);

	$oTpl->parse();
	return $oTpl->output();
}
/**
 * Returns date string in defined dateformat
 *
 * @param  string   $d date in timestamp(14) format
 * @param  string   $ftm date format
 * @return string   date
 */
function dateExtract($d, $fmt = "%d %M %Y %H:%i:%s")
{
	$monthsL = explode(' ', ' ' . $GLOBALS['L']->m('array_month_decl'));
	$monthsS = explode(' ', ' ' . $GLOBALS['L']->m('array_month_short'));
	if ((sizeof($monthsL) < 12) || (sizeof($monthsS) < 12))
	{
		return '';
	}
	/**
	 * %d - day of the month, 2 digits with leading zeros; i.e. "01" to "31"
	 * %m - month; i.e. "01" to "12"
	 * %FL - month, textual, long, lowercase; i.e. "january"
	 * %F - month, textual, long; i.e. "January"
	 * %ML - month, textual, 3 letters, lowercase; i.e. "jan"
	 * %M - month, textual, 3 letters; i.e. "Jan"
	 * %Y - year, 4 digits; i.e. "1999"
	 * %H - hour, 24-hour format; i.e. "00..23"
	 * %s - seconds; i.e. "00..59"
	 */
	$fmt = str_replace( "%d",  (substr($d,6,2)/1), $fmt ); // removes leading 0 from date
	$fmt = str_replace( "%m",  substr($d,4,2), $fmt );
	$fmt = str_replace( "%FL", str_replace('_', ' ', gw_mb_strtolower($monthsL[(substr($d,4,2)/1)])), $fmt );
	$fmt = str_replace( "%F",  str_replace('_', ' ', $monthsL[(substr($d,4,2)/1)]), $fmt );
	$fmt = str_replace( "%ML", str_replace('_', ' ', gw_mb_strtolower($monthsS[(substr($d,4,2)/1)])), $fmt );
	$fmt = str_replace( "%M",  str_replace('_', ' ', $monthsS[(substr($d,4,2)/1)]), $fmt );
	$fmt = str_replace( "%Y",  substr($d,0,4), $fmt );
	$fmt = str_replace( "%H",  substr($d,8,2), $fmt );
	$fmt = str_replace( "%i",  substr($d,10,2), $fmt );
	$fmt = str_replace( "%s",  substr($d,12,2), $fmt );
	return $fmt;
}
/**
 * Converts & into &amp; and encrypts url parameters
 *
 * @param   string  $url url with parameters
 * @param   array   $vars not in use
 * @return  string  converted and encrypted url parameters
 */
function append_url($url, $vars = array())
{
	global $sess, $d, $arDictParam;
	/* removes &amp; to avoid any problems with & */
	$url = str_replace("&amp;", "&", $url);
	$file = $param = '';
	if( preg_match("/\?/", $url) && preg_match("/a=term/", $url) && !preg_match("/q=/", $url) ) // encode link to a term
	{
		list($file, $param) = split("\?", $url);
		if (isset($arDictParam['is_leech']) && ($arDictParam['is_leech'] == 1))
		{
			$url = $file . '?' . url_encrypt($GLOBALS['sys']['hideurl'], $param);
		}
	}
	if (isset($sess->id))
	{
		$url = str_replace(GW_SID.'='.$sess->id, '', $url);
		$url = $sess->url($url);
	}
	$url = str_replace("&", "&amp;", $url);
	return $url;
}
## --------------------------------------------------------
## HTML-library
## (C) 2000-2003 Dmitry Shilnikov
// Depreciated function
function htmlTagsA($url, $text, $underline=false, $target=false, $title=false, $add=false, $id=false, $class=false, $extras = '')
{
	// Do normalize URL in a good manner.
	if( preg_match("/\?/", $url))
	{
		list($file, $param) = split("\?", $url);
		$ar = split("&", $param);
		sort($ar);
		$url = $file .'?'. implode('&', $ar);
	}
	return sprintf("<a href=\"%s\"%s%s%s%s%s>%s</a>",
		append_url($url),
		($target ? ' target="'.$target.'"' : ''),
		($id ? ' id="'.$id.'"' : ''),
		($class ? ' class="'.$class.'"' : ''),
		($title ? ' title="'.$title.'"' : ''),
		($extras ? ' '.$extras : ''),
		($underline ? '<span style="text-decoration:underline">'.$text.'</span>' : $text )
	);
}
function htmlFormsSelect($arData, $default, $formname = "select", $class = "input", $style = "", $dir='ltr')
{
	$class = ($class != '') ? ( ' class="'.$class . '"') : ( '' );
	$style = ($style != '') ? ( ' style="'.$style . '"') : ( '' );
	$dir = ($dir != '') ? ( ' dir="'.$dir . '"') : ( '' );
	$formname = ($formname != '') ? ( ' name="'.$formname . '"') : ( '' );
	$str = '<select' . $formname . $class . $style . $dir. '>';
	for (reset($arData); list($k, $v) = each($arData);)
	{
		if ($k == $default) { $s = ' selected="selected"'; } else { $s= ""; }
		$str .= "\n\t".'<option value="' . $k . '"' . $s . ">";
		if(preg_match("/abbrlang/", $formname) || preg_match("/trnslang/", $formname)) { $str .= $k; }
		else { $str .= $v; }
		$str .= "</option>";
	 }
	$str .= '</select>';
	return $str;
}
## HTML-library
## --------------------------------------------------------
/**
 * Outputs nice help window.
 */
function kTbHelp($title, $content, $w = "100%")
{
	$str = "";
	$str .= '<table cellspacing="1" style="border:1px solid '.$GLOBALS['theme']['color_4'].'" cellpadding="3" border="0" width="' . $w . '">';
	$str .= '<tr valign="top" class="f"><td style="background:'.$GLOBALS['theme']['color_3'].'" class="r">'. $title .'</td></tr>';
	$str .= '<tr valign="top"><td style="background:'.$GLOBALS['theme']['color_2'].'" class="t">'. $content . "</td></tr>";
	$str .= '</table>';
	return $str;
}

/**
 * @param   array  $ar  keywords
 */
function searchkeys($ar)
{
	$k = implode(",", $ar);
	$k = preg_replace("/(\r\n|\r|\n)/", "", $k);
	$k = str_replace(", ",",", $k);
	$wordsA = explode(",", $k);
	for (reset($wordsA); list ($k, $v)= each($wordsA);)
	{
		$v = trim($v);
		$wordsA[$k] = $v;
		if ($v == '') { unset($wordsA[$k]); }
	}
	$wordsA = gwShuffle(50, $wordsA);
	$str = implode(", ", $wordsA);
	return $str;
}
/**
 * Optimizes HTML-code
 */
function gw_text_smooth($t, $is_debug = 0)
{
	/* skip optimization when debug mode */
	if ($is_debug) 
	{
		return $t;
	}
	$t = preg_replace("/(\r\n|\n|\t|\r)/", ' ', $t);
	$t = preg_replace("/ {2,}/" , " ", $t);
	$t = preg_replace("/<br>/i" , "<br/>", $t);
	$t = str_replace('<center>' , '', $t);
	$t = str_replace('</center>' , '', $t);
	$t = str_replace(LF, "\n", $t);
	$t = preg_replace("/<script(.*?)>(.*?)<\/script>/si", "<script\\1>\\2</script>", $t);
	$t = preg_replace("/<!--(.*?)-->/si", '',$t);
#	$t = preg_replace("/\b(--)\b/", '&#8212;',$t);
	$t = preg_replace("/([, ])+([-]{2})([ \w+])/", '\\1&#8212;\\3',$t);
	$t = str_replace(' &#8212;', '&#160;&#8212;', $t);
	$t = preg_replace("/<script(.*?)>(.*?)<\/script>/si", "<script\\1>".CRLF."\\2"."</script>", $t);
	$t = str_replace('//]]></script>', CRLF . '//]]></script>', $t);
	return $t;
}
/**
 * Filter for HTML-code of a definition text
 */
function gw_text_smooth_defn($t, $is_debug = 0)
{
	/* skip optimization when debug mode */
	if ($is_debug) 
	{
		return $t;
	}
	/* preformatted text */
	/* (.*[^>]) */
	if (preg_match_all("/<pre(.*?)>(.*?)<\/pre>/s", $t, $pre))
	{
		for (; list ($k, $v)= each($pre[2]);)
		{
			$pre[2][$k] = str_replace("\t", "&#160;&#160;&#160;", $pre[2][$k]);
			$pre[2][$k] = str_replace("  ", "&#160;&#160;", $pre[2][$k]);
			$pre[2][$k] = str_replace(CRLF, "<br />", $pre[2][$k]);
			$pre[2][$k] = str_replace("\n", "<br />", $pre[2][$k]);
			$t = str_replace($pre[0][$k], '<tt'.$pre[1][$k].'>'.$pre[2][$k].'</tt>', $t);
			$t = preg_replace("/<tt><br \/>/", '<tt' . "\\1" . ' class="pre">', $t);
		}
	}
	return $t;
}

/**
 * Depreciated function name
 */
function textcodetoform($t)
{
	if (is_string($t))
	{
		$t = str_replace("&#228;", chr("228"), $t);
		$t = htmlspecialchars($t);
	}
	return $t;
}
/**
 * Extended Uppercase function.
 * Normaizes already uppercased string
 */
function gw_mb_strtonorm($t)
{
	$t = gw_mb_strtoupper(gw_mb_strtoupper($t, 'lc', 'uc'), 'uc', 'nn');
#	$t = gw_mb_strtoupper($t, 'lc', 'uc');
#	$t = gw_mb_strtoupper($t, 'uc', 'nn');
	return $t;
}
/**
 * Extended Lowercase function
 */
function gw_mb_strtolower($t)
{
	global $sys;
	if (function_exists('mb_strtolower'))
	{
		return mb_strtolower($t, $sys['internal_encoding']);
	}
	return gw_mb_strtoupper($t, 'uc', 'lc');
}
/**
 * Extended Uppercase function
 */
function gw_mb_strtoupper($t, $src = 'lc', $trg = 'uc')
{
	global $sys;
	if (function_exists('mb_strtoupper') && $src == 'lc' && $trg == 'uc')
	{
		return mb_strtoupper($t, $sys['internal_encoding']);
	}
	$arLocales = array('ru-utf8', 'en-utf8', 'nn-utf8', 'el-utf8', 'lv-utf8', 'xcyr-utf8', 'vi-utf8', 'am-utf8');
	${$src} = gw_get_utf8_casemap($src);
	${$trg} = gw_get_utf8_casemap($trg);
	for(; list($k, $v) = each($arLocales);)
	{
		if (isset(${$src}[$v]) && isset(${$trg}[$v]))
		{
			$t = str_replace(${$src}[$v], ${$trg}[$v], $t);
		}
	}
	return $t;
}
function gw_get_utf8_casemap($map = 'lc')
{
		/* 1 Latin */
		$nn['en-utf8'] = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
		$uc['en-utf8'] = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
		$lc['en-utf8'] = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
		/* 2 Cyrillic: Russian, Mongolian */
		$nn['ru-utf8'] = array('А', 'Б', 'В', 'Г', 'Д', 'Е', 'Е', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я');
		$uc['ru-utf8'] = array('А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я');
		$lc['ru-utf8'] = array('а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я');
		/* 3 Basic diacritical signs: covers the most of European languages */
		$nn['nn-utf8'] = array('A', 'A', 'A', 'A', 'A', 'A', 'A', 'Æ', 'C', 'E', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'I', 'Ñ', 'O', 'O', 'O', 'O', 'O', 'O', 'Ø', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'Y', 'Ү', 'Ө');
		$uc['nn-utf8'] = array('À', 'Á', 'Â', 'Ǎ', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ě', 'Ë', 'Ì', 'Í', 'Î', 'Ǐ', 'Ï', 'Ñ', 'Ò', 'Ó', 'Ô', 'Ǒ', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ǔ', 'Ü', 'Ǖ', 'Ǘ', 'Ǚ', 'Ǜ', 'Ý', 'Ү', 'Ө');
		$lc['nn-utf8'] = array('à', 'á', 'â', 'ǎ', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ě', 'ë', 'ì', 'í', 'î', 'ǐ', 'ï', 'ñ', 'ò', 'ó', 'ô', 'ǒ', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ǔ', 'ü', 'ǖ', 'ǘ', 'ǚ', 'ǜ', 'ý', 'ү', 'ө');
		/* 4 Cyrillic Extended: Tatar, Kalmyk */
		$nn['xcyr-utf8'] = array('Г', 'Є', 'Ѕ', 'I', 'I', 'J', 'К', 'У', 'Џ', 'Г', 'Љ', 'Њ', 'ћ', 'Ђ', 'Ҋ', 'Ҍ', 'Ҏ', 'Ґ', 'Ғ', 'Ҕ', 'Җ', 'Ҙ', 'Қ', 'Ҝ', 'Ҟ', 'Ҡ', 'Ң', 'Ҥ', 'Ҧ', 'Ҩ', 'Ҫ', 'Ҭ', 'Ү', 'Ұ', 'Ҳ', 'Ҵ', 'Ҷ', 'Ҹ', 'Һ', 'Ҽ', 'Ҿ', 'Ӂ', 'Ӄ', 'Ӆ', 'Ӈ', 'Ӊ', 'Ӌ', 'Ӎ');
		$uc['xcyr-utf8'] = array('Ѓ', 'Є', 'Ѕ', 'І', 'Ї', 'Ј', 'Ќ', 'Ў', 'Џ', 'Ґ', 'Љ', 'Њ', 'ћ', 'Ђ', 'Ҋ', 'Ҍ', 'Ҏ', 'Ґ', 'Ғ', 'Ҕ', 'Җ', 'Ҙ', 'Қ', 'Ҝ', 'Ҟ', 'Ҡ', 'Ң', 'Ҥ', 'Ҧ', 'Ҩ', 'Ҫ', 'Ҭ', 'Ү', 'Ұ', 'Ҳ', 'Ҵ', 'Ҷ', 'Ҹ', 'Һ', 'Ҽ', 'Ҿ', 'Ӂ', 'Ӄ', 'Ӆ', 'Ӈ', 'Ӊ', 'Ӌ', 'Ӎ');
		$lc['xcyr-utf8'] = array('ѓ', 'є', 'ѕ', 'і', 'ї', 'ј', 'ќ', 'ў', 'џ', 'ґ', 'љ', 'њ', 'Ћ', 'ђ', 'ҋ', 'ҍ', 'ҏ', 'ґ', 'ғ', 'ҕ', 'җ', 'ҙ', 'қ', 'ҝ', 'ҟ', 'ҡ', 'ң', 'ҥ', 'ҧ', 'ҩ', 'ҫ', 'ҭ', 'ү', 'ұ', 'ҳ', 'ҵ', 'ҷ', 'ҹ', 'һ', 'ҽ', 'ҿ', 'ӂ', 'ӄ', 'ӆ', 'ӈ', 'ӊ', 'ӌ', 'ӎ');
		/* 5 Modern Greek */
		$nn['el-utf8'] = array('Α', 'Ε', 'Ι', 'Ι', 'Ο', 'Η', 'Υ', 'Υ', 'Ω', 'Α', 'Β', 'Γ', 'Δ', 'Ε', 'Ζ', 'Η', 'Θ', 'Ι', 'Κ', 'Λ', 'Μ', 'Ν', 'Ξ', 'Ο', 'Π', 'Ρ', 'Σ', 'Σ', 'Τ', 'Υ', 'Φ', 'Χ', 'Ψ', 'Ω','Ϙ','Ϛ','Ϝ','Ϟ','Ϡ','Ϣ','Ϥ','Ϧ','Ϩ','Ϫ','Ϭ','Ϯ');
		$uc['el-utf8'] = array('Ά', 'Έ', 'Ί', 'Ϊ', 'Ό', 'Ή', 'Ύ', 'Ϋ', 'Ώ', 'Α', 'Β', 'Γ', 'Δ', 'Ε', 'Ζ', 'Η', 'Θ', 'Ι', 'Κ', 'Λ', 'Μ', 'Ν', 'Ξ', 'Ο', 'Π', 'Ρ', 'Σ', 'Σ', 'Τ', 'Υ', 'Φ', 'Χ', 'Ψ', 'Ω','Ϙ','Ϛ','Ϝ','Ϟ','Ϡ','Ϣ','Ϥ','Ϧ','Ϩ','Ϫ','Ϭ','Ϯ');
		$lc['el-utf8'] = array('ά', 'έ', 'ί', 'ϊ', 'ό', 'ή', 'ύ', 'ϋ', 'ώ', 'α', 'β', 'γ', 'δ', 'ε', 'ζ', 'η', 'θ', 'ι', 'κ', 'λ', 'μ', 'ν', 'ξ', 'ο', 'π', 'ρ', 'σ', 'ς', 'τ', 'υ', 'φ', 'χ', 'ψ', 'ω','ϙ','ϛ','ϝ','ϟ','ϡ','ϣ','ϥ','ϧ','ϩ','ϫ','ϭ','ϯ');
		/* 6 Latvian, Czech */
		$nn['lv-utf8'] = array('A', 'A', 'A', 'C', 'C', 'C', 'C', 'D', 'D', 'E', 'E', 'E', 'E', 'E', 'G', 'G', 'G', 'G', 'G', 'H', 'H', 'I', 'I', 'I', 'I', 'I', 'I', 'Ĳ', 'J', 'K', 'L', 'L', 'L', 'L', 'L', 'N', 'N', 'N', 'Ŋ', 'O', 'O', 'O', 'Œ', 'R', 'R', 'R', 'S', 'S', 'S', 'S', 'S', 'T', 'T', 'T', 'U', 'U', 'U', 'U', 'U', 'U', 'W', 'Y', 'Y', 'Z', 'Z', 'Z');
		$uc['lv-utf8'] = array('Ā', 'Ă', 'Ą', 'Ć', 'Ĉ', 'Ċ', 'Č', 'Ď', 'Đ', 'Ē', 'Ĕ', 'Ė', 'Ę', 'Ě', 'Ĝ', 'Ğ', 'Ǧ', 'Ġ', 'Ģ', 'Ĥ', 'Ħ', 'Ĩ', 'Ī', 'Ĭ', 'Į', 'İ', 'I', 'Ĳ', 'Ĵ', 'Ķ', 'Ĺ', 'Ļ', 'Ľ', 'Ŀ', 'Ł', 'Ń', 'Ņ', 'Ň', 'Ŋ', 'Ō', 'Ŏ', 'Ő', 'Œ', 'Ŕ', 'Ŗ', 'Ř', 'Ś', 'Ŝ', 'Ş', 'Ș', 'Š', 'Ţ', 'Ť', 'Ŧ', 'Ũ', 'Ū', 'Ŭ', 'Ů', 'Ű', 'Ų', 'Ŵ', 'Ŷ', 'Ÿ', 'Ź', 'Ż', 'Ž');
		$lc['lv-utf8'] = array('ā', 'ă', 'ą', 'ć', 'ĉ', 'ċ', 'č', 'ď', 'đ', 'ē', 'ĕ', 'ė', 'ę', 'ě', 'ĝ', 'ğ', 'ǧ', 'ġ', 'ģ', 'ĥ', 'ħ', 'ĩ', 'ī', 'ĭ', 'į', 'i', 'ı', 'ĳ', 'ĵ', 'ķ', 'ĺ', 'ļ', 'ľ', 'ŀ', 'ł', 'ń', 'ņ', 'ň', 'ŋ', 'ō', 'ŏ', 'ő', 'œ', 'ŕ', 'ŗ', 'ř', 'ś', 'ŝ', 'ş', 'ș', 'š', 'ţ', 'ť', 'ŧ', 'ũ', 'ū', 'ŭ', 'ů', 'ű', 'ų', 'ŵ', 'ŷ', 'ÿ', 'ź', 'ż', 'ž');
		/* 7 Vietnamese */
		$nn['vi-utf8'] = array('A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'I', 'I', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'Y', 'Y', 'Y', 'Y', 'W', 'W', 'W');
		$uc['vi-utf8'] = array('Ạ', 'Ả', 'Ấ', 'Ầ', 'Ẩ', 'Ẫ', 'Ậ', 'Ắ', 'Ằ', 'Ẳ', 'Ẵ', 'Ặ', 'Ẹ', 'Ẻ', 'Ẽ', 'Ế', 'Ề', 'Ể', 'Ễ', 'Ệ', 'Ỉ', 'Ị', 'Ọ', 'Ỏ', 'Ố', 'Ồ', 'Ổ', 'Ỗ', 'Ộ', 'Ớ', 'Ờ', 'Ở', 'Ỡ', 'Ợ', 'Ụ', 'Ủ', 'Ứ', 'Ừ', 'Ử', 'Ư', 'Ữ', 'Ự', 'Ỳ', 'Ỵ', 'Ỷ', 'Ỹ', 'Ẁ', 'Ẃ', 'Ẅ');
		$lc['vi-utf8'] = array('ạ', 'ả', 'ấ', 'ầ', 'ẩ', 'ẫ', 'ậ', 'ắ', 'ằ', 'ẳ', 'ẵ', 'ặ', 'ẹ', 'ẻ', 'ẽ', 'ế', 'ề', 'ể', 'ễ', 'ệ', 'ỉ', 'ị', 'ọ', 'ỏ', 'ố', 'ồ', 'ổ', 'ỗ', 'ộ', 'ớ', 'ờ', 'ở', 'ỡ', 'ợ', 'ụ', 'ủ', 'ứ', 'ừ', 'ử', 'ư', 'ữ', 'ự', 'ỳ', 'ỵ', 'ỷ', 'ỹ', 'ẁ', 'ẃ', 'ẅ');
		/* 8 Armenian */
		$nn['am-utf8'] = array('Ա', 'Բ', 'Գ', 'Դ', 'Ե', 'Զ', 'Է', 'Ը', 'Թ', 'Ժ', 'Ի', 'Լ', 'Խ', 'Ծ', 'Կ', 'Հ', 'Ձ', 'Ղ', 'Ճ', 'Մ', 'Յ', 'Ն', 'Շ', 'Ո', 'Չ', 'Պ', 'Ջ', 'Ռ', 'Ս', 'Վ', 'Տ', 'Ր', 'Ց', 'Ւ', 'Փ', 'Ք', 'Օ', 'Ֆ', '');
		$uc['am-utf8'] = array('Ա', 'Բ', 'Գ', 'Դ', 'Ե', 'Զ', 'Է', 'Ը', 'Թ', 'Ժ', 'Ի', 'Լ', 'Խ', 'Ծ', 'Կ', 'Հ', 'Ձ', 'Ղ', 'Ճ', 'Մ', 'Յ', 'Ն', 'Շ', 'Ո', 'Չ', 'Պ', 'Ջ', 'Ռ', 'Ս', 'Վ', 'Տ', 'Ր', 'Ց', 'Ւ', 'Փ', 'Ք', 'Օ', 'Ֆ', '՚');
		$lc['am-utf8'] = array('ա', 'բ', 'գ', 'դ', 'ե', 'զ', 'է', 'ը', 'թ', 'ժ', 'ի', 'լ', 'խ', 'ծ', 'կ', 'հ', 'ձ', 'ղ', 'ճ', 'մ', 'յ', 'ն', 'շ', 'ո', 'չ', 'պ', 'ջ', 'ռ', 'ս', 'վ', 'տ', 'ր', 'ց', 'ւ', 'փ', 'ք', 'օ', 'ֆ', '');
		/* 9 mail to <dev at glossword.info> if you need more language profiles */
	if (isset(${$map}))
	{
		return ${$map};
	}
	return $lc;
}


/**
 * Depreciated function name
 * Validates HTML-form
 */
function validatePostWalk($a, $reqFieldsA = array())
{
	$brokenFieldsA = array();
	for (reset($a); list($k1, $v1) = each($a);) // read posted array, usually HTTP_POST_VARS
	{
		for (reset($reqFieldsA); list($reqk1, $reqv1) = each($reqFieldsA );) // read required
		{
			if ($k1 == $reqv1) // posted == required
			{
				if (!is_array($v1))
				{
					$v1 = gw_text_sql($v1);
					// url check
					if ($reqv1 == 'url') { if (str_replace("http://","",$v1) == ''){ $v1 = ''; } }
					if ($v1 == ''){ $brokenFieldsA[$k1] = ''; }
				}
				else
				{
					for (reset($v1); list ($k2, $v2)= each($v1);)
					{
						$v1[$k2] = gw_text_sql($v2);
						if ($v1[$k2] == ''){ $brokenFieldsA[$k1] = ''; }
					}
				}
			} //
		} //
	} //
	return $brokenFieldsA;
}

//
## --------------------------------------------------------
## HTML-library
## (C) 1999 Dmitry Shilnikov
/**
* Universal function for date selection.
*
* @param    string  $name  field name
* @param    string  $val  timestamp
* @return   string  HTML-code
*/
function htmlFormSelectDate($name, $val)
{
	global $arLs;
	// month names
	$arLs['0'] = "---";
	// fields width [ year | month | day | (time) ]
	$cfgWidth = array("33%", "34%", "33%");
	// keep zero values?
	$cfgIsNulls = 0;
	// keep seconds?
	$cfgIsSec = 1;
	$str = "";
	if (preg_match( "/([0-9]{4})([0-9]{2})([0-9]{2})([0-9]{6})/", $val, $rowDate ))
	{
		// variable       -> variableD
		// variable[]     -> variableD[]
		// variable[name] -> variable[nameD]
		if (preg_match("/(.*?)\[(.*?)\]$/", $name, $rowName))
		{
			if ($rowName['2'] != "") { $rowName['1'] = $rowName['1'] . '[' . $rowName['2']; $rowName['2'] = ']'; }
			else { $rowName['2'] = "[]"; }
		}
		else
		{
			$rowName['1'] = $name; $rowName['2'] = "";
		}
		if($cfgIsSec){ $cfgWidth = array("25%", "25%", "25%", "25%"); }
		// Year select #-------------------------------------------------
		$strA[0] = '<select name="' . $rowName['1'] . 'Y' . $rowName['2'] . '" class="input">';
		// for 0000
		if ($rowDate[1] == "0000" ) { $strA[0] .= '<option value="0000" selected="selected">0000</option>'; }
		else { if($cfgIsNulls) { $strA[0] .= '<option value="0000">0000</option>'; } }
		for ($y = (date("Y") + 1); $y > (date("Y") - 2); $y--)
		{
			// autoselect
			if ($rowDate[1] == $y) { $s = ' selected="selected"'; } else { $s = ""; }
			$strA[0] .= '<option value="' . $y . '"' . $s . '>' . $y . '</option>';
		}
		$strA[0] .= '</select>';
		// Month select #-------------------------------------------------
		$strA[1] = '<select name="' . $rowName['1'] . 'M' . $rowName['2'] . '" class="input">';
		// 30 sep 2002
		$monthsS = explode(' ', ' ' . $GLOBALS['L']->m('array_month_short'));
		for ($m = 0; $m <= 12; $m++)
		{
			// autoselect
			if ($rowDate[2] == $m) { $s = ' selected="selected"'; } else { $s = ""; }
			// output
			if($m == 0) { if($cfgIsNulls){ $strA[1] .= '<option value="' . sprintf ("%'02s", $m) . '"' . $s . '>' . sprintf ("%'02s", $m) . " " . $arLs[$m] . '</option>'; } }
			else { $strA[1] .= '<option value="' . sprintf ("%'02s", $m) . '"' . $s . '>' . @$monthsS[$m]  . '</option>'; }
		}
		$strA[1] .= "</select>";
		// Day select #-------------------------------------------------
		$strA[2] = '<select name="' . $rowName['1'] . 'D' . $rowName['2'] . '" class="input">';
		for ($d = 0; $d <= 31; $d++)
		{
			// autoselect
			if ($rowDate[3] == $d) { $s = ' selected="selected"'; } else { $s = ""; }
			// output
			if($d == 0) { if($cfgIsNulls){ $strA[2] .= '<option value="' . sprintf ("%'02s", $d) . '"' . $s . '>' . sprintf ("%'02s", $d) . '</option>'; } }
			else { $strA[2] .= '<option value="' . sprintf ("%'02s", $d) . '"' . $s . '>' . sprintf ("%'02s", $d) . '</option>'; }
		}
		$strA[2] .= '</select>';
		// field for seconds
		if($cfgIsSec)
		{
			$rowDate[4] = substr($rowDate[4], 0, 2) . ":" . substr($rowDate[4], 2, 2) . ":" . substr($rowDate[4], 4, 2);
			$strA[3] = '<input class="input" size="8" maxlength="8" name="' . $rowName['1'] . 'S' . $rowName['2'] . '" value="' . $rowDate[4] . '"/>';
		}
	}
	else
	{
		$str .= "Invalid date format: $val";
	}
	if (isset($strA) && is_array($strA))
	{
		$str = '<table style="font-size:100%;" cellspacing="0" cellpadding="0" border="0" width="100%"><tbody><tr>';
		foreach ($strA as $k => $v)
		{
			$str .= '<td style="width:'.$cfgWidth[$k].'">';
			$str .= $v;
			$str .= '</td>';
		}
		$str .= '</tr></tbody></table>';
	}
	return $str;
}

/* end of file */
?>