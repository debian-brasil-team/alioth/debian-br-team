<?php
if (!defined('IN_GW'))
{
	die('<!-- $Id: t.dict.inc.php,v 1.9 2004/11/14 09:27:13 yrtimd Exp $ -->');
}
/**
 *  Glossword - glossary compiler (http://glossword.info/dev/)
 *  � 2002-2004 Dmitry N. Shilnikov <dev at glossword.info>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  (see `glossword/support/license.html' for details)
 */
// --------------------------------------------------------
/**
 *  Actions for a dictionary. Add, Browse, Edit, Remove, Update.
 */
// --------------------------------------------------------
/**
 * HTML-form for dictionary
 *
 * @param    array   $vars       posted variables
 * @param    int     $runtime    is this form posted first time [ 0 - no | 1 - yes ] // todo: bolean
 * @param    array   $arBroken   the names of broken fields (after post)
 * @param    array   $arReq      the names of required fields (after post)
 * @return   string  complete HTML-code
 * @see textcodetoform(), getFormHeight()
 */
function getFormDict($vars, $runtime = 0, $arBroken = array(), $arReq = array())
{
	global $sys, $arFields, ${GW_ACTION}, $id, $tid, $topic_mode, $L, ${GW_SID}, $oFunc;
	$topic_mode = "form";
	$strForm = "";

	$trClass = "t";
	$v_class_1 = "td1";
	$v_class_2 = "td2";
	$v_td1_width = '20%';
	$trDisabled = "disabled";

	$form = new gwForms();

	$form->Set('action', $GLOBALS['sys']['page_admin']);
	$form->Set('submitok', $L->m('3_save'));
	$form->Set('submitcancel', $L->m('3_cancel'));
	$form->Set('formbgcolor', $GLOBALS['theme']['color_2']);
	$form->Set('formbordercolor', $GLOBALS['theme']['color_4']);
	$form->Set('formbordercolorL', $GLOBALS['theme']['color_1']);
	$form->Set('align_buttons', $GLOBALS['sys']['css_align_right']);
	$form->Set('charset', $sys['internal_encoding']);

	$form->arLtr = array('arPost[themename]', 'arPost[lang]', 'arPost[tablename]');
	## ----------------------------------------------------
	##
	// reverse array keys <-- values;
	$arReq = array_flip($arReq);
	// mark fields as "REQUIRED" and make error messages
	while(is_array($vars) && list($key, $val) = each($vars) )
	{
		$arReqMsg[$key] = $arBrokenMsg[$key] = "";
		if (isset($arReq[$key])) { $arReqMsg[$key] = '&#160;<span class="red"><b>*</b></span>'; }
		if (isset($arBroken[$key])) { $arBrokenMsg[$key] = ' <span class="red"><b>' . $L->m('reason_9') .'</b></span>'; }
	} // end of while
	##
	## ----------------------------------------------------
	$ar = ctlgGetTopics();
	//
	$strForm .= getFormTitleNav($L->m('sect_general'), '');
	$strForm .= '<table class="gw2TableFieldset" width="100%">';
	$strForm .= '<tr><td style="width:'.$v_td1_width.'"></td><td></td></tr>';

	if (${GW_ACTION} == GW_A_ADD)
	{
		$strForm .= '<tr>'.
					'<td class="'.$v_class_1.'">' . $form->field("checkbox", "arPost[is_active]", $vars['is_active']) . '</td>'.
					'<td class="'.$v_class_2.'">' . '<label for="'.$form->text_field2id('arPost[is_active]').'">'.$L->m('allow_dict').'</label></td>'.
					'</tr>';
		$strForm .= '<tr>'.
					'<td class="'.$v_class_1.'">' . $L->m('dict_name') . ':' . $arReqMsg['title'] . '</td>'.
					'<td class="'.$v_class_2.'">' . $arBrokenMsg['title'] . $form->field("textarea", "arPost[title]", textcodetoform($vars['title']), 3) . '</td>'.
					'</tr>';
		$strForm .= '<tr>'.
					'<td class="'.$v_class_1.'">' . $L->m('descr') . ':</td>'.
					'<td class="'.$v_class_2.'">' . $form->field("textarea", "arPost[description]", textcodetoform($vars['description']), 7) . '</td>'.
					'</tr>';
		$strForm .= '<tr>'.
					'<td class="'.$v_class_1.'">' . $L->m('keywords') . ':</td>'.
					'<td class="'.$v_class_2.'">' . $form->field("textarea", "arPost[keywords]", textcodetoform($vars['keywords']), 2) . '</td>'.
					'</tr>';
		$strForm .= '<tr>'.
					'<td class="'.$v_class_1.'">' . $L->m('topic')  . ':' . $arReqMsg['id_tp'] . '</td>'.
					'<td class="'.$v_class_2.'">' . $arBrokenMsg['id_tp'].
					'<select class="input50" name="arPost[id_tp]">' . ctlgGetTopicsRow($ar, 0, 1) . '</select>' .
					'</td>'.
					'</tr>';
		$strForm .= '</table>';
		/* */
		$strForm .= getFormTitleNav($L->m('sect_visual'), '');
		$form->setTag('select', 'class',  'input50');
		$strForm .= '<table class="gw2TableFieldset" width="100%">';
		$strForm .= '<tr><td style="width:'.$v_td1_width.'"></td><td></td></tr>';
		$strForm .= '<tr>'.
					'<td class="'.$v_class_1.'">' . $L->m('lang_source')  . ':' . $arReqMsg['lang'] . '</td>'.
					'<td class="'.$v_class_2.'">' . $form->field("select", "arPost[lang]", $vars['lang'], '0', $L->getLanguages()) . '</td>'.
					'</tr>';
		$strForm .= '<tr>'.
					'<td class="'.$v_class_1.'">' . $L->m('themename') . ':' . $arReqMsg['themename'] . '</td>'.
					'<td class="'.$v_class_2.'">' . $form->field("select", "arPost[themename]", $vars['themename'], 0, $GLOBALS['arThemes']) . '</td>'.
					'</tr>';
		$strForm .= '</table>';
		/* */
		$strForm .= getFormTitleNav($L->m('sect_system'), '');
		$strForm .= '<table class="gw2TableFieldset" width="100%">';
		$strForm .= '<tr><td style="width:'.$v_td1_width.'"></td><td></td></tr>';
		$strForm .= '<tr>'.
					'<td class="'.$v_class_1.'">' . $L->m('sysname') . ':' . $arReqMsg['tablename'] . '</td>'.
					'<td class="'.$v_class_2.'">' . $arBrokenMsg['tablename'] . $form->field("input", "arPost[tablename]", textcodetoform( $vars['tablename'] ) ) . '</td>'.
					'</tr>';
		$arNums[1] = '1';
		$arNums[2] = '2';
		$arNums[3] = '3';
		$strForm .= '<tr>'.
					'<td class="'.$v_class_1.'">' . $L->m('min_srch_length') . ':' . $arReqMsg["min_srch_length"] . '</td>'.
					'<td class="'.$v_class_2.'">' . $form->field("select", "arPost[min_srch_length]", $vars['min_srch_length'], 0, $arNums ) . '</td>'.
					'</tr>';
		$strForm .= '</table>';
		$strForm .= $form->field("hidden", GW_ACTION, GW_A_ADD);
		$strForm .= $form->field("hidden", GW_TARGET, GW_T_DICT);
		$strForm .= $form->field("hidden", "arPost[uid]", $vars['tablename']);
		$form->unsetTag('select');
	}
	else
	{
		$form->setTag('select', 'class',  'input50');
		/* Editing the dictionary settings */
		$strForm .= '<tr>'.
					'<td class="'.$v_class_1.'">' . $form->field("checkbox", "arPost[is_active]", $vars['is_active']) . '</td>'.
					'<td class="'.$v_class_2.'">' . '<label for="'.$form->text_field2id('arPost[is_active]').'">'.$L->m('allow_dict').'</label></td>'.
					'</tr>';
		$strForm .= '<tr>'.
					'<td class="'.$v_class_1.'">' . $L->m('dict_name') . ':' . $arReqMsg['title'] . '</td>'.
					'<td class="'.$v_class_2.'">' . $arBrokenMsg['title'] . $form->field("textarea", "arPost[title]",textcodetoform($vars['title']), 2) . '</td>'.
					'</tr>';
		$strForm .= '<tr>'.
					'<td class="'.$v_class_1.'">' . $L->m('announce') . ':' . $arReqMsg['announce'] . '</td>'.
					'<td class="'.$v_class_2.'">' . $arBrokenMsg['announce'] . $form->field("textarea", "arPost[announce]", textcodetoform($vars['announce']), $oFunc->getFormHeight($vars['announce'])) . '</td>'.
					'</tr>';
		$strForm .= '<tr>'.
					'<td class="'.$v_class_1.'">' . $L->m('descr')  . ':' . $arReqMsg['description'] . '</td>'.
					'<td class="'.$v_class_2.'">' . $form->field("textarea", "arPost[description]", textcodetoform($vars['description']), $oFunc->getFormHeight($vars['description'])) . '</td>'.
					'</tr>';
		$strForm .= '<tr>'.
					'<td class="'.$v_class_1.'">' . $L->m('keywords') . ':</td>'.
					'<td class="'.$v_class_2.'">' . $form->field("textarea", "arPost[keywords]", textcodetoform($vars['keywords']), $oFunc->getFormHeight($vars['keywords'])) . '</td>'.
					'</tr>';
		$strForm .= '<tr>'.
					'<td class="'.$v_class_1.'">' . $L->m('topic')  . ':' . $arReqMsg['id_tp'] . '</td>'.
					'<td class="'.$v_class_2.'">' . $arBrokenMsg['id_tp']
						   . '<select class="input50" name="arPost[id_tp]">'
						   . ctlgGetTopicsRow($ar, 0, 1)
						   . '</select>' .
					'</td>'.
					'</tr>';
		$strForm .= '</table>';
		/* */
		$objCells = new htmlRenderCells();
		$objCells->tClass = '';
		$strForm .= getFormTitleNav($L->m('sect_visual'), '');
		$strForm .= '<table class="gw2TableFieldset" width="100%">';
		$strForm .= '<tr><td style="width:'.$v_td1_width.'"></td><td></td></tr>';
		$strForm .= '<tr>'.
					'<td class="'.$v_class_1.'">' . $L->m('lang_source') . ':' . $arReqMsg['lang'] . '</td>'.
					'<td class="'.$v_class_2.'">' . $form->field("select", "arPost[lang]", $vars['lang'], 0, $L->getLanguages() ) . '</td>'.
					'</tr>';
		$strForm .= '<tr>'.
					'<td class="'.$v_class_1.'">' . $L->m('themename') . ':' . $arReqMsg['themename'] . '</td>'.
					'<td class="'.$v_class_2.'">' . $form->field("select", "arPost[themename]", $vars['themename'], 0, $GLOBALS['arThemes']) . '</td>'.
					'</tr>';
		$strForm .= '<tr>'.
					'<td class="'.$v_class_1.'">' . $L->m('options') . ':</td>'.
					'<td>';
		$arFieldsStr = array();
			$arFieldsStr[] = '<table cellspacing="0" cellpadding="0" border="0" width="100%">'.
							 '<tr style="vertical-align:middle" class="t">'.
							 '<td style="width:2%">' . $form->field("checkbox", "arPost[is_show_az]", $vars['is_show_az']) . '</td>'.
							 '<td style="width:98%"><label for="arPost_is_show_az_">' . $L->m('allow_letters') . '</label></td>'.
							 '</tr>'.
							 '</table>';
			$arFieldsStr[] = '<table cellspacing="0" cellpadding="0" border="0" width="100%">'.
							 '<tr style="vertical-align:middle" class="t">'.
							 '<td style="width:2%">' . $form->field("checkbox", "arPost[is_show_full]", $vars['is_show_full']) . '</td>'.
							 '<td style="width:98%"><label for="arPost_is_show_full_">' . $L->m('is_show_full') . '</label></td>'.
							 '</tr>'.
							 '</table>';
			$arFieldsStr[] = '<table cellspacing="0" cellpadding="0" border="0" width="100%">'.
							 '<tr style="vertical-align:middle" class="t">'.
							 '<td style="width:2%">' . $form->field("checkbox", "arPost[is_dict_as_index]", $vars['is_dict_as_index']) . '</td>'.
							 '<td style="width:98%"><label for="arPost_is_dict_as_index_">' . $L->m('is_dict_as_index') . '</label></td>'.
							 '</tr>'.
							 '</table>';
		$objCells->X = 2;
		$objCells->Y = 99;
		$objCells->ar = $arFieldsStr;
		$strForm .= $objCells->RenderCells();

		$strForm .= '</td>';
		$strForm .= '</tr>';
		$strForm .= '</table>';

		$strForm .= getFormTitleNav($L->m('sect_edit_dict'), '');
		$strForm .= '<table class="gw2TableFieldset" width="100%">';
		$strForm .= '<tr><td style="width:'.$v_td1_width.'"></td><td></td></tr>';
		$strForm .= '<tr>'.
					'<td class="'.$v_class_1.'">' . $L->m('fields_to_edit') . ':</td>'.
					'<td>';
		/* Construct fields */
		reset($arFields);
		/* term is required by default */
		unset($arFields[1]);
		$arFieldsStr = array();
		for (; list($k, $v) = each($arFields);)
		{
			$fieldname = 'is_'.$v[0];
			$vars[$fieldname] = isset($vars[$fieldname]) ? $vars[$fieldname] : 0;
			$arFieldsStr[] = '<table cellspacing="0" cellpadding="0" border="0" width="100%">'.
							 '<tr style="vertical-align:middle" class="t">'.
							 '<td style="width:2%">' . $form->field('checkbox', 'arPost['.$fieldname.']', $vars[$fieldname]) . '</td>'.
							 '<td style="width:98%"><label for="'.$form->text_field2id('arPost['.$fieldname.']').'">' . $L->m($v[0]) . '</label></td>'.
							 '</tr>'.
							 '</table>';
		}
		$objCells->X = 2;
		$objCells->Y = 99;
		$objCells->ar = $arFieldsStr;
		$strForm .= $objCells->RenderCells();

		$strForm .= '</td>';
		$strForm .= '</tr>';

		$strForm .= '</table>';
		/* */
		$strForm .= getFormTitleNav($L->m('sect_system'), '');
		$strForm .= '<table class="gw2TableFieldset" width="100%">';
		$strForm .= '<tr><td style="width:'.$v_td1_width.'"></td><td></td></tr>';

		$strForm .= '<tr>'.
					'<td class="'.$v_class_1.'">' . $L->m('date_created') . ':</td>'.
					'<td class="'.$v_class_2.'">' . htmlFormSelectDate("arPost[date_created]", $vars['date_created']) . '</td>'.
					'</tr>';
		$strForm .= '<tr>'.
					'<td class="'.$v_class_1.'">' . $L->m('date_modif') . ':</td>'.
					'<td class="'.$v_class_2.'">' . htmlFormSelectDate("arPost[date_modif]", $vars['date_modified']) . '</td>'.
					'</tr>';
		$strForm .= '<tr>'.
					'<td class="'.$v_class_1 .'">' . $L->m('sysname') . ':</td>'.
					'<td class="'.$v_class_2.' '.$trDisabled .'">' . $vars['tablename'] . '</td>'.
					'</tr>';

		$arNums[1] = '1';
		$arNums[2] = '2';
		$arNums[3] = '3';
		$strForm .= '<tr>'.
					'<td class="'.$v_class_1.'">' . $L->m('min_srch_length') . ':' . $arReqMsg["min_srch_length"] . '</td>'.
					'<td class="'.$v_class_2.'">' . $form->field("select", "arPost[min_srch_length]", $vars['min_srch_length'], 0, $arNums ) . '</td>'.
					'</tr>';

		$strForm .= '<tr>'.
					'<td class="'.$v_class_1 .'">' . $L->m('options') . ':</td>'.
					'<td>';
		$arFieldsStr = array();
			$arFieldsStr[] = '<table cellspacing="0" cellpadding="0" border="0" width="100%">'.
							 '<tr style="vertical-align:middle" class="t">'.
							 '<td style="width:2%">' . $form->field("checkbox", "arPost[is_htmled]", $vars['is_htmled']) . '</td>'.
							 '<td style="width:98%"><label for="'.$form->text_field2id('arPost[is_htmled]').'">' . $L->m('allow_htmleditor') . '</label></td>'.
							 '</tr>'.
							 '</table>';
			$arFieldsStr[] = '<table cellspacing="0" cellpadding="0" border="0" width="100%">'.
							 '<tr style="vertical-align:middle" class="t">'.
							 '<td style="width:2%">' . $form->field("checkbox", "arPost[is_leech]", $vars['is_leech']) . '</td>'.
							 '<td style="width:98%"><label for="'.$form->text_field2id('arPost[is_leech]').'">' . $L->m('allow_leecher') . '</label></td>'.
							 '</tr>'.
							 '</table>';
		$objCells->X = 2;
		$objCells->Y = 99;
		$objCells->ar = $arFieldsStr;
		$strForm .= $objCells->RenderCells();

		$strForm .= '</td>';
		$strForm .= '</tr>';
		$strForm .= '</table>';
		$strForm .= $form->field("hidden", "id", $id);
		$strForm .= $form->field("hidden", GW_ACTION, UPDATE);
		$strForm .= $form->field("hidden", GW_TARGET, GW_T_DICT);
	}
	$strForm .= $form->field("hidden", GW_SID, ${GW_SID});
	return $form->Output($strForm);
}
// --------------------------------------------------------
// Check permission
if ($auth->is_allow('dict', $id) || ( isset($arDictParam['id_user']) && $arDictParam['id_user'] == $user->id) )
{
// --------------------------------------------------------
// action switcher
switch (${GW_ACTION})
{
case GW_A_ADD:
## --------------------------------------------------------
## Add a dictionary
	if (empty($post)) // not submited
	{
		$tid                = 0; // global value for topic list
		$vars['title']      = $vars['description'] = $vars['announce'] = "";
		$vars['tablename']  = kMakeUid("gw_x", 10);
		$vars['id_tp']      = $tid;
		$vars['lang']       = $L->getLocale();
		$vars['themename']  = 'gw_silver';
		$vars['keywords']   = '';
		$vars['is_active']  = 0;
		$vars['min_srch_length'] = '2';

		$strR .= getFormDict($vars, 0, 0, $arReq[$t]);
		$arHelpMap = array(
					'dict_name'  => 'tip010',
#					'announce'   => 'tip009',
					'descr'      => 'tip008',
					'keywords'   => 'tip007',
					'topic'      => 'tip006',
					'lang_source'=> 'tip005',
					'themename'  => 'tip004',
					'sysname'    => 'tip003'
				 );
		$strHelp = '';
		$strHelp .= '<dl>';
		for(; list($k, $v) = each($arHelpMap);)
		{
			$strHelp .= '<dt><b>' . $L->m($k) . '</b></dt>';
			$strHelp .= '<dd>' . $L->m($v) . '</dd>';
		}
		$strHelp .= '</dl>';
		$strR .= '<br/>'.kTbHelp($L->m('2_tip'), $strHelp);
	}
	else
	{
		$arBroken = validatePostWalk($arPost, $arReq[$t]);
		$arPost['is_active'] = isset($arPost['is_active']) ? $arPost['is_active'] : 0;
		if (sizeof($arBroken) == 0)
		{
			$isPostError = 0;
		}
		else
		{
			$tid = $arPost['id_tp'];
			$strR .= getFormDict($arPost, 1, $arBroken, $arReq[$t]);
		}
		if (!$isPostError)
		{
			// Fix on/off options
			$arIsV = array('is_active','is_show_az','is_auth','is_post','is_leech','is_htmled');
			reset($arFields);
			for (; list($k, $v) = each($arFields);)
			{
				$arIsV[] = 'is_'.$v[0];
			}
			for (; list($k, $v) = each($arIsV);)
			{
				$arPost[$v]  = isset($arPost[$v]) ? $arPost[$v] : 0;
			}
			// fix for variable name, unicode
			$arPost['tablename'] = preg_replace("/![0-9a-zA-Z]/", "", $arPost['tablename']);
			$arPost['tablename'] = str_replace('-', '_', $arPost['tablename']);
			//
			$q = $q3 = array();
			// 1.3.8: user_id, auth_level

			//
			$arDictNewSettings = array(
				'min_srch_length' => $arPost['min_srch_length'],
				'is_trsp' => '0',
				'is_trns' => '1',
				'is_abbr' => '1',
				'is_defn' => '1',
				'is_usg' => '0',
				'is_src' => '1',
				'is_syn' => '1',
				'is_see' => '1',
				'is_phone' => '0',
				'is_address' => '0',
				'is_show_full' => '0'
			);
			$q['dict_settings'] = serialize($arDictNewSettings);
			$q['id_tp']         = $arPost['id_tp'];
			$q['is_active']     = $arPost['is_active'];
			$q['is_show_az']    = 1;
			$q['is_leech']      = $arPost['is_leech'];
			$q['lang']          = $arPost['lang'];
			$q['themename']     = $arPost['themename'];
			$q['tablename']     = $arPost['tablename'];
			$q['title']         = $arPost['title'];
			$q['announce']      = '';
			$q['keywords']      = $arPost['keywords'];
			$q['description']   = $arPost['description'];
			$q['date_created']  = $q['date_modified'] = date("YmdHis", mktime());
			$q['uid']           = kMakeUid(8);
			$q['id_user']       = $user->id;
			// create tables
			$queryA[] = $oSqlQ->getQ('create-dict', $q['tablename']);
			$q['id'] = $q3['dict_id'] = $oDb->MaxId(TBL_DICT);
			$queryA[] = prepareSQLqueryINSERT($q, TBL_DICT);
			$q = array();
			$q['hits'] = 0;
			$q['id'] = $q3['dict_id'];
			$queryA[] = prepareSQLqueryINSERT($q, TBL_STAT_DICT);
			//
			$q3['user_id'] = $user->id;
			$queryA[] = prepareSQLqueryINSERT($q3, TBL_USERS_MAP);
			//
			$strR .= postQuery($queryA, GW_ACTION . '=' . GW_A_EDIT . '&' . GW_TARGET . '=' . GW_T_DICT . '&id='.$q['id'], $sys['isDebugQ'], 0);
		} // end of $isPostError
	}
##
## --------------------------------------------------------
break;
case BRWS:
## --------------------------------------------------------
## Browse topics
	$strR .= getDictList($tid);
##
## --------------------------------------------------------
break;
case GW_A_EDIT:
## --------------------------------------------------------
## Edit selected dictionary
	if ($id == '')
	{
		gwtk_header($sys['server_proto'].$sys['server_host'].$sys['page_admin']);
	}
	// settings can be changed only by admin or dictionary owner
	if (($post == '') && ( $auth->have_perm('admin', PERMLEVEL) || ($arDictParam['id_user'] == $user->id) ))
	{
		/* > 1.6.3 */
		if (!isset($arDictParam['is_show_full'])) { $arDictParam['is_show_full'] = 0; }
		if (!isset($arDictParam['is_dict_as_index'])) { $arDictParam['is_dict_as_index'] = 0; }
		$tid = $arDictParam['id_tp']; /* global */
		$strR .= getFormDict($arDictParam, 0, 0, $arReq[$t]);
	}
##
## --------------------------------------------------------
break;
case REMOVE:
## --------------------------------------------------------
## Remove selected dictionary
	// Only author or admin
	if (!isset($id) || (($arDictParam['id_user'] != $user->id) && !$auth->have_perm('admin', PERMLEVEL)) )
	{
		$strR .= $L->m('reason_13');
	}
	else
	{
		if ($isConfirm != "1") // if not confirmed
		{
			$Confirm = new gwConfirmWindow;
			$Confirm->action           = $sys['page_admin'];
			$Confirm->submitok         = $L->m('3_remove');
			$Confirm->submitcancel     = $L->m('3_cancel');
			$Confirm->formbgcolor      = $GLOBALS['theme']['color_2'];
			$Confirm->formbordercolor  = $GLOBALS['theme']['color_4'];
			$Confirm->formbordercolorL = $GLOBALS['theme']['color_1'];
			$Confirm->css_align_right  = $sys['css_align_right'];
			$Confirm->css_align_left   = $sys['css_align_left'];
			$Confirm->setQuestion('<p class="r"><span class="s"><b>'.  $L->m('9_remove') .
										'</b></span></p><p class="t"><span class="f">' . $L->m('3_'.${GW_ACTION}) .
										':</span><br/>'.$arDictParam['title'].'</p>');
			$Confirm->tAlign = "center";
			$Confirm->formwidth = "400";
			$Confirm->setField("hidden", GW_ACTION, REMOVE);
			$Confirm->setField("hidden", GW_TARGET, ${GW_TARGET});
			$Confirm->setField("hidden", GW_SID, ${GW_SID});
			$Confirm->setField("hidden", "id", $id);
			$strR .= $Confirm->Form();
		}
		else // after confirm
		{
			$queryA[] = $oSqlQ->getQ('del-wordmap-by-dict', $id);
			$queryA[] = $oSqlQ->getQ('del-by-dict_id', TBL_USERS_MAP, $id);
			$queryA[] = $oSqlQ->getQ('del-by-id', TBL_DICT, $id);
			$queryA[] = $oSqlQ->getQ('del-by-id', TBL_STAT_DICT, $id);
			$queryA[] = $oSqlQ->getQ('drop-table', $arDictParam['tablename']);
			$strR .= gw_tmp_clear($id);
			$strR .= postQuery($queryA, GW_ACTION . '=' . BRWS . '&' . GW_TARGET . '=' . GW_T_DICT, $sys['isDebugQ'], 0);
		}
	}
##
## --------------------------------------------------------
break;
case CLEAN:
## --------------------------------------------------------
## Clean selected dictionary (todo: optimize with REMOVE)
	if (!isset($id) || (($arDictParam['id_user'] != $user->id) && !$auth->have_perm('admin', PERMLEVEL)) )
	{
		$strR .= $L->m('reason_13');
	}
	elseif ($isConfirm != "1") // not confirmed
	{
		$Confirm = new gwConfirmWindow;
		$Confirm->action           = $sys['page_admin'];
		$Confirm->submitok         = $L->m('3_'.${GW_ACTION});
		$Confirm->submitcancel     = $L->m('3_cancel');
		$Confirm->formbgcolor      = $GLOBALS['theme']['color_2'];
		$Confirm->formbordercolor  = $GLOBALS['theme']['color_4'];
		$Confirm->formbordercolorL = $GLOBALS['theme']['color_1'];
		$Confirm->css_align_right  = $sys['css_align_right'];
		$Confirm->css_align_left   = $sys['css_align_left'];

		$Confirm->setQuestion('<p class="r"><span class="s"><b>'.  $L->m('9_remove') .
									'</b></span></p><p class="t"><span class="f">' . $L->m('3_'.${GW_ACTION}) .
									':</span><br/>'.$arDictParam['title'].'</p>');
		$Confirm->tAlign = "center";
		$Confirm->formwidth = "400";
		$Confirm->setField("hidden", GW_ACTION, CLEAN);
		$Confirm->setField("hidden", GW_TARGET, ${GW_TARGET});
		$Confirm->setField("hidden", "id", $id);
		$Confirm->setField("hidden", GW_SID, ${GW_SID});
		$strR .= $Confirm->Form();
	}
	elseif ($isConfirm) // after confirm
	{
		$queryA = array();
		$q = array();
		$queryA[] = $oSqlQ->getQ('del-table', $arDictParam['tablename']);
		$queryA[] = $oSqlQ->getQ('del-wordmap-by-dict', $id);
		$q['int_terms'] = 0;
		$queryA[] = gw_sql_update($q, TBL_DICT, "id='$id'");
		$strR .= gw_tmp_clear($id);
		/* Redirect to... */
		$arPost['after'] = GW_AFTER_DICT_UPDATE;
		$str_url = gw_after_redirect_url($arPost['after']);
		$strR .= postQuery($queryA, $str_url, $sys['isDebugQ'], 1);
	}
##
## --------------------------------------------------------
break;
case UPDATE:
## --------------------------------------------------------
## Update dictionary

		if(isset($arReq[$t])) // check for broken fields
		{
			$arBroken = validatePostWalk($arPost, $arReq[$t]);
		}
		if (isset($arPost['date_modifS'])) // fix date
		{
			$arPost['date_createdS'] = str_replace(":", "", $arPost['date_createdS']);
			if(!preg_match("/[0-9]{6}/", $arPost['date_createdS'] )) { $arPost['date_createdS'] = '000000'; }
		}
		if (isset($arPost['date_modifS'])) // fix date
		{
			$arPost['date_modifS'] = str_replace(":", "", $arPost['date_modifS']);
			if(!preg_match("/[0-9]{6}/", $arPost['date_modifS'] )) { $arPost['date_modifS'] = '000000'; }
		}
		//
		// Fix on/off options
		//
		$arIsV = array('is_active','is_show_az','is_auth','is_post','is_leech','is_htmled');
		for (reset($arFields); list($k, $v) = each($arFields);)
		{
			$arIsV[] = 'is_'.$v[0];
		}
		for (; list($k, $v) = each($arIsV);)
		{
			$arPost[$v]  = isset($arPost[$v]) ? $arPost[$v] : 0;
		}
		//
		if(isset($arReq[$t]) && sizeof($arBroken) == 0) // no errors
		{
			$isPostError = 0;
		}
		else // on error
		{
			// should never happened, fixed for empty values in $arReq[$t]
			if(!isset($arReq[$t])) { $arReq[$t] = $arBroken = array_keys($arPost); }
			$isPostError = 1;
			// read posted variables, call HTML-form again.
			$arPost['user_name']    = $arDictParam['user_name'];
			$arPost['tablename']    = $arDictParam['tablename'];
			$arPost['int_terms']     = $arDictParam['int_terms'];
			$arPost['date_created'] = $arPost['date_createdY'].$arPost['date_createdM'].$arPost['date_createdD'].$arPost['date_createdS'];
			$arPost['date_modified']= $arPost['date_modifY'].$arPost['date_modifM'].$arPost['date_modifD'].$arPost['date_modifS'];
			$strR .= getFormDict($arPost, 1, $arBroken, $arReq[$t]);
		}
		if (!$isPostError) // final update
		{
			//
			$queryA = array();

			$arDictNewSettings = array(
				'min_srch_length' => $arPost['min_srch_length'],
				'is_trsp' => isset($arPost['is_trsp']) ? $arPost['is_trsp'] : 0,
				'is_trns' => isset($arPost['is_trns']) ? $arPost['is_trns'] : 0,
				'is_abbr' => isset($arPost['is_abbr']) ? $arPost['is_abbr'] : 0,
				'is_defn' => isset($arPost['is_defn']) ? $arPost['is_defn'] : 0,
				'is_usg' => isset($arPost['is_usg']) ? $arPost['is_usg'] : 0,
				'is_src' => isset($arPost['is_src']) ? $arPost['is_src'] : 0,
				'is_syn' => isset($arPost['is_syn']) ? $arPost['is_syn'] : 0,
				'is_see' => isset($arPost['is_see']) ? $arPost['is_see'] : 0,
				'is_phone' => isset($arPost['is_phone']) ? $arPost['is_phone'] : 0,
				'is_address' => isset($arPost['is_address']) ? $arPost['is_address'] : 0,
				'is_show_full' => isset($arPost['is_show_full']) ? $arPost['is_show_full'] : 0,
				'is_dict_as_index' => isset($arPost['is_dict_as_index']) ? $arPost['is_dict_as_index'] : 0,
			);

			$q['dict_settings'] = serialize($arDictNewSettings);
			$q['id_tp']         = sprintf("%d",$arPost['id_tp']);
			$q['is_active']     = isset($arPost['is_active']) ? $arPost['is_active'] : 0;
			$q['is_show_az']    = isset($arPost['is_show_az']) ? $arPost['is_show_az'] : 0;
			$q['is_leech']      = isset($arPost['is_leech']) ? $arPost['is_leech'] : 0;
			$q['is_htmled']     = isset($arPost['is_htmled']) ? $arPost['is_htmled'] : 0;

			$q['lang']          = $arPost['lang'];
			$q['themename']     = $arPost['themename'];
			$q['title']         = $arPost['title'];
			$q['announce']      = $arPost['announce'];
			$q['keywords']      = $arPost['keywords'];
			$q['description']   = $arPost['description'];

			$q['date_created']  = $arPost['date_createdY'].$arPost['date_createdM'].$arPost['date_createdD'].$arPost['date_createdS'];
			$q['date_modified'] = $arPost['date_modifY'].$arPost['date_modifM'].$arPost['date_modifD'].$arPost['date_modifS'];

			// Save dictionary settings
			$queryA[] = gw_sql_update($q, TBL_DICT, 'id = "'.$id.'"');

			/* Clear cache */
			$strR .= gw_tmp_clear($id);

			/* Optimization for previous (not current) modifications */
			gw_sys_dict_check();

			/* Redirect to... */
			$arPost['after'] = GW_AFTER_DICT_UPDATE;
			$str_url = gw_after_redirect_url($arPost['after']);
			$strR .= postQuery($queryA, $str_url, $sys['isDebugQ'], 0);
		}

##
## --------------------------------------------------------
break;
case IMPORT:
## --------------------------------------------------------
## Import dictionary, External
	$pathImport = $sys['path_include'] . '/' . GW_ACTION . '.' . ${GW_ACTION} . '.inc.' . $sys['phpEx'];
	file_exists($pathImport)
	? include_once($pathImport)
	: printf($GLOBALS['L']->m("reason_10"), $pathImport);
##
## --------------------------------------------------------
break;
case EXPORT:
## --------------------------------------------------------
## Export dictionary, External
	$pathExport = $sys['path_include'] . '/' . GW_ACTION . '.' . ${GW_ACTION} . '.inc.' . $sys['phpEx'];
	file_exists($pathExport)
	? include_once($pathExport)
	: printf($GLOBALS['L']->m("reason_10"), $pathImport);
##
## --------------------------------------------------------
break;
default:
break;
} // end of switch
// --------------------------------------------------------
// End check permission
}
else
{
	$strR .= $L->m('reason_13');
}
/* end of file */
?>