<?php
/**
 * Glossword plug-in: Export to XML
 * Location: glossword/inc/export_XML/index.inc.php
 * by Dmitry N. Shilnikov <dev@glossword.ru>
 * @version $Id: index.inc.php,v 1.7 2004/11/13 12:30:09 yrtimd Exp $
 */
// --------------------------------------------------------
if (!defined('IN_GW'))
{
	die("<!-- leave empty -->");
}
// --------------------------------------------------------
define('FORMAT_NAME', str_replace('export_', '', $arPost['fmt']));
define('FORMAT_EXT', 'xml');
// --------------------------------------------------------
/**
 *
 */
function getFormXml($vars, $runtime = 0, $arBroken = array(), $arReq = array())
{
	global ${GW_ACTION}, $id, $sys, $L, $arPost, $arSplit, $arDictParam, $oFunc, ${GW_SID};
	$strForm = "";
	$trClass = "t";
	$form = new gwForms();
	$form->action = append_url($sys['page_admin']);
	$form->submitok = $L->m('3_next').' &gt;';
	$form->submitdel = $L->m('3_remove');
	$form->submitcancel = $L->m('3_cancel');
	$form->formbgcolor = $GLOBALS['theme']['color_2'];
	$form->formbordercolor = $GLOBALS['theme']['color_4'];
	$form->formbordercolorL = $GLOBALS['theme']['color_1'];
	$form->Set('charset', $sys['internal_encoding']);
	$form->arLtr = array('arPost[split]');
	## ----------------------------------------------------
	##
	// reverse array keys <-- values;
	$arReq = array_flip($arReq);
	// mark fields as "REQUIRED" and make error messages
	while(is_array($vars) && list($key, $val) = each($vars) )
	{
		$arReqMsg[$key] = $arBrokenMsg[$key] = "";
		if (isset($arReq[$key])) { $arReqMsg[$key] = ' <span style="color:#E30"><b>*</b></span>'; }
		if (isset($arBroken[$key])) { $arBrokenMsg[$key] = ' <span class="'.$trClass.'" style="color:#E30"><b>' . $L->m('reason_9') .'</b></span>'; }
	} // end of while
	##
	## ----------------------------------------------------
	$strForm = '';
	$strForm .= '<table cellspacing="0" cellpadding="2" border="0" width="100%">';
	$strForm .= '<col width="50%"/><col width="50%"/>';
	$strForm .= '<tr valign="top"><td>';
			$strForm .= getFormTitleNav($L->m('3_export'), '<span class="r">'.FORMAT_NAME.'</span>');
			$strForm .= '<table cellspacing="0" cellpadding="2" border="0" width="100%">';
			$strForm .= '<col width="1%"><col width="99%">';
			$arTmp['id'] = 'arPost-t';
			$strForm .= '<tr class="t">'.
						'<td>' . $form->field("radio", "arPost[td_mode]", 't', ($vars['td_mode'] == 't'), $arTmp) . '</td>'.
						'<td><label for="arPost-t" style="cursor:pointer;">'. $L->m('terms') .'</label></td>'.
						'</tr>';
			$arTmp['id'] = 'arPost-d';
			$strForm .= '<tr class="t">'.
						'<td>' . $form->field("radio", "arPost[td_mode]", 'd', ($vars['td_mode'] == 'd'), $arTmp) . '</td>'.
						'<td><label for="arPost-d" style="cursor:pointer;">'. $L->m('definitions') .'</label></td>'.
						'</tr>';
			$arTmp['id'] = 'arPost-td';
			$strForm .= '<tr class="t">'.
						'<td>' . $form->field("radio", "arPost[td_mode]", 'td', (($vars['td_mode'] == 'td')?1:0), $arTmp) . '</td>'.
						'<td><label for="arPost-td" style="cursor:pointer;">'. $L->m('terms'). ' &amp; ' . $L->m('definitions') .'</label></td>'.
						'</tr>';

			$strForm .= '<tr class="'.$trClass.'">'.
						'<td>' . $form->field("checkbox", "arPost[is_term_t1]", $vars['is_term_t1']) . '</td>'.
						'<td><label for="arPost_is_term_t1_">' . $L->m('xml_is_term_t1') . '</label></td>'.
						'</tr>';
			$strForm .= '<tr class="'.$trClass.'">'.
						'<td>' . $form->field("checkbox", "arPost[is_term_t2]", $vars['is_term_t2']) . '</td>'.
						'<td><label for="arPost_is_term_t2_">' . $L->m('xml_is_term_t2') . '</label></td>'.
						'</tr>';
			$strForm .= '<tr class="'.$trClass.'">'.
						'<td>' . $form->field("checkbox", "arPost[is_term_id]", $vars['is_term_id']) . '</td>'.
						'<td><label for="arPost_is_term_id_">' . $L->m('xml_is_term_id') . '</label></td>'.
						'</tr>';


			$strForm .= '</table>';
	$strForm .= '</td><td>';
			$strForm .= getFormTitleNav($L->m('dictdump_split'));
			$strForm .= '<table cellspacing="0" cellpadding="2" border="0" width="100%">';
			$strForm .= '<col width="1%"><col width="29%"><col width="70%">';
			$arBoxId['id'] = "split_list1";
			$arBoxId['onclick'] = 'checkSplit();';
			$strForm .= '<tr>';
			$strForm .= '<td>' . $form->field("radio", "arPost[split_m]", "list", $vars['is_list1'], $arBoxId) . '</td>';
			$strForm .= '<td class="t"><label id="labelList" for="split_list1">'.$L->m('dictdump_list').'</label></td>';
			$strForm .= '<td>' . $form->field("select", "arPost[split1]", $vars['intsplit'], "100%", $arSplit ) . '</td>';
			$strForm .= '</tr>';
			$arBoxId['id'] = "split_custom";
			$arBoxId['onclick'] = 'checkSplit();';
			$strForm .= '<tr>';
			$strForm .= '<td>' . $form->field("radio", "arPost[split_m]", "custom", $vars['is_list2'], $arBoxId) . '</td>';
			$strForm .= '<td class="t"><label id="labelCustom" for="split_custom">'.$L->m('dictdump_custom').'</label></td>';
			$strForm .= '<td>' . $form->field("input", "arPost[split2]", $vars['int_terms'] ) . '</td>';
			$strForm .= '</tr>';
			$strForm .= '</table>';
	$strForm .= '</td>';
	$strForm .= '</tr>';
	$strForm .= '</table>';
	include($sys['path_include'] . '/export_XML/index.js.' . $GLOBALS['sys']['phpEx']);
	$strForm .= $form->field("hidden", 'arPost['.GW_TARGET.']', FORMAT_NAME);
	$strForm .= $form->field("hidden", 'arPost[fmt]', $vars['fmt']);
	$strForm .= $form->field("hidden", 'arPost[int_terms]', $vars['int_terms']);
	$strForm .= $form->field("hidden", 'arPost[min]', $vars['min']);
	$strForm .= $form->field("hidden", 'arPost[max]', $vars['max']);
	$strForm .= $form->field("hidden", 'id', $id);
	$strForm .= $form->field("hidden", GW_TARGET, GW_T_DICT);
	$strForm .= $form->field("hidden", GW_ACTION, EXPORT);
	$strForm .= $form->field("hidden", GW_SID, ${GW_SID});
	return $form->Output($strForm);
} //
// --------------------------------------------------------
$arReq = array('fmt');
// --------------------------------------------------------
// split per lines
$is_split   = isset($arPost['is_split']) ? $arPost['is_split'] : 100;
$is_list1   = ( isset($arPost['is_list1']) && $arPost['is_list1'] == "list" ) ? 1 : 0;
$is_list2   = ( isset($arPost['is_list2']) && $arPost['is_list2'] == "custom" ) ? 1 : 0;
if (!($is_list1)&&!($is_list2) ) { $is_list1 = 1; }
$is_term_t1   = isset($arPost['is_term_t1']) ? $arPost['is_term_t1'] : 0;
$is_term_t2   = isset($arPost['is_term_t2']) ? $arPost['is_term_t2'] : 0;
$is_term_id   = isset($arPost['is_term_id']) ? $arPost['is_term_id'] : 0;
//
$arSplit = array ('100' => '100', '500' => '500', '1000' => '1000', '2500' => '2500', '5000' => '5000');
$intSplit = 500;
//
if (!isset($arPost[GW_TARGET])) { $post = ''; }
if ($post == '') // not saved
{
	// get number of terms
	$vars['int_terms'] = 0;
	$vars['min'] = $arPost['date_minY'].$arPost['date_minM'].$arPost['date_minD'].str_replace(':', '', $arPost['date_minS']);
	$vars['max'] = $arPost['date_maxY'].$arPost['date_maxM'].$arPost['date_maxD'].str_replace(':', '', $arPost['date_maxS']);
	if (isset($arPost['date_minY']))
	{
		$arSql = $oDb->sqlRun($oSqlQ->getQ('cnt-dict-date', $arDictParam['tablename'], $vars['min'], $vars['max']));
		$vars['int_terms'] = isset($arSql['0']['n']) ? $arSql['0']['n'] : 0;
	}
	//
	$vars['fmt'] = $arPost['fmt'];
	$vars['intsplit'] = $intSplit;
	$vars['is_list1'] = $is_list1;
	$vars['is_list2'] = $is_list2;
	$vars['td_mode'] = 'td';
	$vars['is_term_t1'] = 1;
	$vars['is_term_t2'] = 1;
	$vars['is_term_id'] = 1;
	$strR .= getFormXml($vars, 0, 0, $arReq);
}
else
{
	$arBroken = validatePostWalk($arPost, $arReq);
	if (sizeof($arBroken) == 0)
	{
		$isPostError = 0;
	}
	if (!$isPostError) // single check is ok
	{
		$i = 0;
		$q = array();
		$sqlWhereDate = 'date_created >= ' . $arPost['min'] . ' AND date_created <= ' . $arPost['max'];
		$mode = 'w';
		if ($arPost['split_m'] == 'list')
		{
			$int_split = $arPost['split1'];
		}
		else
		{
			$int_split = $arPost['split2'];
		}
#		prn_r( $arPost);
		$cntFiles = ceil($arPost['int_terms'] / $int_split);
		$fileS = getExportFilename(
				$sys['path_export'] . '/' . FORMAT_EXT . '/' . sprintf("%05s", $id) . '_' . $arDictParam['tablename'],
				$cntFiles,
				FORMAT_EXT
			 );
		$strR .= '<ul class="t">';
		// Export in progress
		for ($i = 0; $i < $cntFiles; $i++)
		{
			$limit = getSqlLimit($arDictParam['int_terms'], $i + 1,  $int_split);
			$filename = sprintf($fileS, ($i + 1), $cntFiles);
			$strQ = '';
			$strR .= '<li><span class="f">';
			$strR .= htmlTagsA($filename, $filename, 0, "_blank") . '</span>... ';
			$sql = 'SELECT term_1, term_2, id, term, defn
					FROM ' . $DBTABLE . '
					WHERE '.$sqlWhereDate.'
					AND id_d = "' . $id . '"
					ORDER BY term' . $limit;
			$arSql = $oDb->sqlExec($sql, '', 0);

			$strQ .= '<' . '?xml version="1.0" encoding="UTF-8"?' . '>' . CRLF;
			$strQ .= '<glossword>' . CRLF;
			//
			for(; list($k, $v) = each($arSql);)
			{
				$strQ .= '<line>';
				if ((($arPost['td_mode'] == 'td')||($arPost['td_mode'] == 't'))&&($v['term'] != ''))
				{
					$str_is_term_t1 = ($is_term_t1) ? sprintf(' t1="%s"', $v['term_1']) : '';
					$str_is_term_t2 = ($is_term_t2) ? sprintf(' t2="%s"', $v['term_2']) : '';
					$str_is_term_id = ($is_term_id) ? sprintf(' id="%s"', $v['id']) : '';
					$strQ .= sprintf('<term%s%s%s>%s</term>',
								$str_is_term_t1, $str_is_term_t2, $str_is_term_id, $v['term']
								);
				}
				if ((($arPost['td_mode'] == 'td')||($arPost['td_mode'] == 'd'))&&($v['defn'] != ''))
				{
					$strQ .= trim($v['defn']);
				}
				$strQ .= '</line>' . CRLF;
			}
			$strQ .= '</glossword>';
			$isWrite = $oFunc->file_put_contents( $filename, $strQ, $mode);
			$strR .= ( $isWrite ?  'ok (' . $oFunc->number_format(strlen($strQ), 0, $L->languagelist('4')) . " " . $L->m('bytes') . ')' : $L->m('error') ) . '</li>';
		}
		$strR .= '</ul>';
	}
}
?>