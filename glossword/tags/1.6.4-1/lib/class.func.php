<?php
/**
 * � 2003-2004 Dmitry N. Shilnikov <dev at glossword.info>
 * $Id: class.func.php,v 1.15 2004/11/13 12:31:42 yrtimd Exp $
 * http://glossword.info/dev/
 */
/* --------------------------------------------------------
 * Library functions for daily use
 * ----------------------------------------------------- */
	$tmp['mtime'] = explode(' ', microtime());
	$tmp['start_time'] = (float)$tmp['mtime'][1] + (float)$tmp['mtime'][0];
/* --------------------------------------------------------
 * Functions that must work without class initialization
 * ----------------------------------------------------- */
/**
 * Includes a php-file.
 * Replacement for include_once(), require_once().
 * Restructions on access to variables apply.
 * Test results for 1000 included files less than 65 KB:
 *     0.004 gw2_include_once()
 *     0.429 include_once()
 *     0.434 require_once()
 *     0.569 include()
 *     0.574 require()
 * Test results for 1000 included files more than 65 KB:
 *     0.005 gw2_include_once()
 *     0.444 include_once()
 *     0.452 require_once()
 *    23.645 include()
 *    24.791 require()
 * @param  string  Path to file
 */
function gw2_include_once($f)
{
	static $ar = array();
	if (!array_key_exists(($rf = crc32($f)), $ar))
	{
		$ar[$rf] = true;
		if (file_exists($f))
		{
			include_once($f);
#			eval('? >'. file_get_contents($f) .'< ? php');
		}
	}
}
/**
 * Shuffles an array. Replacement for shuffle().
 * @usage: usort($arB, 'gw_rand_cmp');
 * @return  int     Random value
 */
function gw_rand_cmp( $a, $b )
{
	$num = (rand(0, 2) - 1);
	return round( $num );
}
/**
 * Replacement for print_r()
 *
 * @param   string  $ar Any string or array.
 * @param   string  $comment Text for additional marker for better visual display
 */
function prn_r($ar, $comment = '')
{
	if (is_array($ar) || is_object($ar))
	{
		if (is_array($ar))
		{
			ksort($ar);
		}
		array_walk($ar, "htmlspecialchars_ltgt");
	}
	elseif (is_string($ar))
	{
		htmlspecialchars_ltgt($ar);
	}
	print '<pre style="font-size:0.9em; margin:0">';
	if ($comment != '')
	{
		print '==&gt; Start <b>'.$comment.'</b><br />';
	}
	print_r($ar);
	if ($comment != '')
	{
		print '&lt;== End <b>'.$comment.'</b><br />';
	}
	print '</pre>';
}
/**
 * Converts <> only, recursive, calls by reference
 *
 * @param   string  $str Any string or array.
 */
function htmlspecialchars_ltgt(&$s)
{
	if (is_array($s))
	{
		/* can't use array_walk() on byself with reference */
		for (reset($s); list($k, $v) = each($s);)
		{
			htmlspecialchars_ltgt($v);
			$s[$k] = $v;
		}
	}
	elseif (is_string($s))
	{
		$s = str_replace('&', '&amp;', $s);
		$s = str_replace('<', '&lt;', $s);
		$s = str_replace('>', '&gt;', $s);
	}
}
/**
 * Fixes 'slash problem', recursive, calls by reference
 *
 * @param   string  $str Any string or array.
 * @param   string  $type Type of quotes to check. [ runtime | gpc ]
 * @param   string  $mode Where to check for quotes. [ php | sql ], 05 may 2003
 */
function gw_fixslash(&$str, $type = 'gpc', $mode = 'php')
{
	$gpc = false;
	$runtime = false;
	if ($type == 'gpc')
	{
		$gpc = true;
	}
	elseif ($type == 'runtime')
	{
		$runtime = true;
	}
	if ($str != '')
	{
		if (is_array($str) || is_object($str))
		{
			for (reset($str); list($k, $v) = each($str);)
			{
				gw_fixslash($str[$k], $type, $mode);
			}
			reset($str);
		}
		else
		{
			if ($gpc && get_magic_quotes_gpc())
			{
				$str = gw_stripslashes($str);
			}
			elseif ($runtime && get_magic_quotes_runtime() )
			{
				$str = gw_stripslashes($str);
			}
			else
			{
				$str = gw_stripslashes($str, 'light');
			}
			$isFirst = true;
		}
	}
}
/**
 * Replacement for addslashes()
 *
 * @param   string  $str String to convert
 * @param   string  $mode What mode to use to add slashes.
 *                        [ hard - internal by PHP | light - custom ], 06 may 2003
 * @return  string  Slashed string
 */
function gw_addslashes($str, $mode = 'hard')
{
	if ($mode == 'hard')
	{
		$str = addslashes($str);
	}
	$str = str_replace('_', '\\_', $str);
	$str = str_replace('%', '\\%', $str);
	return $str;
}
/**
 * Replacement for stripslashes()
 *
 * @param   string  $str String to convert
 * @param   string  $mode What mode to use to strip slashes.
 *                        [ hard - internal by PHP | light - custom ], 06 may 2003
 * @return  string  Stripped slashes string
 */
function gw_stripslashes($str, $mode = 'hard')
{
	if ($mode == 'hard')
	{
		$str = stripslashes($str);
	}
	$str = str_replace('\\_', '_', $str);
	$str = str_replace('\\%', '%', $str);
	if (ini_get('magic_quotes_sybase') && stripslashes("''") == "''")
	{
		$str = str_replace('\'\'', '\'', $str);
	}
	return $str;
}
/* */
function gw_stripslashes_array(&$ar)
{
	if (is_array($ar) || is_object($ar))
	{
		for (reset($ar); list($k, $v) = each($ar);)
		{
			$ar[$k] = gw_stripslashes($v);
		}
		reset($ar);
	}
}
/**
 * Every value for SQL-query should be passed through this function
 */
function gw_text_sql($t)
{
	if (is_array($t) || is_object($t))
	{
		array_walk($t, 'gw_text_sql');
	}
	else
	{
	$t = gw_addslashes($t);
	/* when magic_quotes_sybase is ON
	   it completely overrides magic_quotes_gpc
	   but this function should add slashes anyway */
	if (ini_get('magic_quotes_sybase'))
	{
		$t = str_replace("''", "'", $t);
		$t = str_replace('\\_', '_', $t);
		$t = str_replace('\\%', '%"', $t);
		$t = str_replace('\\', '\\\\', $t);
		$t = str_replace('\'', '\\\'', $t);
		$t = str_replace('"', '\\"', $t);
		$t = str_replace('_', '\\_', $t);
		$t = str_replace('%', '\\%"', $t);
	}
	}
	return $t;
}
/**
 * Normalizes new line character. Recursive, calls by reference.
 * Note: Windows - CRLF, *nix - LF, Mac - CR
 *
 * @param string $str String to normalize
 */
function gw_fix_newline(&$str)
{
	if (is_array($str) || is_object($str))
	{
		array_walk($str, 'gw_fix_newline');
	}
	else
	{
		$str = preg_replace("/(\r\n|\n|\r)/", CRLF, trim($str));
	}
}
/**
 * Safely redirects to another location. Replacement for header()
 *
 * @param string $url Resourse locator name
 * @param int    $isDebug [ 0 - silent | 1 - do not redirect and print URL ]
 */
function gwtk_header($url, $isDebug = 0)
{
	global $db;
	/* fixes */
	$url = str_replace("&amp;", "&", $url);
	if (!empty($db)){ $db->close(); }
	if ($isDebug || headers_sent())
	{
		print '<br />Location:<br />' . sprintf('<a href="%s">%s</a>', $url, $url);
		exit;
	}
	if (@preg_match('/Microsoft|WebSTAR|Xitami/', getenv('SERVER_SOFTWARE')))
	{
		exit( header('Refresh: 0; URL=' . $url) );
	}
	/* redirect */
	if (!preg_match("/cgi/", php_sapi_name()))
	{
		@header('Status: 301 Moved Permanently');
	}
	header('Location: ' . $url);
	exit;
}
/**
 * Detects user IP
 *
 * @return  string   IP-address
 */
function gwGetRemoteIp()
{
	$HTTP_X_FW = getenv("HTTP_X_FORWARDED_FOR");
	$HTTP_RA = getenv("REMOTE_ADDR");
	$arIana = array("127.0.", "192.168.", "1.", "0.", "10.", "172.16.", "224.", "240.");
	if ($HTTP_X_FW != '')
	{
		for (reset($arIana); list($k, $v) = each($arIana);) // check values
		{
			if (preg_match("/^" . $v . "/", $HTTP_X_FW) ||
				!preg_match("/^([0-9]{1,3}\.){3,3}[0-9]{1,3}$/", $HTTP_X_FW))
			{
				return $HTTP_RA;
			}
		}
		$HTTP_RA = $HTTP_X_FW;
	}
	if (HTTP_HOST == $HTTP_RA)
	{
		$HTTP_RA = '127.0.0.1';
	}
	return $HTTP_RA;
}
/* --------------------------------------------------------
 * Other useful functions
 * ----------------------------------------------------- */
class gw_functions {


	function js_addslashes($t)
	{
		return str_replace(array('\\', '\'', "\n", "\r") , array('\\\\', "\\'","\\n", "\\r") , $t);
	}
	/* */
	function text_htmlspecialchars($t)
	{
		return str_replace(array('&', '<', '>', '"'), array('&amp;', '&lt;', '&gt;', '&quot;'), $t);
	}
	/* */
	function text_unhtmlspecialchars($t)
	{
		return str_replace(array('&lt;', '&gt;', '&quot;', '&amp;'), array('<', '>', '"', '&'), $t);
	}
	/**
	 *
	 */
	function text_crc_unsigned($t, $pass = '')
	{
		return sprintf("%010u", crc32($pass.$t));
	}
	/**
	 * Convert HTML-code into Javascript using document.write()
	 *
	 * @param   string Text to output
	 * @return  Javascript code
	 */
	function text_html2js($t)
	{
		$t = '<script type="text/javascript">/*<![CDATA[*/'
			. 'document.write(\''
			. str_replace("'", "\'", $t)
			. '\');/*]]>*/</script>';
		return $t;
	}
	/**
	 * Generates random string. Better characters` strength,
	 * two groups of illegal symbols excluded.
	 *
	 * @param    int    $maxchar  Maximum generated string length
	 * @param    int    $nChar    Character set, 22 Oct 2003, n = numbers, uc = uppercase, lc = lowercase
	 *                            [0 = all | 1 - n | 2 - lc | 3 - uc | 4 - n+lc | 5 - lc+uc ]
	 * @param    string $first    First character for returned string
	 * @return   string Generated text
	 */
	function text_make_uid($maxchar = 8,  $nChar = 0, $first = '')
	{
		/* Exclude bad symbols. */
		/* 1st bad group: 0, 1, l, I */
		/* 2nd bad group: a, c, e, o, p, x, A, C, E, H, O, K, M, P, X */
		$str = "";
		$charN = '23456789';
		$charL = 'bdfghijkmnqrstuvwyz';
		$charU = 'QWRYUSDFGJLZVN';
		$charN = ($nChar == 1)
				? $charN
				: (($nChar == 2) ? $charL
					: (($nChar == 3) ? $charU
					: ($nChar == 4) ? $charN.$charL
					: ($nChar == 5) ? $charL.$charU
					: $charN.$charL.$charU)
				);
		$len = strlen($charN);
		mt_srand( (double) microtime()*1000000);
		for ($i = 0; $i < $maxchar; $i++)
		{
			$sed = mt_rand(0, $len-1);
			$str .= $charN[$sed];
		}
		$str = $first . substr($str, 0, strlen($str) - strlen($first));
		return $str;
	}

	/**
	 * Returns correct number format in HTML-code.
	 * For example, English notation:
	 * 1,234.56
	 * French (also Russian and many others) notation:
	 * 1 234,56
	 * This function also fixes problem with HTML-code
	 * occured by space in every group of thousands (French notation).
	 *
	 * @param   integer $int   numbers
	 * @param   integer $dec   decimals
	 * @return  string  complete HTML-code
	 */
	function number_format($int, $dec = 0, $ar = array('decimal_separator'=> '.', 'thousands_separator'=> ' '))
	{
		return str_replace(' ', '&#160;',
					number_format($int, $dec, $ar['decimal_separator'], $ar['thousands_separator'] )
				);
	}

	/**
	 * Get a random number
	 * @return  float  Random number
	 */
	function make_seed()
	{
		list($usec, $sec) = explode(' ', microtime());
		return (float) $sec + ((float) $usec * 100000);
	}

	/**
	 * Executes php-code.
	 *
	 * @param   string  $filename Full path to filename
	 * @param   int     $is_db_restart Re-connect to database. Useful when included script connects to another database.
	 * @return  string  File results
	 */
	function file_exe_contents($filename, $is_db_restart = 1)
	{
		$str = '';
		if (file_exists($filename))
		{
			ob_start();
			include($filename);
			$str_return = ob_get_contents();
			ob_end_clean();
			if ($is_db_restart)
			{
				global $oDb;
				$oDb = new gwtkDb;
			}
			return $str_return;
		}
		else
		{
			return '[loadfile: file '. $filename . ' does not exist]';
		}
	}

	/**
	 * Get file contents. Binary and fail safe.
	 *
	 * @param   string  $filename Full path to filename
	 * @return  string  File contents
	 */
	function file_get_contents($filename)
	{
		if (!file_exists($filename))
		{
			return '[file_get_contents: file '. $filename . ' does not exist]';
		}
		if (function_exists('file_get_contents')) /* PHP4 CVS only */
		{
			$str = file_get_contents($filename);
		}
		else
		{
			/* file() is binary safe from PHP 4.3.0
			faster: $str = implode('', file($filename));
			*/
			$fd = fopen($filename, "rb");
			$str = fread($fd, filesize($filename));
			fclose($fd);
		}
		if ($str == '')
		{
			return '[loadfile: ' . $filename. ' is empty]';
		}
		if (get_magic_quotes_runtime()) /* remove slashes, 23 march 2002 */
		{
			$str = stripslashes($str);
		}
		return $str;
	}
	/**
	 * Put contents into a file. Binary and fail safe.
	 *
	 * @param   string  $filename Full path to filename
	 * @param   string  $content File contents
	 * @param   string  $mode [ w = write new file (default) | a = append ]
	 * @return  TRUE if success, FALSE otherwise
	 */
	function file_put_contents($filename, $content, $mode = "w")
	{
		$filename = str_replace('\\', '/', $filename);
		/* new file */
		if (!file_exists($filename))
		{
			/* check & create directories first */
			$arParts = explode('/', $filename);
			$intParts = (sizeof($arParts) - 1);
			$d = '';
			for ($i = 0; $i < $intParts; $i++)
			{
				$d .= $arParts[$i] . '/';
				if (is_dir($d))
				{
					continue;
				}
				else
				{
					$oldumask = umask(0);
					@mkdir($d, 0777);
					@chmod($d, 0777);
					umask($oldumask);
				}
			}
			/* Nothing to write */
			if ($content == '')
			{
				return true;
			}
			/* Write to file */
			$fp = @fopen($filename, "wb");
			@chmod($filename, 0777);
			if ($fp)
			{
				fputs($fp, $content);
			}
			else
			{
				return false;
			}
			fclose($fp);
		}
		else
		{
			/* Append to file */
			/* note: binary mode is transparent */
			if ($fp = @fopen($filename, $mode.'b'))
			{
				$is_allow = flock($fp, 2); /* lock for writing & reading */
				if ($is_allow)
				{
					fputs($fp, $content, strlen($content));
				}
				flock($fp, 3); /* unlock */
				fclose($fp);
			}
			else
			{
				return false;
			}
		}
		return true;
	}
	/**
	 * Removes file from disk
	 */
	function file_remove_f($filename)
	{
		if (file_exists($filename) && is_file($filename) && unlink($filename))
		{
			return true;
		}
		return false;
	}
	/**
	 * Makes string wrapped, multibyte. (p) 1999
	 *
	 * @param   string  $str A string to wrap
	 * @param   int     $len Maximum length, characters
	 * @param   string  $d Delimiter, default is "\n"
	 * @param   int     $isBinary [ 0 - off | 1 - use binary-safe convertion ]
	 * @return  string  Parsed string
	 * @see mb_strlen(), mb_substr()
	 */
	function hardWrap($str, $len, $d = "\n", $isBinary = 0)
	{
		$arr = array();
		/* return empty string, 31 march 2003 */
		if ($len < 0) { return $str; };
		if ($isBinary)
		{
			$slen = strlen($str);
		}
		else
		{
			$slen = $this->mb_strlen($str);
		}
		for ( $i = 0; $i <= (ceil($slen/$len) - 1); $i++ )
		{
			if ($isBinary)
			{
				$arr[$i] = substr($str, ($len * $i), $len);
			}
			else
			{
				$arr[$i] = $this->mb_substr($str, ($len * $i), $len);
			}
		}
		return implode($d, $arr);
	}
	/**
	 * Converts a string with e-mail address
	 * into unresolvable crap for mail robots.
	 *
	 * @param   string  $s String with HTML-tag <a href="mailto:">
	 * @return  string  Parsed string
	 * @see hardWrap()
	 */
	function text_mailto($s)
	{
		preg_match_all("/href=\"mailto:(.*?)\">(.*?)<\/a>/", $s, $e);
		/* encode `mailto:' */
		if (isset($e[1][0]))
		{
			$s = str_replace($e[1][0], '', $s);
			$s = str_replace(
						'href="mailto:',
						'href="mailto:'.$this->text_make_uid(rand(2,8), 2).'@'.$this->text_make_uid(rand(2,8), 2).'.com" onmouseover="this.href=\''
						. $this->hardWrap('mailto:' . strtolower($e[1][0]), rand(2,4), "'+'" )
						. "'", $s);
			return $s;
		}
	}
	/**
	 * Coverts a string into sequence of hex values, \xNN
	 *
	 * @param    string  $t Text data
	 * @param    int     $is_x Print `\x' before a character
	 * @return   string  Hex value for string
	 */
	function text_utf2hex($t, $is_x = 1)
	{
		$str = '';
		$len = strlen($t);
		for ($i = 0; $i < $len; $i++)
		{
			$str .= ($is_x) ? '\x'.dechex(ord(substr($t, $i, 1))) : dechex(ord(substr($t, $i, 1)));
		}
		return $str;
	}
	/**
	 * Converts a CSS-file contents into one string
	 *
	 * @param    string  $t Text data
	 * @param    int     $is_debug Skip convertion
	 * @return   string  Optimized string
	 */
	function text_smooth_css($t, $is_debug = 0)
	{
		if ($is_debug) { return $t; }
		/* Remove comments */
		$t = preg_replace("/\/\*(.*?)\*\//" , ' ', $t);
		/* Remove new lines, spaces */
		$t = preg_replace("/(\s{2,}|\r\n|\n|\t|\r)/" , ' ', $t);
		/* Join rules */
		$t = preg_replace('/(,|;|:|{|}) /', '\\1', $t);
		$t = str_replace(' {' , '{', $t);
		/* Remove ; for the last attribute */
		$t = str_replace(';}' , '}', $t);
		$t = str_replace(' }' , '}', $t);
		return $t;
	}
	/**
	 * Converts a HTML-file contents into one string
	 *
	 * @param    string  $t Text data
	 * @param    int     $is_debug Skip convertion
	 * @return   string  Optimized string
	 * @globals  LF
	 */
	function text_smooth_html($t, $is_debug = 0)
	{
		/* Note that <pre>formatted text will be converted into single line too */
		if ($is_debug) { return $t; }
		/* Remove new lines and tabs */
		$t = preg_replace("/(\r\n|\n|\t|\r)/", ' ', $t);
		/* Remove comments */
		$t = preg_replace("/<!--(.*?)-->/si", '', $t);
		/* Connect HTML-tags */
		$t = str_replace('> </' , '></', $t);
		/* \s is not allowed for multibyte characters */
		$t = preg_replace("/ {2,}/" , ' ', $t);
		/* Place a newline character if any */
		$t = str_replace(LF, "\n", $t);
		return $t;
	}
	/**
	 * Automatic height for textarea in HTML-forms
	 *
	 * @param    string  $v Text data
	 * @return   int     Number of lines
	 * @see mb_strlen()
	 */
	function getFormHeight($v, $int_max = 25)
	{
		preg_match_all("/\n/", $v, $vLines);
		$n = intval($this->mb_strlen($v) / 70) + count($vLines[0]) + 2;
		if ($n > $int_max) { $n = $int_max; }
		return $n;
	}
	/*
		Calculates the number of years passed from a date.
	*/
	function date_get_passed_y($time_unix, $y, $m, $d)
	{
		/* 2678400 is number of seconds in month */
		$years = gmdate("Y", $time_unix) - $y;
		if (gmdate("m", $time_unix + 2678400) < $m)
		{
			$years--;
		}
		if ((gmdate("m", $time_unix + 2678400) == $m)
			&& ($d < intval(gmdate("d", $time_unix))) )
		{
			$years--;
		}
		return $years;
	}
	/**
	 * Get current time with GMT offset
	 * @param float $gmt_offset GMT offset (+3 Moscow, -6 USA & Canada)
	 * @param int $is_use_dst Day time saving
	 */
	function date_get_localtime($gmt_offset, $is_use_dst = 1)
	{
		$r = $gmt_offset * 3600;
		if ($is_use_dst)
		{
			$r += 3600;
		}
		return time() + $r;
	}
	/**
	 * Converts date from `timestamp(14)' into `mktime()' format
	 *
	 * @param   string  $t Date in timestamp(14) format
	 * @return  int     Unixtime format
	 */
	function date_Ts14toTime($t)
	{
		$t = sprintf("%s", @mktime(substr($t,8,2),substr($t,10,2),substr($t,12,2),substr($t,4,2),substr($t,6,2),substr($t,0,4)));
		if ( $t < 0 ) { $t = 0; }
		return $t;
	}
	/**
	 * Converts seconds into readable time format
	 *
	 * @param   int     $totalsec Amount of seconds
	 * @return  string  Text pattern 00:00:00
	 */
	function date_SecToTime($totalsec)
	{
		$secH = intval($totalsec / 3600);
		$secMin = intval($totalsec / 60);
		$secSec = ($totalsec - ($secMin * 60));
		$secMin = $secMin - ($secH * 60);
		return sprintf("%02d:%02d:%02d", $secH, $secMin, $secSec);
	}
	/**
	 * Finds whether a variable is a positive integer number
	 *
	 * @param   int  $v Some string to check
	 * @return  TRUE if var is a number, FALSE otherwise.
	 */
	function is_num($v)
	{
		if ( preg_match("/^\d+$/", $v) )
		{
			return true;
		}
		return false;
	}
	/**
	 * Get string length, multibyte.
	 *
	 * @param   string  $t Any string content
	 * @return  int     String length
	 */
	function mb_strlen($t, $encoding = 'UTF-8')
	{
		/* --enable-mbstring */
		if (function_exists('mb_substr'))
		{
			return mb_strlen($t, $encoding);
		}
		else
		{
			return strlen(utf8_decode($t));
		}
	}
	/**
	 * Replacement for substr(), multibyte
	 * Returns the portion of $t specified by the $start and $end parameters.
	 *
	 * @param  string  $t String to substr
	 * @param  int     $start Start position, positive
	 * @param  int     $end End position, positive
	 * @param  string  $encoding Charset encoding [ UTF-8 (default) | windows-1251 | ISO-8859-1 ]
	 * @return string
	 */
	function mb_substr($t, $start = 0, $end = 0, $encoding = 'UTF-8')
	{
		/* --enable-mbstring */
		if (function_exists('mb_substr'))
		{
			return mb_substr($t, $start, $end, $encoding); /* hundred times faster, ~0.000382 */
		}
		$strD = '';
		$pos = $cntLetter = 0;
		$len = strlen($t);
		if ($end == 0)
		{
			$end = $len;
		}
		while ($pos < $len)
		{
			$charAt = substr($t, $pos, 1);
			$asciiPos = ord($charAt);
			$isConcat = (($cntLetter >= $start) && ($cntLetter < ($start + $end))) ? 1 : 0;
			if (($asciiPos >= 240) && ($asciiPos <= 255))
			{
				$char2 = substr($t, $pos, 4);
				$strD .= ($isConcat) ? $char2 : '';
				$cntLetter++;
				$pos += 4;
			}
			elseif (($asciiPos >= 224) && ($asciiPos <= 239))
			{
				$char2 = substr($t, $pos, 3);
				$strD .= ($isConcat) ? $char2 : '';
				$cntLetter++;
				$pos += 3;
			}
			elseif (($asciiPos >= 192) && ($asciiPos <= 223))
			{
				$char2 = substr($t, $pos, 2);
				$strD .= ($isConcat) ? $char2 : '';
				$cntLetter++;
				$pos += 2;
			}
			else
			{
				$strD .= ($isConcat) ? $charAt : '';
				$cntLetter++;
				$pos++;
			}
		}
		return $strD;
	}
	/**
	 * Get character position, multibyte.
	 *
	 * @param   string  $t Any string contents
	 * @param   string  $s Character to find
	 * @param   string  $encoding Charset encoding [ UTF-8 (default) | windows-1251 | ISO-8859-1 ]
	 * @return  int     String position
	 */
	function mb_strpos($t, $s, $encoding = 'UTF-8')
	{
		/* --enable-mbstring */
		if (function_exists('mb_strpos'))
		{
			return mb_strpos($t, $s, 0, $encoding);
		}
		else
		{
			/* convert $s character into something,
			   which will be not converted into question mark "?"
			   after parsing through utf8_decode() */
			$s_new = "\x01";
			$t = str_replace($s, $s_new, $t);
			return strpos(utf8_decode($t), $s_new);
		}
	}
	/**
	 * Detect UTF-8 encoding, multibyte.
	 *
	 * @param   string  $t Any string content
	 * @return  boolean TRUE if the string is UTF-8, FALSE otherwise
	 */
	function is_detect_utf8($t)
	{
		/* --enable-mbstring */
		if (function_exists('mb_detect_encoding') && @ini_get('mbstring.internal_encoding') == 'UTF-8')
		{
			return (mb_detect_encoding($t, 'UTF-8, GB2312, Windows-1251') == 'UTF-8') ? true : false;
		}
		else
		{
			$is_high = preg_match( '/[\x80-\xff]/', $t);
			return ($is_high ? preg_match( '/^([\x00-\x7f]|[\xc0-\xdf][\x80-\xbf]|' .
					'[\xe0-\xef][\x80-\xbf]{2}|[\xf0-\xf7][\x80-\xbf]{3})+$/', $t ) : true );
		}
	}
	/**
	 * Converts character encoding
	 *
	 * @param   string    $str The string encoded in $from encoding
	 * @param   string    $from Source encoding
	 * @param   string    $to Target encoding
	 * @return  string    Encoded string
	 * @todo    read xD3 (in xD0xD3)
	 */
	function gwConvertCharset($str, $from, $to)
	{
		/* Skip processing when two strings are the same, 6 jan 2003 */
		if ($from == $to)
		{
			return $str;
		}
		/* Process */
		if (function_exists('mb_convert_encoding'))
		{
			/* Some people have the same problem:
				http://bugs.php.net/bug.php?id=23470
				Text returned from mb_convert_encoding() and iconv()
				must be the same, but often it is not
				when only iconv() is correct.
			*/
			$result_mb = mb_convert_encoding($str, $to, $from);
			$result_iconv = @iconv($from, $to, $str);
			if ($result_mb != $result_iconv)
			{
				return $result_iconv;
			}
			return $result_mb;
		}
		elseif (function_exists('iconv'))
		{
			return iconv($from, $to, $str);
		}
		elseif (function_exists('recode_string')) /* Linux */
		{
			return recode_string($from . '..' . $to, $str);
		}
		else
		{
			print '<br />Error: function <b>iconv</b> not installed. Update your PHP version.';
			return $str;
		}
	}
	/**
	 * Page finder. Modified version.
	 * Thanks to Donald E.Knut (Fundamental Algorithms, Vol.I)
	 *
	 * @param    int   $m Total number of items
	 * @param    int   $n Number of items per page
	 * @return   int   Number of pages
	 */
	function math_divisor($m, $n)
	{
		if ($n == 0){ $n = 1; }
		$r = intval($m / $n);
		$m = $m - ($r * $n);
		$a = $r;
		if ($m > 0) { $a = $r + 1; }
		return $a;
	}
	/**
	 * Converts hex values into array with integer values
	 * @usage math_hexbg2ar('0F0');
	 * @usage math_hexbg2ar('EE4400');
	 */
	function math_hex2ar($t)
	{
		$t = str_replace('#', '', $t);
		/* convert short form into full form */
		if (strlen($t) == 3)
		{
			list($r, $g, $b) = sscanf($t, '%1s%1s%1s');
			$t = $r.$r.$g.$g.$b.$b;
		}
		return $this->str_split($t, 2);
	}
	/* Calculates factorial (a!) of a. */
	function math_fact($a)
	{
		$r = 1;
		for ($f = 1; $f <= $a; $f++)
		{
			$r = $f * $r;
		}
		return $r;
	}
	/**
	 * Fail-safe str_split() function
	 * PHP 5 CVS only
	 */
	function str_split($t, $length = 1)
	{
		if (function_exists('str_split'))
		{
			return str_split($t, $length);
		}
		return explode(':', wordwrap($t, $length, ':', 1));
	}
	/**
	 * Converts dotted IP-address (IPV4) into database storable format
	 */
	function ip2int($ip)
	{
		$a = explode('.',$ip);
		return ($a[0]*256*256*256 + $a[1]*256*256 + $a[2]*256 + $a[3]);
	}
	/**
	 * Converts IP-address (IPV4) from storable format into dotted
	 */
	function int2ip($i){
		$d[0]= intval($i/256/256/256);
		$d[1]= intval(($i-$d[0]*256*256*256)/256/256);
		$d[2]= intval(($i-$d[0]*256*256*256-$d[1]*256*256)/256);
		$d[3]=$i-$d[0]*256*256*256-$d[1]*256*256-$d[2]*256;
		return $d[0].'.'.$d[1].'.'.$d[2].'.'.$d[3];
	}

	/**
	 * Create a GZip-compressed string
	 *
	 * @param  string $t Input data
	 * @param  int    $level Gzip compress level [1..9], 1 by default.
	 * @param  int    $is_send_header Use headers class [1 - yes | 0 - no]
	 * @return string GZipped text
	 * @globals  $_SERVER, $oHdr, PHP_VERSION_INT
	 */
	function text_gzip($str_return, $level = 1, $is_send_header = 1)
	{
		global $_SERVER, $oHdr;
		$int_length = strlen($str_return);
		$encoding = 0;
		if (function_exists('crc32') && function_exists('gzcompress'))
		{
			/* strpos() should be always compared as boolean */
			if (strpos(' ' . $_SERVER['HTTP_ACCEPT_ENCODING'], 'x-gzip') !== false)
			{
				$encoding = 'x-gzip';
			}
			elseif (strpos(' ' . $_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip') !== false)
			{
				$encoding = 'gzip';
			}
			if ($encoding)
			{
				if (function_exists('gzencode') && PHP_VERSION_INT > 40200)
				{
					$str_return = gzencode($str_return, $level);
				}
				else
				{
					$size = strlen($str_return);
					$crc = crc32($str_return);
					$str_return = "\x1f\x8b\x08\x00\x00\x00\x00\x00\x00\xff";
					$str_return .= substr(gzcompress($str_return, $level), 2, -4);
					$str_return .= pack('V', $crc);
					$str_return .= pack('V', $size);
				}
				if ($is_send_header)
				{
					$oHdr->add('Content-Encoding: ' . $encoding);
					$oHdr->add('Content-Length: ' . strlen($str_return));
				}
			}
		}
		return $str_return;
	}
}
$tmp['mtime'] = explode(' ', microtime());
$tmp['endtime'] = (float)$tmp['mtime'][1] + (float)$tmp['mtime'][0];
$tmp['time'][__FILE__] = ($tmp['endtime'] - $tmp['start_time']);
/* automatic initialization */
$oFunc = new gw_functions;

?>