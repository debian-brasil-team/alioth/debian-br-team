<?php
/**
 * Glossword2
 * � 2003-2004 Dmitry N. Shilnikov <dev at glossword.info>
 * http://glossword.info/
 */
/**
 *  CSS loader
 *
 *  @version $Id: css.php,v 1.1 2004/07/06 14:21:33 yrtimd Exp $
 */
define('IN_GW', 1);
/* Load config */
include_once('/etc/glossword/db_config.php');

$sys['path_include'] = 'inc';
$sys['id_prepend'] = 0;
include_once($sys['path_include'].'/config.inc.php');

/* Constants */
define('CRLF', "\r\n");
define('LF', "\\n\\");
define('GW_TIME_NOW_UNIX', mktime());
define('GW_TIME_NOW', date("YmdHis", GW_TIME_NOW_UNIX));
define('REQUEST_URI', isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI']
					: ((getenv('REQUEST_URI!') != '') ? getenv('REQUEST_URI')
					: ((isset($HTTP_SERVER_VARS['QUERY_STRING']) && $HTTP_SERVER_VARS['QUERY_STRING'] != '') ? ($HTTP_SERVER_VARS['PHP_SELF'] . '?' . $HTTP_SERVER_VARS['QUERY_STRING'])
					: $_ENV['PHP_SELF'])));
define('REMOTE_UA', trim(substr(getenv('HTTP_USER_AGENT'), 0, 256)));

include_once($sys['path_gwlib'].'/class.globals.php');
include_once($sys['path_gwlib'].'/class.func.php');
include_once($sys['path_gwlib'].'/class.headers.php');
include_once($sys['path_gwlib'].'/class.timer.php');
include_once($sys['path_gwlib'].'/class.logwriter.php');

$_SERVER['HTTP_ACCEPT_ENCODING'] = isset($_SERVER['HTTP_ACCEPT_ENCODING']) ? $_SERVER['HTTP_ACCEPT_ENCODING'] : (isset($_SERVER['HTTP_TE']) ? $_SERVER['HTTP_TE'] : '');

$tmp['arPhpVer'] = explode('.', PHP_VERSION);
define('PHP_VERSION_INT', intval(sprintf('%d%02d%02d', $tmp['arPhpVer'][0], $tmp['arPhpVer'][1], $tmp['arPhpVer'][2])));
/* --------------------------------------------------------
 * Additional functions
 * ----------------------------------------------------- */
function gw_text_tpl_compile($t = '', $ar = array())
{
	$arCmd = array();
	/* Search for template tags */
	$preg = "/({)([ A-Za-z0-9:\/\-_]+)(})/i";
	if (preg_match_all($preg, $t, $tmp['tpl_matches']))
	{
		while (list($k, $cmd_src) = each($tmp['tpl_matches'][2]))
		{
			$arCmd[] = $tmp['tpl_matches'][1][$k].$cmd_src.$tmp['tpl_matches'][3][$k];
			$tmp['cmd'] = trim($cmd_src);
			$tmp['cmd'] = isset($ar[$tmp['cmd']]) ? $ar[$tmp['cmd']] : '';
			$arRpl[] = $tmp['cmd'];
		}
		/* replaces variables only */
		$t = str_replace($arCmd, $arRpl, $t);
	}
	return $t;
}
/* --------------------------------------------------------
 * Register global variables
 * ----------------------------------------------------- */
$gv['vars'] = $oGlobals->register(array('t','is_host','is_gzip', 'dir'));
$gv['vars']['t'] = preg_replace("/[^a-zA-Z0-9_\.]/", '', $gv['vars']['t']);
$oGlobals->do_default($gv['vars']['is_host'], 1);
$oGlobals->do_default($gv['vars']['is_gzip'], $sys['is_use_gzip']);

/* --------------------------------------------------------
 * Variables used in CSS style
 * ----------------------------------------------------- */
$arPairsV = array(
		'v:path_css' => $sys['server_dir'].'/'.$sys['path_tpl'].'/'.$gv['vars']['t'],
		'v:path_img' => $sys['server_dir'].'/'.$sys['path_img']
);
$sys['css_align_right'] = 'right';
$sys['css_align_left'] = 'left';
if ($gv['vars']['dir'] == 'rtl')
{
	$sys['css_align_right'] = 'left';
	$sys['css_align_left'] = 'right';
}
$sys['path_img_full'] = $sys['server_proto'] . $sys['server_host'] .'/'. $sys['server_dir'] .'/'. $sys['path_img'];
$sys['path_img_ph'] = $sys['path_img'];
$sys['path_img'] = $sys['server_dir'] .'/'. $sys['path_img'];
include_once($sys['path_tpl'].'/'.$gv['vars']['t'].'/theme.inc.php');
$arPairsV = array_merge($arPairsV, $theme );
$arPairsV = array_merge($arPairsV, $sys );

/* --------------------------------------------------------
 * Parse CSS-file contents
 * ----------------------------------------------------- */
$gv['vars']['t'] = $sys['path_tpl'].'/'.$gv['vars']['t'].'/style.css';
$gv['vars']['css_contents'] = '';
$sys['date_modified_u'] = time();

if (file_exists($gv['vars']['t']))
{
	$sys['date_modified_u'] = filectime($gv['vars']['t']);
	$gv['vars']['css_contents'] = $oFunc->file_get_contents($sys['path_tpl'].'/common/gw_tags.css');
	$gv['vars']['css_contents'] .= $oFunc->file_get_contents($gv['vars']['t']);
	$gv['vars']['css_contents'] = gw_text_tpl_compile($gv['vars']['css_contents'], $arPairsV);
	$gv['vars']['css_contents'] = $oFunc->text_smooth_css($gv['vars']['css_contents'], 0);
}
/* --------------------------------------------------------
 * Send Expires
 * ----------------------------------------------------- */
if (!$gv['vars']['is_host'])
{
	/* enable caching for non-localhost */
	$oHdr->add("Expires: " . gmdate("D, d M Y H:i:s", ($sys['date_modified_u'] + 43200)) . " GMT");
	$oHdr->add("Last-Modified: " . gmdate("D, d M Y H:i:s", $sys['date_modified_u']) . " GMT");
}
$oHdr->add('Content-Type: text/css; charset=UTF-8');
/* --------------------------------------------------------
 * GZip compression
 * ----------------------------------------------------- */
if ($gv['vars']['is_gzip'])
{
	/* Start time */
	if ($sys['is_LogGZip'])
	{
		$arLogGzip = array();
		$oLog = new gw_logwriter($sys['path_logs']);
		$arLogGzip[1] = strlen($gv['vars']['css_contents']);
		$arLogGzip[3] = REMOTE_UA;
		$oTimer = new gw_timer('gzip');
	}
	/* requires $oHdr */
	$gv['vars']['css_contents'] = $oFunc->text_gzip($gv['vars']['css_contents'], 9);
	/* End time */
	if ($sys['is_LogGZip'])
	{
		$arLogGzip[0] = sprintf("%1.5f", $oTimer->end());
		$arLogGzip[2] = strlen($gv['vars']['css_contents']);
		if ($arLogGzip[1] != $arLogGzip[2])
		{
			$oFunc->file_put_contents($oLog->get_filename('gzip'), $oLog->make_str($arLogGzip), 'a');
		}
	}
}
$oHdr->output();
print $gv['vars']['css_contents'];

/* end of file */
?>