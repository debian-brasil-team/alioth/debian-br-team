<?php
/**
 *  DO NOT EDIT. Alteration of this file may cause the Glossword to malfunction.
 *  For any changes, please, contact translators.
 *  Translation file generated by Translation Kit (http://glossword.info/tkit/)
 *  © 2002-2004 Dmitry Shilnikov <dev at glossword.info>
 *  ------------------------------------------------------
 *  Project  : Glossword
 *  Topic    : Action names (to do)
 *  Language : Russian
 *  Charset  : UTF-8
 *  Phrases  : 51
 *  Location : locale/ru-utf8/actions.php
 *  Created  : 14 Nov 2004, 14:11
 *  ------------------------------------------------------
 *  Translation created by:
 *      - Dmitry <dev at glossword.info>
 */
$lang['1036'] = 'Отправить';
$lang['1035'] = 'Пред. просмотр';
$lang['1007'] = 'Получить HTML-код формы поиска';
$lang['1006'] = 'Очистить кэш поисковых фраз';
$lang['1023'] = 'Вы уверены чтобы очистить кэш?';
$lang['1004'] = 'Использование кэш';
$lang['1003'] = 'Пересчитать количество терминов для каждого словаря';
$lang['1002'] = 'Проверить на незадействованные ключевые слова';
$lang['1001'] = 'Отправить сообщение в техническую поддержку Glossword';
$lang['3_maintenance'] = 'Обслуживание';
$lang['page_refresh'] = 'Обновить страницу';
$lang['2_continue'] = 'Продолжить';
$lang['3_property'] = 'Свойства';
$lang['3_change'] = 'Сменить';
$lang['3_logout'] = 'Выход';
$lang['3_send'] = 'Отправить';
$lang['3_add_term'] = 'Добавить термин';
$lang['3_next'] = 'Далее';
$lang['3_profile'] = 'Профиль';
$lang['3_register'] = 'Регистрация';
$lang['3_import'] = 'Импорт';
$lang['3_clean'] = 'Очистить';
$lang['3_add_dict'] = 'Создать словарь';
$lang['3_browse'] = 'Список';
$lang['3_cancel'] = 'Отменить';
$lang['3_save'] = 'Сохранить';
$lang['1_prevd'] = 'Предыдущие %d';
$lang['1_nextd'] = 'Следующие %d';
$lang['1_prevpage'] = 'Пред.';
$lang['3_srch_submit'] = 'Поиск';
$lang['3_reset'] = 'Сброс';
$lang['3_down'] = 'Вниз';
$lang['3_up'] = 'Вверх';
$lang['3_more'] = 'Ещё';
$lang['3_export'] = 'Экспорт';
$lang['3_add_subtopic'] = 'Добавить тему';
$lang['3_create'] = 'Создать';
$lang['3_edit'] = 'Редактировать';
$lang['3_remove_topic'] = 'Удалить тему';
$lang['3_remove_term'] = 'Удалить термин';
$lang['3_remove_defn'] = 'Удалить определение';
$lang['3_remove_trns'] = 'Удалить перевод';
$lang['3_remove_abbr'] = 'Удалить аббревиатуру';
$lang['3_remove'] = 'Удалить';
$lang['3_add_topic'] = 'Создать тему';
$lang['3_add_defn'] = 'Добавить определение';
$lang['3_add_trns'] = 'Добавить перевод';
$lang['3_add_abbr'] = 'Добавить аббревиатуру';
$lang['3_add'] = 'Добавить';
$lang['3_cfg'] = 'Конфигурация';
$lang['3_update'] = 'Обновить';
/**
 * The end of translation file.
 */
?>