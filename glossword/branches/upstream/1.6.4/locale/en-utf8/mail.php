<?php
/**
 *  DO NOT EDIT. Alteration of this file may cause the Glossword to malfunction.
 *  For any changes, please, contact translators.
 *  Translation file generated by Translation Kit (http://glossword.info/tkit/)
 *  © 2002-2004 Dmitry Shilnikov <dev at glossword.info>
 *  ------------------------------------------------------
 *  Project  : Glossword
 *  Topic    : Mail messages
 *  Language : English
 *  Charset  : UTF-8
 *  Phrases  : 4
 *  Location : locale/en-utf8/mail.php
 *  Created  : 14 Nov 2004, 14:11
 *  ------------------------------------------------------
 *  Translation created by:
 *      - Dmitry <dev at glossword.info>
 *      - Лхагвасүрэнгийн Хүрэлбаатар <hujii247 at yahoo.com>
 */
$lang['mail_newuser_subj'] = 'Welcome to {SITENAME}';
$lang['mail_newpass_subj'] = 'New password activation';
$lang['mail_newuser'] = '<p>Hello {USERNAME}.</p>
<p>Please keep this email for your records. Your account information:</p>
<p>{LOGIN}: <b>{U_LOGIN}</b> <br />{PASSWORD}: <b>{U_PASSWORD}</b></p>
<p>Use the following URL to log in: <br/>{LOGIN_URL}</p>
<p>If you have any questions, contact to {CONTACT_EMAIL}.</p>';
$lang['mail_newpass'] = '<p>Hello {USERNAME}</p> <p>You have requested a new password be sent for your account on {SITENAME}.</p> <p>To use the new password click the link provided below: <br/> {U_ACTIVATE}</p>';
/**
 * The end of translation file.
 */
?>