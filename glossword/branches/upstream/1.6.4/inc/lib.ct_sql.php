<?php

##
## Copyright (c) 1998-2000 NetUSE AG
##                    Boris Erdmann, Kristian Koehntopp
##
## Copyright (c) 1998-2000 Sascha Schumann <sascha@schumann.cx>
## 
## $Id: lib.ct_sql.php,v 1.5 2004/03/16 20:57:14 yrtimd Exp $
##
## PHPLIB Data Storage Container using a SQL database
##


	/**
	 * Converts IP-address from storable format into dotted
	 */
	function ip2int($ip)
	{
		$a = explode('.',$ip);
		return ($a[0]*256*256*256 + $a[1]*256*256 + $a[2]*256 + $a[3]);
	}
	/**
	 * Converts dotted IP-address into database storable format
	 */
	function int2ip($i){
		$d[0]= intval($i/256/256/256);
		$d[1]= intval(($i-$d[0]*256*256*256)/256/256);
		$d[2]= intval(($i-$d[0]*256*256*256-$d[1]*256*256)/256);
		$d[3]=$i-$d[0]*256*256*256-$d[1]*256*256-$d[2]*256;
		return $d[0].'.'.$d[1].'.'.$d[2].'.'.$d[3];
	}
	

class CT_Sql {
  ##
  ## Define these parameters by overwriting or by
  ## deriving your own class from it (recommened)
  ##
    
  var $database_table = "active_sessions";
  var $database_class = "gwtkDataBase";
  var $database_lock_semaphore = "";
  var $user_ip;
  var $encoding_mode = "slashes";

  ## end of configuration

  var $db;
   
  function ac_start() {
    $name = $this->database_class;
    $this->db = new $name;
  }

  function ac_get_lock() {
    if ( "" != $this->database_lock_semaphore ) {
      $query = sprintf("SELECT get_lock('%s')", $this->database_lock_semaphore);
      while ( ! $this->db->query($query)) {
        $t = 1 + time(); while ( $t > time() ) { ; }
      }
    }
  }

  function ac_release_lock() {
    if ( "" != $this->database_lock_semaphore ) {
      $query = sprintf("SELECT release_lock('%s')", $this->database_lock_semaphore);
      $this->db->query($query);
    }
  }

  function ac_gc($gc_time, $name) {
    $timeout = time();
    $sqldate = date("YmdHis", $timeout - ($gc_time * 60));
    $this->db->query(sprintf("DELETE FROM %s WHERE changed < '%s' AND name = '%s'",
                    $this->database_table, 
                    $sqldate,
                    gw_addslashes($name)));
    }

  function ac_store($id, $name, $str) {
    $ret = true;

    switch ( $this->encoding_mode ) {
      case "slashes":
        $str = gw_addslashes($name . ":" . $str);
      break;

      case "base64":
      default:
        $str = base64_encode($name . ":" . $str);
    };

    $name = gw_addslashes($name);

    ## update duration of visit
    global $HTTP_REFERER, $HTTP_USER_AGENT;

    $now = date("YmdHis", time());
    $uquery = sprintf("UPDATE %s SET val='%s', changed='%s' WHERE sid='%s' AND name='%s' AND ip='%s'",
      $this->database_table,
      $str,
      $now,
      $id,
      $name,
      ip2int($this->user_ip));
    $squery = sprintf("SELECT count(*) FROM %s WHERE val='%s' AND changed='%s' AND sid='%s' AND name='%s' AND ip='%s'",
      $this->database_table,
      $str,
      $now,
      $id,
      $name,
      ip2int($this->user_ip));
    $iquery = sprintf("INSERT INTO %s (sid, name, val, changed, ip) VALUES ('%s', '%s', '%s', '%s', '%s')",
      $this->database_table,
      $id,
      $name,
      $str,
      $now,
      ip2int($this->user_ip));

    $this->db->query($uquery);

    # FIRST test to see if any rows were affected.
    #   Zero rows affected could mean either there were no matching rows
    #   whatsoever, OR that the update statement did match a row but made
    #   no changes to the table data (i.e. UPDATE tbl SET col = 'x', when
    #   "col" is _already_ set to 'x') so then,
    # SECOND, query(SELECT...) on the sid to determine if the row is in
    #   fact there,
    # THIRD, verify that there is at least one row present, and if there
    #   is not, then
    # FOURTH, insert the row as we've determined that it does not exist.
	
	
    if ( $this->db->affected_rows() == 0
        && $this->db->query($squery)
        && $this->db->next_record() && $this->db->r(0) == 0
        && !$this->db->isDuplicate($iquery) 
        )
        {
            $ret = false;
        }
        
    return $ret;
  }

  function ac_delete($id, $name) {
    $this->db->query(sprintf("DELETE FROM %s WHERE name = '%s' AND sid = '%s' AND ip='%s'",
      $this->database_table,
      gw_addslashes($name),
      $id,
      ip2int($this->user_ip)));
  }

  function ac_get_value($id, $name) {
    $this->db->query(sprintf("SELECT val FROM %s WHERE sid  = '%s' AND name = '%s' AND ip='%s'",
      $this->database_table,
      $id,
      gw_addslashes($name),
      ip2int($this->user_ip)));
	
    if ($this->db->next_record()) {

      $str  = $this->db->r("val");
      $str2 = base64_decode( $str );
	
      if ( ereg("^".$name.":.*", $str2) ) {
         $str = ereg_replace("^".$name.":", "", $str2 );
      } else {

		$str3 = $str;
		if (get_magic_quotes_runtime())
		{
        	$str3 = gw_stripslashes( $str );
        }

        if ( ereg("^".$name.":.*", $str3) ) {
          $str = ereg_replace("^".$name.":", "", $str3 );
        } else {

          switch ( $this->encoding_mode ) {
            case "slashes":
              $str = gw_stripslashes($str);
            break;

            case "base64":
            default:
              $str = base64_decode($str);
          }
        }
      };
      return $str;
    };
    return "";
  }

  function ac_newid($str, $name) {
    return $str;
  }

  function ac_halt($s) {
    $this->db->halt($s);
  }
}

class s_sql extends CT_Sql
{
	var $database_class = "gwtkDb"; ## Which database to connect...
	var $database_table = TBL_SESS; ## and find our session data in this table.
	/* init */
	function s_sql()
	{
		global $sys;
		if ($sys['is_check_ip'])
		{
			$this->user_ip = REMOTE_IP;
		}
		else
		{
			$this->user_ip = '127.0.0.2';
		}
	}
}

?>
