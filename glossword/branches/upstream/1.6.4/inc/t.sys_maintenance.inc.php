<?php
if (!defined('IN_GW'))
{
	die('<!-- $Id: t.sys_maintenance.inc.php,v 1.1 2004/11/13 12:30:09 yrtimd Exp $ -->');
}
/**
 *  Glossword - glossary compiler (http://glossword.info/dev/)
 *  � 2002-2004 Dmitry N. Shilnikov <dev at glossword.info>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  (see `glossword/support/license.html' for details)
 */
/* Check permission */
if (!$auth->have_perm('admin', PERMLEVEL))
{
	return;
}
/* Common functions below */
function html_array_to_table_multi($ar, $is_print = 0)
{
	if (empty($ar)) { $ar = array(); }
	if (is_string($ar)) { $ar = array(array($ar)); }
	$str = '<table cellpadding="2" cellspacing="1" width="95%" border="0">';
	for (reset($ar); list($k, $arV) = each($ar);)
	{
		if (is_string($arV)) { $arV = array($arV); }
		$td_width = empty($arV) ? 1 : ceil(100 / sizeof($arV));
		$td_style = '';
		if ($k == 0)
		{
			$td_style = ' style="width:'.$td_width.'%"';
		}
		$str .= '<tr>';
		for (reset($arV); list($k2, $v2) = each($arV);)
		{
			$str .= '<td'. $td_style .'>'.  $v2 .'</td>';
		}
		$str .= '</tr>';
	}
	$str .= '</table>';
	if ($is_print)
	{
		print $str;
	}
	else
	{
		return $str;
	}
}
/* */
$oL =& $L;


/* */
if (!$gw_this['vars']['tid'])
{
	$ar_task[1] = $L->m('1001');
	$ar_task[2] = $L->m('1002');
	$ar_task[3] = $L->m('1003');
	$ar_task[4] = $L->m('1004');
#	$ar_task[5] = $L->m('1005');
#	$ar_task[6] = $L->m('1006');
	$ar_task[7] = $L->m('1007');
	$strR .= '<p class="r">'.$L->m('task_list').'</p>';
	$strR .= '<ul class="u">';
	for (reset($ar_task); list($k, $v) = each($ar_task);)
	{
		$strR .= sprintf('<li><p><a href="%s">%s</a></p></li>',
			append_url($sys['page_admin'].'?'.GW_ACTION.'='.A_MAINTENANCE.'&tid='.$k.'&'.GW_TARGET.'='.SYS),
			$v
		);
	}
	$strR .= '</ul>';
}
$tmp['funcname'] = 't.'.SYS.'_'.A_MAINTENANCE.'_'.$gw_this['vars']['tid'].'.inc.'.$sys['phpEx'];
if (file_exists($sys['path_include'].'/'.$tmp['funcname']))
{
	include_once($tmp['funcname']);
}
?>