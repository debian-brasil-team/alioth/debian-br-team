<?php
if (!defined('IN_GW'))
{
/*
	die('<!-- $Id: a.export.inc.php,v 1.10 2004/11/13 12:30:08 yrtimd Exp $ -->');
*/
}
/*
 *  Glossword - glossary compiler (http://glossword.info/dev/)
 *  � 2002-2004 Dmitry N. Shilnikov <dev at glossword.info>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  (see `glossword/support/license.html' for details)
 */
// --------------------------------------------------------
/**
 *  Export. External utility for a dictionary.
 *
 *   <line>
 *    <term>A Term</term>
 *    <defn><abbr>abbreviation</abbr>1st definition</defn>
 *    <defn><trns lang="104">translation</trns>2nd definition</defn>
 *   </line>
 *
 *  @version $Id: a.export.inc.php,v 1.10 2004/11/13 12:30:08 yrtimd Exp $
 */
// --------------------------------------------------------

/**
 *
 */
function getTermDates($dictID, $DBTABLE)
{
	global $oDb, $oSqlQ;
	global $arDictParam;

	$arSql = $oDb->sqlRun($oSqlQ->getQ('get-date-mm', $arDictParam['tablename']), '', 0);
	$strA = array('max' => '', 'min' => '');
	for (; list($arK, $arV) = each($arSql);)
	{
		if (!empty($arV['max'])&&!empty($arV['min'])) // no date
		{
			$strA = $arV;
		}
		else
		{
			$strA['max'] = $strA['min'] = date("YmdHis");
		}
	}
	return $strA;
}
/**
 *
 */
function getExportFilename($f, $cnt, $fmt)
{
	$r = strlen($cnt);
	$seq = "_%0" . $r . "d_of_%0" . $r . "d";
	return $f . $seq . '.' . $fmt;
}

function gw_file_copy($oldname, $newname)
{
	if (is_file($oldname))
	{
		$perms = fileperms($oldname);
		return copy($oldname, $newname) && chmod($newname, $perms);
	}
	else
	{
		die("Cannot copy file: $oldname (it's neither a file nor a directory)");
	}
}



/**
 *
 */
function getFormExport($vars, $runtime = 0, $arBroken = array(), $arReq = array())
{
	global $sys, ${GW_ACTION}, $id, $L, $arPost, $oFunc, ${GW_SID};
	$strForm = "";
	$trClass = "t";
	$form = new gwForms();
	$form->Set('action',   $GLOBALS['sys']['page_admin']);
	$form->Set('submitok',  $L->m('3_next'). ' &gt;&gt;');
	$form->Set('submitdel',  $L->m('3_remove'));
	$form->Set('submitcancel',  $L->m('3_cancel'));
	$form->Set('formbgcolor',    $GLOBALS['theme']['color_2']);
	$form->Set('formbordercolor', $GLOBALS['theme']['color_4']);
	$form->Set('formbordercolorL', $GLOBALS['theme']['color_1']);
	$form->Set('align_buttons',   $GLOBALS['sys']['css_align_right']);
	$form->Set('charset', $sys['internal_encoding']);
	## ----------------------------------------------------
	##
	 // check vars
	// reverse array keys <-- values;
	$arReq = array_flip($arReq);
	// mark fields as "REQUIRED" and make error messages
	while(is_array($vars) && list($key, $val) = each($vars) )
	{
		$arReqMsg[$key] = $arBrokenMsg[$key] = "";
		if (isset($arReq[$key])) { $arReqMsg[$key] = ' <span style="color:#E30"><b>*</b></span>'; }
		if (isset($arBroken[$key])) { $arBrokenMsg[$key] = ' <span class="'.$trClass.'" style="color:#E30"><b>' . $L->m('reason_9') .'</b></span>'; }
	} // end of while
	##
	## ----------------------------------------------------


	$strForm .= '<table cellspacing="0" cellpadding="2" border="0" width="100%">';
	$strForm .= '<col width="70%"/><col width="30%"/>';
	$strForm .= '<tr valign="top"><td>';

		// Time frame
		$strForm .= getFormTitleNav($L->m('timeframe'), '');

		$strForm .= '<span class="t">'.$L->m('tip001').'</span>';

		$strForm .= '<table cellspacing="0" cellpadding="2" border="0" width="100%">';
		$strForm .= '<tr>';
		$strForm .= '<td>';
		$strForm .= '<table cellspacing="0" cellpadding="1" border="0" width="100%">'.
					'<col width="1%"/><col width="99%"/>'.
					'<tr valign="top">'.
					'<td class="t">'.$L->m('from_time').'&#160;</td><td>' . htmlFormSelectDate("arPost[date_min]", $vars['min']) . '</td>'.
					'</tr>'.
					'<tr valign="top">'.
					'<td class="t">'.$L->m('till_time').'&#160;</td><td>' . htmlFormSelectDate("arPost[date_max]", $vars['max']) . '</td>'.
					'</tr>'.
					'</table>';
		$strForm .= '</td>';
		$strForm .= '</tr>';
		$strForm .= '<tr valign="top" class="t">'.
					'<td style="text-align:center;background:'.$GLOBALS['theme']['color_1'].'">';
		$strForm .= '<a href="javascript:setToday();">' . $L->m('today') . '</a>';
		$strForm .= ' | <a href="javascript:setD();">' . $L->m('yesterday') . '</a>';
		$strForm .= ' | <a href="javascript:setM();">' . $L->m('month') . '</a>';
		$strForm .= ' | <a href="javascript:setAll();">' . $L->m('3_all_time') . '</a>';
		$strForm .= '</td>';
		$strForm .= '</tr>';
		$strForm .= '</table>';


	$strForm .= '</td><td>';
		// Select format
		$strForm .= getFormTitleNav($L->m('select_format'), '');

		$strForm .= '<table cellspacing="0" cellpadding="2" border="0" width="100%">';
		reset($vars['arFmt']);
		for (; list($k, $v) = each($vars['arFmt']);)
		{
			$strForm .= '<tr class="t">';
			$arBoxId['id'] = 'r_'.$v;
			$checked = ($vars['fmt_default'] == $v) ? 1 : 0;
			$strForm .= '<td style="text-align:right">' . $form->field("radio", "arPost[fmt]", $k, $checked, $arBoxId) . '</td>';
			$strForm .= '<td><label for="r_'.$v.'">'.$v.'</label></td>';
			$strForm .= '</tr>';
		}
		$strForm .= '</table>';

	$strForm .= '</td>';
	$strForm .= '</tr>';
	$strForm .= '</table>';

	include($sys['path_include'] . '/'. GW_ACTION . '.' . ${GW_ACTION} . '.js.' . $GLOBALS['sys']['phpEx']);

	$strForm .= $form->field("hidden", "id", $id);
	$strForm .= $form->field("hidden", GW_TARGET, GW_T_DICT);
	$strForm .= $form->field("hidden", GW_ACTION, EXPORT);
	$strForm .= $form->field("hidden", GW_SID, ${GW_SID});

	if ($arPost[GW_ACTION] == GW_A_ADD)
	{
		$strForm .= $form->field("hidden", 'arPost['.GW_ACTION.']', GW_A_ADD);
	}
	else
	{
		$strForm .= $form->field("hidden", 'arPost['.GW_ACTION.']', UPDATE);
	}
	return $form->Output($strForm);
} // end of getFormImport()
// --------------------------------------------------------
// Language
$L->getCustom('export', $sys['locale_name'], 'join');
// --------------------------------------------------------
// Prepare variables
if ($arPost == '') { $arPost = array(); }
if (!isset($arPost[GW_ACTION])) { $arPost[GW_ACTION] = GW_A_ADD; }
//
$is_idadd   = isset($arPost['is_idadd']) ? $arPost['is_idadd'] : 0;
$is_idupdate= isset($arPost['is_idupdate']) ? $arPost['is_idupdate'] : 1;
$is_struc   = isset($arPost['is_struc']) ? $arPost['is_struc'] : 1;
//
$arReq = array('date_min','date_max');

//
// --------------------------------------------------------
switch ($arPost[GW_ACTION])
{
case GW_A_ADD:
## --------------------------------------------------------
## Show HTML-form
	if ($post == '') // not saved
	{
		// get MAX and MIN date from terms
		$vars = getTermDates($id, $DBTABLE);

		$arThemes = readDirD($sys['path_include'] . '/', "/^export_/");
		$vars['arFmt'] = str_replace('export_', '', $arThemes);
		$vars['arFmt'] = str_replace('_', ' ', $vars['arFmt']);
		// set default export option
		$vars['fmt_default'] = 'CHM';
		$strR .= getFormExport($vars, 0, 0, $arReq);
	}
	else
	{
		if (!isset($arPost['fmt']))
		{
			$vars['min'] = $arPost['date_minY'].$arPost['date_minM'].$arPost['date_minD'].str_replace(':', '', $arPost['date_minS']);
			$vars['max'] = $arPost['date_maxY'].$arPost['date_maxM'].$arPost['date_maxD'].str_replace(':', '', $arPost['date_maxS']);
			$vars['arFmt'] = str_replace('export_', '', $arThemes);
			$vars['fmt_default'] = 'XML';
			$strR .= getFormExport($vars, 0, 0, $arReq);
		}
		else
		{
			/* Increase time limit */
			@set_time_limit(3600);
			$pathExportFmt = $sys['path_include'].'/'. $arPost['fmt']. '/' . 'index.inc.php';
			file_exists($pathExportFmt)
			? include_once($pathExportFmt)
			: printf($L->m("reason_10"), $pathExportFmt);
		}
	} // submitted
##
## --------------------------------------------------------
break;
default:
break;
} // end of switch

/* end of file */
?>