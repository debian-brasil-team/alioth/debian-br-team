<?php
if (!defined('IN_GW'))
{
	die('<!-- $Id: func.stat.inc.php,v 1.8 2004/11/14 10:53:49 yrtimd Exp $ -->');
}
/**
 *  Glossword - glossary compiler (http://glossword.info/dev/)
 *  � 2002-2004 Dmitry N. Shilnikov <dev at glossword.info>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  (see `glossword/support/license.html' for details)
 */
// --------------------------------------------------------
/**
 *  Logging data, file functions, replacement functions.
 */
// --------------------------------------------------------

/**
 * Prepare message
 *
 * @param   string  $user_name  User name, not login
 * @param   string  $login      Login, system user name
 * @param   string  $password   Password
 * @param   string  $template   HTML-template
 * @global  $sys
 * @global  $L
 */
function gwCreateMessage($user_name, $login_url, $login, $password, $template)
{
	global $sys, $L;
	/* load language files */
	$L->getCustom('mail', $sys['locale_name'], 'join');
	$mail_sig = $sys['site_name'] . ' - ' . $sys['site_desc'];
	/* templates */
	$arSrc = array('{USERNAME}', '{SITENAME}', '{LOGIN_URL}', '{LOGIN}', '{U_LOGIN}', '{PASSWORD}', '{U_PASSWORD}', '{CONTACT_EMAIL}');
	$arTrg = array($user_name,
				   $sys['site_name'],
				   $login_url, $L->m('login'),
				   $login,
				   $L->m('password'),
				   $password,
				   '<a href="mailto:'. $sys['y_email'].'">'.$sys['y_email'].'</a>'
			 );
	$msgBody = str_replace($arSrc, $arTrg, $L->m('mail_newuser'));
	$msgSubj = str_replace($arSrc, $arTrg, $L->m('mail_newuser_subj'));
	//
	$tpl_mail = new gwTemplate();
	$tpl_mail->addVal( 'LANG',           $L->languagelist("0") );
	$tpl_mail->addVal( 'TEXT_DIRECTION', $L->languagelist("1") );
	$tpl_mail->addVal( 'CHARSET',        $L->languagelist("2") );
	$tpl_mail->addVal( 'EMAIL_SIG',      $mail_sig );
	$tpl_mail->addVal( 'MSG_NEWUSER',    $msgBody );
	$tpl_mail->addVal( 'SUBJECT',        $msgSubj );
	$tpl_mail->setHandle(0, $sys['path_admin'] . '/' . $sys['path_tpl'] . '/' . $template);
	$tpl_mail->parse();
	return $tpl_mail->output();
}



/**
 * Reads direcrory
 *
 * @param   string  $strDir path to directory
 * @param   string  $ex     preg_match pattern
 *
 * @return  array   The list of directories
 */
 function readDirD($strDir, $ex = "(.*)"){
	$ar = array();
	if (is_dir($strDir))
	{
		$dir = opendir($strDir);
		while (($f = readdir($dir)) !== false)
		{
			if ($f != '.' && $f != '..' && is_dir($strDir . "/" . $f))
			{
				if ( preg_match($ex, $f ) )
				{
					$ar[$f] = $f;
				}
			}
		} // end of while
	}
	return $ar;
} // end of readDirD();


/**
 * Sends mail
 *
 * @param   string  $email   e-mail address.
 * @param   string  $subj    subject.
 * @param   string  $body    message text.
 * @param   int     $isDebug if true, do not send message and display it on screen.
 *
 * @return  boolean True if success.
 * @global  array   $sys System variables
 */
function kMailTo($email, $from, $subj = '', $body = "\n kMailTo()", $isDebug = 0)
{
	global $sys, $oLog, $oFunc;
	$arHeader = array();
	$arHeader[] = 'From: ' . htmlspecialchars($from);
	$arHeader[] = 'Return-Path: <' . htmlspecialchars($from) . '>';
	$arHeader[] = 'X-Mailer: Glossword ' . $sys['version'];
	$arHeader[] = 'X-Priority: 3';
	$arHeader[] = 'MIME-Version: 1.0';
	$arHeader[] = 'Content-type: text/html; charset='.$sys['internal_encoding'];
	$header = implode("\n", $arHeader);
	$isSuccess = 0;
	if ($isDebug)
	{
		$header = str_replace(">", "&gt;", $header);
		$header = str_replace("<", "&lt;", $header);
		print "\n\n<pre>$header\nSubject: $subj</pre>\n$body";
		return $isDebug;
	}
	$subj = 'Glossword notification'; /* can't put UTF-8 characters into subject */
	if (@mail($email, $subj, $body, $header))
	{
		$isSuccess = 1; //
	}
	/* Log mail messages */
	if ($sys['is_LogMail'])
	{
		if (!isset($oLog))
		{
			$oLog = new gw_logwriter($sys['path_logs']);
		}
		$arLogMail = array(
			GW_TIME_NOW_UNIX,
			$oFunc->ip2int(REMOTE_IP),
			$body
		);
		$oFunc->file_put_contents($oLog->get_filename('mail'), $oLog->make_str($arLogMail), 'a');
	}
	return $isSuccess;
}

?>