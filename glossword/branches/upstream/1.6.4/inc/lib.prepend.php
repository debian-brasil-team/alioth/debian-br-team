<?php
if (!defined('IN_GW'))
{
	die('<!-- $Id: lib.prepend.php,v 1.13 2004/11/13 12:30:08 yrtimd Exp $ -->');
}
/**
 *  Glossword - glossary compiler (http://glossword.info/dev/)
 *  � 2002-2004 Dmitry N. Shilnikov <dev at glossword.info>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
  * (see `glossword/support/license.html' for details)
 */
/* ------------------------------------------------------- */
/**
 *  Configuration scheme:
 *  index -> config.inc -> lib.prepend, constants.inc -> custom
 *                         ^^^^^^^^^^^
 *  @version $Id: lib.prepend.php,v 1.13 2004/11/13 12:30:08 yrtimd Exp $
 */
/* ------------------------------------------------------- */
/* PHP variables */
$sys['internal_encoding'] = 'UTF-8';
/* Those damn quotes_* must be removed from PHP at all! */
@ini_set('register_globals', 0);
@ini_set('set_magic_quotes_gpc', 0);
@ini_set('set_magic_quotes_runtime', 0);
@ini_set('mbstring.internal_encoding', $sys['internal_encoding']);


/* ------------------------------------------------------- */
/* Load functions */
include_once( $sys['path_gwlib'] .'/class.timer.php' );
$oTimer = new gw_timer;
include_once( $sys['path_include'] . '/class.gwtk.'       . $sys['phpEx'] );
include_once( $sys['path_include'] . '/class.rendercells.'. $sys['phpEx'] );
include_once( $sys['path_include'] . '/class.tpl.'        . $sys['phpEx'] );
include_once( $sys['path_include'] . '/class.forms.'      . $sys['phpEx'] );
include_once( $sys['path_include'] . '/class.gw_htmlforms.' . $sys['phpEx'] ); // extends gw_forms
include_once( $sys['path_include'] . '/lib.user.'         . $sys['phpEx'] );
include_once( $sys['path_include'] . '/func.browse.inc.'  . $sys['phpEx'] );
include_once( $sys['path_include'] . '/func.catalog.inc.' . $sys['phpEx'] );
include_once( $sys['path_include'] . '/func.crypt.inc.'   . $sys['phpEx'] );
include_once( $sys['path_include'] . '/func.shuffle.'     . $sys['phpEx'] );
include_once( $sys['path_include'] . '/func.sql.inc.'     . $sys['phpEx'] );
include_once( $sys['path_include'] . '/func.srch.inc.'    . $sys['phpEx'] );
include_once( $sys['path_include'] . '/func.stat.inc.'    . $sys['phpEx'] );
include_once( $sys['path_include'] . '/func.text.inc.'    . $sys['phpEx'] );
include_once( $sys['path_include'] . '/constants.inc.'    . $sys['phpEx'] );
include_once( $sys['path_gwlib'] . '/class.xslt.php' );
include_once( $sys['path_gwlib'] . '/class.render.php' ); /* extends gw_htmlforms */
/* New from Glossword 2.0 */
include_once( $sys['path_gwlib']. '/class.db.cache.php' );
include_once( $sys['path_gwlib']. '/class.db.mysql.php');
include_once( $sys['path_gwlib']. '/class.db.q.php' );
include_once( $sys['path_gwlib']. '/class.domxml.php' );
include_once( $sys['path_gwlib']. '/class.headers.php' );
include_once( $sys['path_gwlib']. '/class.globals.php');
include_once( $sys['path_gwlib']. '/class.html.php' );
include_once( $sys['path_gwlib']. '/class.logwriter.php');

if (!isset($sys['server_proto']))
{
	$sys['server_proto'] = 'http://';
}
/* HTTP_HOST and REQUEST_URI constants */
if (!isset($sys['server_host']))
{
	$sys['server_host'] = isset($_SERVER["HTTP_HOST"])&&!empty($_SERVER["HTTP_HOST"]) ? $_SERVER["HTTP_HOST"]
					: (isset($HTTP_SERVER_VARS["HTTP_HOST"]) ? $HTTP_SERVER_VARS["HTTP_HOST"]
					: (getenv('SERVER_NAME') != '') ? getenv('SERVER_NAME')
					: 'localhost');
}
define('HTTP_HOST',  $sys['server_host']);
define('REQUEST_URI', isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI']
					: ((getenv('REQUEST_URI!') != '') ? getenv('REQUEST_URI')
					: ((isset($HTTP_SERVER_VARS['QUERY_STRING']) && $HTTP_SERVER_VARS['QUERY_STRING'] != '') ? ($HTTP_SERVER_VARS['PHP_SELF'] . '?' . $HTTP_SERVER_VARS['QUERY_STRING'])
					: $_ENV['PHP_SELF'])));
if (!isset($sys['server_dir']))
{
	$sys['server_dir'] = dirname(REQUEST_URI);
	if (preg_match("/\/$/", REQUEST_URI))
	{
		$sys['server_dir'] = dirname(REQUEST_URI.'index.php');
	}
	/* allow to login when the script is not installed */
	if (preg_match("/\/gw_admin/", $sys['server_dir']))
	{	
		$sys['server_dir'] = str_replace('/gw_admin', '', $sys['server_dir']);
	}
}
if (!isset($sys['server_url']))
{
	$sys['server_url'] = $sys['server_proto'].$sys['server_host'].$sys['server_dir'];
}
$_SERVER['HTTP_ACCEPT_ENCODING'] = isset($_SERVER['HTTP_ACCEPT_ENCODING']) ? $_SERVER['HTTP_ACCEPT_ENCODING'] : (isset($_SERVER['HTTP_TE']) ? $_SERVER['HTTP_TE'] : '');
/* Get PHP version */
$tmp['arPhpVer'] = explode('.', PHP_VERSION);
define('PHP_VERSION_INT', intval(sprintf('%d%02d%02d', $tmp['arPhpVer'][0], $tmp['arPhpVer'][1], $tmp['arPhpVer'][2])));
/* ------------------------------------------------------- */
define('GW_TIME_NOW_UNIX', mktime());
define('GW_TIME_NOW', date("YmdHis", GW_TIME_NOW_UNIX));
define('REMOTE_IP', gwGetRemoteIp());
define('REMOTE_UA', trim(substr(getenv('HTTP_USER_AGENT'), 0, 256)));
$HTTP_REF = getenv('HTTP_REFERER');
/* ------------------------------------------------------- */
/* Specific techical purposes (error level, banners, counters etc.) */
$arLocalIp = array('192.168.', '127.', '10.');
for (reset($arLocalIp); list($k, $v) = each($arLocalIp);) // check values
{
	if (preg_match("/^" . $v . "/", HTTP_HOST))
	{
		define('IS_MYHOST', 1);
		break;
	}
}
if (!defined('IS_MYHOST')) { define('IS_MYHOST', 0); }
(IS_MYHOST) ? error_reporting(E_ALL) : error_reporting(0);
/* ------------------------------------------------------- */
define('CRLF', "\r\n");
define('LF', "\\n\\");
/* ------------------------------------------------------- */
/* Does upload allowed? */
$sys['is_upload'] = (function_exists('ini_get'))
				  ? ((strtolower(ini_get('file_uploads')) == 'on' || ini_get('file_uploads') == 1) && intval(ini_get('upload_max_filesize')))
				  : 0;
/* ------------------------------------------------------- */
/* Read visual theme names */
$arThemes = readDirD( './' . $sys['path_tpl'], "/[^CVS|tpl|common]/");
/* ------------------------------------------------------- */
/* Custom configuration. Must be always at the end */
if (@file_exists( './' . $sys['path_include']. '/gw_config.' . $sys['phpEx']))
{
	include( './' . $sys['path_include']. '/gw_config.' . $sys['phpEx']);
}
/* ------------------------------------------------------- */
/* Dynamic path names */
$sys['path_img_full'] = $sys['server_proto'] . $sys['server_host'] . $sys['server_dir'] .'/'. $sys['path_img'];
$sys['path_img_ph'] = $sys['path_img'];
$sys['path_img'] = $sys['server_dir'] .'/'. $sys['path_img'];
$sys['page_index'] = $sys['server_dir'] .'/index.php';
$sys['page_login'] = $sys['server_dir'] .'/'. $sys['path_admin'] . '/login.php';
$sys['page_admin'] = $sys['server_dir'] .'/gw_admin.php';
/* ------------------------------------------------------- */
/* Database class */
$oDb = new gwtkDb;
if ($sys['is_cache_sql'])
{
	$oDb->cache_lifetime = $sys['cache_lifetime'];
	$oDb->setCache($sys['path_cache_sql']);
}
/* ------------------------------------------------------- */
/* SQL-queries */
$oSqlQ = new gw_query_storage;

/* end of file */
?>