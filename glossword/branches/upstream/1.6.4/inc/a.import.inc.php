<?php
if (!defined('IN_GW'))
{
	die('<!-- $Id: a.import.inc.php,v 1.11 2004/11/16 13:26:30 yrtimd Exp $ -->');
}
/**
 *  Glossword - glossary compiler (http://glossword.info/dev/)
 *  � 2002-2004 Dmitry N. Shilnikov <dev at glossword.info>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  (see `glossword/support/license.html' for details)
 */
// --------------------------------------------------------
/**
 *  Import. External utility for a dictionary.
 *
 *   <line>
 *    <term>A Term</term>
 *    <defn><abbr>abbreviation</abbr>1st definition</defn>
 *    <defn><trns lang="en.">translation</trns>2nd definition</defn>
 *   </line>
 *
 *  @version $Id: a.import.inc.php,v 1.11 2004/11/16 13:26:30 yrtimd Exp $
 */
// --------------------------------------------------------
/**
 *
 */
function getFormImport($vars, $runtime = 0, $arBroken = array(), $arReq = array())
{
	global ${GW_ACTION}, $id, $L, $arPost, $oFunc, ${GW_SID}, $sys;
	$strForm = "";
	$trClass = "t";
	$form = new gwForms();
	$form->Set('action',          $sys['page_admin']);
	$form->Set('submitok',        $L->m('3_save'));
	$form->Set('submitdel',       $L->m('3_remove'));
	$form->Set('submitcancel',    $L->m('3_cancel'));
	$form->Set('formbgcolor',     $GLOBALS['theme']['color_2']);
	$form->Set('formbordercolor', $GLOBALS['theme']['color_4']);
	$form->Set('formbordercolorL',$GLOBALS['theme']['color_1']);
	$form->Set('align_buttons',   $GLOBALS['sys']['css_align_right']);
	$form->Set('charset', $sys['internal_encoding']);
	// Upload xml-file
	if ($sys['is_upload']) { $form->Set('enctype', 'multipart/form-data'); }
	## ----------------------------------------------------
	##
	 // check vars
	// reverse array keys <-- values;
	$arReq = array_flip($arReq);
	// mark fields as "REQUIRED" and make error messages
	while(is_array($vars) && list($key, $val) = each($vars) )
	{
		$arReqMsg[$key] = $arBrokenMsg[$key] = "";
		if (isset($arReq[$key])) { $arReqMsg[$key] = '&#160;<span style="color:#E30"><b>*</b></span>'; }
		if (isset($arBroken[$key])) { $arBrokenMsg[$key] = ' <span class="'.$trClass.'" style="color:#E30"><b>' . $L->m('reason_9') .'</b></span>'; }
	} // end of while
	##
	## ----------------------------------------------------

	$strForm .= '<table cellspacing="0" cellpadding="2" border="0" width="100%">';
	$strForm .= '<col style="width:15%;text-align:'.$GLOBALS['sys']['css_align_right'].'"/><col style="width:85%"/>';
	// Import
	$strForm .= '<tr valign="top">'.
				'<td class="' . $trClass . '">' . $L->m('enter_xml') . ':' . $arReqMsg['xml'] . '</td>'.
				'<td>' . $arBrokenMsg['xml'] . '<textarea '.
				' onfocus="if(typeof(document.layers)==\'undefined\'||typeof(ts)==\'undefined\'){ts=1;this.form.elements[\'arPost[\'+\'xml\'+\']\'].select();}"'.
				' style="border:1px solid '.$GLOBALS['theme']['color_5'].';overflow:scroll;word-wrap:normal;width:100%;color:#888;background:'.$GLOBALS['theme']['color_1'].';font:70% \'verdana\',arial,sans-serif"'.
				' onmouseover="javascript:vbform.file_location.value=this.value" name="arPost[xml]" cols="70" rows="12">' . textcodetoform($vars['xml']) . '</textarea>'.
				'</td>'.
				'</tr>';
	// 3 mart 2003
	// Allows to upload xml-file
	if ($sys['is_upload'])
	{
		$strForm .= '<tr valign="top">'.
					'<td class="' . $trClass . '">' . '&#160;' . '' . $arReqMsg['file_location'] . '</td>'.
					'<td>' . $arBrokenMsg['file_location'] . $form->field('file', 'file_location', $vars['file_location']) . '</td>'.
					'</tr>';
	}
	$strForm .= '<tr valign="top">'.
				'<td class="'.$trClass .'">' . $L->m('options') . ':</td>'.
				'<td>';
		global $user;
		$vars['is_validate'] = $user->get('import_is_validate') ? 1 : 0;
		$vars['is_overwrite'] = $user->get('import_is_overwrite') ? 1 : 0;
		$vars['is_specialchars'] = $user->get('import_is_specialchars') ? 1 : 0;
	
		$strForm .= '<table cellspacing="0" cellpadding="1" border="0" width="100%">';
		$strForm .= '<col width="1%"/><col width="31%"/><col width="1%"/><col width="31%"/><col width="1%"/><col width="33%"/>';
		$strForm .= '<tr class="t">';
		$strForm .= '<td>' . $form->field("checkbox", "arPost[is_validate]", $vars['is_validate']) . '</td>';
		$strForm .= '<td><label for="arPost_is_validate_">' . $L->m('validate') . '</label></td>';
		$strForm .= '<td>' . $form->field("checkbox", "arPost[is_overwrite]", $vars['is_overwrite']) . '</td>';
		$strForm .= '<td><label for="arPost_is_overwrite_">' . $L->m('overwrite') . '</label></td>';
		$strForm .= '<td>' . $form->field("checkbox", "arPost[is_specialchars]", $vars['is_specialchars']) . '</td>';
		$strForm .= '<td><label for="arPost_is_specialchars_">' . $L->m('is_specialchars') . '</label></td>';
		$strForm .= '</tr>';
		$strForm .= '</table>';
	$strForm .= '</td>';
	$strForm .= '</tr>';
	$strForm .= '</table>';

	/* 1.6.3 */
	$strForm .= getFormTitleNav($L->m('options'));
	$tmp['after_post'][1] = $tmp['after_post'][3] = $tmp['after_post'][4] = '';
	$tmp['after_post'][$user->get('after_post_import')] = ' checked="checked"';
	if (isset($tmp['after_post'][0]))
	{
		/* turn on second option by default */
		$tmp['after_post'][GW_AFTER_TERM_IMPORT] = $tmp['after_post'][0];
	}
	$strForm .= '<table class="gw2TableFieldset" cellspacing="0" cellpadding="0" border="0" width="100%">';
	$strForm .= '<tr><td style="width:15%"></td><td></td></tr>';
	$strForm .= '<tr>'.
				'<td class="td1">' . $L->m('after_post') . '</td>'.
				'<td class="td2">' .
				'<input type="radio" name="arPost[after]" id="arPost_after_1" value="1"'.$tmp['after_post'][1].'><label for="arPost_after_1">'.$L->m('after_post_1').'</label>'.
				'<input type="radio" name="arPost[after]" id="arPost_after_4" value="4"'.$tmp['after_post'][4].'><label for="arPost_after_4">'.$L->m('3_import').'</label>'.
				'<input type="radio" name="arPost[after]" id="arPost_after_3" value="3"'.$tmp['after_post'][3].'><label for="arPost_after_3">'.$L->m('after_post_3').'</label>'.
				'</select>'.
				'</td>'.
				'</tr>';
	$strForm .= '</table>';
	

	$strForm .= $form->field("hidden", 'id', $id);
	$strForm .= $form->field("hidden", GW_TARGET, GW_T_DICT);
	$strForm .= $form->field("hidden", GW_ACTION, IMPORT);
	$strForm .= $form->field("hidden", GW_SID, ${GW_SID});

	if ($arPost[GW_ACTION] == GW_A_ADD)
	{
		$strForm .= $form->field("hidden", 'arPost['.GW_ACTION.']', GW_A_ADD);
	}
	else
	{
		$strForm .= $form->field("hidden", 'arPost['.GW_ACTION.']', UPDATE);
	}
	return $form->Output($strForm);
} // end of getFormImport()
// --------------------------------------------------------
// Prepare variables
if ($arPost == '') { $arPost = array(); }
if (!isset($arPost[GW_ACTION])) { $arPost[GW_ACTION] = GW_A_ADD; }
$vars['is_overwrite'] = isset($arPost['is_overwrite']) ? $arPost['is_overwrite'] : 0;
$vars['is_validate'] = isset($arPost['is_validate']) ? $arPost['is_validate'] : 0;
$vars['is_specialchars'] = isset($arPost['is_specialchars']) ? $arPost['is_specialchars'] : 0;
$vars['file_location'] = $file_location;
$arReq['dict'] = array("xml");

switch ($arPost[GW_ACTION])
{
case GW_A_ADD:
## --------------------------------------------------------
## Show HTML-form

if ($post == '') // not saved
{
	$cntFiles = 1;
	$vars['xml'] = '';
	$strR .= getFormImport($vars, 0, 0, $arReq[$t]);
	$arHelpMap = array(
					'enter_xml' => htmlspecialchars('<line><term>A Term</term><trsp></trsp><defn><abbr>abbreviation</abbr>First definition</defn><defn><trns lang="103">translation</trns>Second definition</defn></line>'),
					'validate'  => 'tip011',
					'overwrite' => 'tip012',
					'is_specialchars' => 'tip013'
				 );
	$strHelp = '';
	$strHelp .= '<dl>';
	for(; list($k, $v) = each($arHelpMap);)
	{
		$strHelp .= '<dt><b>' . $L->m($k) . '</b></dt>';
		$strHelp .= '<dd>' . $L->m($v) . '</dd>';
	}
	$strHelp .= '</dl>';
	$strR .= '<br />' . kTbHelp($L->m('2_tip'), $strHelp);
}
else 
{
	/* start import */
	$objTimer = new gw_timer;

	@set_time_limit(3600);
	// options
	$arPost['is_validate'] = isset($arPost['is_validate']) ? $arPost['is_validate'] : 0;
	$arPost['is_overwrite'] = isset($arPost['is_overwrite']) ? $arPost['is_overwrite'] : 0;
	$arPost['is_specialchars'] = isset($arPost['is_specialchars']) ? $arPost['is_specialchars'] : 0;

	$user->save('import_is_validate', $arPost['is_validate']);
	$user->save('import_is_overwrite', $arPost['is_overwrite']);
	$user->save('import_is_specialchars', $arPost['is_specialchars']);

	$arBroken = validatePostWalk($arPost, $arReq[$t]);
	// remove xml field from broken fields if file location exists
	if (isset($arBroken['xml']) && $vars['file_location'] != '')
	{
		unset($arBroken['xml']);
	}
	if(sizeof($arBroken) == 0) // no broken fields
	{
		$isPostError = 0;
	}
	if (!$isPostError) // single check is ok
	{
		/* Uploaded file */
		$xml_file = isset($file_location['tmp_name']) ? $file_location['tmp_name'] : '';
		if (is_uploaded_file($xml_file)
			&& move_uploaded_file($xml_file, $sys['path_temporary'].'/'.urlencode($file_location['name']))
			)
		{
			$arPost['xml'] = $oFunc->file_get_contents($sys['path_temporary'].'/'.urlencode($file_location['name']));
			unlink($sys['path_temporary'].'/'.urlencode($file_location['name']));
		}
		/* XML-syntax check */
		if ($vars['is_validate'] && function_exists('xslt_create'))
		{
			if (preg_match("/^([\s])*<line/", $arPost['xml'])) // fix single lines
			{
				$arPost['xml'] = '<?xml version="1.0" encoding="'.$sys['internal_encoding'].'"?>' . CRLF.
								 '<glossword>' . CRLF . $arPost['xml'] . '</glossword>';
			}
			include_once( $sys['path_include']. '/class.xmlparse.' . $sys['phpEx'] );
			$xml_parser = & new xmlTinyParser();
			$return = $xml_parser->parse($arPost['xml']);
			if (!$return)
			{
				// Uploaded file can be too big
				// so we do not need to put file contents into HTML-form
				// show filename instead
				if (file_exists($xml_file) && is_uploaded_file($xml_file))
				{
					$arPost['xml'] = $file_location['name'];
				}
				$arBroken['xml'] = $isPostError = 1;
			}
		}
	}
	if ($isPostError) // call HTML-form again
	{
		$arPost['is_overwrite'] = isset($arPost['is_overwrite']) ? $arPost['is_overwrite'] : 0;
		$arPost['is_validate'] = isset($arPost['is_validate']) ? $arPost['is_validate'] : 0;
		$arPost['is_specialchars'] = isset($arPost['is_specialchars']) ? $arPost['is_specialchars'] : 0;
		$strR .= getFormImport($arPost, 1, $arBroken, $arReq[$t]);
	}
	else // final update
	{
		$objDom = new gw_domxml;
		$cnt = 0;
		$queryA = array();
		$sfA1 = explode("<line>", $arPost['xml']); // should be changed to DOM model
		// get stopwords
		$arStop = $L->getCustom('stop_words',  $arDictParam['lang'], 'return');
		//
		// Enter to debug mode. None of `INSERT' or `DELETE' queries will be executed.
		//
#        $sys['isDebugQ'] = 1;
		//
		error_reporting(E_ALL ^ E_NOTICE);
		if (is_array($sfA1)) // <line> found
		{
			for (reset($sfA1); list($k1, $v1) = each($sfA1);) // for each <line>
			{
				$arPre = array();
				$v1 = str_replace('</line>', '', $v1);
				$v1 = str_replace('</glossword>', '', $v1);
				$v1 = str_replace('<glossword>', '', $v1);
				//
				// Parse term parameters
				//
				preg_match_all("/<term(.*?)>/", $v1, $term_attr);
				if (isset($term_attr[1][0]))
				{
					$v1 = preg_replace("/<term(.*?)>/", '<term>', $v1);
					$term_attr[1][0] = str_replace('"', '', $term_attr[1][0]);
					$arAttr = explode(" ", trim($term_attr[1][0]));
					for (reset($arAttr); list($attrK, $attrV) = each($arAttr);)
					{
						$arAttrV = explode('=', $attrV);
						$arPre['term'][0]['attributes'][$arAttrV[0]] = $arAttrV[1];
					}
					$arPre['term'][0]['tag'] = 'term';
				}
				$arPre = array_merge_clobber($arPre, gw_Xml2Array($v1));
				//
				$arPre['parameters']['xml'] = preg_replace("/<term>(.*?)<\/term>/", '', $v1);;
				//
				if ( isset($arPre['term']) ) // parsed terms only
				{
					$i = 0;
					$arPre['parameters']['action'] = ${GW_ACTION};
					//
					// Prepare definition
					//
					$ar = gwAddTerm($arPre, $id, $arStop, 1, $arPost['is_specialchars'], $arPost['is_overwrite'], $k1);
					if (is_array($ar))
					{
						if ($sys['isDebugQ'])
						{
							$queryA = array_merge($queryA, $ar);
						}
						else
						{
							// post query
							$cntQ = sizeof($ar);
							for ($i2 = 0; $i2 <= $cntQ; $i2++)
							{
								if ($oDb->sqlExec($ar[$i2])){ $isPostError = false; }
							}
						}
						$cnt++;
					}
					else // on error
					{
						$strR .= $ar;
					}
				} // end of only childrens
			} // end of for each <line>
			//
			// Clean search results for edited dictionary
			$queryA[] = $oSqlQ->getQ('del-srch-by-dict', $d);
			//
			// Clear dictionary cache (hidden)
			gw_tmp_clear($id);
			//
			// Show time usage
			//
			$strR .= '<table cellpadding="2" cellspacing="1" width="100%" border="0">';
			if (@ini_get('max_execution_time'))
			{
				$strR .= '<tr class="t"><td>max_execution_time:</td><td><b>' . @ini_get('max_execution_time') . '</b> seconds</td></tr>';
			}
			$strR .= '<tr class="t"><td style="width:25%">php_running_time:</td>';
			$strR .= '<td style="width:75%"><b>' . sprintf("%1.5f", $objTimer->end()) . '</b> seconds</td></tr>';
			if (($xml_file != '') && ($xml_file != 'none'))
			{
				$strR .= '<tr class="t"><td>local file:</td><td><b>' . $xml_file. '</b></td></tr>';
				$strR .= '<tr class="t"><td>uploaded file:</td><td><b>' . $file_location['name'] . '</b></td></tr>';
				$strR .= '<tr class="t"><td>'. sprintf("%s:</td><td><b>%s</b> %s</td></tr>", $L->m('size'), $oFunc->number_format($file_location['size'], 0, $L->languagelist('4')), $L->m('bytes'));
			}
			$intAddedTerms = $cnt;
			$strR .= '<tr class="t"><td>'. sprintf("%s:</td><td><b>%s</b></td></tr>", $L->m('str_on_page'), $intAddedTerms);
			$strR .= '</table>';
			//
			/* Redirect to... */
			$str_url = gw_after_redirect_url($arPost['after']);
			$strR .= postQuery($queryA, $str_url, $sys['isDebugQ'], 1);

		} // is_array, <line> found
	} // final update
} // submitted
##
## --------------------------------------------------------
break;
default:
break;
} // end of switch

?>