<?php
/**
 * Glossword update from 1.6.3 to 1.6.4
 * by Dmitry N. Shilnikov <dev at glossword dot info>
 * @version $Id: update_1.6.3_to_1.6.4.php,v 1.1 2004/11/14 11:17:36 yrtimd Exp $
 */
/* Change current directory to access for website files */
if (!@chdir('..'))
{
    exit(print "Can't change directory to `..'");
}
define('IN_GW', TRUE);
define('THIS_SCRIPT', 'update_1.6.3_to_1.6.4.php');
error_reporting(E_ALL);

/* Load configuration */
$sys['path_include'] = 'inc';
$sys['is_debug']  = 0;
$sys['is_prepend'] = 0;
include_once('./db_config.php');
include_once($sys['path_include'] . '/config.inc.php');
if (file_exists('gw_install/install_functions.php'))
{
	include_once('gw_install/install_functions.php');
}
else
{
	printf('<br/><b>Error:</b> File %s required.', 'gw_install/install_functions.php');
}
/* */

/* */
include_once($sys['path_include'] . '/constants.inc.php');
include_once($sys['path_include'] . '/func.sql.inc.php');
include_once($sys['path_include'] . '/class.forms.php');
include_once($sys['path_include'] . '/class.gwtk.php');
include_once($sys['path_include'] . '/func.browse.inc.php');
include_once($sys['path_gwlib'].'/class.globals.php');
include_once($sys['path_gwlib'].'/class.func.php');
include_once($sys['path_gwlib'].'/class.db.mysql.php');
include_once($sys['path_gwlib'].'/class.db.q.php');

/* Constants */
define('CRLF', "\r\n");
define('LF', "\\n\\");
$tmp['arPhpVer'] = explode('.', PHP_VERSION);
define('PHP_VERSION_INT', intval(sprintf('%d%02d%02d', $tmp['arPhpVer'][0], $tmp['arPhpVer'][1], $tmp['arPhpVer'][2])));

@header("Expires: " . gmdate("D, d M Y H:i:s") . "GMT");
@header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
@header("Cache-Control: no-cache, must-revalidate");
@header("Pragma: no-cache");

/* */
class gw_query_storage_up extends gw_query_storage
{
	function setQ()
	{
		$arSql = $this->import(array('query_storage_global.php', '../gw_install/update_1.6.3_to_1.6.4_q.php'));
		return $arSql;
	}
}
/* */
$oDb = new gwtkDb;
$oSqlQ = new gw_query_storage_up;

$sys = array_merge($sys, getSettings());

$sys['user_name'] =& $sys['y_email'];
$sys['user_email'] =& $sys['y_name'];

$sys['user_login'] = 'admin';
$sys['user_pass'] = '';
$sys['is_mod_rewrite']  = 0;
$sys['themename']  = 'gw_silver';
$sys['path_css'] =  $sys['server_dir'].'/'.$sys['path_tpl'].'/'.$sys['themename'];
$sys['version_to'] = '1.6.4';

include_once($sys['path_tpl'].'/'.$sys['themename'].'/theme.inc.php');
/* */
$gv['vars'] = $oGlobals->register(array('step', 'arpost', 'id_dict', GW_T_LANGUAGE));
$oGlobals->do_default($gv['vars']['step'], 1);
$oGlobals->do_default($gv['vars']['arpost']['dbhost'], 'localhost');
$oGlobals->do_default($gv['vars']['arpost']['dbport'], '');
$oGlobals->do_default($gv['vars']['arpost']['dbname'], 'glossword1');
$oGlobals->do_default($gv['vars']['arpost']['dbprefix'], 'gw_');
$oGlobals->do_default($gv['vars']['arpost']['dbuser'], 'root');
$oGlobals->do_default($gv['vars']['arpost']['dbpass'], 'root');
/* English by default */
$oGlobals->do_default($gv['vars'][GW_T_LANGUAGE], 'en');
/* --------------------------------------------------------
 * Translation kit
 * -------------------------------------------------------- */
$oL = new gwtk;
$oL->setHomeDir('gw_install/gw_locale');
$oL->setLocale($gv['vars'][GW_T_LANGUAGE].'-utf8');
$oL->getCustom('l_install', $gv['vars'][GW_T_LANGUAGE].'-utf8');
$oL->getCustom('l_update', $gv['vars'][GW_T_LANGUAGE].'-utf8');
/* */
$sys['html_title'] = sprintf($oL->m('up001'), $sys['version'], $sys['version_to']);

/* */
class gw_upgrade
{
	var $id_dict = 0;
	var $str_step = '';
	var $str_before = '';
	var $ar_status = '';
	var $str_after = '';
	var $cur_funcname = '';
	var $oDb;
	function gw_upgrade()
	{
		global $oDb, $oFunc, $sys, $gv, $theme, $oL, $oDb, $oSqlQ;
		$this->oDb =& $oDb;
		$this->oForm = new gwForms;
		$this->oFunc =& $oFunc;
		$this->oL =& $oL;
		$this->oSqlQ =& $oSqlQ;
		$this->sys =& $sys;
		$this->gv =& $gv;
		$this->arTheme =& $theme;
	}
	/* */
	function is_locked()
	{
		return file_exists($this->sys['file_lock']);
	}
	function get_html_steps_progress($step)
	{
		$ar = array();
		for ($i = 1; $i <= 3; $i++)
		{
			$ar[$i] = ' '. $this->oL->m('054') . ' ' . $i.'  ';
			if ($step == $i) { $ar[$i] = '<b class="green">'.$ar[$i].'</b>'; }
		}
		return '<span class="gray">'.implode('&#x2192;', $ar).'</span>';
	}
	/* */
	function step()
	{
		if ($this->is_locked())
		{
			$this->ar_status[] = sprintf($this->oL->m('052'), $this->sys['file_lock']);
		}
		else
		{
			$this->{$this->cur_funcname}();
			$this->str_step .= $this->get_html_steps_progress($this->gv['vars']['step']);
		}
	}
	/* */
	function step_1()
	{
		$this->str_before .= $this->oL->m('up002');
		$this->str_before .= '<ol>';
		$this->str_before .= '<li>'.$this->oL->m('005').'</li>';
		$this->str_before .= '<li>'.$this->oL->m('006').'</li>';
		$this->str_before .= '<li>'.$this->oL->m('007').'</li>';
		$this->str_before .= '</ol>';
		$is_continue = 0;
		/* Get permisions */
		$is_continue = 0;
		$this->ar_status[2] = $this->oL->m('059');
		if ( file_exists('db_config.php') && is_writeable('db_config.php') )
		{
			$this->ar_status[2] .= get_html_item_progress(sprintf($this->oL->m('060'), 'db_config.php'), 1);
			$is_continue = 1;
		}
		else
		{
			$this->ar_status[2] .= get_html_item_progress(sprintf($this->oL->m('061'), 'db_config.php'), 3);
		}
		$is_continue = 0;
		$this->ar_status[3] = $this->oL->m('074');
		if ( file_exists('gw_temp') && is_writeable('gw_temp') )
		{
			$this->ar_status[3] .= get_html_item_progress(sprintf($this->oL->m('060'), 'gw_temp/'), 1);
			$is_continue = 1;
		}
		else
		{
			$this->ar_status[3] .= get_html_item_progress(sprintf($this->oL->m('061'), 'gw_temp/'), 3);
		}
		$this->ar_status[4] = gw_next_step($is_continue, 'step=2');
	}
	/* */
	function step_2()
	{
		$this->str_before .= $this->oL->m('up003');
		$this->str_before .= $this->get_form(2);
	}
	function step_3()
	{
		$this->str_before .= '<br/><br/>';
		/* Check database connection */
		$is_continue = 0;
		if (@mysql_connect($this->gv['vars']['arpost']['dbhost'], $this->gv['vars']['arpost']['dbuser'], $this->gv['vars']['arpost']['dbpass']))
		{
			if (@mysql_select_db($this->gv['vars']['arpost']['dbname']))
			{
				$is_continue = 1;
			}
			else
			{
				$this->str_before .= get_html_item_progress(sprintf($this->oL->m('084'), $this->gv['vars']['arpost']['dbname']), 3);
			}
		}
		else
		{
			$this->str_before .= get_html_item_progress(sprintf($this->oL->m('083'), $this->gv['vars']['arpost']['dbhost'], $this->gv['vars']['arpost']['dbuser']), 3);
		}
		/* Check contact information */
		$this->sys['user_email'] = $this->gv['vars']['arpost']['user_email'];
		$this->sys['user_name'] = $this->gv['vars']['arpost']['user_name'];
		/* Server settings */
		$this->sys['server_proto'] = $this->gv['vars']['arpost']['server_proto'];
		$this->sys['server_host'] = $this->gv['vars']['arpost']['server_host'];
		$this->sys['server_dir'] = $this->gv['vars']['arpost']['server_dir'];
		/* */
		if ($is_continue)
		{
			$this->ar_status[] = sprintf($this->oL->m('085'), $this->gv['vars']['arpost']['dbname']);

			/* Prepare database queries */
			$arQuery = array();
			$this->ar_status[1] = $this->oL->m('086');

			/* Sessions */
			$arQuery[] = 'DELETE FROM '.TBL_SESS;
			$arQuery[] = 'OPTIMIZE TABLE '.TBL_SESS;

			/* Dictionaries */
			$sql = 'DESCRIBE ' . TBL_DICT;
			$arD = $this->oDb->sqlExec($sql);
			for (; list($dK, $fldV) = @each($arD);)
			{
				if (isset($fldV['Field']) && $fldV['Field']=='numterms')
				{
					$arQuery[] = $this->oSqlQ->getQ('dict01');
				}
				if (isset($fldV['Field']) && $fldV['Field']=='size_bytes')
				{
					$arQuery[] = $this->oSqlQ->getQ('dict02');
				}
			}
			$sql = 'SELECT * FROM '. TBL_DICT.' order by id ASC';
			$arSql = $this->oDb->sqlExec($sql);
			$this->ar_status[] = sprintf($this->oL->m('up005'), sizeof($arSql));
			/* new dictionary settings */
			for (reset($arSql); list($arK, $arV) = each($arSql);)
			{
				$vDict['dict_settings'] = unserialize($arV['dict_settings']);
				unset($arV['dict_settings']);
				$vDict['dict_settings']['is_show_full'] = 0;
				$vDict['dict_settings']['is_dict_as_index'] = 0;
				$qDict['dict_settings'] = serialize($vDict['dict_settings']);
				$arQuery[] = gw_sql_update($qDict, TBL_DICT, 'id = "'.$arV['id'].'"');
			}
			$arQuery[] = 'OPTIMIZE TABLE '. TBL_DICT;
			/* Users */
			$sql = 'DESCRIBE ' . TBL_USERS;
			$arD = $this->oDb->sqlExec($sql);
			for (; list($dK, $fldV) = @each($arD);)
			{
				if (isset($fldV['Field']) && $fldV['Field']=='location')
				{
					$arQuery[] = $this->oSqlQ->getQ('user01');
				}
			}
			$arQuery[] = $this->oSqlQ->getQ('user02');
			/* Auth */
			$arQuery[] = $this->oSqlQ->getQ('auth01');
			$sql = 'SELECT * FROM '. TBL_AUTH.' order by id ASC';
			$arSql = $this->oDb->sqlExec($sql);
			/* new passwords */
			for (reset($arSql); list($arK, $arV) = each($arSql);)
			{
				/* Do not reset passwords for 1.6.4-beta */
				if (strlen($arV['password'] < 32))
				{
					$arQuery[] = $this->oSqlQ->getQ('upd-auth', md5($arV['password']), $arV['user_id']);
				}
			}
			$arQuery[] = $this->oSqlQ->getQ('sett01');
			$arQuery[] = $this->oSqlQ->getQ('sett02');
			$arQuery[] = $this->oSqlQ->getQ('sett03');
			$arQuery[] = $this->oSqlQ->getQ('sett04');
			$arQuery[] = $this->oSqlQ->getQ('sett05');
			$arQuery[] = $this->oSqlQ->getQ('sett06');
			$arQuery[] = $this->oSqlQ->getQ('sett07');
			$arQuery[] = $this->oSqlQ->getQ('sett08');
			$arQuery[] = $this->oSqlQ->getQ('sett09');
			$arQuery[] = $this->oSqlQ->getQ('sett10');	
			$arQuery[] = $this->oSqlQ->getQ('upd-setting', gw_text_sql($this->gv['vars']['arpost']['user_email']), 'y_email');
			$arQuery[] = $this->oSqlQ->getQ('upd-setting', gw_text_sql($this->gv['vars']['arpost']['user_name']), 'y_name');
			$arQuery[] = $this->oSqlQ->getQ('upd-setting', $this->sys['version_to'], 'version');
			$arQuery[] = $this->oSqlQ->getQ('upd-setting', gw_text_sql('gw_silver'), 'themename');
			$arQuery[] = 'OPTIMIZE TABLE ' . TBL_USERS;
			/* !!! POSTING QUERIES !!! */
			$is_continue = 1; /* Continue by default */
			for (reset($arQuery); list($k, $v) = each($arQuery);)
			{
				if ($this->sys['is_debug'])
				{
					$this->ar_status[] = $v . ';';
				}
				else
				{
					if (!$this->oDb->sqlExec($v))
					{
						if (!preg_match("/UPDATE/", $v))
						{
							$is_continue = 0;
						}
						$this->ar_status[] = $v;
					}
				}
			}
			if ($is_continue) { $this->ar_status[1] .= '... Done'; }

			/* Prepare configuration file */
			$is_continue = 0;
			$str_file = '<'.'?php';
			$str_file .= CRLF . '/* Database settings for Glossword */';
			$str_file .= CRLF . sprintf("define('GW_DB_HOST', '%s');", $this->gv['vars']['arpost']['dbhost']);
			$str_file .= CRLF . sprintf("define('GW_DB_DATABASE', '`%s`');", $this->gv['vars']['arpost']['dbname']);
			$str_file .= CRLF . sprintf("define('GW_DB_USER', '%s');", $this->gv['vars']['arpost']['dbuser']);
			$str_file .= CRLF . sprintf("define('GW_DB_PASSWORD', '%s');", $this->gv['vars']['arpost']['dbpass']);
			$str_file .= CRLF . sprintf("define('GW_TBL_PREFIX', '%s');", $this->gv['vars']['arpost']['dbprefix']);
			$str_file .= CRLF . '/* Path names for Glossword */';
			$str_file .= CRLF . sprintf("\$sys['server_proto'] = '%s';", $this->gv['vars']['arpost']['server_proto']);
			$str_file .= CRLF . sprintf("\$sys['server_host'] = '%s';", $this->gv['vars']['arpost']['server_host']);
			$str_file .= CRLF . sprintf("\$sys['server_dir'] = '%s';", $this->gv['vars']['arpost']['server_dir']);
			$str_file .= CRLF . sprintf("\$sys['server_url'] = '%s';", $this->gv['vars']['arpost']['server_proto'].$this->gv['vars']['arpost']['server_host'].$this->gv['vars']['arpost']['server_dir']);
			$str_file .= CRLF . '?'.'>';
			$this->ar_status[] = $this->oL->m('087');
			if (!$this->sys['is_debug'])
			{
				$is_continue = $this->oFunc->file_put_contents('db_config.php', $str_file, 'w');
			}
			if ($is_continue)
			{
				$this->ar_status[(sizeof($this->ar_status)-1)] .= '... Done';
				$is_continue = 0;
			}
			else
			{
				$this->ar_status[(sizeof($this->ar_status)-1)] .= get_html_item_progress('<b>db_config.php</b><pre>'.htmlspecialchars($str_file).'</pre>', 3);
			}
			/* Lock installer */
			$this->ar_status[] = $this->oL->m('088');
			if (!$this->sys['is_debug'])
			{
				$is_continue = $this->oFunc->file_put_contents($this->sys['file_lock'], 'locked', 'w');
			}
			if ($is_continue)
			{
				$this->ar_status[(sizeof($this->ar_status)-1)] .= '... Done';
				$is_continue = 0;
			}
			else
			{
				$this->ar_status[(sizeof($this->ar_status)-1)] .= get_html_item_progress('<b>'.$this->sys['file_lock'].'</b><pre>locked</pre>', 3);
			}
			/* Do something more */
#			$this->ar_status[] = $this->oL->m('007');
			/* COMPLETE */
			$this->ar_status[] = '<b class="green">'.$this->oL->m('up004').'</b>';
			/* Link to administrative interface */
			$login_url =  $this->gv['vars']['arpost']['server_proto'].$this->gv['vars']['arpost']['server_host'].$this->gv['vars']['arpost']['server_dir'] . '/gw_admin/login.php';
			$this->ar_status[] = 'Login URL: ' . '<a href="' . $login_url . '" target="_blank">' . $login_url.'</a>';
		}
		else
		{
			/* Back to previous step */
			$this->str_before .= '<br/>';
			$this->gv['vars']['step']--;
			$this->str_before .= $this->get_form($this->gv['vars']['step']);
		}
	}
	/* */
	function get_form($step)
	{
		$tmp = '';
		$this->oForm->set('formbgcolor',       $this->arTheme['color_2']);
		$this->oForm->set('formbordercolor',   $this->arTheme['color_4']);
		$this->oForm->set('formbordercolorL',  $this->arTheme['color_1']);
		$this->oForm->set('formwidth', '75%');
		$this->oForm->set('action', THIS_SCRIPT);
		$this->oForm->set('isButtonCancel', 0);
		$this->oForm->set('align_buttons', 'right');
		switch($step)
		{
			case 2:
				/* Default password */
				if (empty($this->sys['pass_new']))
				{
					$this->sys['pass_new'] = $this->oFunc->text_make_uid(8, 0);
				}
				$tmp .= $this->get_form_title($this->oL->m('094'));
				$tmp .= '<table class="gw2TableFieldset" width="100%">';
				$tmp .= '<tr><td style="width:30%"></td><td></td></tr>';
				$tmp .= $this->get_form_tr($this->oL->m('095'), $this->oForm->field('input', 'arpost[user_name]', $this->sys['user_name']));
				$tmp .= $this->get_form_tr($this->oL->m('096'), $this->oForm->field('input', 'arpost[user_email]', $this->sys['user_email']));
				$tmp .= '</table>';
				$tmp .= $this->get_form_title($this->oL->m('050'));
				$tmp .= '<table class="gw2TableFieldset" width="100%">';
				$tmp .= '<tr><td style="width:30%"></td><td></td></tr>';
				$tmp .= $this->get_form_tr($this->oL->m('081'), $this->oForm->field('input', 'arpost[server_proto]', $this->sys['server_proto']));
				$tmp .= $this->get_form_tr($this->oL->m('046'), $this->oForm->field('input', 'arpost[server_host]', $this->sys['server_host']));
				$tmp .= $this->get_form_tr($this->oL->m('048'), $this->oL->m('101').'<br/>'.$this->oForm->field('input', 'arpost[server_dir]', $this->sys['server_dir']));
				$tmp .= '</table>';
				$tmp .= $this->get_form_title($this->oL->m('049'));
				$tmp .= '<table class="gw2TableFieldset" width="100%">';
				$tmp .= '<tr><td style="width:30%"></td><td></td></tr>';
				$tmp .= $this->get_form_tr($this->oL->m('040'), $this->oForm->field('input', 'arpost[dbhost]', $this->gv['vars']['arpost']['dbhost']));
#				$tmp .= $this->get_form_tr($this->oL->m('041'), $this->oForm->field('input', 'arpost[server_proto]', $this->gv['vars']['arpost']['dbport']));

				$arDatabases = $this->oDb->get_databases();
				if (!empty($arDatabases))
				{
					$tmp .= $this->get_form_tr($this->oL->m('042'), $this->oForm->field('select', 'arpost[dbname]', $this->gv['vars']['arpost']['dbname'], 'glossword1', $arDatabases));
				}
				else
				{
					$tmp .= $this->get_form_tr($this->oL->m('042'), $this->oForm->field('input', 'arpost[dbname]', $this->gv['vars']['arpost']['dbname']));
				}
				$tmp .= $this->get_form_tr($this->oL->m('043'), $this->oForm->field('input', 'arpost[dbprefix]', $this->gv['vars']['arpost']['dbprefix']));
				$tmp .= $this->get_form_tr($this->oL->m('044'), $this->oForm->field('input', 'arpost[dbuser]', $this->gv['vars']['arpost']['dbuser']));
				$tmp .= $this->get_form_tr($this->oL->m('045'), $this->oForm->field('pass', 'arpost[dbpass]', $this->gv['vars']['arpost']['dbpass']));
				$tmp .= '</table>';
			break;
		}
		$tmp .= $this->oForm->field('hidden', 'step', $step+1);
		return '<div class="center">'.$this->oForm->Output($tmp).'</div>';
	}
	/* */
	function get_form_tr($td1, $td2)
	{
		return sprintf('<tr><td class="td1">%s</td><td class="td2">%s</td></tr>', $td1, $td2);
	}
	/* */
	function get_form_title($t)
	{
		return '<div class="r" style="padding:4px;background:'.$this->arTheme['color_4'].'">'.$t.'</div>';
	}
	/* */
	function output()
	{
		$str_status = '';
		if (!empty($this->ar_status))
		{
			$str_status .= '<ul class="gwstatus"><li>' . implode('</li><li>', $this->ar_status) . '</li></ul>';
		}
		$str = '<div class="contents u">';
		$str .= $this->str_step;
		$str .= $this->str_before;
		$str .= $str_status;
		$str .= $this->str_after;
		$str .= '</div>';
		return $str;
	}
}
/* */

/* */


$oInstall = new gw_upgrade;
$gv['vars']['cur_funcname'] = 'step_'.$gv['vars']['step'];
/* Open step number  */
if (in_array(strtolower($gv['vars']['cur_funcname']), get_class_methods($oInstall)))
{
	$oInstall->cur_funcname = $gv['vars']['cur_funcname'];
	$oInstall->step();
}

gw_html_open();
print $oInstall->output();
gw_html_close();

?>