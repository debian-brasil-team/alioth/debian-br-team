<?php
/**
 *  $Id: theme.inc.php,v 1.9 2004/11/15 00:01:29 yrtimd Exp $
 */
/**
 *  Glossword - glossary compiler (http://glossword.info/dev/) 
 *  � 2002-2004 Dmitry N. Shilnikov <dev at glossword.info>
 */
// --------------------------------------------------------
/**
 *  Visual theme configuration file
 */
// --------------------------------------------------------
// About theme.
## name            Nice theme. What it`s name? Use latin characters only, please.
## author          Is your theme valid XHTML 1.1? Wow, what is your name?
## e-mail          Your HTML is good. Can you tell me your e-mail address?
## url             Great. Do you have a website with more themes for Glossword?
// Thank you. I hope this theme will be in popular demand.
$arThemeCfg = array(
    'name'      => 'Silver',
    'author'    => 'Dmitry N. Shilnikov',
    'email'     => 'dev@glossword.info',
    'url'       => 'http://glossword.info/'
);
// --------------------------------------------------------
// CSS colors.
// Supported notations: #FFFFFF, #FFF (recommended), rgb(255,255,255)
## color_1         background document
## color_2         background light (HTML-forms)
## color_3         color for <HR>
## color_4         <HR> light, headers
## color_5         <HR> dark
## color_6         some dark
## color_7         some gray
// --------------------------------------------------------

    $theme['color_1'] = '#FFF';    
    $theme['color_2'] = '#F9FAF8'; 
    $theme['color_3'] = '#EDF2FD'; 
    $theme['color_4'] = '#CEDBF0'; 
    $theme['color_5'] = '#BCC8E2'; 
    $theme['color_6'] = '#88A2D9'; 
    $theme['color_7'] = '#8E9AB4'; 
    $theme['color_black'] = '#000';
    $theme['color_white'] = '#FFF';

// --------------------------------------------------------
// Reconfiguration
## tblwidth        site width
## dplayput        [ 0 - default | 1 - single | 2..9 - multicolumn ] Mode for `definition preview' layout.
## split_nav       What characher should split navigation bar? ' &#160; ', ' | '.
## ...             Too deep to describe... For real PHP-freaks only.
$sys['tblwidth']  = "100%";
$sys['split_nav'] = ' &#160; ';

?>
