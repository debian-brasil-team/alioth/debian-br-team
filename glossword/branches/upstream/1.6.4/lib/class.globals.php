<?php
/**
 *  Filter global variables
 *  ==============================================
 *  � 2002-2004 Dmitry N. Shilnikov <dev at glossword.info>
 *  http://glossword.info/
 */
/** 
 * Requires:
 *     gw_fixslash();
 *     gw_fix_newline();
 *     PHP_VERSION_INT
 */
if (!defined('IS_CLASS_GLOBALS'))
{
	define('IS_CLASS_GLOBALS', 1);

class gw_register_globals
{
	function register($ar = array())
	{
		if (!is_array($ar)) { return array(); }
		global $HTTP_GET_VARS, $_GET;
		global $HTTP_POST_VARS, $_POST;
		global $HTTP_POST_FILES, $_FILES;
		$tmp = array();
		for (reset($ar); list($k, $v) = each($ar);)
		{
			if (isset($_POST[$v]) && ($_POST[$v] != ''))
			{
				/* get values from _POST */
				$tmp[$v] = $_POST[$v];
			}
			elseif (isset($_GET[$v]) && ($_GET[$v] != ''))
			{
				/* get values from _GET */
				$tmp[$v] = $_GET[$v];
			}
			elseif (isset($HTTP_POST_VARS[$v]) && ($HTTP_POST_VARS[$v] != ''))
			{
				/* compatibility */
				$tmp[$v] = $HTTP_POST_VARS[$v];
			}
			elseif (isset($HTTP_GET_VARS[$v]) && ($HTTP_GET_VARS[$v] != ''))
			{
				/* compatibility */
				$tmp[$v] = $HTTP_GET_VARS[$v];
			}
			else
			{
				/* default */
				$tmp[$v] = '';
			}
			gw_fixslash($tmp[$v], 'gpc');
			gw_fix_newline($tmp[$v]);
			if (isset($_FILES[$v]) && ($_FILES[$v] != ''))
			{
				/* get values from FILES */
				$tmp[$v] = $_FILES[$v];
			}
			elseif (isset($HTTP_POST_FILES[$v]) && ($HTTP_POST_FILES[$v] != ''))
			{
				/* compatibility for FILES */
				$tmp[$v] = $HTTP_POST_FILES[$v];
			}
		}
		return $tmp;
	}
	function sprintf(&$t, $format = '%d')
	{
		return sprintf($format, $t);
	}
	function do_default(&$t, $v)
	{
		$t = (trim($t) == '') ? $v : $t;
	}
	function do_numeric(&$t)
	{
		$t = $this->sprintf($t, '%d');
		$t = ($t == 0) ? 1 : $t;
	}
	function do_numeric_zero(&$t)
	{
		$t = $t+0;
		$t = $this->sprintf($t, '%u');
	}
	function do_substring(&$t, $int_limit = 1024)
	{
		$t = substr($t, 0, $int_limit);
	}
	function do_substring_specials(&$t, $int_limit = 1024)
	{
		htmlspecialchars($this->do_substring(strip_tags($t), $int_limit));
	}
}
/* Auto initialization */
$oGlobals = new gw_register_globals;
/* We don't need any global variables, really */
$ar = array_merge(
		(isset($_POST) ? $_POST : $HTTP_POST_VARS),
		(isset($_GET) ? $_GET : $HTTP_GET_VARS),
		(isset($_COOKIE) ? $_COOKIE : $HTTP_COOKIE_VARS),
		(isset($_FILES) ? $_FILES : $HTTP_POST_FILES)
	);
for (; list($k, $v) = each($ar);)
{
	unset($$k);
}
unset($ar);
}
/* end of file */
?>