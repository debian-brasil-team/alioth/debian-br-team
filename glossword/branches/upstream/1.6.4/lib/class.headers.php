<?php 
/**
 * � 2003-2004 Dmitry N. Shilnikov <dev at glossword.info>
 * http://glossword.info/dev/
 */
/* --------------------------------------------------------
 * Simple HTTP-headers class
 * ----------------------------------------------------- */
if (!defined('IN_GW'))
{
	die('<!-- $Id: class.headers.php,v 1.6 2004/07/06 14:18:15 yrtimd Exp $ -->');
}
/* ----------------------------------------------------- */
if (!defined('IS_CLASS_HEADERS'))
{
	define('IS_CLASS_HEADERS', 1);

class gw_headers
{
	var $is_debug = DEBUG_HTTP;
	var $arH = array();
	var $arHText = array();
	
	function add($str)
	{
		if ($str != '')
		{
			array_push($this->arH, $str);
		}
	}
	function output()
	{
		for (reset($this->arH); list($k, $v) = each($this->arH);)
		{
			@header($v);
			if ($this->is_debug)
			{
			}
			else
			{
			}
		}
	}
	function get()
	{
		return $this->arH;
	}
} /* end of class */
/* Autostart */
$oHdr = new gw_headers;
}

?>