<?php
if (!defined('IN_GW'))
{
	die('<!-- $Id: t.sys_maintenance_3.inc.php,v 1.1 2004/11/13 12:30:09 yrtimd Exp $ -->');
}
/**
 *  Glossword - glossary compiler (http://glossword.info/dev/)
 *  � 2002-2004 Dmitry N. Shilnikov <dev at glossword.info>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  (see `glossword/support/license.html' for details)
 */
/* Check permission */
if (!$auth->have_perm('admin', PERMLEVEL))
{
	return;
}
/* Script variables below */

/* Script functions below */
function gw_dict_list_cnt($vars)
{
	global $oL, $sys, ${GW_ACTION}, ${GW_TARGET}, ${GW_SID}, $tid, $oFunc, $gw_this;

	$form = new gwForms();
	$form->Set('action', $sys['page_admin']);
	$form->Set('submitok', $oL->m('2_continue'));
	$form->Set('submitcancel', $oL->m('3_cancel'));
	$form->Set('formbgcolor', $GLOBALS['theme']['color_2']);
	$form->Set('formbordercolor', $GLOBALS['theme']['color_4']);
	$form->Set('formbordercolorL', $GLOBALS['theme']['color_1']);
	$form->Set('align_buttons', $GLOBALS['sys']['css_align_right']);
	$form->Set('charset', $sys['internal_encoding']);

	$trClass = 't';
	$strForm = '';

	$strForm .= '<table cellspacing="3" cellpadding="0" border="0" width="100%">';
	$strForm .= '<tbody><tr valign="top">'.
				'<td>';
	$strForm .= '<span class="t f"><a href="#" onclick="setCheckboxes(true); return false;">'.$oL->m('select_on').'</a>'.
				' | '.
				'<a href="#" onclick="setCheckboxes(false); return false;">'.$oL->m('select_off').'</a></span>';
	$strForm .= '<table cellspacing="1" cellpadding="0" border="0" width="100%">';
	$strForm .= '<col width="1%"/><col width="4%"/><col width="95%"/><tbody>';


	$ar_dict_ids = array();
	/* Per each dictionary */
	for (reset($gw_this['arDictList']); list($k, $arDictParam) = each($gw_this['arDictList']);)
	{
		$ar_dict_ids[] = $arDictParam['id'];
		$is_assigned = 0;
		/* $vars['dictionaries'] is flipped */
		if (isset($vars['dictionaries'][$arDictParam['id']]))
		{
			$is_assigned = 1;
		}
		$str_external_link = $arDictParam['is_active'] ? '<a href="'.$sys['page_index'].'?a=list&amp;d='. $arDictParam['id'] .'" onclick="window.open(this.href);return false;">&gt;&gt;&gt;</a> ' : '';
		$strForm .= '<tr>'.
					'<td class="' . $trClass . '">' .
					$form->field('checkbox', "arPost[dictionaries][". $arDictParam['id'] . "]", $is_assigned) .
					'</td><td class="' . $trClass . '">'.
					$oFunc->number_format($arDictParam['int_terms'], 0, $oL->languagelist('4')).
					'</td><td class="' . $trClass . '">'.
					$str_external_link .
					'<label for="arPost_dictionaries_' . $arDictParam['id'] . '_">'.
					$arDictParam['title'] .
					'</label></td>'.
					'</tr>';
	}
	$strForm .= $form->field('hidden', GW_ACTION, ${GW_ACTION});
	$strForm .= $form->field('hidden', 'tid', $tid);
	$strForm .= $form->field('hidden', GW_TARGET, ${GW_TARGET});
	$strForm .= $form->field('hidden', GW_SID, ${GW_SID});
	$strForm .= $form->field('hidden', 'is', 1);
	$strForm .= '</tbody></table>';
	/* Check/Uncheck All */
	$strForm .= '<script type="text/javascript">/*<![CDATA[*/';
	$strForm .= '
		function setCheckboxes(is_check) {
			str = "";
			ardict = new Array(' . implode(',', $ar_dict_ids) . ');
			for (i = 0; i < ardict.length; i++) {
				document.forms[\'vbform\'].elements[\'arPost_dictionaries_\' + ardict[i] + \'_\'].checked = is_check;
			}
		}
	';
	$strForm .= '/*]]>*/</script>';
	/* */
	$strForm .= '</td></tr>';
	$strForm .= '</tbody></table>';
	return $form->Output($strForm);
}
/* */
function gw_dict_recount($vars)
{
	global $gw_this, $oDb;
	if (empty($vars))
	{
		return;
	}
	$str = '';
	$str .= '<ul class="t">';
	/* Per each dictionary */
	for (; list($id_dict, $v) = each($vars['dictionaries']);)
	{
		$arQ = array();
		global $arDictParam;
		$arDictParam = getDictParam($id_dict);
		if (!isset($arDictParam['tablename']))
		{
			continue;
		}
		$qDict['int_terms'] = gw_sys_dict_count_terms();
		$qDict['int_bytes'] = gw_sys_dict_count_kb();
		$arQ[] = gw_sql_update($qDict, TBL_DICT, "id = '".$arDictParam['id']."'");
		$arQ[] = 'CHECK TABLE ' . $arDictParam['tablename'];
		$arQ[] = 'OPTIMIZE TABLE ' . $arDictParam['tablename'];
		/* */
		for (; list($sqlk, $sqlv) = each($arQ);)
		{
			$oDb->sqlExec($sqlv);
		}
		$str .= '<li><span class="green"><b>'.$qDict['int_terms']. '</b></span> ' . $arDictParam['title'] .'</li>';
	}
	$str .= '</ul>';
	return $str;
}
/* Script action below */
$strR .= getFormTitleNav($oL->m(1003));
if ($is == '1')
{
	$strR .= gw_dict_recount($arPost);
}
else
{
	/* Check all dictionaries by default */
	for (reset($gw_this['arDictList']); list($k, $arDictParam) = each($gw_this['arDictList']);)
	{
		$arPost['dictionaries'][$arDictParam['id']] = 1;
	}
}
/* Get the list of dictionaries to recount */
$strR .= gw_dict_list_cnt($arPost);

?>