<?php
/**
 * HTML-Form constructor
 *
 *
 * @author  Dmitry Shilnikov <dev at glossword.info>
 * @version $Id: class.forms.php,v 1.15 2004/11/14 09:27:12 yrtimd Exp $
 */
class gwForms {
	var $title              = '';   
	var $action             = 'post.php';
	var $align_buttons      = 'right';
	var $arReq              = array();
	var $enctype            = 'application/x-www-form-urlencoded';
	var $formbgcolor        = 'DDD';
	var $formbordercolor    = '444';
	var $formbordercolorL   = 'FFF';
	var $formname           = 'vbform';
	var $formvalue          = 'none';
	var $formwidth          = '100%';
	var $isButtonDel        = 0;
	var $isButtonHelp       = 0;
	var $isButtonCancel     = 1;
	var $onclickCancel      = 'history.back(-1);document.getElementById(\'vbcontrol\').style.visibility=\'hidden\'';
	var $isButtonSubmit     = 1;
	var $str                = '';
	var $strNotes           = '&#160;';
	var $submitcancel       = ' Cancel ';
	var $submitdel          = ' Remove ';
	var $submitdelname      = 'remove';
	var $submitok           = ' OK ';
	var $arLtr              = array();
	var $charset            = 'utf-8';
	var $is_htmlspecialchars= 0;
	/**
	 *
	 */
	function setTag($tag, $var, $value)
	{
		if ($tag != '')
		{
			$this->tags[$tag][$var] = $value;
		}
	}
	function unsetTag($tag, $var = '')
	{
		if (isset($this->tags[$tag][$var]))
		{
			unset($this->tags[$tag][$var]);
		}
		elseif (isset($this->tags[$tag]))
		{
			$this->tags[$tag] = array();
		}
	}
	/**
	 *
	 */
	function htmlParamValue($ar)
	{
		$str = '';
		if (is_array($ar))
		{
			// Do sort attributes in a good manner.
			ksort($ar);
			for (reset($ar); list($k, $v) = each($ar);)
			{
				$str .= ($v != '') ? (' ' . $k . '="' . $v . '"') : '';
			}
		}
		return $str;
	} // end of htmlParamValue()
	/* */
	function Set($varname, $value)
	{
		$this->$varname = $value;
	}
	function text_field2id($t)
	{
		$t = preg_replace("/[^a-zA-Z0-9-_]/", "_", $t);
		$t = preg_replace("/_{2,}/" , "_", $t);
		return $t;
	}
	/* */
	function field($formtype = 'input', $formname = '', $value = '', $textareaheight = 2, $array = '', $autofocus = 0)
	{
		global $oFunc;
		$str = $strForm = '';
		$strSep = "..";
		if ($autofocus) // TODO: add to attribute instead of replacing
		{
			$this->setTag($formtype, 'onmouseover', 'this.focus()');
		}
		//
		$ar['type'] = $formtype;
		$ar['name'] = $formname;
		//
		$formname_id = $this->text_field2id($formname);
		//
		switch ($formtype)
		{
		case "input":
			$this->setTag($formtype, 'name',  $formname);
			$this->setTag($formtype, 'value', $value);
			$this->setTag($formtype, 'id',    $formname_id);
			$this->setTag($formtype, 'style', '');
			if (!isset($this->tags['input']['class']))
			{
				$this->setTag($formtype, 'class',  'input');
			}			
			if (!isset($this->tags['input']['type']))
			{
				$this->setTag($formtype, 'type',  'text');
			}
			if (!isset($this->tags['input']['size']))
			{
				$this->setTag('input', 'size', '20');
			}
			// fix for too long strings
			if ($oFunc->mb_strlen($value) > $this->tags['input']['size'])
			{
				$this->setTag('input', 'style', 'width:'.intval($this->tags['input']['size']/1.2).'em');
			}
			if (in_array($formname, $this->arLtr))
			{
				$this->setTag('input', 'dir', 'ltr');
			}
			$extras = $this->htmlParamValue($this->tags[$formtype]);
			$str = sprintf('<input%s />', $extras);
			#$str = htmlspecialchars($str);            
		break;
		case "file":
			if ($textareaheight > 2)
			{
				$ar['maxlength'] = $textareaheight;
			}
			$ar['type']     = "file";
			$ar['class']    = "input";
			$str .= '<input';
			$str .= $this->htmlParamValue(array_merge($ar, is_array($array) ? $array : array()));
			$str .= ' />';
		break;
		case "pass":
			if ($textareaheight > 2)
			{
				$ar['maxlength'] = $textareaheight;
			}
			$ar['type']     = "password";
			$ar['value']    = $value;
			$ar['class']    = "input";
			$ar['size']     = 20;
			$str .= '<input';
			$str .= $this->htmlParamValue(array_merge($ar, is_array($array) ? $array : array()));
			$str .= ' />';
		break;
		case "input90":
			$str .= "<input name=\"$formname\" value=\"$value\" style=\"width:60px\" class=\"input\"$mouseEvents />";
		break;
		case "radio":
			if ($textareaheight) { $ar['checked'] = "checked"; }
			$ar['type']     = "radio";
			$ar['value']    = $value;
			$str .= '<input';
			$str .= $this->htmlParamValue(array_merge($ar, $array));
			$str .= ' />';
		break;
		case "checkbox":
			$this->setTag($formtype, 'name', $formname);
			$this->setTag($formtype, 'type', $formtype);
			$this->setTag($formtype, 'id',    $formname_id);            
			$this->setTag($formtype, 'value', 1);
			if ($value == 1) // sometimes value is not just `1' or `0'
			{
				$this->setTag($formtype, 'checked', 'checked');
			}
			$extras = $this->htmlParamValue($this->tags[$formtype]);
			$str = sprintf('<input%s />', $extras);
			$this->unsetTag('checkbox');
#            $str = htmlspecialchars($str);
		break;
		case "hidden":
			$str .= '<input name="'.$formname.'" value="'.$value.'" type="hidden" />';
		break;
		case "textarea":
			$this->setTag($formtype, 'name', $formname);
			$this->setTag($formtype, 'id', $formname_id);
			$this->setTag($formtype, 'class', 'input');
			$this->setTag($formtype, 'rows', $textareaheight);
			$this->setTag($formtype, 'cols', (isset($this->tags['textarea']['cols']) ? $this->tags['textarea']['cols'] : 25) );
			$extras = $this->htmlParamValue($this->tags[$formtype]);
			$str = sprintf('<textarea%s>%s</textarea>', $extras, $value);
#            $str = htmlspecialchars($str);
		break;
		case "select":
			$this->setTag($formtype, 'name',  $formname);
			$this->setTag($formtype, 'id',    $formname_id);
			if (!isset($this->tags['select']['style']))
			{
				$this->setTag($formtype, 'style',  'width:50%');
			}
			if (in_array($formname, $this->arLtr))
			{
				$this->setTag($formtype, 'dir', 'ltr');
			}
			if ($textareaheight)
			{
				if (!preg_match("/%/", $textareaheight))
				{
					$textareaheight = $textareaheight. 'px';
				}
				$this->setTag($formtype, 'style', 'width:' . $textareaheight);
			}
			$extras = $this->htmlParamValue($this->tags[$formtype]);
			$str = sprintf('<select%s>', $extras);
			while (is_array($array) && list($k, $v) = each($array) )
			{
				$s = '';
				if (strval($k) == strval($value)){ $s = ' selected="selected"'; }
				$str .= sprintf(CRLF . "\t". '<option value="%s"%s>%s</option>', $k, $s, $array[$k]);
			}
			$str .= '</select>';
		break;
		default:
		break; // should never be
	}
		if ($this->is_htmlspecialchars)
		{
			$str .= htmlspecialchars($str);
		}
		return $str;
	} // end of field();
	/* */
	function Output($formedhtml = "")
	{
		$str = "";
		$ar = array();
		$ar['id']       = $this->formname;
		$ar['action']   = $this->action;
		$ar['enctype']  = $this->enctype;
		$ar['method']   = 'post';
		$ar['style']    = 'margin:0;padding:0';
		$ar['accept-charset'] = $this->charset;        
		// HTML start here
		$str .= '<form'. $this->htmlParamValue($ar) . '>';
		$str .= '<table width="'.$this->formwidth.'" border="0" cellspacing="1" cellpadding="1" style="margin: 0 auto;background:'.$this->formbordercolor.'">';
		$str .= '<tr><td style="background:'.$this->formbordercolorL.'">';
		$str .= '<table width="100%" border="0" cellspacing="0" cellpadding="3" style="background:'.$this->formbgcolor.'">';
		if ($this->title != '')
		{
			$str .= '<tr style="vertical-align:top"><td colspan="2" style="background:'.$this->formbordercolor.'">';
			$str .= $this->title;
			$str .= "</td></tr>";
		}
		$str .= '<tr style="vertical-align:top"><td colspan="2" style="background:'.$this->formbgcolor.'">';
		$str .= $formedhtml;
		$str .= '</td></tr>';
		$str .= '<tr>';
		$str .= '<td style="background:'.$this->formbgcolor.'">';
		$str .= $this->strNotes;
		$str .= '</td>';
		$str .= '<td style="background:'.$this->formbgcolor.'">';
		$str .= '<table style="float:'.$this->align_buttons.'" width="1%" border="0" cellspacing="0" cellpadding="2" id="vbcontrol"><tr align="center">';
		if($this->isButtonHelp)
		{
			$str .= '<td><input type="button" name="'.$this->submithelpname.'" value="'.$this->submithelp.'" class="submithelp" /></td>';
		}
		if($this->isButtonCancel)
		{
			$str .= '<td><input onclick="'.$this->onclickCancel.'" type="reset" value="'.$this->submitcancel.'" class="submitcancel"/></td>';
		}
		if($this->isButtonDel)
		{
			$str .= '<td><input onclick="document.getElementById(\'vbcontrol\').style.visibility=\'hidden\'" type="submit" name="'.$this->submitdelname.'" value="'.$this->submitdel.'" class="submitdel" /></td>';
		}
		if($this->isButtonSubmit)
		{
			$str .= '<td><input accesskey="S" tabindex="1" onclick="document.getElementById(\'vbcontrol\').style.visibility=\'hidden\'" type="submit" name="post" value="'.$this->submitok.'" class="submitok" /></td>';
		}
		$str .= '</tr></table>';
		$str .= '</td>';
		$str .= '</tr>';
		$str .= '</table>';
		$str .= '</td></tr></table>';
		$str .= '</form>';
		return $str;
	} // end of Output();

} // end of class
?>