<?php
/**
 * $Id: lib.perm.php,v 1.6 2004/06/15 15:27:54 yrtimd Exp $
 */
/**
 *  Glossword - glossary compiler (http://glossword.info/dev/) 
 *  � 2002-2004 Dmitry N. Shilnikov <dev at glossword.info>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  (see `glossword/support/license.html' for details)
 */
// --------------------------------------------------------
/**
 * Permission class.
 * Based on PHPlib concepts
 * http://phplib.shonline.de/
 */
// --------------------------------------------------------
class gwPerm
{
	var $classname = "gwPerm";
	// User levels
	var $permissions = array(
                            "anonym"	=> 1,
                            "abonent"	=> 2,
                            "assistant"  => 4,
                            "author"	=> 8,
                            "admin"      => 16
                          );
	// permissions
	var $accessnames = array(
                            "topic",
                            "dict",
                            "dict_export",
                            "term", 
                            "html",
                            "sys",
                            "user",
                            "other"
                          );
	// default access levels
	var $accesslevels = array(
                            "1" =>  "00000000",
                            "2" =>  "00000000",
                            "4" =>  "00110000",
                            "8" =>  "11110010",
                            "16" => "11111111"
                          );
	/* */
	function isAccess($name, $level)
	{
		return false;
	}

	/* */
	function have_perm($p)
	{
		global $auth;
		if ( !isset($auth->auth["perm"]) )
		{
			$auth->auth["perm"] = "";
		}
		$pageperm = split(",", $p);
		$userperm = split(",", $auth->auth["perm"]);
    	list ($ok0, $pagebits) = $this->permsum($pageperm);
		list ($ok1, $userbits) = $this->permsum($userperm);
		$has_all = (($userbits & $pagebits) == $pagebits);
		if (!($has_all && $ok0 && $ok1) )
		{
			return false;
		}
		else
		{
			return true;
		} // end of $ok
	} // end of have_perm();
	/* */
	function permsum($p)
	{
		global $auth;
    		if (!is_array($p))
		{
			return array(false, 0);
		}
		$perms = $this->permissions;
    	$r = 0;
		reset($p);
		while(list($key, $val) = each($p))
		{
			if (!isset($perms[$val]))
			{
				return array(false, 0);
			}
			$r |= $perms[$val];
		}
		return array(true, $r);
	} // end of persum();
	/* */
	function perm_invalid($does_have, $must_have)
	{ 
		printf("Access denied."); 
	} // end of perm_invalid()
} //end of class


?>