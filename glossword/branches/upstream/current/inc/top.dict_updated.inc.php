<?php
if (!defined('IN_GW'))
{
	die('<!-- $Id: top.dict_updated.inc.php,v 1.8 2004/11/13 12:30:09 yrtimd Exp $ -->');
}
/**
 *  Glossword - glossary compiler (http://glossword.info/dev/)
 *  � 2002-2004 Dmitry N. Shilnikov <dev at glossword.info>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  (see `glossword/support/license.html' for details)
 */
// --------------------------------------------------------
/**
 *  Makes the list of last updated dictionaries.
 *  This code runs inside function getTop10();
 *  switch keyname: R_DICT_UPDATED
 */
// --------------------------------------------------------
/**
 * Variables:
 * $amount, $arThText,  $arThAlign, $strTopicName, $strData, $curDateMk
 */

	$arSql = $oDb->sqlRun(sprintf($oSqlQ->getQ('top-dict-updated', $amount)), 'st');
	if (!$isItemOnly)
	{
		$arThText = array($L->m('th_1'));
		$arThWidth = array('79%');
	}
	else
	{
		$arThText = array();
		$arThWidth = array('99%');
	}
	$arThAlign = array($sys['css_align_left'], $sys['css_align_right']);
	$strTopicName = '';
	/* for each dictionary */
	for (; list($arK, $arV) = each($arSql);)
	{
		$cnt % 2 ? ($bgcolor = $GLOBALS['theme']['color_2']) : ($bgcolor = $GLOBALS['theme']['color_1']);
		$cnt++;
		$strData .= '<tr valign="top" class="t" style="background:' . $bgcolor . '">';
		$strData .= '<td>' . $cnt . '</td>';
		$strData .= '<td>' . $oHtml->a($sys['page_index'] . '?' . GW_ACTION . '=list&d=' . $arV['id'], $arV['title']) . '</td>';
		$strData .= '</tr>';
	}
	// table footer
	if (!$isItemOnly)
	{
		$strData .= '<tr valign="top" class="t" style="background:' . $GLOBALS['theme']['color_4'] . '">';
		$strData .= '<td></td>';
		$strData .= '<td>' . $oHtml->a($sys['page_index']."?a=catalog", '<b>'.$L->m('web_m_catalog').'&#160;&gt;</b>') . '</td>';
		$strData .= '</tr>';
	}
/**
 * end of R_DICT_UPDATED
 */
?>