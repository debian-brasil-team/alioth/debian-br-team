<?php
/**
 * Universal config reader/writer.
 * Copyright (c) 2002 Dmitry Shilnikov <dev at glossword.info>
 * $Id: lib.user.php,v 1.8 2004/07/09 12:44:23 yrtimd Exp $ 
 *  
 * Usage:
 *      $cfg = new gwConfigReader;
 *      $cfg->db_class  = "gwtkDataBase";
 *      $cfg->db_name   = "gwtk_users";
 *      $cfg->db_field  = "user_settings";
 *      $cfg->db_plain  = array("name", "email");
 *      $cfg->db_where  = "id";
 *      $cfg->id        = "5a09652fc358a66b247e20aa940d50be";  // session ID, if session used
 *
 *      Save new:
 *      $var = array(1 => "value");
 *      $user = "John"; 
 *      $cfg->save("a", $var);
 *      $cfg->save("user_name", $user); 
 *       
 *      Load prev. saved:
 *      print $cfg->get("user_name");
 *      print_r( $cfg->get("a") ); 
 *        
 *      Remove prev. saved:
 *      print $cfg->del("user_name"); 
 *       
 *      Access to all prev. saved values at once:
 *      print_r( $cfg->get() ); 
 *      Access to only encoded settings:
 *      print_r( $cfg->get( $cfg->db_field ) );
 *
 */

class gwConfigReader {

	## ------------------------------------------------------
	## Configuration
	/**
	 * Database class which used to read/save settings.
	 * Required for "new $this->db_class;"
	 */
	var $db_class = "";

	/**
	 * Database table where settings are stored.
	 */
	var $db_name = "";

	/**
	 * Field name where encoded (serialized) settings are stored.
	 */
	var $db_field = "";

	/**
	 * Field name, how to identify settings.
	 */
	var $db_where = "";

	/**
	 * Encoding method for reading/saving variables. Default is none (plain serialize).
	 * [ (none) | base64 | gzip ]
	 */
	var $encoding_mode = "none";

	## End of configuration
	## ------------------------------------------------------

	var $db;
	var $ar = array();
	var $isKeepClass = 0;
	var $isChanged = 0;
	var $isCache = 0;
	
	/* init */
	function start($id)
	{
		$this->id = $id;

		if (!$this->isCache)
		{
			if (defined('IS_CLASS_DB'))
			{
				$this->db = new $this->db_class;
			}
			else
			{
				print 'gwConfigReader Error: Unknown database class <b>'.$this->db_class.'</b>';
				exit;
			}
		}        
		$this->ar = $this->select_values();
	} //

	/**
	 * Updates information, if changed.
	 */
	function update()
	{
		// list plain fields
		if (isset($this->db_plain))
		{
			reset($this->db_plain);
			for(;list($k, $v) = each($this->db_plain);)
			{
				if (isset($this->ar[$v]))
				{
					$arSql[$v] = gw_addslashes(trim($this->ar[$v]));
					unset($this->ar[$v]);
				}
			}
		}
		// encoded user settings
		$arSql[$this->db_field] = gw_addslashes( serialize($this->ar) );
		reset($arSql);
		for (; list($k, $v) = each($arSql);)
		{
			$arSqlV[] = $k ."= '" . $v . "'";
		}
		$sql = sprintf("UPDATE %s SET %s WHERE %s = '%s'",
					$this->db_name,
					implode(", ", $arSqlV),
					$this->db_where,
					$this->id
				);
		$this->db->query($sql);
	}
   
	/**
	 * Saves settings.
	 * TODO: overwrite or keep existed variable
	 */
	function save($varname, $value = "")
	{
		$this->isChanged = 1;    
		$this->ar[$varname] = $value;
		return true;
	}

	/**
	 * Removes variable from settings.
	 */
	function del($varname = "")
	{
		$this->isChanged = 1;
		if ($varname == "")
		{
			reset($this->db_plain);
			for(;list($k, $v) = each($this->db_plain);)
			{
				$ar[$v] = $this->ar[$v];
			}
			$this->ar = $ar;
		}
		else
		{        
			reset($this->ar);
			for(;list($k, $v) = each($this->ar);)
			{
				if ($k == $varname)
				{
					unset($this->ar[$varname]);
				}
			}
		}
		return true;
	}
	/**
	 * Gets settings.
	 */
	function get($varname = "")
	{
		if ($varname == "")
		{
			return $this->ar;
		}
		else
		{
			for (reset($this->ar); list($k, $v) = each($this->ar);)
			{
				if ($k == $varname)
				{
					return $v;
				}
			}
		}   
		return false; 
	}

	/**
	 * Gets user settings by user Id
	 */
	function get_by_id($user_id, $varname = '')
	{
		$ar = $this->select_values($user_id);
		if ($varname == '')
		{
			return $ar;
		}
		else
		{
			for (reset($ar); list($k, $v) = each($ar);)
			{
				if ($k == $varname)
				{
					return $v;
				}
			}
		}
	}
	/**
	 * Get setting. Autoloaded from start();
	 * @see gw_fixslash(), array_merge_clobber()
	 */
	function select_values($user_id = '')
	{
		$selected = '';
		$user_id = ($user_id == '') ? $this->id : $user_id;
		$sql_select = $this->db_field;
		if (!empty($this->db_plain) && is_array($this->db_plain))
		{
			$sql_select .= ', ' . implode(', ', $this->db_plain);
		}
		/* 1st query, read database fields */
		$this->db->query(sprintf('SELECT %s FROM %s WHERE %s = "%s"',
			$sql_select,
			$this->db_name,
			$this->db_where,
			$user_id
		));
		if ($this->db->next_record())
		{
			$ar = $this->db->record;
		}
		else
		{
			$ar[$this->db_field] = 'a:0:{}';
		}
		gw_fixslash($ar);
		$arSql2 = unserialize($ar[$this->db_field]);
		unset($ar[$this->db_field]);
		$ar = array_merge($ar, $arSql2);
		/* $ar[0] could be a real problem! */
		if (isset($ar[0])) { unset($ar[0]); }
		/* 2nd query, read dictionaries */
		$this->db->query(sprintf('SELECT dict_id FROM %s WHERE user_id = "%s"',
			TBL_USERS_MAP, $user_id
		));
		$arDicts['dictionaries'] = array();
		while ($this->db->next_record())
		{ 
			$arDicts['dictionaries'][] = $this->db->record['dict_id'];
		}
		$ar['dictionaries'] = $arDicts['dictionaries'];
		if ($user_id > 1 )
		{
			# prn_r( $ar, __LINE__.__FILE__);
		}
		return $ar;
	}

	/**
	 * Called from page_close
	 */
	function freeze()
	{   
		if($this->isChanged)
		{            
			$this->update();
		}   
		if (!$this->isKeepClass)
		{
			unset($this->db);
			unset($this->db_plain);
		}
	}
} /* end of class */


/**
 *  Custom settings for gwConfigReader
 */
class gwUser extends gwConfigReader
{
	var $db_class  = 'gwtkdb';
	var $db_name   = TBL_USERS;
	var $db_plain  = array('date_reg', 'date_login', 'auth_level', 'user_name', 'user_email', 'is_showcontact');
	var $db_where  = 'auth_id';
	var $db_field = 'user_settings';
	var $isKeepClass = DEBUG_SQL_TIME;
}



?>