<?php
if (!defined('IN_GW'))
{
	die('<!-- $Id: t.sys.inc.php,v 1.9 2004/11/13 12:30:08 yrtimd Exp $ -->');
}
/**
 *  Glossword - glossary compiler (http://glossword.info/dev/)
 *  � 2002-2004 Dmitry N. Shilnikov <dev at glossword.info>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  (see `glossword/support/license.html' for details)
 */
// --------------------------------------------------------
/**
 *  Actions for a dictionary. Add, Browse, Edit, Remove, Update.
 */
/**
 * HTML-form for dictionary
 *
 * @param    array   $vars       posted variables
 * @param    int     $runtime    is this form posted first time [ 0 - no | 1 - yes ] // todo: bolean
 * @param    array   $arBroken   the names of broken fields (after post)
 * @param    array   $arReq      the names of required fields (after post)
 * @return   string  complete HTML-code
 * @see textcodetoform(), getFormHeight()
 */
function getFormSys($vars, $runtime = 0, $arBroken = array(), $arReq = array())
{
	global $a, $sys, $id, $tid, $topic_mode, $oFunc, $L, ${GW_SID};
	$topic_mode = "form";
	$strForm = "";
	$trClass = "t";
	$form = new gwForms();

	$form->Set('action', $GLOBALS['sys']['page_admin']);
	$form->Set('submitok', $L->m('3_save'));
	$form->Set('submitcancel', $L->m('3_cancel'));
	$form->Set('formbgcolor', $GLOBALS['theme']['color_2']);
	$form->Set('formbordercolor', $GLOBALS['theme']['color_4']);
	$form->Set('formbordercolorL', $GLOBALS['theme']['color_1']);
	$form->Set('align_buttons', $GLOBALS['sys']['css_align_right']);
	$form->Set('charset', $sys['internal_encoding']);
	
	$form->arLtr = array('arPost[y_email]',
						 'arPost[locale_name]', 'arPost[themename]','arPost[path_log]',
						 'arPost[page_limit]', 'arPost[time_new]', 'arPost[time_upd]', 'arPost[defn_cut]',
						 );
	## ----------------------------------------------------
	##
	// reverse array keys <-- values;
	$arReq = array_flip($arReq);
	// mark fields as "REQUIRED" and make error messages
	while(is_array($vars) && list($key, $val) = each($vars) )
	{
		$arReqMsg[$key] = $arBrokenMsg[$key] = "";
		if (isset($arReq[$key])) { $arReqMsg[$key] = '&#160;<span class="red"><b>*</b></span>'; }
		if (isset($arBroken[$key])) { $arBrokenMsg[$key] = ' <span class="'.$trClass.'" style="color:#E30"><b>' . $L->m('reason_9') .'</b></span><br/>'; }
	} // end of while
	##
	## ----------------------------------------------------

	$strForm .= getFormTitleNav($L->m('sect_general'), '');
	$strForm .= '<table cellspacing="3" cellpadding="0" border="0" width="100%">';
	$strForm .= '<col style="width:30%;text-align:'.$GLOBALS['sys']['css_align_left'].'"/><col style="width:70%"/>';
	$strForm .= '<tr valign="top">'.
				'<td class="' . $trClass . '">' . $L->m('site_name') . ':' .  $arReqMsg["site_name"] . '</td>'.
				'<td>' . $arBrokenMsg['site_name'] . $form->field("input", "arPost[site_name]", textcodetoform($vars['site_name']), 255) . '</td>'.
				'</tr>';
	$strForm .= '<tr valign="top">'.
				'<td class="' . $trClass . '">' . $L->m('site_desc') . ':' .  $arReqMsg["site_desc"] . '</td>'.
				'<td>' . $arBrokenMsg['site_desc'] . $form->field("input", "arPost[site_desc]", textcodetoform($vars['site_desc']), 255) . '</td>'.
				'</tr>';
	$strForm .= '<tr valign="top">'.
				'<td class="' . $trClass . '">' . $L->m('contact_name') . ':' .  $arReqMsg["y_name"] . '</td>'.
				'<td>' . $arBrokenMsg['y_name'] . $form->field("input", "arPost[y_name]", textcodetoform($vars['y_name']), 127) . '</td>'.
				'</tr>';
	$strForm .= '<tr valign="top">'.
				'<td class="' . $trClass . '">' . $L->m('contact_email') . ':' .  $arReqMsg["y_email"] . '</td>'.
				'<td>' . $arBrokenMsg['y_email'] . $form->field("input", "arPost[y_email]", textcodetoform($vars['y_email']), 127) . '</td>'.
				'</tr>';
	$strForm .= '<tr valign="top">'.
				'<td class="' . $trClass . '">' . $L->m('keywords') . ':' .  $arReqMsg["keywords"] . '</td>'.
				'<td>' . $arBrokenMsg['keywords'] . $form->field("textarea", "arPost[keywords]", textcodetoform($vars['keywords']), $oFunc->getFormHeight($vars['keywords'])) . '</td>'.
				'</tr>';
	$strForm .= '<tr valign="top">'.
				'<td class="' . $trClass . '">' . $L->m('default_lang') . ':' .  $arReqMsg["locale_name"] . '</td>'.
				'<td>' . $arBrokenMsg['locale_name'] . $form->field("select", "arPost[locale_name]", $vars['locale_name'], 0, $L->getLanguages()) . '</td>'.
				'</tr>';
	$strForm .= '</table>';

	$strForm .= getFormTitleNav($L->m('sect_visual'), '');
	$strForm .= '<table cellspacing="3" cellpadding="0" border="0" width="100%">';
	$strForm .= '<col style="width:70%;text-align:'.$GLOBALS['sys']['css_align_left'].'"/><col style="width:30%"/>';

	$form->setTag('select', 'style', 'width:100%');
	$strForm .= '<tr valign="top">'.
				'<td class="' . $trClass . '">' . $L->m('themename') . ':' .  $arReqMsg['themename'] . '</td>'.
				'<td>' . $form->field("select", "arPost[themename]", $vars['themename'], 0, $GLOBALS['arThemes']) . '</td>'.
				'</tr>';

	$form->setTag('input', 'maxlength', '4');
	$strForm .= '<tr valign="top">'.
				'<td class="' . $trClass . '">' . $L->m('page_limit') . ':' .  $arReqMsg["page_limit"] . '</td>'.
				'<td>' . $arBrokenMsg['page_limit'] . $form->field('input', "arPost[page_limit]", $vars['page_limit']) . '</td>'.
				'</tr>';
	$strForm .= '<tr valign="top">'.
				'<td class="' . $trClass . '">' . $L->m('time_new') . ':' . $arReqMsg["time_new"] . '</td>'.
				'<td>' . $arBrokenMsg['time_new'] . $form->field('input', "arPost[time_new]", $vars['time_new']) . '</td>'.
				'</tr>';
	$strForm .= '<tr valign="top">'.
				'<td class="' . $trClass . '">' . $L->m('time_upd') . ':' . $arReqMsg["time_upd"] . '</td>'.
				'<td>' . $arBrokenMsg['time_upd'] . $form->field('input', "arPost[time_upd]", $vars['time_upd']) . '</td>'.
				'</tr>';
	$strForm .= '<tr valign="top">'.
				'<td class="' . $trClass . '">' . $L->m('defn_cut') . ':' . $arReqMsg["defn_cut"] . '</td>'.
				'<td>' . $arBrokenMsg['defn_cut'] . $form->field('input', "arPost[defn_cut]", $vars['defn_cut']) . '</td>'.
				'</tr>';
	$strForm .= '</table>';

	$strForm .= $form->field("hidden", GW_SID, ${GW_SID});
	$strForm .= $form->field("hidden", GW_ACTION, CFG);
	$strForm .= $form->field("hidden", GW_TARGET, SYS);


	return $form->Output($strForm);
}
// --------------------------------------------------------
$arReq = array(
	'path_tpl', 'path_log',  'path_cache',
	'site_name', 'site_desc', 'y_email', 'y_name', 'locale_name', 'themename',
	'page_limit', 'time_new', 'time_upd', 'defn_cut', 'srch_len',
);
// --------------------------------------------------------
// Check permission
if ($auth->have_perm('admin', PERMLEVEL))
{
// --------------------------------------------------------
// action switcher
switch (${GW_ACTION})
{
case CFG:
## --------------------------------------------------------
##
	if ($post == '') // not saved
	{
		if(!isset($tid))
		{
			gwtk_header($sys['server_proto'].$sys['server_host'].$sys['page_admin']);
		}
		// set default values
		$arBroken   = array();
		$strR .= getFormSys($sys, 0, $arBroken, $arReq);
	}
	else // parse posted values
	{
		$arBroken = validatePostWalk($arPost, $arReq);
		if(sizeof($arBroken) == 0)
		{
			$isPostError = 0;
		}
		else
		{
			$strR .= getFormSys($arPost, 1, $arBroken, $arReq);
		}
		if (!$isPostError) // final update
		{
			$arPost['is_cache'] = isset($arPost['is_cache']) ? 1 : 0;
			$arPost['cache_zlib'] = isset($arPost['cache_zlib']) ? 1 : 0;
			$arPost['page_limit'] = preg_replace("/[^0-9]/", '', $arPost['page_limit']);
			$arPost['time_new'] = preg_replace("/[^0-9]/", '', $arPost['time_new']);
			$arPost['time_upd'] = preg_replace("/[^0-9]/", '', $arPost['time_upd']);
			$arPost['defn_cut'] = preg_replace("/[^0-9]/", '', $arPost['defn_cut']);
			for (reset($arPost); list($k, $v) = each($arPost);)
			{
				$q = array();
				$q['settings_val'] = $v;
				$queryA[] = gw_sql_update($q, TBL_SETTINGS, 'settings_key = "' . $k .'"');
			}
			$strR .= gw_tmp_clear();
			$strR .= postQuery($queryA, '', $sys['isDebugQ']);

		} // final update
	}
##
## --------------------------------------------------------
break;
default:
break;
} // end of switch
}

?>
