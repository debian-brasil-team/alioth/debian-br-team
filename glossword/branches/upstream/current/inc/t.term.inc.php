<?php
if (!defined('IN_GW'))
{
	die('<!-- $Id: t.term.inc.php,v 1.9 2004/07/06 14:18:14 yrtimd Exp $ -->');
}
/**
 *  Glossword - glossary compiler (http://glossword.info/dev/)
 *  � 2002-2004 Dmitry N. Shilnikov <dev at glossword.info>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  (see `glossword/support/license.html' for details)
 */
// --------------------------------------------------------
/**
 *  Actions for a terms. Add, Edit, Remove.
 *
 */
// --------------------------------------------------------

function gw_getFormDefinition($arParsed, $is_first = 1, $arReq = array())
{
	global $theme, $L, $sys, $id, $tid, ${GW_SID}, ${GW_ACTION},
			$arDictParam, $arFields, $objDom;

	$strForm = ''; // all HTML-code for the form
	$tmp = array(); // all temporary variables for this function
	$tmp['cssTrClass'] = 't';
	// ----------------------------------------------------
	// prepare HTML-form settings
	$form = new gw_htmlforms;
	$form->Set('action',          $sys['page_admin']     );
	$form->Set('submitok',        $L->m('3_save')   );
	$form->Set('submitdel',       $L->m('3_remove') );
	$form->Set('submitcancel',    $L->m('3_cancel') );
	$form->Set('formbgcolor',     $theme['color_2'] );
	$form->Set('formbordercolor', $theme['color_4'] );
	$form->Set('formbordercolorL',$theme['color_1'] );
	$form->Set('align_buttons',   $sys['css_align_right']);
	$form->Set('charset',         $sys['internal_encoding']);

	$form->Set('Gsys', $sys ); // system, settings
	$form->Set('Gtmp', $tmp ); // temporary settings for current function
	$form->Set('L', $L ); // language
	$form->Set('objDom', $objDom ); //
	$form->Set('arDictParam', $arDictParam );
	$form->load_abbr_trns();

	// ----------------------------------------------------
	//
	// ----------------------------------------------------
	$strForm .= $form->field("hidden", GW_TARGET, GW_T_TERM);
	$strForm .= $form->field("hidden", "id", $id);
	$strForm .= $form->field("hidden", "tid", $tid);
	$strForm .= $form->field("hidden", GW_SID, ${GW_SID});
	if (${GW_ACTION} == GW_A_ADD)
	{
		$tmp['intFormHeight'] = 8;
		$strForm .= $form->field("hidden", GW_ACTION, GW_A_ADD);
		$strForm .= getFormTitleNav($L->m('term'), '');
	}
	else
	{
		$strForm .= $form->field("hidden", GW_ACTION, UPDATE);
		$strForm .= getFormTitleNav($L->m('term') .
				' (<a href="index.php?a=term&d='.$id.'&t='.$tid.'" onclick="window.open(this.href);return false;">www</a>)',
				'<input style="width:24px" class="submitdel" title="'. $L->m('3_remove') . ' ' .
				'" type="submit" value="&#215;" name="arPre[term][remove]['.$id.']" ' .
				'onclick="vbcontrol.style.visibility=\'hidden\'"/>');
	}
	// ...

	// Send XML array to $form class
	$form->Set('arEl', $arParsed );

	// prepare form fields
	$strForm .= '<table cellspacing="1" cellpadding="2" border="0" width="100%">';

	$form->Set('arFields', $arFields );
	// Go for each configured root field.
	for (reset($arFields); list($fK, $fV) = each($arFields);)
	{
		if(isset($fV[4]) && $fV[4]) // select root elements only here
		{
			//
			if ($fV[0] == 'term') // terms always presents
			{
				$strForm .= $form->tag2field($fV[0]);
			}
			else
			{
				// other tags are switchable, parsed inside $form class
				if ($arDictParam['is_'.$fV[0]])
				{
					$strForm .= $form->tag2field($fV[0]);
				}
			}
		} // end of root elements
	} // end of per-field process

	$strForm .= '</table>';

	/* 1.6.3 */
	$strForm .= getFormTitleNav($L->m('options'));
	global $user;
	$tmp['after_post'][1] = $tmp['after_post'][2] = $tmp['after_post'][3] = '';
	$tmp['after_is_save'] = $user->get('after_is_save') ? ' checked="checked"' : '';
	$tmp['after_post'][$user->get('after_post')] = ' checked="checked"';
	if (isset($tmp['after_post'][0]))
	{
		/* turn on second option by default */
		$tmp['after_post'][GW_AFTER_TERM_ADD] = $tmp['after_post'][0];
	}
	$strForm .= '<table class="gw2TableFieldset" cellspacing="0" cellpadding="0" border="0" width="100%">';
	$strForm .= '<tr><td style="width:15%"></td><td></td></tr>';
	$strForm .= '<tr>'.
				'<td class="td1">' . $L->m('after_post') . '</td>'.
				'<td class="td2">' .
				'<input type="radio" name="arPost[after]" id="arPost_after_1" value="1"'.$tmp['after_post'][1].'><label for="arPost_after_1">'.$L->m('after_post_1').'</label>'.
				'<input type="radio" name="arPost[after]" id="arPost_after_2" value="2"'.$tmp['after_post'][2].'><label for="arPost_after_2">'.$L->m('3_add_term').'</label>'.
				'<input type="radio" name="arPost[after]" id="arPost_after_3" value="3"'.$tmp['after_post'][3].'><label for="arPost_after_3">'.$L->m('after_post_3').'</label>'.
				'<br/><input type="checkbox" name="arPost[after_is_save]" id="arPost_after_is_save" value="1"'.$tmp['after_is_save'].'><label for="arPost_after_is_save">'.$L->m('after_post_is_save_term').'</label>'.
				'</select>'.
				'</td>'.
				'</tr>';
	$strForm .= '</table>';

	return $form->Output($strForm);
}

// --------------------------------------------------------
// Check permission

if ($auth->is_allow('term', $id))
{
// --------------------------------------------------------
// action switcher
switch (${GW_ACTION})
{
case GW_A_ADD:
## --------------------------------------------------------
## Add a term
	if ($post == '') // not saved
	{
		$is_first = 1;
		if (!isset($id))
		{
			gwtk_header($sys['server_proto'].$sys['server_host'].$sys['page_admin']);
		}
		$arBroken = array();
		// default values
		$arParsed['term'][0]['value'] = '';
		$arParsed['term'][0]['attributes']['t1'] = '';
		$arParsed['term'][0]['attributes']['t2'] = '';
		$arParsed['trsp'][0]['value'] = '';
		$arParsed['abbr'][0][0]['value'] = '';
		$arParsed['abbr'][0][0]['attributes']['lang'] = '';
		$arParsed['defn'][0]['value'] = '';
		/* Restore term settings */
		if ($user->get('form_term_'.$id))
		{
			$arParsed = $user->get('form_term_'.$id);
		}
		/* additional actions */
		if (is_array($arPre))
		{
			$is_first = 0;
			$arParsed = gw_ParsePre($arParsed, $arPre);
		}
		$objDom = new gw_domxml;
		$objDom->setCustomArray($arParsed);
		$strR .= gw_getFormDefinition($arParsed, $is_first, $arReq[$t]);
	}
	elseif ($post != '') // parse post
	{
		// for custom DOM model
		$arPre['term'][0]['tag'] = 'term';
		$isPostError = 0;
		$queryA = array();

		$objDom = new gw_domxml;
		$objDom->setCustomArray($arPre);

		// check for broken fields
		if ($arPre['term'][0]['value'] == '')
		{
			$is_first = 0;
			$isPostError = 1;
			$strR .= gw_getFormDefinition($arPre, $is_first, $arReq[$t]);
		}
		if (!$isPostError) // final update
		{
#			$id_w = $oDb->MaxId($arDictParam['tablename']);
			$q = array();
			/* Exclude stopwords */
			$arStop = $L->getCustom('stop_words', $arDictParam['lang'], 'return');

			$tmp['cssTrClass'] = 't';
			$oRender = new gw_render;

			$oRender->Set('Gtmp', $tmp );
			$oRender->Set('Gsys', $sys );
			$oRender->Set('L', $L );
			$oRender->Set('objDom', $objDom );
			$oRender->Set('arDictParam', $arDictParam );
			$oRender->Set('arEl', $arPre );
			$oRender->Set('arFields', $arFields );
			//
			$arPre['parameters']['xml'] = $oRender->array_to_xml($arPre);
			$arPre['parameters']['action'] = UPDATE;
			//
			// update table TBL_DICT
			$queryA = $q = array();
			$arPre['is_specialchars'] = 1;
			$arPre['is_overwrite'] = 0;
#            $sys['isDebugQ'] = 1;
			//
			// Exclude stopwords
			$arStop = $L->getCustom('stop_words',  $arDictParam['lang'], 'return');
			//
			// Construct queries for the term
			$ar = gwAddTerm($arPre, $id, $arStop, 1, $arPre['is_specialchars'], $arPre['is_overwrite'], 0);
			//
			if (is_array($ar))
			{
				$queryA = array_merge($queryA, $ar);
			}
			else
			{
				$strR .= $ar;
			}
			/* New query: clean search results for edited dictionary */
			$queryA[] = $oSqlQ->getQ('del-srch-by-dict', $d);
			/* Clear dictionary cache */
			$strR .= gw_tmp_clear($id);
			$arPost['after_is_save'] = isset($arPost['after_is_save']) ? 1 : 0;

			/* Add term settings to user settings */
			if ($arPost['after_is_save'])
			{
				/* Save, but clear 'value' keys */
				$user->save('after_is_save', 1 );
				$user->save('form_term_'. $id, array_clear_key($arPre, 'value') );
			}
			else
			{
				$user->del('after_is_save');
				$user->del('form_term_'. $id);
			}
			if (empty($strR))
			{
				/* Redirect to... */
				$str_url = gw_after_redirect_url($arPost['after']);
				$strR .= postQuery($queryA, $str_url, $sys['isDebugQ'], 0);
			}
		}
	}
##
## --------------------------------------------------------
break;
case GW_A_EDIT:
## --------------------------------------------------------
## Edit selected term

	if ($post == '') // not saved
	{
		// read data from database,
		// see `if(!empty($tid))' at admin.php
		if (isset($arTermParam['term']))
		{
			//
			$arParsed['term'][0]['value'] = $arTermParam['term'];
			$arParsed['term'][0]['attributes']['t1'] = $arTermParam['term_1'];
			$arParsed['term'][0]['attributes']['t2'] = $arTermParam['term_2'];
			//
			// Convert XML data into structured array
			//
			$arParsed = array_merge_clobber($arParsed, gw_Xml2Array($arTermParam['defn']));
			//
			$objDom = new gw_domxml;
			$objDom->setCustomArray($arParsed);
			//
			$is_first = 1;
			if (is_array($arPre))
			{
				$is_first = 0;
				$arParsed = gw_ParsePre($arParsed, $arPre);
			}
			$strR .= gw_getFormDefinition($arParsed, $is_first, $arReq[$t]);
		}
		else // error
		{
			$strR .= $L->m('reason_13');
		}
	} // end of submit
	else
	{
		$objDom = new gw_domxml;
		$objDom->setCustomArray($arPre);
		$isPostError = 0;
		// check for broken fields
		if ($arPre['term'][0]['value'] == '')
		{
			$is_first = 0;
			$isPostError = 1;
			$strR .= gw_getFormDefinition($arPre, $is_first, $arReq[$t]);
		}
		if (!$isPostError) // final update
		{
			// for custom DOM model
			$arPre['term'][0]['tag'] = 'term';

		$tmp['cssTrClass'] = 't';
		$oRender = new gw_render;
		$oRender->Set('Gtmp', $tmp );
		$oRender->Set('Gsys', $sys ); // system, settings
		$oRender->Set('L', $L ); // language
		$oRender->Set('objDom', $objDom ); //
		$oRender->Set('arDictParam', $arDictParam );
		$oRender->Set('arEl', $arPre );
		$oRender->Set('arFields', $arFields );
		//
		$arPre['parameters']['xml'] = $oRender->array_to_xml($arPre);
		$arPre['parameters']['action'] = UPDATE;
		//
		// update table TBL_DICT
		$queryA = $q = array();
		$arPre['is_specialchars'] = 1;
		$arPre['is_overwrite'] = 1;
#		$sys['isDebugQ'] = 1;
		//
		// Exclude stopwords
		$arStop = $L->getCustom('stop_words',  $arDictParam['lang'], 'return');
		//
		// Construct queries for the term
		$ar = gwAddTerm($arPre, $id, $arStop, 1, $arPre['is_specialchars'], $arPre['is_overwrite'], 0, 1);
		//
		if (is_array($ar))
		{
			$queryA = array_merge($queryA, $ar);
		}
		else // on error
		{
			$strR .= $ar;
		}
			/* Term was edited, clean search results for edited dictionary */
			$queryA[] = $oSqlQ->getQ('del-srch-by-dict', $d);
			/* Clear cache */
			$strR .= gw_tmp_clear($id);
			/* Redirect to... */
			$str_url = gw_after_redirect_url($arPost['after']);
			$strR .= postQuery($queryA, $str_url, $sys['isDebugQ'], 0);
		}
	}
##
## --------------------------------------------------------
break;
case REMOVE:
## --------------------------------------------------------
## Remove selected term
	if (!isset($tid) || !isset($arTermParam["term"]))
	{
		$strR .= $L->m('reason_13');
	}
	else
	{
		if (!$isConfirm) /* if not confirmed */
		{
			$str_question = '<p class="r"><span class="s"><b>' . $L->m('9_remove') .'</b></span></p>'
										. '<p class="t"><span class="f">' . $L->m('3_'.${GW_ACTION}) .':</span><br/>' . $arTermParam['term']. '</p>'
										. '<p class="t"><span class="f">'. $L->m('defn') . ':</span><br/>'
										. $oFunc->mb_substr(strip_tags($arTermParam['defn']), 0, $GLOBALS['sys']['defn_cut']). '&#8230;</p>';
	$tmp['after_post'][1] = $tmp['after_post'][2] = $tmp['after_post'][3] = '';
	$tmp['after_post'][$user->get('after_post')] = ' checked="checked"';
	if (isset($tmp['after_post'][0]))
	{
		/* turn on second option by default */
		$tmp['after_post'][3] = $tmp['after_post'][0];
	}

			$strForm = '';
			$strForm .= '<table class="gw2TableFieldset" cellspacing="0" cellpadding="0" border="0" width="100%">';
			$strForm .= '<tr><td style="width:25%"></td><td></td></tr>';
			$strForm .= '<tr>'.
						'<td class="td1">' . $L->m('after_post') . '</td>'.
						'<td class="td2">' .
						'<input type="radio" name="arPost[after]" id="arPost_after_1" value="'.GW_AFTER_DICT_UPDATE.'"'.$tmp['after_post'][1].'><label for="arPost_after_1">'.$L->m('after_post_1').'</label>'.
						'<br/><input type="radio" name="arPost[after]" id="arPost_after_2" value="'.GW_AFTER_TERM_ADD.'"'.$tmp['after_post'][2].'><label for="arPost_after_2">'.$L->m('3_add_term').'</label>'.
						'<br/><input type="radio" name="arPost[after]" id="arPost_after_3" value="'.GW_AFTER_SRCH_BACK.'"'.$tmp['after_post'][3].'><label for="arPost_after_3">'.$L->m('after_post_3').'</label>'.
						'</select>'.
						'</td>'.
						'</tr>';
			$strForm .= '</table>';

			$str_question .= $strForm;

			$Confirm = new gwConfirmWindow;
			$Confirm->action = $GLOBALS['sys']['page_admin'];
			$Confirm->submitok = $L->m('3_remove');
			$Confirm->submitcancel = $L->m('3_cancel');
			$Confirm->formbgcolor = $GLOBALS['theme']['color_2'];
			$Confirm->formbordercolor = $GLOBALS['theme']['color_4'];
			$Confirm->formbordercolorL = $GLOBALS['theme']['color_1'];
			$Confirm->setQuestion($str_question);
			$Confirm->tAlign = "center";
			$Confirm->formwidth = "400";
			$Confirm->setField("hidden", GW_ACTION, REMOVE);
			$Confirm->setField("hidden", GW_TARGET, ${GW_TARGET});
			$Confirm->setField("hidden", "id", $id);
			$Confirm->setField("hidden", "tid", $tid);
			$Confirm->setField("hidden", GW_SID, ${GW_SID});
			$strR .= $Confirm->Form();

		}
		else
		{
			// 1) delete record from table DBTABLE
			// 2) update table TBL_DICT
			// 3) CHECK and OPTIMIZE table TBL_DICT
			$queryA = array();
			$queryA[] = 'DELETE FROM ' . $DBTABLE . ' WHERE id = "' . $tid . '"';
			$queryA[] = 'DELETE FROM ' . TBL_WORDMAP . ' WHERE term_id = "' . $tid . '"';
			$qDict['date_modified'] = date("YmdHis", mktime());
			$qDict['int_terms'] = gw_sys_dict_count_terms();
			$qDict['int_bytes'] = gw_sys_dict_count_kb();
			$where = 'id = "' . $id . '"';
			$queryA[] = gw_sql_update($qDict, TBL_DICT, $where);
			/* Clear cache */
			$strR .= gw_tmp_clear($id);
			/* Optimization for previous (not current) modifications */
			gw_sys_dict_check();
			/* Redirect to... */
			$str_url = gw_after_redirect_url($arPost['after']);
			$strR .= postQuery($queryA, $str_url, $sys['isDebugQ'], 0);
		}
	}
##
## --------------------------------------------------------
break;
case UPDATE:
## --------------------------------------------------------
## Update a term
	if (isset($arReq[$t]) && isset($arParsed)) /* check for broken fields */
	{
		$arBroken = validatePostWalk($arParsed, $arReq[$t]);
	}
	if (isset($arReq[$t]) && sizeof($arBroken) == 0)
	{
		$isPostError = 0;
	}
	else
	{
		$arTermParam['t_term'] = $t_term;
		$isPostError = 1;
		$strR .= getFormDefn($arParsed, $arTermParam, 0, $arBroken, $arReq[$t]);
	}
	if (!$isPostError)
	{

	}
##
## --------------------------------------------------------
break;
default:
break;
} // end of switch
// --------------------------------------------------------
// End check permission
}
else
{
	$strR .= $L->m('reason_13');
}

?>