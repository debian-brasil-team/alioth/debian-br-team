<?php
if (!defined('IN_GW'))
{
	die('<!-- $Id: func.browse.inc.php,v 1.16 2004/11/14 09:27:12 yrtimd Exp $ -->');
}
/**
 *  Glossword - glossary compiler (http://glossword.info/dev/)
 *  © 2002-2004 Dmitry N. Shilnikov <dev at glossword.info>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  (see `glossword/support/license.html' for details)
 */
// --------------------------------------------------------
/**
 *  Math, SQL, HTML functions for browsing dictionary.
 */
// --------------------------------------------------------

/**
 * Feedback form
 *
 * @param    array   $vars       posted variables
 * @param    int     $runtime    is this form posted first time [ 0 - no | 1 - yes ]
 * @param    array   $arBroken   the names of broken fields (after post)
 * @param    array   $arReq      the names of required fields (after post)
 * @return   string  complete HTML-code
 * @see textcodetoform()
 */
function scrFeedback($vars, $runtime = 0, $arBroken = array(), $arReq = array())
{
	global $L, $sys, $tmp;
	$strForm = "";
	include_once( $sys['path_include'].'/class.forms.php' );

	$form = new gwForms();
	$form->action = "index.php";
	$trClass = "t";
	$form->Set('submitok', $L->m('3_send'));
	$form->Set('submitcancel', $L->m('3_cancel'));
	$form->Set('formbgcolor', $GLOBALS['theme']['color_2']);
	$form->Set('formbordercolor', $GLOBALS['theme']['color_4']);
	$form->Set('formbordercolorL', $GLOBALS['theme']['color_1']);
	$form->Set('formwidth', 450);
	$form->Set('align_buttons', $sys['css_align_right']);
	$form->Set('charset', $sys['internal_encoding']);
	$form->arLtr = array('email','url');
	## ----------------------------------------------------
	##

	/* reverse array keys <-- values; */
	$arReq = array_flip($arReq);
	/* mark fields as "REQUIRED" and make error messages */
	while(is_array($vars) && list($key, $val) = each($vars) )
	{
		$arReqMsg[$key] = $arBrokenMsg[$key] = "";
		if (isset($arReq[$key])) { $arReqMsg[$key] = '&#160;<span style="color:#E30"><b>*</b></span>'; }
		if (isset($arBroken[$key])) { $arBrokenMsg[$key] = ' <span style="color:#E30"><b>' . $L->m('reason_9') . '</b></span><br/>'; }
	}
	##
	## ----------------------------------------------------
		$strForm = "";
		$strForm .= '<table class="gw2TableFieldset" cellspacing="0" cellpadding="0" border="0" width="100%">';
		$strForm .= '<tr><td style="width:25%"></td><td></td></tr>';
		$strForm .= '<tr>'.
					'<td class="td1">' .$L->m('y_name') . ':' . $arReqMsg['name'] . '</td>'.
					'<td class="td2">' . $arBrokenMsg['name'] . $form->field("input","name", textcodetoform($vars['name'])) . '</td>'.
					'</tr>';
		$strForm .= '<tr>'.
					'<td class="td1">' . $L->m('y_email') . ':' . $arReqMsg['email'] . '</td>'.
					'<td class="td2">' . $arBrokenMsg['email'] . $form->field("input", "email", textcodetoform($vars['email']) ) . '</td>'.
					'</tr>';
		$strForm .= '<tr>'.
					'<td class="td1">' . $L->m('message') . ':' . $arReqMsg['message'] . '</td>'.
					'<td class="td2">' . $arBrokenMsg['message'] . $form->field("textarea", "arPost[message]", textcodetoform($vars['message']), 10) . '</td>'.
					'</tr>';
		$strForm .= '</table>';
		$strForm .= $form->field("hidden", "a","fb");
		$strForm .= $tmp['input_url_append'];
		return $form->Output($strForm);
}


/**
 * Top 10
 *
 * @param    int    $m [ R_DICT_AVERAGEHITS | R_DICT_EFFIC |
 *                       R_DICT_NEWEST | R_TERM_NEWEST |
 *                       R_DICT_UPDATED | R_POLL ] // todo ...
 * @return   string  complete HTML-code
 * @see htmlTagsA()
 * @global $L
 */
function getTop10($m, $amount = 10, $isItemOnly = 0)
{
	global $L, $sys, $gw_this;
	global $oHtml, $oDb, $oSqlQ, $arDictParam;
	global $dictID, $oFunc;
	$str = '';
	$cnt = 0;
	/* Current date in UNIX timestamp */
	$curDateMk  = mktime(date("H,i,s,Y,m,d"));
	$arThText = $arThWidth = array();
	$strData = $strTopicName = '';
	$m = strtolower(str_replace('R_', '', $m));
	$filename = $sys['path_include'] . '/top.' . $m . '.inc.' . $sys['phpEx'];
	if (file_exists($filename))
	{
		include_once( $filename );
	}
	else
	{
		print("Can't find " . $filename);
	}

	$intRows = sizeof($arThWidth);

	if (!empty($arThText) || !empty($arThWidth))
	{
		$str .= '<table style="background:'.$GLOBALS['theme']['color_1'].'" width="100%" border="0" cellpadding="3" cellspacing="1">';
		$str .= '<col align="'.$sys['css_align_right'].'" width="1%"/>';
		for (reset($arThWidth); list ($kT, $vT)= each($arThWidth);)
		{
			$str .= '<col width="'.$vT.'" align="'.$arThAlign[$kT].'"/>';
		}
		$str .= '<thead>';
		if ($strTopicName != '')
		{
			$str .= '<tr>'
				 . '<td style="background:'.$GLOBALS['theme']['color_4'].';text-align:'.$sys['css_align_left'].'" colspan="' . ($intRows + 1) . '" class="r">'
				 . '&#160;' . $strTopicName
				 . '</td>'
				 . '</tr>';
		}
		if (!empty($arThText))
		{
			$str .= '<tr class="a" style="color:'.$GLOBALS['theme']['color_1'].';background:'.$GLOBALS['theme']['color_6'].'">';
			$str .= '<th class="gw" style="text-align:center">N</th>';
			reset($arThText);
			for (; list ($kT, $vT)= each($arThText);)
			{
				$str .= '<th class="gw">' . $vT . '</th>';
			}
			$str .= '</tr>';
		}
		$str .= '</thead>';
		$str .= '<tbody>';
		$str .= $strData;
		$str .= '</tbody></table>';
	}
	return $str;
}

/* */
function getValidDictID()
{
	global $oSqlQ, $oDb;
	$arSql = $oDb->sqlRun($oSqlQ->getQ('get-dict-valid'), 'st');
	return $arSql;
}

/* */
function getStat()
{
	global $oSqlQ, $oDb;
	$arSql = $oDb->sqlRun($oSqlQ->getQ('get-terms-total'), 'st');
	$arSql = isset($arSql[0]) ? $arSql[0] : array('num' => 0, 'sum' => 0);
	$arSql['date'] = date("YmdHis");
	return $arSql;
}

/* */
function getSettings()
{
	global $oSqlQ, $oDb, $sys;
	$strA = array();
	$arSql = $oDb->sqlRun($oSqlQ->getQ('get-settings'), 'st');
	for (; list($k, $v) = each($arSql);)
	{
		$strA[$v['settings_key']] = $v['settings_val'];
	}
	/* No system settings found, run install */
	if (empty($strA))
	{
		gwtk_header($sys['server_proto'].$sys['server_host'].$sys['server_dir'].'/gw_install/install.php');
	}
	return $strA;
}



/**
 * Builds HTML-code for page navigation, like [ Pages: 1 .. 5 6 7 .. 11 ]
 *
 * @return   string  simple HTML-code, ready to put inside a table or else.
 */
function getNavToolbar($intSumPages, $page = 1, $url)
{
	global $sys, $oHtml;
	$strA = array();
	if ($intSumPages == 1)
	{
		return "&#160;";
	}
	$sys['split_pagenums'] = isset($sys['split_pagenums']) ? $sys['split_pagenums'] : ' | ';

	$isShowJump = 0;
	$intVisible = 7;
	$intJump = 10;
#   [prev 10] [prev] 1 2 3 .. 11 [next] [next 10]

	$strNext = $GLOBALS['L']->m('1_nextpage').'&#160;&gt;&gt;';
	$strPrev = '&lt;&lt;&#160;'.$GLOBALS['L']->m('1_prevpage');
	$strNextN = $GLOBALS['L']->m('next_n %d');
	$strPrevN = $GLOBALS['L']->m('prev_n %d');
	// 1 <<
	// 2 <
	// 3 min
	// 4 ..
	// 5 n
	// 6 ..
	// 7 max
	// 8 >
	// 9 >>
	// min & max values
	$max = $page + $intJump;
	if ($max > $intSumPages) { $max = $intSumPages; }
	$min = $page - $intJump;
	if ($min < 1) { $min = 1; }
	// prev/next processing
	if ( ($page - 1) != 0 ) { $linkPrev = $oHtml->a( append_url($url . ($page - 1)), $strPrev); }
	else { $linkPrev = ''; }
	if ( ($page + 1) <= $intSumPages )
	{
		$linkNext = $oHtml->a( append_url($url . ($page + 1)), $strNext);
	}
	else { $linkNext = ''; }
	//
	if ($isShowJump)
	{
		$strA[] = (($page == 1) || ($page <= $intJump)) ? '' : $oHtml->a( append_url($url . $min), sprintf($strPrevN, $intJump));
	}
	$strA[] = $linkPrev;
	$strA[] = ($page == 1) ? '<b style="color:'.$GLOBALS['theme']['color_5'].';font:bold 110%">1</b>' : $oHtml->a( append_url($url . 1), '<b>1</b>');
	//
	$p1 = ($page - intval($intVisible / 2));
	$p2 = ($p1 + $intVisible);
	if ($p2 > $intSumPages)
	{
		$p2 = ($intSumPages - 1);
		$p1 = ($p2 - $intVisible);
	}
	// $intVisible..
	if((($p2 - $intVisible) < 0) || ($p1 <= 1))
	{
		$p1 = 2;
		$p2 = $p1 + $intVisible;
	}
	if (($p1 - 1) > 1 ) { $strA[] = '..'; }

	if ( (($intSumPages - $page) * 2) < $intVisible )
	{
		$p2++;
	}
	if ($p2 > $intSumPages) { $p2 = $intSumPages; }
	// visible part
	for ($i = $p1; $i < $p2; $i++)
	{
		$strA[] = ($i == $page) ? '<b style="color:'.$GLOBALS['theme']['color_5'].';font:bold 110%">'.$i.'</b>' : $oHtml->a( append_url($url . $i), '<b>'.$i.'</b>');
	}
	if (($intSumPages - $p2) >= 1) { $strA[] = '..'; }
	$strA[] = ($page == $intSumPages) ? '<b style="color:'.$GLOBALS['theme']['color_5'].';font:bold 110%">'.$intSumPages.'</b>' : $oHtml->a( append_url($url . $intSumPages), '<b>'.$intSumPages.'</b>');

	$strA[] = $linkNext;

	if ($isShowJump)
	{
		$strA[] = (($page == $intSumPages) || (($page + $intJump) > $intSumPages)) ? "" : $oHtml->a( append_url($url . $max), sprintf($strPrevN, $intJump));
	}
	$str = implode($sys['split_pagenums'], $strA);
	return $str;
}



/**
 * Depreciated function
 *
 * Builds query "LIMIT n,n" for database
 *
 * @param    int   $total    total number of items
 * @param    int   $page     current page number
 * @param    int   $perpage  items per page
 * @return   string  part of database query
 */
function getSqlLimit($total, $page, $perpage)
{
	$numpages = ceil($total / $perpage);
	for ($i=1; $i <= $numpages; $i++)
	{
		$pos = ($i * $perpage);
		if ($i == ($page - 1))
		{
			return " LIMIT $pos, $perpage";
		}
	}
	return " LIMIT 0, $perpage";
}


/**
 * Get all dictionary parameters, such as:
 * title, description, number of terms, sql-table name etc.
 *
 * @param    int     dictionary ID
 * @return   array   dictionary name, description, total terms etc.
 */
function getDictParam($dict_id)
{
	global $gw_this;
	$ar = array();
	for (reset($gw_this['arDictList']); list($kDict, $vDict) = each($gw_this['arDictList']);)
	{
		if ($vDict['id'] == $dict_id)
		{
			$vDict['dict_settings'] = unserialize($vDict['dict_settings']);
			if (is_array($vDict['dict_settings']))
			{
				$vDict = array_merge($vDict, $vDict['dict_settings']);
			}
			unset($vDict['dict_settings']);
			$ar = $vDict;
			break;
		}
	}
	return $ar;
} // end of getDictName


/**
 * Get a random term from a random dictionary
 * @param   int     dictionary ID
 * @return  array   array with term and dictionary
 */
function getTermRandom()
{
	global $gw_this, $oDb, $oSqlQ;
	$arSqlD = $gw_this['arDictList'][rand(0, sizeof($gw_this['arDictList'])-1)];
	$sql = $oSqlQ->getQ('get-term-rand', $arSqlD['tablename']);
	$arSql = $oDb->sqlExec($sql, '', 0);
	$arSql = isset($arSql[0]) ? $arSql[0] : array();
	$arSql = array_merge($arSqlD, $arSql);
	return $arSql;
}


/**
 * Get term parameters:
 * term id, term, definition
 *
 * @param    int     term ID
 * @return   array   term id, defn id, name, definition(s), synonym(s)
 */
function getTermParam($tid = '', $name = '')
{
	global $dictID, $L, $arDictParam, $oDb, $oSqlQ;

	$arFound = $arFoundInit = array('term' => '', 'term_1' => ' ', 'term_2' => ' ', 'defn' => '', 'tid' => '');
	if ($tid != '') // search by id (faster)
	{
		$sql = ( (GW_IS_BROWSE_ADMIN && (PERMLEVEL < 8))
				? $oSqlQ->getQ('get-term-by-id-adm', $arDictParam['tablename'], $tid, $user->id)
				: $oSqlQ->getQ('get-term-by-id', $arDictParam['tablename'], $tid)
				);
		$arFound = $oDb->sqlExec($sql, sprintf("%05d",$dictID), 0);
		$arFound = isset($arFound[0]) ? $arFound[0] : array();
	}
	elseif ($name != '')
	{
		// remove specials
		$arKeywordsT = text2keywords( text2term_uniq( gw_stripslashes($name), 1 ), 1);
		$word_srch_sql = "'" . implode("', '", $arKeywordsT) . "'";
		$sql = $oSqlQ->getQ('get-term-by-name',
				TBL_WORDLIST, TBL_WORDMAP, $arDictParam['tablename'], $dictID, $word_srch_sql);
		$arSql = $oDb->sqlExec($sql, sprintf("%05d",$dictID), 0);
		for (reset($arSql); list($arK, $arV) = each($arSql);) // compare founded values (Q) with imported (T)
		{
			$isTermExist = 0;
			// first method, 08 july 2000
			if (!$isTermExist && ($arV['term'] == $name))
			{
				$isTermExist = 1;
				$arFound = $arV;
#				prn_r( $arV['term'].' = '.$name."\narsize=".sizeof($arSql) );
				break; // breaks at first loop, usually.
			}
			// do NOT remove specials
			$arKeywordsQ = text2keywords( text2term_uniq($arV['term'], 1), 1); // 1 - is the minimum length
			$div1 = sizeof(gw_array_exclude($arKeywordsT, $arKeywordsQ));
			$div2 = sizeof(gw_array_exclude($arKeywordsQ, $arKeywordsT));
			$isTermNotMatched = ($div1 + $div2);
			// if the sum of excluded arrays is 0, this term already exists
			if (!$isTermNotMatched) // in english, double negative means positive. yeah.
			{
				$isTermExist = 1;
			}
			if ($isTermExist)
			{
				$arFound = $arV;
			}
		} // end of for each founded terms
	}
	if (empty($arFound))
	{
		$arFound = $arFoundInit;
	}
	return $arFound;
}





## --------------------------------------------------------
## Toolbar functions A-Z, 00-ZZ

/**
 * === Toolbar functions.
 * 1 of 2 functions to create alphabetic index.
 * Get first letters from all terms in dictionary
 *
 * @param    int     $dictID dictionary ID
 * @param    int     $w      second symbol (optional) // not in use since 1.3
 * @return   array   initial letters 00-ZZ
 */
function getLettersArray($id_dict, $w = '')
{
	global $oDb, $oSqlQ, $oFunc;
	global $arDictParam;
	$arSql = $oDb->sqlRun($oSqlQ->getQ('get-az', $arDictParam['tablename']), sprintf("%05d", $id_dict));
	/* One array for both indexes (single and double) */
	$arL = array();
	for (; list($k, $v) = each($arSql);)
	{
		/* Must be mb_substr($v['L1'], 1, 3), but parameter (0, 3)
		allows to override Unicode sorting order for diacritics.
		Example (urlencoded): S%CC%8C overrides %C5%A0 */
		$arL[$oFunc->mb_substr($v['L1'], 0, 3)][$oFunc->mb_substr($v['L2'], 0, 6)] = $v['L2'];
	}
	/* Remove single character from double's index */
	for (; list($k, $v) = each($arL);)
	{
		if ( isset($v[$k]) ) /* is there any single character? */
		{
			unset($arL[$v[$k]][$v[$k]]);
		}
	}
	return $arL;
}

/**
 * === Toolbar functions.
 * 2 of 2 functions to create alphabetic index.
 * Get HTML-code for A-Z letters. Universal function.
 *
 * @param    array   $ar  array[49][49] = 11;
 * @param    int     $dictID dictionary ID
 * @param    string  $w  selected letter (optional)
 * @param    int     $isLink
 * @param    int     $isLine
 * @return   string  HTML-code
 */
function getLetterHtml($ar, $dictID, $w1 = '', $w2 = '')
{
	global $oFunc, $oHtml, $sys;
	$str = "";
	$arS = array();
	$class[1] = 'on';
	$class[0] = '';
	$cnt = 0;
	for (reset($ar); list($k1, $v1) = each($ar);)
	{
		$cnt++;
		$cnt1_str = (isset($sys['is_print_toolbar_num']) && $sys['is_print_toolbar_num'] == 1) ? $cnt : '';

		$arTmp = array();
		$strTmp = '';
		$arTmp['href']['a'] = 'list';
		$arTmp['href']['d'] = $dictID;
		$arTmp['href']['p'] = '1';

		// 0-Z
		if (($w1 != '') && ($w2 == ''))
		{
			$oHtml->setTag('a', 'title', $cnt1_str);
			$arTmp['href']['w1'] = urlencode($k1);
			$int_str = hexdec(str2hex($k1));
#			print hexdec(str2hex('•')).' ';
			// TODO: set toolbar according to unicode mapping.
			if (($int_str > 18199218) && ($int_str <= 18199259))
			{
				$strTmp = 'num';
			}
			elseif (ord($k1) < 65 ) // numeric should be first, Latin A
			{
				$strTmp = 'num';
			}
			elseif((ord($k1) >= ord("A")) && (ord($k1) <= ord("Z"))) // latin1
			{
				$strTmp = 'lat';
			}
			elseif((ord($k1) >= ord("А")) && (ord($k1) <= ord("Я"))) // russian
			{
				$strTmp = 'ru';
			}
			elseif (($int_str > 62675 ) && ($int_str <= 18133716 ))
			{
				$strTmp = 'spec';
			}
			else
			{
				$strTmp = 'other';
			}
			if ( sprintf("%s", $k1) == sprintf("%s", trim($w1)) ) // underline active letter, $k1 and $w1 should be strings.
			{
				$isLink = 1;
				$oHtml->setTag('a', 'class', $class[$isLink]);
				$arS[$strTmp][] = ( $isLink )
								? ( $oHtml->a( $sys['page_index'] . '?' . $oHtml->paramValue($arTmp['href'], '&', ''), htmlspecialchars($k1) ) )
								: ( '<span class="f">' . $k1 .'</span>' ) ;
			}
			else
			{
				$oHtml->setTag('a', 'class', '');
				$arS[$strTmp][] = $oHtml->a( $sys['page_index'] . '?' . $oHtml->paramValue($arTmp['href'], '&', ''), htmlspecialchars($k1));
			}
			$oHtml->setTag('a', 'title', '');
		}
		else // length, 00-ZZ
		{
			if (sprintf("%s", $k1) == sprintf("%s", trim($w1)))
			{
				for (reset($v1); list($k2, $v2) = each($v1);)
				{
					// $k1 - first letter
					$arTmp['href']['w1'] = urlencode($k1);
					$arTmp['href']['w2'] = urlencode($v2);
#					$v2 = $oFunc->is_num($v2) ? ($v2 / 1) : $v2;
					if ( sprintf("%s", $v2) == sprintf("%s", trim($w2)) ) // unlink active letter
					{
						$isLink = 1;
						$oHtml->setTag('a', 'class', $class[$isLink]);
						$arS['0z'][] = ( $isLink )
									 ? ( $oHtml->a( $sys['page_index'] . '?' . $oHtml->paramValue($arTmp['href'], '&', ''), htmlspecialchars($v2)) )
									 : ( '<span class="f">' . $v2 .'</span>' );
					}
					else // second level toolbar with the first level is selected
					{
						$oHtml->setTag('a', 'class', '');
						$arS['0z'][] = $oHtml->a( $sys['page_index'] . '?' . $oHtml->paramValue($arTmp['href'], '&', ''), htmlspecialchars($v2));
					}
				}
			}
		} // end of length
	} // end of loop
	$strTmp = "";
	$arTemp = array();
	// build html-code
	for (reset($arS); list($k1, $v1) = each($arS);)
	{
		if ( is_array($v1) )
		{
			$str .= '&#160;' . implode('&#32;', $v1) . '&#160;';
			$str .= '<br />';
		}
	} // built
	$oHtml->setTag('a', 'class', '');
	return $str;
} // end of function getLetterHtml
## Toolbar functions A-Z, 00-ZZ
## --------------------------------------------------------




/**
 * Parses XML-data and converts it into structured array.
 *
 * @param   string  $str        XML-code, (from database)
 * @return  array   Fields content structure
 */
function gw_Xml2Array($str)
{
	global $arFields;
	//
	$xmlRoot = array();
	$xmlTags = array();
	$xmlAttr = array("link", "lang"); // possible attributes
	// Get defined tags
	for (reset($arFields); list($fk, $fv) = each($arFields);)
	{
		$fieldname = 'is_'.$fv[0];
		if (isset($fv[4]) && $fv[4]) // root
		{
			$xmlRoot[] = $fv[0];
		}
		else // not root elements
		{
			$xmlTags[] = $fv[0];
		}
	}
	// go for each root element
	for (reset($xmlRoot); list($kp, $vp) = each($xmlRoot);)
	{
		preg_match_all("/<$vp>(.+?)<\/$vp>/s", $str, $strDefnA); // root tags without attributes
		if (isset($strDefnA[0]) && isset($strDefnA[0][0]) && !empty($strDefnA[0][0]))
		{
			$intDefnS = sizeof($strDefnA[0]);
			for ($intDefnC = 0; $intDefnC < $intDefnS; $intDefnC++) // foreach <defn>
			{
				// value, 10 march 2003
				$parsedAr[$vp][$intDefnC]['value'] = $strDefnA[1][$intDefnC];
				//
				for (reset($xmlTags); list($kt, $vt) = each($xmlTags);)
				{
					for (reset($xmlAttr); list($ka, $va) = each($xmlAttr);)
					{
						preg_match_all("/<$vt( $va=\"(.*?)\")*\>(.*?)\<\/$vt\>/s", $strDefnA[1][$intDefnC], $strTmpA);
						#prn_r($strTmpA);
						if (isset($strTmpA[0]) && !empty($strTmpA[0]))
						{
							$intTmpS = sizeof($strTmpA[0]);
							for ($intTmpC = 0; $intTmpC < $intTmpS; $intTmpC++) // foreach <trns>
							{
								$parsedAr[$vt][$intDefnC][$intTmpC]['value'] = $strTmpA[3][$intTmpC];
								$parsedAr[$vp][$intDefnC]['value'] = trim(str_replace($strTmpA[0][$intTmpC], '', $parsedAr[$vp][$intDefnC]['value']));
								// per-tag
								if ($vt == 'syn') // collect data for syn
								{
									// 31 may 2002 - new attribute [text]: <syn link="" text="">
									$strSynText = '';
									if (preg_match("/\" text=\"/", $strTmpA[2][$intTmpC]))
									{
										$strSynText = preg_replace("'(.*)(\" text=\")(.*)'", "\\3", $strTmpA[2][$intTmpC]);
									}
									$strTmpA[2][$intTmpC] = preg_replace("'\" text=\"(.*)'", '', $strTmpA[2][$intTmpC]);
									$parsedAr[$vt][$intDefnC][$intTmpC]['attributes']['link'] = $strTmpA[2][$intTmpC];
									if ( ($strTmpA[2][$intTmpC] != '') && ($strTmpA[2][$intTmpC] != '--') )
									{
										$parsedAr[$vt][$intDefnC][$intTmpC]['attributes']['is_link'] = 1;
									}
									if ($strSynText != '') // text=""
									{
										$parsedAr[$vt][$intDefnC][$intTmpC]['attributes']['text'] = ' [['.$strSynText.']]';
									}
									$strSynText = '';
								}
								elseif ($vt == 'see') // collect data for see
								{
									#prn_r( $strTmpA , __LINE__);
									// 31 may 2002 - new attribute [text]: <see link="" text="">
									$strSeeText = '';
									if (preg_match("/\" text=\"/", $strTmpA[1][$intTmpC]))
									{
										$strSeeText = preg_replace("'(.*)(\" text=\")(.*)'", "\\3", $strTmpA[2][$intTmpC]);
									}
									$strTmpA[2][$intTmpC] = preg_replace("'\" text=\"(.*)'", '', $strTmpA[2][$intTmpC]);
									$parsedAr[$vt][$intDefnC][$intTmpC]['attributes']['link'] = $strTmpA[2][$intTmpC];
									if ( ($strTmpA[2][$intTmpC] != '') && ($strTmpA[2][$intTmpC] != '--') )
									{
										$parsedAr[$vt][$intDefnC][$intTmpC]['attributes']['is_link'] = 1;
									}
									if ($strSeeText != '') // text=""
									{
										$parsedAr[$vt][$intDefnC][$intTmpC]['attributes']['text'] = ' [['.$strSeeText.']]';
									}
									$strSeeText = '';
								}
								elseif ($vt == 'abbr') // collect data for abbr
								{
									$parsedAr[$vt][$intDefnC][$intTmpC]['attributes']['lang'] = $strTmpA[2][$intTmpC];
								}
								elseif ($vt == 'trns') // collect data for trns
								{
									$parsedAr[$vt][$intDefnC][$intTmpC]['attributes']['lang'] = $strTmpA[2][$intTmpC];
								}
								elseif ($vt == 'src') // collect data for src
								{
									if ( ($strTmpA[2][$intTmpC] != '') && ($strTmpA[2][$intTmpC] != '--') )
									{
										$parsedAr['src'][$intDefnC][] = $strTmpA[1][$intTmpC];
									}
								}
							}
						}
					} // xmlAttr
				} // xmlTags
			} // foreach <defn>
		}
	} // end of $xmlPrnt
	if(!isset($parsedAr)){ $parsedAr['defn'][0] = ""; }
	return $parsedAr;
} // end of ParseFieldDbToInput()



?>