<?php
if (!defined('IN_GW'))
{
	die('<!-- $Id: top.term_updated.inc.php,v 1.1 2004/11/13 12:30:09 yrtimd Exp $ -->');
}
/**
 *  Glossword - glossary compiler (http://glossword.info/dev/)
 *  � 2002-2004 Dmitry N. Shilnikov <dev at glossword.info>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  (see `glossword/support/license.html' for details)
 */
// --------------------------------------------------------
/**
 *  Makes the list of last added terms.
 *  This code runs inside function getTop10();
 *  switch keyname: R_TERM_UPDATED
 */
// --------------------------------------------------------
/**
 * Variables:
 * $amount, $arThText,  $arThAlign, $strTopicName, $strData, $curDateMk
 */
	$ar_terms = array();
	for (reset($gw_this['arDictList']); list($k, $arDictParam) = each($gw_this['arDictList']);)
	{
		$arSql = $oDb->sqlRun(sprintf($oSqlQ->getQ('top-term-new', $arDictParam['tablename'], $amount)), sprintf("%05d", $dictID));
		for (; list($arK, $arV) = each($arSql);)
		{
			$arV['id_dict'] = $arDictParam['id'];
			$arV['title'] = $arDictParam['title'];
			/* -$arK to show terms with the same date */
			$ar_terms[$oFunc->date_Ts14toTime($arV['date_created'])-$arK] = $arV;
		}
	}
	if (empty($ar_terms)){ return; }
	krsort($ar_terms);
	$arThText = array();
	/* Set width for colums */
	$arThWidth = array('50%', '49%');
	/* Set alignment for colums */
	$arThAlign = array($sys['css_align_left'], $sys['css_align_left']);
	$strTopicName = '';
	/* for each term */
	for (; list($k, $arV) = each($ar_terms);)
	{
		if ($cnt == $amount) { break; }
		$cnt % 2 ? ($bgcolor = $GLOBALS['theme']['color_2']) : ($bgcolor = $GLOBALS['theme']['color_1']);
		$cnt++;
		$strData .= '<tr style="background:' . $bgcolor . '">';
		$strData .= '<td class="q">' . $cnt . '</td>';
		$url_term = $oHtml->a( append_url($sys['page_index']
							. '?a=term&d=' . $arV['id_dict']
							. '&t=' . $arV['id']), strip_tags($arV['term']), '');
		$url_dict = $oHtml->a( append_url($sys['page_index']
							. '?a=list&d=' . $arV['id_dict']), strip_tags($arV['title']), '');
		$strData .= '<td class="termpreview">' . $url_term . '</td>';
		$strData .= '<td class="t">' . $url_dict . '</td>';
		$strData .= '</tr>';
	}
/**
 * end of R_TERM_UPDATED
 */
?>