<?php
if (!defined('IN_GW'))
{
	die('<!-- $Id: top.dict_newest.inc.php,v 1.6 2004/06/15 15:27:54 yrtimd Exp $ -->');
}
/**
 *  Glossword - glossary compiler (http://glossword.info/dev/) 
 *  � 2002-2004 Dmitry N. Shilnikov <dev at glossword.info>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  (see `glossword/support/license.html' for details)
 */
// --------------------------------------------------------
/**
 *  Makes the list of last added dictionaries.
 *  This code runs inside function getTop10();
 *  switch keyname: R_DICT_NEWEST
 */
// --------------------------------------------------------
/**
 * Variables:
 * $amount, $arThText,  $arThAlign, $strTopicName, $strData, $curDateMk
 */
	$arSql = $oDb->sqlRun(sprintf($oSqlQ->getQ('top-dict-new', $amount)), 'st');
	//
	$arThText = array($L->m('th_1'), $L->m('th_5'));
	// Sets width for colums
	$arThWidth = array('74%', '25%');
	// Sets alignment for colums
	$arThAlign = array($sys['css_align_left'], $sys['css_align_right']);
	$strTopicName = $L->m('r_dict_newest');
	// for each dictionary
	for (; list($arK, $arV) = each($arSql);)
	{
		$cnt % 2 ? ($bgcolor = $GLOBALS['theme']['color_3']) : ($bgcolor = $GLOBALS['theme']['color_2']);
		$cnt++;        
		$strData .= '<tr valign="top" class="t" style="background:' . $bgcolor . '">';
		$strData .= '<td>' . $cnt . '</td>';
		$strData .= '<td>' . $oHtml->a($sys['page_index']."?a=list&d=" . $arV['id'], $arV['title']) . '</td>';
		$strData .= '<td>';
		$strData .= (dateExtract($arV['date_created'], '%d') / 1) . dateExtract($arV['date_created'], ' %F %Y');
		$strData .= '</td>';
		$strData .= '</tr>';
	}
/**
 * end of R_DICT_NEWEST
 */
?>