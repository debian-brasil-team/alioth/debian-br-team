<?php
/**
 * Glossword - glossary compiler (http://glossword.info/dev/)
 * � 2002-2004 Dmitry N. Shilnikov <dev at glossword.info>
 */
/**
 * Login process.
 */
/* Turn on any warnings */
error_reporting(E_ALL);
/* Load login configuration */
if (!include("login.cfg.php"))
{
    print "sys: Can't include login.cfg.php. Check configuration.";
    exit;
}
/* Change current directory to access for website files */
if (!chdir($path['doc_root']))
{
    print "sys: Can't change directory to " . $path['doc_root'].". Check login.cfg.php";
    exit;
}
/* Load website configuration */
$sys['path_include'] = 'inc';
$sys['path_theme']   = 'gw_silver';
$sys['is_prepend'] = 1;
include_once('db_config.php');
include_once($sys['path_include'] . '/config.inc.php');

if (!file_exists($sys['file_lock']))
{
	print 'Installer is not locked';
	exit;
}

/* Append system settings */
$sys = array_merge($sys, getSettings());

$sys['is_mod_rewrite']  = 0;
if(!defined('GW_DB_HOST'))
{
    print "sys: Can't find config.inc.php GW_DB_HOST is not defined.";
    exit;
}
/* PHP-page header */
include_once( $sys['path_include'] . '/class.forms.'      . $sys['phpEx'] );
include_once( $sys['path_include'] . '/lib.auth.'         . $sys['phpEx'] );
include_once( $sys['path_include'] . '/lib.ct_sql.'       . $sys['phpEx'] );
include_once( $sys['path_include'] . '/lib.page.'         . $sys['phpEx'] );
include_once( $sys['path_include'] . '/lib.user.'         . $sys['phpEx'] );
include_once( $sys['path_include'] . '/lib.session.'      . $sys['phpEx'] );
include_once( $sys['path_include'] . '/func.admin.inc.'   . $sys['phpEx'] );

/* registered variables */
$arPostVars = array('submit','post','mode','t_tpl','strict','in',
                    'abbr','abbrlang','trns','trnslang','actkey',
                    't_term','defn','see','syn','src','d', GW_SID,
                    'id','q', GW_ACTION, GW_TARGET,'p','tid', 'isUpdate',
                    'isLinkSyn','isLinkSee','isConfirm', 'arPost','arPre'
);
for (reset($arPostVars); list($k, $v) = each($arPostVars);)
{
    if (isset($_POST[$v]) && ($_POST[$v] != ''))
    {
        $$v = $_POST[$v];
    }
    elseif (isset($_GET[$v]) && ($_GET[$v] != ''))
    {
        $$v = $_GET[$v];
    }
    elseif (isset($HTTP_POST_VARS[$v]) && ($HTTP_POST_VARS[$v] != ''))
    {
        $$v = $HTTP_POST_VARS[$v];
    }
    elseif (isset($HTTP_GET_VARS[$v]) && ($HTTP_GET_VARS[$v] != ''))
    {
        $$v = $HTTP_GET_VARS[$v];
    }
    else
    {
        $$v = '';
    }
	gw_fixslash($$v);
}
unset($arPostVars);

    $themefile = $sys['path_tpl'] . '/' . $sys['path_theme'] . '/theme.inc.' . $sys['phpEx'];
    if (file_exists($themefile))
    {
        include_once( $themefile );
    }
    else
    {
        die("Can't find " . $themefile);
    }
##------------------------------------------------

    /* Translation engine */
    $L = new gwtk;
    $L->setHomeDir("locale");
    $L->setLocale($sys['locale_name']);
    $L->getCustom('admin', $sys['locale_name'], 'join');
    $L->getCustom('err', $sys['locale_name'], 'join');
    $L->getCustom('mail', $sys['locale_name'], 'join');

/* Redirect URL */
$url_admin = $sys['server_proto'] . $sys['server_host'] . $sys['page_admin'];
/* Start session page */
page_open(array('sess' => 'gwtkSession', 'auth' => 'gwAuth'));
/* Close session */
page_close();
/* */
gwtk_header(append_url($url_admin), 0);

?>