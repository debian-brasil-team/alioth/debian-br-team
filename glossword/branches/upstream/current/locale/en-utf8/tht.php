<?php
/**
 *  DO NOT EDIT. Alteration of this file may cause the Glossword to malfunction.
 *  For any changes, please, contact translators.
 *  Translation file generated by Translation Kit (http://glossword.info/tkit/)
 *  © 2002-2004 Dmitry Shilnikov <dev at glossword.info>
 *  ------------------------------------------------------
 *  Project  : Glossword
 *  Topic    : Table header title
 *  Language : English
 *  Charset  : UTF-8
 *  Phrases  : 17
 *  Location : locale/en-utf8/tht.php
 *  Created  : 14 Nov 2004, 14:11
 *  ------------------------------------------------------
 *  Translation created by:
 *      - Dmitry <dev at glossword.info>
 *      - Лхагвасүрэнгийн Хүрэлбаатар <hujii247 at yahoo.com>
 */
$lang['1037'] = 'Permissions';
$lang['1028'] = 'Database version';
$lang['r_term_updated'] = 'Recently updated terms';
$lang['recent'] = 'Recently added';
$lang['action'] = 'Action';
$lang['r_term_newest'] = 'Last added terms';
$lang['r_dict_updated'] = 'Recently updated dictionaries';
$lang['2_tip'] = 'Tips';
$lang['2_page_6'] = 'The list of dictionaries';
$lang['order'] = 'Order';
$lang['srch_title'] = 'Search dictionaries';
$lang['th_5'] = 'Last post';
$lang['th_4'] = 'Hits total';
$lang['th_3'] = 'Hits avg.';
$lang['th_1'] = 'Name';
$lang['r_dict_averagehits'] = 'Most popular dictionaries';
$lang['r_dict_newest'] = 'Last added dictionaries';
/**
 * The end of translation file.
 */
?>