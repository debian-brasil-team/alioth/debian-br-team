<?php
/**
 *  DO NOT EDIT. Alteration of this file may cause the Glossword to malfunction.
 *  For any changes, please, contact translators.
 *  Translation file generated by Translation Kit (http://glossword.info/tkit/)
 *  © 2002-2004 Dmitry Shilnikov <dev at glossword.info>
 *  ------------------------------------------------------
 *  Project  : Glossword
 *  Topic    : Table header title
 *  Language : Russian
 *  Charset  : UTF-8
 *  Phrases  : 17
 *  Location : locale/ru-utf8/tht.php
 *  Created  : 14 Nov 2004, 14:11
 *  ------------------------------------------------------
 *  Translation created by:
 *      - Dmitry <dev at glossword.info>
 */
$lang['1037'] = 'Права доступа';
$lang['1028'] = 'Версия базы данных';
$lang['r_term_updated'] = 'Обновленные термины';
$lang['recent'] = 'Недавно добавленное';
$lang['action'] = 'Действие';
$lang['r_term_newest'] = 'Новые термины';
$lang['r_dict_updated'] = 'Обновленные словари';
$lang['2_tip'] = 'Справка';
$lang['2_page_6'] = 'Список словарей';
$lang['order'] = 'Порядок';
$lang['srch_title'] = 'Поиск в словарях';
$lang['th_5'] = 'Дата публикации';
$lang['th_4'] = 'Хитов всего';
$lang['th_3'] = 'Хитов средн.';
$lang['th_1'] = 'Название';
$lang['r_dict_averagehits'] = 'Популярные словари';
$lang['r_dict_newest'] = 'Новые словари';
/**
 * The end of translation file.
 */
?>