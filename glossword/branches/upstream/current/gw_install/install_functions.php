<?php
/**
 * Glossword installation functions
 * by Dmitry N. Shilnikov <dev at glossword dot info>
 * $Id: install_functions.php,v 1.7 2004/11/13 12:33:23 yrtimd Exp $
 */
if (!isset($sys['server_proto']))
{
	$sys['server_proto'] = 'http://';
}
/* HTTP_HOST and REQUEST_URI constants */
if (!isset($sys['server_host']))
{
	$sys['server_host'] = isset($_SERVER["HTTP_HOST"])&&!empty($_SERVER["HTTP_HOST"]) ? $_SERVER["HTTP_HOST"]
					: (isset($HTTP_SERVER_VARS["HTTP_HOST"]) ? $HTTP_SERVER_VARS["HTTP_HOST"]
					: (getenv('SERVER_NAME') != '') ? getenv('SERVER_NAME')
					: 'localhost');
}
define('HTTP_HOST',  $sys['server_host']);
define('REQUEST_URI', isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI']
					: ((getenv('REQUEST_URI!') != '') ? getenv('REQUEST_URI')
					: ((isset($HTTP_SERVER_VARS['QUERY_STRING']) && $HTTP_SERVER_VARS['QUERY_STRING'] != '') ? ($HTTP_SERVER_VARS['PHP_SELF'] . '?' . $HTTP_SERVER_VARS['QUERY_STRING'])
					: $_ENV['PHP_SELF'])));
if (!isset($sys['server_dir']))
{
	$sys['server_dir'] = explode('/', dirname(REQUEST_URI));
	unset($sys['server_dir'][(sizeof($sys['server_dir'])-1)]);
	$sys['server_dir'] = implode('/', $sys['server_dir']);
}


/* Variables */
$sys['is_prepend'] = 1;
$sys['is_mod_rewrite']  = 0;
$sys['themename']  = 'gw_silver';
$sys['path_css'] =  '../'.$sys['path_tpl'].'/'.$sys['themename'];

$arTables = array('TBL_SETTINGS', 'TBL_AUTH', 'TBL_DICT', 'TBL_TPCS', 'TBL_STAT_DICT', 'TBL_SESS', 'TBL_USERS', 'TBL_USERS_MAP', 'TBL_WORDLIST', 'TBL_WORDMAP', 'TBL_SRCH_RESULTS');

function gw_next_step($is_allow, $url)
{
	global $oL;
	return $is_allow
		 ? '<span class="green">'.$oL->m('057').'</span> '.sprintf('<a href="%s"><b>%s</a></b>', THIS_SCRIPT.'?'.$url, $oL->m('002'))
		: $oL->m('058');
}
/* */
function get_html_item_progress($text, $status = 1)
{
	switch($status)
	{
		case 1: $class = 'green'; break;
		case 2: $class = 'yellow'; break;
		case 3: $class = 'red'; break;
		default: $class = 'black'; break;
	}
	return sprintf('<div style="margin-left:2em" class="%s">%s</div>', $class, $text);
}

/* */
function gw_html_open()
{
	global $sys;
?>
<html>
<head>
<title><?php print strip_tags($sys['html_title']); ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<link rel="stylesheet" href="<?php print $sys['path_css']; ?>/style.css" type="text/css">
<style type="text/css">
form { margin:0; padding:0; }
label { cursor: pointer; }
img { border:0; }
tt { color: #808; background: transparent; font-size: 120%; }
a { position:relative; top:0px;left:0px; }
a:link, a:visited { text-decoration:underline; }
a:hover { text-decoration:underline; }
.black { color : #000; background : transparent; }
.gray { color : #888; background : transparent; }
.green { color : #180; background : transparent; }
.red { color : #E63; background : transparent; }
.white { color : #EEE; background : transparent; }
.yellow { color : #C90; background : transparent; }
.hr3 { height: 1px; overflow: hidden; background : #CEDBF0; }
.hr4 { height: 1px; overflow: hidden; background : #CEDBF0; }
.hr5 { height: 1px; overflow: hidden; background : #BCC8E2; }
.contents { padding: 0.5em 1em; }
.center { margin: 0 auto; text-align: center; }
ul.gwstatus li { padding: 5px; }
.debugwindow { width: 100%; font: 75% verdana,arial; }
.iframe { border: 3px solid #CEDBF0; padding: 1em; color: #000; background: #F9FAF8; height: 240px; overflow: auto; }
input.input {
	font: 75% 'courier new',courier,monospace;
	width: 100%;
	color: #000; background: #FFF;
	border-top: #7B9CBC 1px solid; border-left: #7B9CBC 1px solid; border-right: #A0D0EC 1px solid; border-bottom: #A0D0EC 1px solid;
}
select.input {
	font: 75% 'courier new',courier,monospace;
	width: 100%;
	color: #000; background: #FEFEFE;
}
table.gw2TableFieldset tr {
	vertical-align: top;
	font: 70% verdana,sans-serif;
}
table.gw2TableFieldset td {
	padding: 2px;
	text-align: left;
}
table.gw2TableFieldset td.td1 {
	text-align: {css_align_right}; font-size: 100%;
}
table.gw2TableFieldset td.td2 {
	text-align: {css_align_left}; font-size: 100%;
}
table.gw2TableFieldset td.tdinput, table.gw2TableFieldset td.td2 .input {
	font-size: 110%;
}
</style>
<script type="text/javascript">/*<![CDATA[*/
function clipboard() {
	window.clipboardData.setData('text', document.w.sysinfo.value);
}
/*]]>*/</script>
</head>
<body>
<div style="text-align:left;background:#FFE;padding:1em;" class="r">
<div style="float:right;padding:2px;" class="t">Sponsored by <a href="https://www.nukezone.com/">NukeZone Hosting</a></div>
<?php print $sys['html_title']; ?>
</div>
<div class="hr3" style="height:2px"></div>
	<?
}


function gw_html_close()
{
	global $sys, $oL;
	?>
<div class="hr3" style="height:2px"></div>
<table cellspacing="0" cellpadding="10" border="0" width="100%">
<tr class="f">
<td align="left" class="t">
<p><b><?php
print sprintf($oL->m('020'), $sys['server_dir'].'/support/');
?></b></p>
</td>
</tr>
</table>

	</body>
</html><?
}
/* */
function html_array_to_table($ar, $is_print = 1)
{
	if (empty($ar)) { $ar = array(); }
	if (is_string($ar)) { $ar = array($ar); }
	$str = '<table cellpadding="2" cellspacing="1" width="75%">';
	for (reset($ar); list($k, $v) = each($ar);)
	{
		if (is_string($v) && ($v == '')) { $v = '-'; }
		$str .= '<tr>';
		$str .= '<td style="width:10%"><tt>'. $k .'</tt></td>';
		$str .= '<td>'. $v .'</td>';
		$str .= '</tr>';
	}
	$str .= '</table>';
	if ($is_print)
	{
		print $str;
	}
	else
	{
		return $str;
	}
}
/* */
function html_array_to_table_multi($ar, $is_print = 1)
{
	if (empty($ar)) { $ar = array(); }
	if (is_string($ar)) { $ar = array(array($ar)); }
	$str = '<table cellpadding="2" cellspacing="1" width="95%" border="0">';
	for (reset($ar); list($k, $arV) = each($ar);)
	{
		if (is_string($arV)) { $arV = array($arV); }
		$td_width = empty($arV) ? 1 : ceil(100 / sizeof($arV));
		$td_style = '';
		if ($k == 0)
		{
			$td_style = ' style="width:'.$td_width.'%"';
		}
		$str .= '<tr>';
		for (reset($arV); list($k2, $v2) = each($arV);)
		{
			$str .= '<td'. $td_style .'>'.  $v2 .'</td>';
		}
		$str .= '</tr>';
	}
	$str .= '</table>';
	if ($is_print)
	{
		print $str;
	}
	else
	{
		return $str;
	}
}

?>