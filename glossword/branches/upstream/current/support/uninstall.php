<?php
/**
 * Glossword Uninstaller
 *
 * $Id: uninstall.php,v 1.2 2004/07/10 15:14:57 yrtimd Exp $
 */
/* Change current directory to access for website files */
if (!@chdir('..'))
{
    exit(print "Can't change directory to `..'");
}
define('IN_GW', TRUE);
define('THIS_SCRIPT', 'uninstall.php');
error_reporting(E_ALL);

/* Load configuration */
$sys['path_include'] = 'inc';
include_once('./db_config.php');
include_once($sys['path_include'] . '/config.inc.php');
if (file_exists('gw_install/install_functions.php'))
{
	include_once('gw_install/install_functions.php');
}
else
{
	printf('<br/><b>Error:</b> File %s required.', 'gw_install/install_functions.php');
}
include_once($sys['path_include'] . '/constants.inc.php');
include_once($sys['path_include'] . '/func.sql.inc.php');
include_once($sys['path_gwlib'].'/class.globals.php');
include_once($sys['path_gwlib'].'/class.func.php');
include_once($sys['path_gwlib'].'/class.db.mysql.php');

/* --------------------------------------------------------
 * Translation kit
 * -------------------------------------------------------- */
include_once($sys['path_include'] . '/class.gwtk.php');
$gv['vars'][GW_T_LANGUAGE] = 'en';
$oL = new gwtk;
$oL->setHomeDir('gw_install/gw_locale');
$oL->setLocale($gv['vars'][GW_T_LANGUAGE].'-utf8');
$oL->getCustom('l_install', $gv['vars'][GW_T_LANGUAGE].'-utf8');


/* Constants */
define('CRLF', "\r\n");
define('LF', "\\n\\");
/* */
$gv['vars'] = $oGlobals->register(array('step', 'id_dict', GW_T_LANGUAGE));
$oGlobals->do_default($gv['vars']['step'], 1);
/* */
$sys['html_title'] = $oL->m('034');

function sqlGetQ($keyname)
{
	$arSql = array(
		'get-dict_tables' => 'SELECT tablename FROM ' . TBL_DICT,

	);
	return isset($arSql[$keyname]) ? $arSql[$keyname] : 0;
}
/* */
class gw_uninstall
{
	var $id_dict = 0;
	var $ar_status = '';
	var $str_step = '';
	var $cur_funcname = '';
	var $oDb;
	function gw_uninstall()
	{
		global $oDb, $oFunc, $sys, $oL;
		$this->oDb = new gwtkDb;
		$this->oFunc =& $oFunc;
		$this->sys =& $sys;
		$this->oL =& $oL;
	}
	/* */
	function is_locked()
	{
		return file_exists($this->sys['file_lock']);
	}
	/* */
	function step()
	{
		if ($this->is_locked())
		{
			$this->ar_status[] = sprintf($this->oL->m('052'), $this->sys['server_dir'].'/'.$this->sys['file_lock']);
		}
		else
		{
			$this->{$this->cur_funcname}();
		}
	}
	/* */
	function step_1()
	{
		$this->str_step = $this->oL->m('035');
		$this->ar_status[0] = $this->oL->m('036');

		global $arTables;
		$arSqlTables = array();
		$arSqlTablesRemove = array();
		$int_sum_kb = 0;
		for (reset($arTables); list($k, $v) = each($arTables);)
		{
			if ($arTableInfo = $this->oDb->table_info(constant($v)) )
			{
				$int_sum_kb += ($arTableInfo['Data_length']+$arTableInfo['Index_length']);
				$arSqlTablesRemove[] = constant($v);
			}
			else
			{
				$arSqlTables[constant($v)] = $this->oL->m('038');
			}
		}
		$arSql = array();
		if ($this->oDb->table_info(TBL_DICT))
		{
			$arSql = $this->oDb->sqlExec(sqlGetQ('get-dict_tables'));
		}
		while (list($kSql, $arV) = each($arSql))
		{
			if ($arTableInfo = $this->oDb->table_info($arV['tablename']) )
			{
				$int_sum_kb += ($arTableInfo['Data_length']+$arTableInfo['Index_length']);
				$arSqlTablesRemove[] = $arV['tablename'];
			}
			else
			{
				$arSqlTables[$arV['tablename']] = $this->oL->m('037');
			}
		}
		/* */
		$this->ar_status[0] .=  sprintf('<br/>%s: <b>%s</b>', $this->oL->m('042'), GW_DB_DATABASE);
		$this->ar_status[0] .= '<br/><tt>' .implode('</tt>, <tt>', $arSqlTablesRemove). '</tt>';
		$this->ar_status[0] .= '<br/>'.$this->oFunc->number_format($int_sum_kb).' '.$this->oL->m('023');
		if (!empty($arSqlTables))
		{
			$this->ar_status[] = '&#160;'.html_array_to_table($arSqlTables, 0);
		}
		if ($int_sum_kb)
		{
			$this->ar_status[] = sprintf('<b>%s</b>', $this->oL->m('039')) . sprintf('<br/><a href="%s">%s</a>', '?step=2', $this->oL->m('011'));
		}
	}
	/* */
	function step_2()
	{
		$this->str_step = $this->oL->m('035');
		global $arTables;
		$arSqlTablesRemove = array();
		for (reset($arTables); list($k, $v) = each($arTables);)
		{
			if ($arTableInfo = $this->oDb->table_info(constant($v)) )
			{
				$arSqlTablesRemove[] = constant($v);
			}
		}
		$arSql = array();
		if ($this->oDb->table_info(TBL_DICT))
		{
			$arSql = $this->oDb->sqlExec(sqlGetQ('get-dict_tables'));
		}
		while (list($kSql, $arV) = each($arSql))
		{
			if ($arTableInfo = $this->oDb->table_info($arV['tablename']) )
			{
				$arSqlTablesRemove[] = $arV['tablename'];
			}
		}
		$this->ar_status[] = sprintf('%s: <b>%s</b>', $this->oL->m('042'), GW_DB_DATABASE);
		if (empty($arSqlTablesRemove))
		{
			$this->ar_status[] = sprintf($this->oL->m('053'), $this->sys['server_dir']);
		}
		else
		{
			$sql = 'DROP TABLE '.implode(', ', $arSqlTablesRemove);
			if ($this->oDb->sqlExec($sql))
			{
				$this->ar_status[] = sprintf('<span class="red"><b>%s</b></span>', $this->oL->m('051'));
				$this->ar_status[] = sprintf($this->oL->m('053'), $this->sys['server_dir']);
			}
		}
	}
	function output()
	{
		$str_status = '';
		if (!empty($this->ar_status))
		{
			$str_status .= '<ul class="gwstatus"><li>' . implode('</li><li>', $this->ar_status) . '</li></ul>';
		}
		$str = '<div class="contents u">';
		$str .= $this->str_step;
		$str .= $str_status;
		$str .= '</div>';
		return $str;
	}
}
/* */

#	if (getTableStructure(TBL_SETTINGS) != '')
#	{
#	}
/* */


$oUninstall = new gw_uninstall;
$gv['vars']['cur_funcname'] = 'step_'.$gv['vars']['step'];
/* Open step number  */
if (in_array(strtolower($gv['vars']['cur_funcname']), get_class_methods($oUninstall)))
{
	$oUninstall->cur_funcname = $gv['vars']['cur_funcname'];
	$oUninstall->step();
}

gw_html_open();
print $oUninstall->output();
gw_html_close();

?>