<?php
/**
 * This file shows php and server variables.
 *
 * $Id: sys_info.php,v 1.7 2004/07/20 16:04:57 yrtimd Exp $
 */

/* Change current directory to access for website files */
if (!@chdir('..'))
{
    exit(print "Can't change directory to `..'");
}
define('IN_GW', TRUE);
error_reporting(E_ALL);

/* Load configuration */
$sys['path_include'] = "inc";
include_once('./db_config.php');
$sys['is_prepend'] = 0;
include_once($sys['path_include'] . "/config.inc.php");

if (file_exists('gw_install/install_functions.php'))
{
	include_once('gw_install/install_functions.php');
}
else
{
	printf('<br/><b>Error:</b> File %s required.', 'gw_install/install_functions.php');
}
include_once($sys['path_include'] . '/constants.inc.php');
include_once($sys['path_gwlib'].'/class.db.mysql.php');

/* --------------------------------------------------------
 * Translation kit
 * -------------------------------------------------------- */
include_once($sys['path_include'] . '/class.gwtk.php');
$gv['vars'][GW_T_LANGUAGE] = 'en';
$oL = new gwtk;
$oL->setHomeDir('gw_install/gw_locale');
$oL->setLocale($gv['vars'][GW_T_LANGUAGE].'-utf8');
$oL->getCustom('l_install', $gv['vars'][GW_T_LANGUAGE].'-utf8');


$oDb = new gwtkDb;
/* Constants */
define('CRLF', "\r\n");
define('LF', "\\n\\");

/* */
$arSql = $oDb->sqlExec('SELECT version() as v');
# mysql_SELECT version();
$arInfoA = array(
	'{VERSION}'              => PHP_VERSION,
	'{SQL_VERSION}'          => (isset($arSql[0]['v']) ? $arSql[0]['v'] : ''),
	'{OS}'                   => PHP_OS,
	'{API}'                  => php_sapi_name(),
	'{DOCUMENT_ROOT}'        => getenv('DOCUMENT_ROOT'),
	'{SCRIPT_FILENAME}'      => getenv('SCRIPT_FILENAME'),
	'{SERVER_SOFTWARE}'      => getenv('SERVER_SOFTWARE'),
	'{REQUEST_URI}'          => REQUEST_URI,
	'{sys_server}'           => $sys['server_proto'].$sys['server_host'].$sys['server_dir'],
);
$arInfoT = array(
	'{expose_php}'           => (int) ini_get("expose_php"),
	'{magic_quotes_gpc}'     => (int) ini_get("magic_quotes_gpc"),
	'{magic_quotes_runtime}' => (int) ini_get("magic_quotes_runtime"),
	'{magic_quotes_sybase}'  => (int) ini_get("magic_quotes_sybase"),
	'{max_execution_time}'   => (int) ini_get("max_execution_time"),
	'{post_max_size}'        => (int) ini_get("post_max_size"),
	'{register_globals}'     => (int) ini_get("register_globals"),
	'{safe_mode}'            => (int) ini_get("safe_mode"),
	'{short_open_tag}'       => (int) ini_get("short_open_tag"),
	'{mbstring.internal_encoding}' => ini_get('mbstring.internal_encoding')
);


function gw_get_cfg()
{
	global $arInfoA, $arInfoT, $sys;
	$str = '';
	$str .= '<line>'.CRLF;
	$str .= '<term>'.$sys['server_host'].'</term>'.CRLF;
	$str .= '<defn>'.CRLF;

	while (list($k, $v) = each($arInfoA))
	{
		$str .= '<abbr lang="'.$k.'">'.$v.'</abbr>'.CRLF;
	}
	while (list($k, $v) = each($arInfoT))
	{
		$str .= '<trns lang="'.$k.'">'.$v.'</trns>'.CRLF;
	}
	if (function_exists('get_loaded_extensions'))
	{
		$ar = get_loaded_extensions();
		sort($ar);
		while(list($k, $v) = each($ar))
		{
			$str .= '<see>'.$v.'</see>'.CRLF;
		}
	}
	$str .= '</defn>'.CRLF;
	$str .= '</line>'.CRLF;
	return $str;
}
function gw_html_form($value = '')
{
	global $oL;
	print '<div class="contents u">';
	print '<form id="w" name="w">';
	print '<textarea rows="24" cols="40" id="sysinfo" class="debugwindow">'.htmlspecialchars($value).'</textarea>';
	print number_format(strlen($value), 0, '', ' ') .' '.  $oL->m('023');
	print '<table cellpadding="10" cellspacing="0" border="0">';
	print '<tr>';
	print '<td><ul>';
	print '<li><a href="#" onclick="document.w.sysinfo.select();clipboard();">'.$oL->m('022').'</a></li>';
	print '<li><a href="mailto:dev'. '@'. 'glossword.info?subject=sys_info">'.$oL->m('024').'</a></li>';
	print '</ul>';
	print '</tr>';
	print '</table>';

	print '</form>';
	print '</div>';
}

/* */
$sys['path_css'] =  $sys['server_dir'].'/'.$sys['path_tpl'].'/'.$sys['themename'];
$sys['html_title'] = sprintf($oL->m('021'));

/* */
gw_html_open();

gw_html_form(gw_get_cfg());

gw_html_close();

/* end of file */
?>