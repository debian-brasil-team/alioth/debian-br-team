<?php
/**
 *  Glossword - glossary compiler (http://glossword.info/dev/)
 *  � 2002-2004 Dmitry N. Shilnikov <dev at glossword.info>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  (see `glossword/support/license.html' for details)
 */
/* ------------------------------------------------------- */
/**
 *  Any custom variables for Glossword templates can be added here
 */
/* ------------------------------------------------------- */
/**
	Recommendations
	===============

	How to register variable

	1. Register variable.
		Glossword versions 1.6 or below:
		$tpl->addVal( 'my:variable', $my['variable_name'] );
		$tpl->addVal( 'my:variable1', $oFunc->get_file_contents('file.txt') );
		$tpl->addVal( 'my:variable2', $oFunc->file_exe_contents('file.php') );

		Glossword versions 1.7 or above (planned):
		$oTpl->assign( array('my:variable' => $my['variable_name']) );

	2. Add variable name into HTML-template.

			<b>{my:variable}</b>

		Note: `:' is not required, but Glossword template engine
		will support namespaces, so if you start to use this syntax today,
		it will be faster to update to future Glossword versions.

	Keep all your variables in $custom array as possible. For example, use
		$custom['a'] = 1;
	instead of
		$a = 1;

*/

/* ------------------------------------------------------- */
// Load custom phrases.
// To load custom language names for dictionary elements,
// see `locale/../d_language_custom.php'.
/* ------------------------------------------------------- */
# $L->getCustom('l_custom', $gw_this['vars'][GW_T_LANGUAGE].'-utf8', 'join');

// --------------------------------------------------------
// Load Add-ons
// --------------------------------------------------------
include_once('gw_addon/multilingual_vars.php');

include_once('gw_addon/class.autolinks.php');
function gw_autolinks($t)
{
	$oAutolinks = new gw_autolinks;
	$oAutolinks->init('gw_xml/autolinks/words.txt');
	return $oAutolinks->autolink($t);
}
$sys['filters_defn'] = array('gw_text_smooth_defn', 'gw_autolinks');

// --------------------------------------------------------
// Load custom functions
// --------------------------------------------------------
/*
$gw_this['vars']['funcnames'][GW_T_TERM] = array('my_function_1');
function my_function_1()
{
	global $listA, $tpl, $oFunc;
	$tpl->addVal( 'my:variable', $oFunc->file_exe_contents('your_script.php') );
}
*/
// --------------------------------------------------------
// Counters
// --------------------------------------------------------
/*
$strCounters = '';
$sys['arThemeCounters'] = array();
if (!IS_MYHOST)
{
	if (!isset($sys['arThemeCounters'][])){ $sys['arThemeCounters'] = array(); }
	// Theme-only counters
	$gw_this['path_counters_default'] = $sys['path_tpl'] . '/gw_silver/counters.inc.' . $sys['phpEx'];
	$gw_this['path_counters'] = $sys['path_tpl'] . '/' . $sys['path_theme'] . '/counters.inc.' . $sys['phpEx'];
	// Read counters from default theme
	if (file_exists($gw_this['path_counters_default']))
	{
		include_once( $gw_this['path_counters_default'] );
	}
	// Read counters from the current theme
	if (file_exists($gw_this['path_counters']))
	{
		include_once( $gw_this['path_counters'] );
	}
}
$sys['arThemeCounters'][] = '<img src="'.$sys['path_img'].'/0x0.gif" width="1" height="1" alt="" />';
if (!empty($sys['arThemeCounters']))
{
	$strCounters = '<div style="height:1px">'.implode('', $sys['arThemeCounters']) . '</div>';
}
$tpl->addVal( 'block:counters', $strCounters );
*/
// --------------------------------------------------------
// Glossword.info search form (1)
// --------------------------------------------------------
$tpl->addVal( 'block:gwsearch1', $oFunc->file_get_contents($sys['path_tpl'].'/common/gw_info1.html') );
# $tpl->addVal( 'block:google_ads', $oFunc->file_get_contents($sys['path_tpl'].'/common/google_ads.txt') );

class gw_render_custom extends gw_render
{
#	var $prepend_abbr  = '';
#	var $postlang_abbr = '&#32;';
#	var $split_abbr    = '; ';
#	var $append_abbr   = '&#32;';

#	var $prepend_trns  = '<table class="defntable"><tr><td style="width:35%">';
#	var $postlang_trns = '</td><td>';
#	var $split_trns    = '</td></tr><tr><td>';
#	var $append_trns   = '</td></tr></table>';

	/* enables full abbreviation forms for web and/or admin interface */
#	var $abbr_element_web = 1; // [ 0 - abbreviation | 1 - abbr. ]
}
# Overwrites system rendering scheme
# $gw_this['vars']['class_render'] = 'gw_render_custom';

?>