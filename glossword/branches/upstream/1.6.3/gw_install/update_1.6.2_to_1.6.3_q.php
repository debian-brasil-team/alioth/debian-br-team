<?php
/*
 * Glossword update from 1.6.2 to 1.6.3
 * Query storage
 * � 2004 Dmitry N. Shilnikov <dev at glossword.info>
 * $Id: update_1.6.2_to_1.6.3_q.php,v 1.2 2004/09/06 12:16:47 yrtimd Exp $
 */
$tmp['ar_queries'] = array(
		'auth01' => "ALTER TABLE " . TBL_AUTH . " CHANGE `password` `password` VARCHAR( 32 ) NOT NULL",
		'user01' => "ALTER TABLE " . TBL_USERS . " CHANGE `location` `user_settings` BLOB NOT NULL",
		'user02' => "UPDATE " . TBL_USERS . " SET `user_settings` = 'a:0:{}', date_login = date_login",
		'dict01' => "ALTER TABLE " . TBL_DICT . " CHANGE `numterms` `int_terms` MEDIUMINT(8) UNSIGNED DEFAULT '0' NOT NULL",
		'dict02' => "ALTER TABLE " . TBL_DICT . " CHANGE `size_bytes` `int_bytes` INT(12) UNSIGNED DEFAULT '0' NOT NULL",
		'sett01' => "DELETE FROM " . TBL_SETTINGS . " `settings_key` = 'login_proto' LIMIT 1",
		'sett02' => "DELETE FROM " . TBL_SETTINGS . " WHERE `settings_key` = 'login_host' LIMIT 1",
		'sett03' => "DELETE FROM " . TBL_SETTINGS . " WHERE `settings_key` = 'login_url' LIMIT 1",
		'sett04' => "DELETE FROM " . TBL_SETTINGS . " WHERE `settings_key` = 'admin_proto' LIMIT 1",
		'sett05' => "DELETE FROM " . TBL_SETTINGS . " WHERE `settings_key` = 'admin_host' LIMIT 1",
		'sett06' => "DELETE FROM " . TBL_SETTINGS . " WHERE `settings_key` = 'admin_url' LIMIT 1",
		'sett07' => "DELETE FROM " . TBL_SETTINGS . " WHERE `settings_key` = 'path_img' LIMIT 1",
		'sett08' => "DELETE FROM " . TBL_SETTINGS . " WHERE `settings_key` = 'path_tpl' LIMIT 1",
		'sett09' => "DELETE FROM " . TBL_SETTINGS . " WHERE `settings_key` = 'path_log' LIMIT 1",
		'sett10' => "DELETE FROM " . TBL_SETTINGS . " WHERE `settings_key` = 'is_polls' LIMIT 1",
		'upd-setting' => "UPDATE " . TBL_SETTINGS . " SET settings_val = '%s' WHERE settings_key = '%s'",
		'upd-auth' => "UPDATE " . TBL_AUTH . " SET password = '%s' WHERE user_id = '%s'",
);