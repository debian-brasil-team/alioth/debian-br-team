<?php
/**
 * Glossword Updrade from 1.6.1 to 1.6.2
 * by Dmitry N. Shilnikov <dev at glossword.info>
 * @version $Id: update_1.6.1_to_1.6.2.php,v 1.1 2004/07/06 14:21:34 yrtimd Exp $
 */
// --------------------------------------------------------
error_reporting(E_ALL);
@set_time_limit(2400);
define('IN_GW', "1");
define('PERMLEVEL', 16);
// --------------------------------------------------------
// Load config
$path['include'] = 'inc';
include_once('./db_config.php');
include_once('./'.$path['include'] . '/config.inc.php');
if (!defined('GW_DB_HOST'))
{
	exit(print "<br/>Can't find config.inc.php");
}
// --------------------------------------------------------
// Load admin functions
include_once( $path['include'] . '/func.admin.inc.' . $sys['phpEx'] );
// --------------------------------------------------------
// Registered variables
$arPostVars = array('a','t','tablenum','arPost','post');
// --------------------------------------------------------
$sys['default_pass'] = kMakeUid('', $maxchar = 10, 1);
$sys['is_mod_rewrite']  = 0;
$sys['themename']  = 'gw_silver';

$L = new gwtk;
$L->setHomeDir("locale"); // path to ./your_project/locale 
$L->setLocale('en-utf8'); // path to ./your_project/locale/ru-ru/
	
$arStatus = array();
reset($arPostVars);
for (; list($k, $v) = each($arPostVars);)
{
	if (isset($_POST[$v]) && ($_POST[$v] != '')) // get values from post
	{
		$$v = $_POST[$v];
	}
	elseif (isset($_GET[$v]) && ($_GET[$v] != '')) // get values from get
	{
		$$v = $_GET[$v];
	}
	elseif (isset($HTTP_POST_VARS[$v]) && ($HTTP_POST_VARS[$v] != '')) // capability
	{
		$$v = $HTTP_POST_VARS[$v];
	}
	elseif (isset($HTTP_GET_VARS[$v]) && ($HTTP_GET_VARS[$v] != '')) // capability
	{
		$$v = $HTTP_GET_VARS[$v];
	}
	else // default
	{
		$$v = '';
	}
	gw_fixslash($$v);
}
unset($arPostVars);
if ($tablenum == ''){ $tablenum = 0; }
// --------------------------------------------------------
//

@header("Expires: " . gmdate("D, d M Y H:i:s") . "GMT");
@header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
@header("Cache-Control: no-cache, must-revalidate");
@header("Pragma: no-cache");

//
// --------------------------------------------------------
// Read system settings
/**
 * Displays link to the next step
 */
function next_step($a, $msg)
{
	$str = "";
	$str .= '<br />';
	$str .= '<table width="100%" border="0" cellspacing="0" cellpadding="1">';
	$str .= '<tr class="r">';
	$str .= '<td align="center">';
	if ($a != '')
	{
		$str .= 'Continue to <a href="' . basename(PHP_SELF) . "?a=" . $a . '"><b>' . $msg . '</b> &gt;&gt;&gt;</a>';
	}
	else
	{
		$str .= $msg;
	}
	$str .= '</td>';
	$str .= '</tr>';
	$str .= '</table>';
	$str .= '<br />';
	return $str;
}

	function get_html_item_progress($text, $status = 1)
	{
		switch($status)
		{
			case 1: $class = 'green'; break;
			case 2: $class = 'yellow'; break;
			case 3: $class = 'red'; break;
			default: $class = 'black'; break;
		}
		return sprintf('<span class="%s">%s</span>', $class, $text);
	}
	

/**
 * 
 */
function sqlGetQ($keyname)
{
	global $sys;
	$arSql = array(
		'set_version' => 'UPDATE ' . TBL_SETTINGS . ' 
						SET settings_val = "1.6.2" 
						WHERE settings_key = "version"
					'
		);
	if (isset($arSql[$keyname]))
	{
		$arSql[$keyname] = preg_replace("/\s{2,}/s", ' ', $arSql[$keyname]);
		return $arSql[$keyname];
	}
}

$arTables = array('TBL_AUTH', 'TBL_DICT', 'TBL_TPCS', 'TBL_STAT_DICT', 'TBL_SESS', 'TBL_USERS', 'TBL_USERS_MAP', 'TBL_WORDLIST', 'TBL_WORDMAP');
							
$arActive = array('','',''); // amount of steps
$arQuery = array();
$a = ($a != "") ? $a : 0;
$path['css'] =  $path['tpl'] . '/' . $sys['themename'];
$path['css_common'] =  $path['tpl'] . '/common';
$title = 'Glossword Update 1.6.1. to 1.6.2';
$thisfile = 'update_1.6.1_to_1.6.2.php';
$finished = 'Update completed. Now you may remove <tt>'. $thisfile . '</tt> file.';
$arActive[$a] = '<b style="color:#F00">&gt;</b> ';
$msg[0] = 'No';
$msg[1] = 'Yes';

$strStatus = $strError = "";
$isError = 1;
switch ($a)
{
case 0:
## --------------------------------------------------------
## INTRODUCTION
	//
	// Get PHP version
	//
	$arStatus[] = 'Checking for PHP-version...';
	$arPhpVer = explode('.', PHP_VERSION);
	define('GW_PHP_VERSION_INT', intval(sprintf('%d%02d%02d', $arPhpVer[0], $arPhpVer[1], $arPhpVer[2])));
	if (GW_PHP_VERSION_INT > '40100')
	{
		$status = 1;
		$isError = 0;        
		$arStatus[] = get_html_item_progress(sprintf("PHP version %s", PHP_VERSION), 1);        
	}
	else
	{
		$arStatus[] = get_html_item_progress(sprintf("PHP version %s is too old. Version 4.1.0 is required.", PHP_VERSION), 3);
		$strError = "You cannot continue update.";
	}
	if (!$isError)
	{
		//
		// Get PHP-extensions
		//
		$arStatus[] = 'Checking for PHP-extensions...';
		$arPhpExt = array();
		if (function_exists('get_loaded_extensions'))
		{
			$arPhpExt = get_loaded_extensions();
		}
		if (sizeof($arPhpExt) == 0)
		{            
			$arStatus[] = get_html_item_progress('Extensions not found.', 3);        
			$strError = "You cannot continue update.";
			$isError = 1;
		}
	}
	if (!$isError)
	{
		// xml
		if (in_array('xml', $arPhpExt))
		{
			$arStatus[] = get_html_item_progress('Mandatory extension `xml\' exists.', 1);
		}
		else
		{
			$arStatus[] = get_html_item_progress('Mandatory extension `xml\' does not exist.', 3);
			$isError = 1;
			$strError = "You cannot continue update.";
		}
		// xslt
		if (in_array('xslt', $arPhpExt))
		{
			$arStatus[] = get_html_item_progress('Optional extension `xslt\' exists.', 1);
		}
		else
		{
			$arStatus[] = get_html_item_progress('Optional extension `xslt\' does not exist. Can\'t validate XML-syntax when checking the option &quot;Validate&quot;.', 2);
		}       
		// mbstring
		if (in_array('mbstring', $arPhpExt))
		{
			$arStatus[] = get_html_item_progress('`mbstring\' extension exists! Content indexing works faster.', 1);
		}
		else
		{
			$arStatus[] = get_html_item_progress('Optional extension `mbstring\' does not exist. You should upgrade PHP version to get faster content indexing.', 2);
		}
	}

##
## --------------------------------------------------------
break;
case 1:
## --------------------------------------------------------
## UPDATE STEP 1

	$arStatus[] = 'Setting up new version number...';
	$arQuery[] = sqlGetQ('set_version');
	// --------------------------------------------------------
	// Sessions
	$arStatus[] = 'Updating sessions... table <tt>'.TBL_SESS.'</tt>';
	$arQuery[] = 'DELETE FROM '.TBL_SESS;
	$tmp['str_cache_clear'] = cacheClear(sprintf("%05d", 0));
	if (!empty($tmp['str_cache_clear']))
	{
		$arStatus[] = $tmp['str_cache_clear'];
	}
	//
	if ($t == '')
	{
		$arStatus[] = postQuery($arQuery, 'a=1&t=1', 0, 0); // is_debug, is_delay
	}
	else
	{
		$arStatus[] = "<b>Completed.</b>";
		$isError = 0;
	}
##
## --------------------------------------------------------
break;
case 2:
## --------------------------------------------------------
## UPDATE STEP 2

	$cntDicts = 0;
	if ($cntDicts > 0)
	{
		$arStatus[] = '<a href="' . basename(PHP_SELF) . "?a=" . $a . '&amp;tablenum='.$tablenum.'">Click to upgrade <b>' . $arDicts[0] . '</b></a>';
	}
	else
	{
		$arQuery = array('SELECT max(id) from ' . TBL_DICT);
		$arStatus[] = postQuery($arQuery, 'a=3&t=1', 0, 0); // is_debug, is_pause
	}
##
## --------------------------------------------------------
break;
case 3:
## --------------------------------------------------------
## UPDATE STEP 3	
	
	$isError  = 0;
	
##
## --------------------------------------------------------
break;
default:
	$strError = 'Wrong step number. Return to <a href="'.basename(PHP_SELF).'?a=0">Introduction</a>.';
	$isError = 1;
break;
}

if (is_array($arStatus))
{
	$strStatus .= "<ul><li>".implode("</li><li>", $arStatus) . "</li></ul>";
}

?>
<html>
<head>
<title><?php print $title; ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="Content-Style-Type" content="text/css">
<link rel="stylesheet" href="<?php print $path['css']; ?>/style.css" type="text/css">
<link rel="stylesheet" href="<?php print $path['css_common']; ?>/gw_tags.css" type="text/css">
</head>
<body>

<table cellspacing="0" cellpadding="10" border="0" width="100%">
<tr>
<td class="r" style="text-align:left;background:#FFE"><?php print $title; ?></td>
</tr>
</table>

<table cellspacing="0" cellpadding="0" border="0" width="100%"> <tr> <td style="height:2px;background-color:#EEE"><table cellspacing="0" cellpadding="0" border="0" width="1%"><tr><td></td></tr></table></td> </tr> </table>

<table cellspacing="0" cellpadding="15" border="0" width="100%">
<tr>
<td>
	<p class="u">
	This script updates Glossword versions <tt>1.6.1</tt> up to version <tt>1.6.2</tt>.
	</p>
	<p class="u">	
	After update it is recommended to 
	optimize keywords database using 
	<a href="support/optimize_keywords.php">support/optimize_keywords.php</a> script.
	</p>	
	<ul class="u" type="circle">
	<li><?php print $arActive[0];?>Introduction</li>
	<li><?php print $arActive[1];?>Step 1: Update <b>Database layout</b></li>
	<li><?php print $arActive[2];?>Step 2: Update <b>Dictionaries</b></li>
	</ul>
</td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" border="0" width="100%"><tr><td style="height:2px;background:#EEE"></td></tr></table>

<table cellspacing="0" cellpadding="15" border="0" width="100%">
<tr>
<td class="u"><?php print $strStatus;?></td>
</tr>
</table>

<table cellspacing="0" cellpadding="0" border="0" width="100%"><tr><td style="height:2px;background:#EEE"></td></tr></table>

<?php

$int_next = '';
if (!$isError)
{
	// step switcher
	if (($a + 1) < sizeof($arActive))
	{
		$int_next = ($a + 1);
		$str_next = 'Step ' . $int_next;
	}
	else
	{
		$str_next = $finished;
	}
}
else
{
	$str_next = $strError;
}

print next_step($int_next, $str_next);

?>

<table cellspacing="0" cellpadding="0" border="0" width="100%"><tr><td style="height:2px;background:#EEE"></td></tr></table>

<table cellspacing="0" cellpadding="10" border="0" width="100%">
<tr class="f">
<td align="left" class="t">
<p><b>If you have problems with this update
please visit <a href="support/index.html">support page</a>.</b></p>
</td>
</tr>
</table>

</body>
</html>
