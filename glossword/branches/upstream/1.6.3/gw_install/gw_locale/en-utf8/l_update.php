<?php
$lang = array(
	'up001' => 'Glossword update from %s to %s',
	'up002' => '<p>This update includes the following:</p>',
	'up003' => '<p>Please specify variables for server and database.</p>',
	'up004' => 'Update complete',
	'up005' => 'Total dictionaries found: <b>%d</b>',
	'up006' => 'Dictionaries to upgrade: <b>%d</b>',
);

?>