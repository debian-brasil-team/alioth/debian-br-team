<?php
/**
 *  DO NOT EDIT. Alteration of this file may cause the Glossword to malfunction.
 *  For any changes, please, contact translators.
 *  Translation file generated by Translation Kit (http://glossword.info/tkit/)
 *  © 2002-2004 Dmitry Shilnikov <dev at glossword.info>
 *  ------------------------------------------------------
 *  Project  : Glossword
 *  Topic    : Option text
 *  Language : English
 *  Charset  : UTF-8
 *  Phrases  : 59
 *  Location : locale/en-utf8/options.php
 *  Created  : 09 Jul 2004, 16:48
 *  ------------------------------------------------------
 *  Translation created by:
 *      - Dmitry <dev at glossword.ru>
 *      - Лхагвасүрэнгийн Хүрэлбаатар <hujii247 at yahoo.com>
 */
$lang['is_dict_as_index'] = 'Assign this dictionary as main page';
$lang['is_show_full'] = 'Show the full text of definitions';
$lang['after_post_3'] = 'Open the last search results';
$lang['after_post_is_save_term'] = 'Save term settings';
$lang['after_post_1'] = 'Update dictionary statistics';
$lang['after_post'] = 'After posting...';
$lang['xml_is_term_id'] = 'Term ID';
$lang['xml_is_term_t2'] = 'Alphabetic order 2';
$lang['xml_is_term_t1'] = 'Alphabetic order 1';
$lang['fields_to_edit'] = 'Fields to edit';
$lang['min_srch_length'] = 'Minumum search string length';
$lang['user_notice'] = 'Send notice of registration';
$lang['allow_user'] = 'Access allowed';
$lang['allow_htmleditor'] = 'Use HTML-editor';
$lang['allow_leecher'] = 'Activate anti-leecher';
$lang['user_dictionaries'] = 'Assigned dictionaries';
$lang['date_logged'] = 'Last logged date';
$lang['date_register'] = 'Registered at';
$lang['allow_showcontact'] = 'Show contact information';
$lang['lang_source'] = 'Source language';
$lang['is_specialchars'] = 'Keep special characters';
$lang['admin_url'] = 'Path to administration page';
$lang['login_url'] = 'Path to login page';
$lang['admin_host'] = 'Administrative host';
$lang['login_host'] = 'Login host';
$lang['admin_proto'] = 'Administrative protocol';
$lang['login_proto'] = 'Login protocol';
$lang['path_cache'] = 'Path to cache directory (should be writable by server, chmod 0777)';
$lang['path_log'] = 'Path to log-files';
$lang['site_desc'] = 'Site description';
$lang['site_name'] = 'Site name';
$lang['default_lang'] = 'Default language';
$lang['cache_lifetime'] = 'Cache expired in minutes (0 - never expired)';
$lang['cache_zlib'] = 'Use compression GZip to store/read cache files';
$lang['cache_is'] = 'Enable cache engine';
$lang['defn_cut'] = 'The maximum number of characters for each definition in the list of terms';
$lang['time_upd'] = 'Number of days each dictionary is marked as updated';
$lang['time_new'] = 'Number of days each dictionary is marked as new';
$lang['page_limit'] = 'Number of terms per page';
$lang['sys'] = 'System';
$lang['enter_xml'] = 'Enter a valid XML-code';
$lang['date_modif'] = 'Last modified';
$lang['date_created'] = 'Created at';
$lang['dict_name'] = 'Title';
$lang['announce'] = 'Short description';
$lang['keywords'] = 'Keywords';
$lang['themename'] = 'Color scheme';
$lang['srch_selectdict'] = 'Select dictionary';
$lang['validate'] = 'Validate';
$lang['overwrite'] = 'Overwrite';
$lang['from_time'] = 'from';
$lang['3_undertopic'] = 'Place topic under';
$lang['allow_letters'] = 'Show alphabetic toolbar A-Z';
$lang['allow_dict'] = 'Publish dictionary';
$lang['sysname'] = 'System name';
$lang['options'] = 'Options';
$lang['ed_linksee'] = 'Enable link for See also';
$lang['ed_linksyn'] = 'Enable link for synonym';
$lang['ed_xref'] = 'place link to a term';
/**
 * The end of translation file.
 */
?>