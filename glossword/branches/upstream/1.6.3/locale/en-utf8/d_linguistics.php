<?php
/**
 *  DO NOT EDIT. Alteration of this file may cause the Glossword translations to malfunction.
 *  For any changes, please, contact translators.
 *  Translation file generated by Translation Kit (http://glossword.info/tkit/)
 *  © 2002-2004 Dmitry Shilnikov <dev at glossword.info>
 *  ------------------------------------------------------
 *  Project  : Glossword translations
 *  Topic    : Linguistics
 *  Language : English
 *  Charset  : UTF-8
 *  Phrases  : 42
 *  Location : locale/en-utf8/d_linguistics.php
 *  Created  : 09 Jul 2004, 16:50
 *  ------------------------------------------------------
 *  Translation created by:
 *      - Dmitry <dev at glossword.ru>
 *      - Waseem A. Baobaid <shibamonline at yahoo.com>
 */
$lang['190'] = array('proper noun','p.noun');
$lang['143'] = array('ironical','iron.');
$lang['141'] = array('object','obj.');
$lang['140'] = array('joke, jokingly','joke');
$lang['127'] = array('neuter gender','neut.');
$lang['099'] = array('slang','sl.');
$lang['083'] = array('perfect (tense)','perf.');
$lang['081'] = array('imperative','imper.');
$lang['078'] = array('syllable','syll.');
$lang['050'] = array('archaic','arc.');
$lang['044'] = array('abbreviation','abbr.');
$lang['042'] = array('rare, not in use','rare');
$lang['041'] = array('extended, more common','extend.');
$lang['040'] = array('colloquial(ly)','colloq.');
$lang['035'] = array('figurative(ly)','fig.');
$lang['033'] = array('popular, not technical','pop.');
$lang['029'] = array('plural','pl.');
$lang['025'] = array('masculine gender','m.');
$lang['015'] = array('feminine gender','f.');
$lang['014'] = array('singular','sing.');
$lang['008'] = array('literal(ly)','lit.');
/**
 * The end of translation file.
 */
?>