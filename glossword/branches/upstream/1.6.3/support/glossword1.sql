﻿# phpMyAdmin SQL Dump
# version 2.5.6
# http://www.phpmyadmin.net
#
# Host: localhost
# Generation Time: Jul 09, 2004 at 11:17 PM
# Server version: 3.23.55
# PHP Version: 4.3.6
# 
# Database : `glossword1`
# 

# --------------------------------------------------------

#
# Table structure for table `gw_auth`
#

DROP TABLE IF EXISTS `gw_auth`;
CREATE TABLE `gw_auth` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `user_id` varchar(32) NOT NULL default '',
  `username` varchar(32) NOT NULL default '',
  `password` varchar(32) NOT NULL default '',
  `perms` varchar(8) NOT NULL default '00000000',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `username` (`username`)
) TYPE=MyISAM;

# --------------------------------------------------------

#
# Table structure for table `gw_dict`
#

DROP TABLE IF EXISTS `gw_dict`;
CREATE TABLE `gw_dict` (
  `id` smallint(5) unsigned NOT NULL auto_increment,
  `id_tp` tinyint(3) unsigned NOT NULL default '1',
  `is_active` enum('0','1') NOT NULL default '0',
  `is_show_az` enum('0','1') NOT NULL default '1',
  `is_leech` enum('0','1') NOT NULL default '0',
  `is_htmled` enum('0','1') NOT NULL default '0',
  `id_user` smallint(5) unsigned NOT NULL default '1',
  `auth_level` tinyint(3) unsigned NOT NULL default '0',
  `lang` varchar(12) NOT NULL default 'en-utf8',
  `date_modified` timestamp(14) NOT NULL,
  `date_created` timestamp(14) NOT NULL,
  `uid` varchar(8) NOT NULL default '',
  `int_terms` mediumint(8) unsigned NOT NULL default '0',
  `int_bytes` int(12) unsigned NOT NULL default '0',
  `tablename` varchar(32) NOT NULL default '',
  `themename` varchar(64) NOT NULL default 'gw_silver',
  `title` varchar(255) binary NOT NULL default '',
  `announce` varchar(255) binary NOT NULL default '',
  `description` blob NOT NULL,
  `keywords` varchar(255) binary NOT NULL default '',
  `dict_settings` blob NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `uid` (`uid`)
) TYPE=MyISAM;

# --------------------------------------------------------

#
# Table structure for table `gw_search_results`
#

DROP TABLE IF EXISTS `gw_search_results`;
CREATE TABLE `gw_search_results` (
  `id_srch` varchar(32) NOT NULL default '',
  `id_d` mediumint(5) unsigned NOT NULL default '0',
  `srch_date` timestamp(14) NOT NULL,
  `found` int(12) unsigned NOT NULL default '1',
  `hits` mediumint(5) unsigned NOT NULL default '0',
  `q` varchar(254) NOT NULL default '',
  `srch_settings` mediumblob NOT NULL,
  PRIMARY KEY  (`id_srch`)
) TYPE=MyISAM;

# --------------------------------------------------------

#
# Table structure for table `gw_sessions`
#

DROP TABLE IF EXISTS `gw_sessions`;
CREATE TABLE `gw_sessions` (
  `sid` varchar(32) NOT NULL default '',
  `name` varchar(32) NOT NULL default '',
  `val` text,
  `changed` varchar(14) NOT NULL default '',
  `ip` int(10) unsigned NOT NULL default '0',
  `ua` varchar(32) NOT NULL default '',
  PRIMARY KEY  (`name`,`sid`),
  KEY `changed` (`changed`)
) TYPE=MyISAM;

# --------------------------------------------------------

#
# Table structure for table `gw_settings`
#

DROP TABLE IF EXISTS `gw_settings`;
CREATE TABLE `gw_settings` (
  `settings_key` varchar(127) NOT NULL default '',
  `settings_val` mediumtext NOT NULL,
  PRIMARY KEY  (`settings_key`)
) TYPE=MyISAM;

# --------------------------------------------------------

#
# Table structure for table `gw_stat_dict`
#

DROP TABLE IF EXISTS `gw_stat_dict`;
CREATE TABLE `gw_stat_dict` (
  `id` smallint(5) unsigned NOT NULL auto_increment,
  `hits` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM;

# --------------------------------------------------------

#
# Table structure for table `gw_tpcs`
#

DROP TABLE IF EXISTS `gw_tpcs`;
CREATE TABLE `gw_tpcs` (
  `id` smallint(5) unsigned NOT NULL auto_increment,
  `s` smallint(5) unsigned NOT NULL default '10',
  `p` smallint(5) unsigned NOT NULL default '0',
  `tpname` varchar(255) binary NOT NULL default '',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM;

# --------------------------------------------------------

#
# Table structure for table `gw_users`
#

DROP TABLE IF EXISTS `gw_users`;
CREATE TABLE `gw_users` (
  `auth_id` smallint(5) unsigned NOT NULL auto_increment,
  `date_reg` timestamp(14) NOT NULL,
  `date_login` timestamp(14) NOT NULL,
  `is_active` enum('0','1') NOT NULL default '0',
  `user_actkey` varchar(16) NOT NULL default '',
  `auth_level` tinyint(3) unsigned NOT NULL default '4',
  `user_name` varchar(64) binary NOT NULL default '',
  `user_email` varchar(64) NOT NULL default '',
  `is_showcontact` enum('0','1') NOT NULL default '0',
  `user_settings` blob NOT NULL,
  PRIMARY KEY  (`auth_id`),
  UNIQUE KEY `user_email` (`user_email`)
) TYPE=MyISAM;

# --------------------------------------------------------

#
# Table structure for table `gw_users_map`
#

DROP TABLE IF EXISTS `gw_users_map`;
CREATE TABLE `gw_users_map` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `user_id` smallint(5) unsigned NOT NULL default '1',
  `dict_id` smallint(5) unsigned NOT NULL default '1',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM;

# --------------------------------------------------------

#
# Table structure for table `gw_wordlist`
#

DROP TABLE IF EXISTS `gw_wordlist`;
CREATE TABLE `gw_wordlist` (
  `word_text` varchar(100) binary NOT NULL default '',
  `word_id` int(10) unsigned NOT NULL auto_increment,
  PRIMARY KEY  (`word_text`),
  UNIQUE KEY `word_id` (`word_id`)
) TYPE=MyISAM;

# --------------------------------------------------------

#
# Table structure for table `gw_wordmap`
#

DROP TABLE IF EXISTS `gw_wordmap`;
CREATE TABLE `gw_wordmap` (
  `word_id` int(10) unsigned NOT NULL default '0',
  `term_id` int(10) unsigned NOT NULL default '0',
  `dict_id` smallint(5) unsigned NOT NULL default '0',
  `term_match` tinyint(2) NOT NULL default '0',
  KEY `word_id` (`word_id`),
  KEY `term_id` (`term_id`)
) TYPE=MyISAM;

# --------------------------------------------------------

#
# Table structure for table `gw_dictionary`
#

DROP TABLE IF EXISTS `gw_dictionary`;
CREATE TABLE `gw_xwkgqzy` (
  `id` int(10) unsigned NOT NULL default '0',
  `id_d` mediumint(5) unsigned NOT NULL default '0',
  `id_user` smallint(5) unsigned NOT NULL default '1',
  `date_created` timestamp(14) NOT NULL,
  `hits` int(10) unsigned NOT NULL default '0',
  `term_1` varchar(6) binary NOT NULL default '',
  `term_2` varchar(12) binary NOT NULL default '',
  `term` varchar(255) binary NOT NULL default '',
  `defn` text NOT NULL,
  PRIMARY KEY  (`id`)
) TYPE=MyISAM;
