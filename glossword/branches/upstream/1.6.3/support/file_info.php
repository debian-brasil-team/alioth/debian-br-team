<?php
/**
 *  Glossword - glossary compiler
 *  =============================
 *  Copyright (c) 2002-2003 Dmitry Shilnikov <dev at glossword.info>
 *  http://glossword.info/dev/ 
 */
// --------------------------------------------------------
/**
 *  Prepares checksum for files in Glossword directory.
 *
 *  $Id: file_info.php,v 1.3 2004/03/16 20:47:37 yrtimd Exp $
 */
// --------------------------------------------------------
error_reporting(E_ALL);
// --------------------------------------------------------
// Change current directory to access for website files
// --------------------------------------------------------	
if (!@chdir(".."))
{
    exit(print "Can't change directory to `..'");
}
// --------------------------------------------------------
// New variables
// --------------------------------------------------------
define('CRLF', "\r\n");
define('HTTP_USER_AGENT', isset( $_SERVER["HTTP_USER_AGENT"] ) ? $_SERVER["HTTP_USER_AGENT"] :
					( isset( $HTTP_SERVER_VARS["HTTP_USER_AGENT"] ) ? $HTTP_SERVER_VARS["HTTP_USER_AGENT"] : '' ) );
// --------------------------------------------------------
// Functions
// --------------------------------------------------------
include('./lib/class.func.php');
//
function gw_file_ls($strDir, $reg = "(.*)")
{
	$ar = array();
	if (is_dir($strDir))
	{
		$dir = opendir($strDir);
		while (($f = readdir($dir)) !== false)
		{
			if ($f != '.' && $f != '..')
			{
				if (is_file($strDir . '/' . $f) && preg_match("/".$reg."/", $f))
				{
					$ar[$strDir][] = $f;
				}
				elseif (is_dir($strDir . '/' . $f))
				{
					$ar = array_merge($ar, gw_file_ls($strDir . '/' . $f, $reg));
				}
			}
		} // end of while
	}
	return $ar;
} // end of file_ls();
//
function gw_file_checksum($file)
{
	global $oFunc;
	if (file_exists($file))
	{
		$array = array(2 => '', 3 => '');
		$contents = $oFunc->file_get_contents($file);
		preg_match_all("/Id: (.*?),v(.*?) (\d{4}\/\d{2}\/\d{2}) (\d{2}:\d{2}:\d{2})/", substr($contents, 100, 800), $arPreg);
		if (isset($arPreg[0][0]))
		{
			// 1[0] = filname
			// 2[0] = version
			// 3[0] = date (YYYY/MM/DD)
			// 4[0] = time (00:00:00)
			$array[2] = trim($arPreg[2][0]);
			$array[3] = trim(str_replace('/', '', $arPreg[3][0]) . str_replace(':', '', $arPreg[4][0]));
		}
		$array[0] = $file;
		$array[1] = md5($contents);
		ksort($array);
		return implode(';', $array);
	}
	return '';
}
//
function gw_make_fileinfo($arFiles)
{
	if (!is_array($arFiles))
	{
		$arFiles = array();
	}
	$tmp = array('str' => '');
	$arUnset = array('./logs', './logs/mail', './logs/ref', './logs/srch', 
	                 './admin/export/chm', './admin/export/xml', './admin/export/sql',
                     './cache'
			   );
	// clean cache directory name from index
	while (list($k, $v) = each($arUnset))
	{
		if (isset($arFiles[$v]))
		{
			unset($arFiles[$v]);
		}
	}
	$i = 0;
	while (list($dir, $arF) = each($arFiles))
	{
		for (reset($arF); list($k, $filename) = each($arF);)
		{
			$i++;
			$tmp['str'] .= gw_file_checksum($dir.'/'.$filename);
			$tmp['str'] .= CRLF;
		}
	}
	return $tmp['str'];
}
//
function gw_compress($str, $encode_type = "gzip")
{
	$is_gzip = @function_exists('gzencode');
	$is_bzip = @function_exists('bzcompress');
	
	if ($is_gzip && $encode_type == 'gzip')
	{
		// deflate  = 10576 bytes
		// gzencode = 10361 bytes
		$str = gzencode($str);
	}
	return $str;
}
// --------------------------------------------------------
// Prepare headers
// --------------------------------------------------------
$this_gw['h_mime_type'] = 'application/x-gzip';
$this_gw['h_file_ex'] = '.gz';
$this_gw['h_expires'] = gmdate('D, d M Y H:i:s') . ' GMT';
$this_gw['h_file'] = 'gw_file_info.csv';
//
//
//
$arDirs = gw_file_ls('.', "\.(xml|html|php|css)$");	
$gw_this['str'] = gw_make_fileinfo($arDirs);
$gw_this['str_c'] = gw_compress($gw_this['str']);
//
// Change current directory to support files
//
if (!@chdir("./support"))
{
    exit(print "Can't change directory to `./support'");
}
// --------------------------------------------------------
// Send contents to browser
// --------------------------------------------------------
header('Content-Type: ' . $this_gw['h_mime_type']);
header('Expires: ' . $this_gw['h_expires']);
if (preg_match("/MSIE/", HTTP_USER_AGENT))
{
	header('Content-Disposition: inline; filename="' . $this_gw['h_file'] . $this_gw['h_file_ex'] . '"');
	header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	header('Pragma: public');	
}
else
{
	header('Content-Disposition: attachment; filename="' . $this_gw['h_file'] . $this_gw['h_file_ex'] . '"');
	header('Pragma: no-cache');
}
print $gw_this['str_c'];
// --------------------------------------------------------
// end of file_info.php
?>