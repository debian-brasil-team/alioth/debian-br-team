<?php
/**
 *  $Id: queries.txt,v 1.2 2004/06/28 06:17:07 yrtimd Exp $
 */
/**
 *  Glossword - glossary compiler (http://glossword.info/dev/) 
 *  � 2002-2004 Dmitry N. Shilnikov <dev at glossword.info>
 */
// --------------------------------------------------------
// A few helpful SQL-queries for Glossword maintance.
// --------------------------------------------------------

/*
 Get the most popular keywords.
*/

SELECT count(*) AS n, l.word_text
FROM gw_wordmap as m, gw_wordlist as l
WHERE m.word_id = l.word_id
GROUP BY m.word_id
ORDER BY n DESC  LIMIT 0, 30

/*
 Get the most popular keywords from the dictionary (dictionay ID = 1).
*/

SELECT count(*) AS n, l.word_text
FROM gw_wordmap as m, gw_wordlist as l
WHERE m.word_id = l.word_id
AND m.dict_id = 1
GROUP BY m.word_id
ORDER BY n DESC
LIMIT 0, 50

/*
 Get dictionary ID and term ID for specified keyword (keyword ID = 1).
*/

SELECT * 
FROM gw_wordmap
WHERE word_id = 1
ORDER BY term_id

/*
 Count the total number of mapped keywords for dictonary (dictionary ID = 1).
*/

SELECT count( * )
FROM gw_wordmap 
WHERE dict_id = 1

/*
 Count the total number of UNIQUE mapped keywords for dictonary (dictionary ID = 1).
*/

SELECT count( * ), word_id
FROM gw_wordmap 
WHERE dict_id = 1
GROUP BY word_id 

/*
 Select unused keywords.
*/

SELECT gw_wordlist.*
FROM gw_wordlist LEFT JOIN gw_wordmap ON gw_wordlist.word_id = gw_wordmap.word_id
WHERE gw_wordmap.word_id IS NULL
ORDER BY gw_wordlist.word_text

/*
 Select 10 last search queries.
*/

SELECT q, sum(found) as found, sum(hits) as hits, id_d, srch_date
FROM gw_search_results
WHERE found > 0
GROUP BY q
ORDER BY srch_date DESC
LIMIT 0, 50


/*
 Select top 10 search queries.
*/

SELECT q, found, hits, id_d
FROM gw_search_results
WHERE found > 0
ORDER BY hits DESC
LIMIT 0, 50

/*
 Select the biggest search results
*/

SELECT q, found, hits, id_d, srch_date
FROM gw_search_results
WHERE found > 0
ORDER BY found DESC
LIMIT 0, 50


