<?php
/**
 * $Id: optimize_keywords.php,v 1.4 2004/07/10 15:14:57 yrtimd Exp $
 *
 * Optimizes keywords database. For Administrator only.
 */

/* Change current directory to access for website files */
if (!@chdir('..'))
{
    exit(print "Can't change directory to `..'");
}
define('IN_GW', TRUE);
error_reporting(E_ALL);

/* Load configuration */
$sys['path_include'] = "inc";
include_once('./db_config.php');
include_once($sys['path_include'] . "/config.inc.php");

if (file_exists('gw_install/install_functions.php'))
{
	include_once('gw_install/install_functions.php');
}
else
{
	printf('<br/><b>Error:</b> File %s required.', 'gw_install/install_functions.php');
}
include_once($sys['path_include'] . '/func.sql.inc.php');
include_once($sys['path_include'] . '/constants.inc.php');
include_once($sys['path_include'] . '/func.browse.inc.php');

/* --------------------------------------------------------
 * Translation kit
 * -------------------------------------------------------- */
include_once($sys['path_include'] . '/class.gwtk.php');
$gv['vars'][GW_T_LANGUAGE] = 'en';
$oL = new gwtk;
$oL->setHomeDir('gw_install/gw_locale');
$oL->setLocale($gv['vars'][GW_T_LANGUAGE].'-utf8');
$oL->getCustom('l_install', $gv['vars'][GW_T_LANGUAGE].'-utf8');


if (!file_exists($sys['file_lock']))
{
	print 'Installer is not locked: ' .$sys['file_lock'];
	exit;
}

// time counter
$mtime = explode(" ", microtime());
$starttime = $mtime[1] + $mtime[0];

include_once($sys['path_gwlib'].'/class.db.mysql.php');
include_once($sys['path_gwlib'].'/class.db.q.php');
include_once( $sys['path_include'] . '/lib.auth.'         . $sys['phpEx'] );
include_once( $sys['path_include'] . '/lib.ct_sql.'       . $sys['phpEx'] );
include_once( $sys['path_include'] . '/lib.page.'         . $sys['phpEx'] );
include_once( $sys['path_include'] . '/lib.user.'         . $sys['phpEx'] );
include_once( $sys['path_include'] . '/lib.session.'      . $sys['phpEx'] );
$oSqlQ = new gw_query_storage;
$oDb = new gwtkDb;
// --------------------------------------------------------
// Append system settings
$sys = array_merge($sys, getSettings());

##------------------------------------------------
## registered variables
$arPostVars = array('submit','post','mode','t_tpl','strict','in',
                    'abbr','abbrlang','trns','trnslang',
                    't_term','defn','see','syn','src','d', GW_SID,
                    'id','q', GW_ACTION, GW_TARGET,'p','tid', 'isUpdate',
                    'isLinkSyn','isLinkSee','isConfirm', 'arPost','arPre'
);
//
reset($arPostVars);
for (; list($k, $v) = each($arPostVars);)
{
    if (isset($_POST[$v]) && ($_POST[$v] != '')) // get values from POST
    {
        $$v = $_POST[$v];
    }
    elseif (isset($_GET[$v]) && ($_GET[$v] != '')) // get values from GET
    {
        $$v = $_GET[$v];
    }
    elseif (isset($HTTP_POST_VARS[$v]) && ($HTTP_POST_VARS[$v] != '')) // capability
    {
        $$v = $HTTP_POST_VARS[$v];
    }
    elseif (isset($HTTP_GET_VARS[$v]) && ($HTTP_GET_VARS[$v] != '')) // capability
    {
        $$v = $HTTP_GET_VARS[$v];
    }
    else // default
    {
        $$v = '';
    }
	gw_fixslash($$v);
}
unset($arPostVars);
## end of variables registration
##------------------------------------------------

    function getUnusedKeywords()
    {
    	global $oDb;
        $sql = 'SELECT '.TBL_WORDLIST.'.word_id
				FROM '.TBL_WORDLIST.'
				LEFT JOIN '.TBL_WORDMAP.'
				ON '.TBL_WORDLIST.'.word_id = '.TBL_WORDMAP.'.word_id
				WHERE '.TBL_WORDMAP.'.word_id IS NULL
				ORDER BY '.TBL_WORDLIST.'.word_text';
        $arSql = $oDb->sqlExec($sql, '', 0);
        return $arSql;
    }

// ------------------------------------------------
// Local config

$sys['path_css'] =  $sys['server_dir'].'/'.$sys['path_tpl'].'/'.$sys['themename'];
$sys['html_title'] = sprintf($oL->m('025'));
$arStatus = array();


/* */
function gw_optimize_keywords()
{
	global $id, $arStatus, $oDb, $oL;
	$arKeyUnused = getUnusedKeywords();
	$sql_o = 'DELETE FROM '.TBL_WORDLIST.' WHERE word_id IN (\'%s\')';
	$arKeyId = array();
	for (reset($arKeyUnused); list($k, $v) = each($arKeyUnused);)
	{
		$arKeyId[] = $v['word_id'];
	}
	if (!empty($arKeyId))
	{
		$sql = sprintf($sql_o, implode("', '", $arKeyId));
		if ($oDb->sqlExec($sql, '', 0))
		{
			$arStatus[] = array(sprintf('<b class="red">%s</b>', $oL->m('014')), '');
		}
	}
	$oDb->sqlExec('CHECK TABLE '.TBL_WORDMAP);
	$oDb->sqlExec('OPTIMIZE TABLE '.TBL_WORDMAP);
	$oDb->sqlExec('CHECK TABLE '.TBL_WORDLIST);
	$oDb->sqlExec('OPTIMIZE TABLE '.TBL_WORDLIST);
	$arStatus[] = array(sprintf('<b class="red">%s</b>', $oL->m('015')), '');
}
/* */
function gw_html_contents()
{
	global $id, $arStatus, $oL;

	$arKeyUnused = getUnusedKeywords();
	$arWordlist = getTableInfo(TBL_WORDLIST);
	$intKeyUnused = sizeof($arKeyUnused);

	$arStatus[] = array($oL->m('027'), sprintf('<b>%s</b>', number_format($arWordlist['Rows'], 0, '', ' ')));
	$arStatus[] = array($oL->m('028'), sprintf('<b class="red">%s</b>', number_format($intKeyUnused, 0, '', ' ')));
	$arStatus[] = array($oL->m('029'), sprintf('<b class="green">%s</b>', number_format(($arWordlist['Rows']-$intKeyUnused), 0, '', ' ')));

	$intKbWordlist = ($arWordlist['Data_free'] + $arWordlist['Data_length'] + $arWordlist['Index_length']);
	$arStatus[] = array($oL->m('030'), sprintf('<b>%s</b>', number_format($intKbWordlist, 0, '', ' ')));

	if ($intKeyUnused > 0)
	{
		$arStatus[] = array($oL->m('031'), sprintf('<b class="red">%s</b>',
			number_format((($intKbWordlist / $arWordlist['Rows']) * $intKeyUnused), 0, '', ' '))
		);
		$arStatus[] = array($oL->m('032'), sprintf('<b class="green">%s</b>',
			number_format((($intKbWordlist / $arWordlist['Rows']) * ($arWordlist['Rows']-$intKeyUnused)), 0, '', ' '))
		);
		/* Link to confirm */
		if ($id == '')
		{
			$arStatus[] = array('&#160;');
			$arStatus[] = array(sprintf('<b>%s</b>', $oL->m('033')), sprintf('<a href="%s">%s</a>', '?id=1', $oL->m('011')));
			$arStatus[] = array('&#160;');
		}
	}
	else
	{
		$arStatus[] = array('&#160;');
		$arStatus[] = array($oL->m('013'), '');
		$arStatus[] = array('&#160;');
	}
	print '<div class="contents u">';
	print $oL->m('026');
	print html_array_to_table_multi($arStatus);
	print '</div>';
}

/* */
gw_html_open();

if ($id == '1')
{
	gw_optimize_keywords();
}
gw_html_contents();

gw_html_close();

if (isset($db)) { $db->close(); }

/* end of file */
?>