<?php
/* Database settings for Glossword */
define('GW_DB_HOST', 'localhost');
define('GW_DB_DATABASE', '`glossword1`');
define('GW_DB_USER', 'root');
define('GW_DB_PASSWORD', 'root');
define('GW_TBL_PREFIX', 'gw_');
/* Path names for Glossword */
$sys['server_proto'] = 'http://';
$sys['server_host'] = '127.0.0.1';
$sys['server_dir'] = '/dev/MamboV4.5.1-RC-3';
$sys['server_url'] = 'http://127.0.0.1/dev/MamboV4.5.1-RC-3';
?>