<?php
if (!defined('IN_GW'))
{
	die('<!-- $Id: func.sql.inc.php,v 1.12 2004/07/06 14:18:14 yrtimd Exp $ -->');
}
/**
 *  Glossword - glossary compiler (http://glossword.info/dev/)
 *  � 2002-2004 Dmitry N. Shilnikov <dev at glossword.info>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  (see `glossword/support/license.html' for details)
 */
// --------------------------------------------------------
/**
 *   Functions to handle numerios SQL-requests.
 */
// --------------------------------------------------------

/**
 * Adds or updates a term.
 * @param   array   $arPre Data in structured array
 * @param   int     $dictId Dictionary id
 * @param   array   $arStop Stop words
 * @param   int     $in_term Do search in term or not
 * @param   int     $is_specialchars Is allow special characters in a term
 * @param   int     $is_overwrite Overwrite if already exist
 * @param   int     $intLine Line number, used for Import to report
 * @param   int     $isDelete Is always delete (all checks for existent term will be skipped)
 */
function gwAddTerm($arPre, $dictId, $arStop, $in_term, $is_specialchars, $is_overwrite, $intLine = 0, $isDelete = 0)
{
	global $objDom, $oFunc, $oDb, $oSqlQ;
	global $arFields, $arDictParam, $cnt, $tid, $user, $sys;
	global $gw_this, $qT;

	$id_w = $oDb->MaxId($arDictParam['tablename']);
	$isQ = 1;
	$isCleanMap = 0;
	$id_old = isset($tid) ? $tid : 0;
	$queryA = $qT = array();
	$isTermExist = 0;
	// Custom Term ID, 22 july 2003
	$qT['id'] = $objDom->get_attribute('id', 'term', $arPre['term'] );
	$qT['id'] = preg_replace("/[^0-9]/", '', trim($qT['id']));
	//
	if (!$isDelete) 
	{
		/* check for an existed term */
		/* prepare keywords for a term */
		if ($is_specialchars) // maximum match mode
		{
			$strTermFiltered = text2term_uniq( $objDom->get_content($arPre['term']), 0);
			$strTermFiltered = gw_text_wildcars($strTermFiltered);
			$arKeywordsT = text2keywords( $strTermFiltered, 1 );
		}
		else 
		{
			/* normal match mode */
			$strTermFiltered = text2term_uniq( $objDom->get_content($arPre['term']) );
			$arKeywordsT = text2keywords( $strTermFiltered, $arDictParam['min_srch_length'] ); //
		}
		// is there any keywords for a term?
		$isTermEmpty = (empty($arKeywordsT) && (strlen(trim( $objDom->get_content($arPre['term']) )) == 0)) ? 1 : 0;
		if (!$isTermEmpty) 
		{
			/* no empty keywords for a term */ 
			/* SQL-query */
			/* Get existent keywords, standart mode */
			/* TODO: use alternate keywords database (connect to phpBB, for example) */
			$word_srch_sql = ($qT['id'] != '') ? 't.id = "' . $qT['id'] . '"' : '';
			if ($word_srch_sql == '')
			{
				$word_srch_sql = "k.word_text IN ('" . implode("', '", $arKeywordsT) . "')";
			}
			$sql = $oSqlQ->getQ('get-term-exists', TBL_WORDLIST, $arDictParam['tablename'], $dictId,
						$in_term, $word_srch_sql
					);
			// Get existent keywords, keep specialchars mode
			if (($is_specialchars) || empty($arKeywordsT))
			{
				$sql = $oSqlQ->getQ('get-term-exists-spec', $arDictParam['tablename'], $dictId,
						gw_addslashes( $objDom->get_content($arPre['term']) )
					);
			}
			$arSql = $oDb->sqlExec($sql, '', 0);
#            prn_r($arSql, __LINE__ . '<br />' . $sql);
			$isTermNotMatched = 1; // `No term found' by default
			if (!empty($arSql)) // existent keywords found
			{
				for (; list($arK, $arV) = each($arSql);) // compare founded values (Q) with imported (T)
				{
					$id_old = $arV['id']; // what is ID for existent keywords?
					if ($id_old == $qT['id'])
					{
						$isTermNotMatched = 0;
						break;
					}
					if ($is_specialchars)
					{
						$arKeywordsQ = text2keywords( text2term_uniq( $arV['term'], 0), 1); // 1 - is the minimum length
					}
					else
					{
						$arKeywordsQ = text2keywords( text2term_uniq( $arV['term'] ), $arDictParam['min_srch_length']); // 1 - is the minimum length
					}
#					prn_r($arKeywordsQ);
					$div1 = sizeof(gw_array_exclude($arKeywordsT, $arKeywordsQ));
					$div2 = sizeof(gw_array_exclude($arKeywordsQ, $arKeywordsT));
					$isTermNotMatched = ($div1 + $div2);
					// if the sum of excluded arrays is 0, this term already exists
					if (!$isTermNotMatched) // in english, double negative means positive... yeah
					{
						break;
					}
				} // end of for each founded terms
			}
			if ($isTermNotMatched > 0)
			{
				$isQ = 1;
				$isTermExist = 0;
			}
			else
			{
				$isTermExist = 1;
			}
		} // !$isTermEmpty
	} // end of delete
	// construct query
	if (!isset($strTermFiltered))
	{
		$strTermFiltered = text2term_uniq( $objDom->get_content($arPre['term']), 0);
		$strTermFiltered = gw_text_wildcars($strTermFiltered);
	}

	// Better protection by adding random Next ID number, 21 july 2003
	$qT['id']       = ($qT['id'] =='') ? rand($id_w, ($sys['leech_factor'] * 2) + $id_w) : $qT['id'];
	// Need to replace `/' to avoid URLs like `/4/1,S,S/N,xhtml'
	$qT['id_d']     = $dictId;
	$qT['date_created'] = date("YmdHis", mktime());
	$qT['term_2']   = $objDom->get_attribute('t2', 'term', $arPre['term'] );
	$qT['term_1']   = $objDom->get_attribute('t1', 'term', $arPre['term'] );
	$qT['term_2']   = ($qT['term_2'] == '') ? $oFunc->mb_substr(str_replace('/', '', $strTermFiltered), 0, 2, $sys['internal_encoding']) : str_replace('/', '', $qT['term_2']);
	$qT['term_1']   = ($qT['term_1'] == '') ? $oFunc->mb_substr($qT['term_2'], 0, 1, $sys['internal_encoding']) : $qT['term_1'];
	//
	// Set content for the term
	//
	$qT['term']     = $objDom->get_content($arPre['term']);
	//
	// Set content for the definition
	// keys: 0 - all tags, 3 - content only (default)
	//
	$qT['defn']     = $arPre['parameters']['xml'];
	$qT['id_user']  = $user->id;
	//
#    prn_r($arParsed);
#    prn_r($qT, __LINE__);
	// remove slashes on update
#	if ( ($arPre['parameters']['action'] == UPDATE) && get_magic_quotes_gpc() )
#	{
#		$qT['defn'] = gw_stripslashes( $qT['defn'] );
#	}
	if ($isDelete || $isTermExist)
	{
		if ($is_overwrite)
		{
			$qT['id'] = $id_old;
			$isCleanMap = 1;
			$isTermExist = 0;
			$queryA[] = $oSqlQ->getQ('del-id-id_d', $arDictParam['tablename'], $id_old, $dictId);
		}
	}
	if (!$isDelete && $isTermExist)
	{
		$strLine = 'Line ' . $intLine;
		if (isset($arSql) && sizeof($arSql) == 1)
		{
			$strLine = htmlTagsA($GLOBALS['sys']['page_admin'].'?'.GW_ACTION.'='.GW_A_EDIT.'&'.GW_TARGET.'=' . GW_T_TERM.'&id='.$dictId.'&tid=' . $arSql[0]['id'],
								$objDom->get_content($arPre['term']), '', '', $GLOBALS['L']->m('3_edit'));
		}
		$queryA = '<span class="r" style="color:#E30"><b>'.$GLOBALS['L']->m('reason_25').'</b> ('.$strLine.') </span><br />';
		$isQ = 0;
	}
	// allow query;
	if ($isQ)
	{
		//
		// Prepare keywords per field
		//
#		$tt = new gw_timer('nn');
		for (reset($arFields); list($fK, $fV) = each($arFields);)
		{
			// Init
			$arKeywords[$fK] = array();
			//
			// Get maximum search length per field
			$srchLength = (isset($fV[2]) && ($fV[2] != 'auto')) ? $fV[2] : $arDictParam['min_srch_length'];
			//
			// Make keywords from array
			// space is required, otherwise `...word</defn><defn>word...' will become `wordword'
			$tmpStr = '';
			if (isset($arPre[$fV[0]]))
			{
				$tmpStr = $objDom->get_content( $arPre[$fV[0]] );
			}
			//
			if ($tmpStr != '') // do not parse empty strings
			{
#				$isStrip = ($srchLength == 1) ? 0 : 1;
#                prn_r( text2term_uniq( $tmpStr, $isStrip ) . ' ' . $fV[0] . '; is_strip='.$isStrip.'; len=' . $srchLength);
				$arKeywords[$fK] = text2keywords( gw_text_sql(text2term_uniq( $tmpStr, 1 )), $srchLength, 25, $sys['internal_encoding'] );
				// fix wildcars, 1.6.1
				$arKeywords[$fK] = gw_text_wildcars($arKeywords[$fK]);
				// Remove stopwords from parsed strings only (others are empty,)
				$arKeywords[$fK] = gw_array_exclude( $arKeywords[$fK], $arStop);
			}
		}
		// total keywords convertion time
#		print '<br/>'. sprintf("%1.16f", $tt->end());
		//
		// Remove double keywords from definition
		//
		for (reset($arFields); list($fK, $fV) = each($arFields);)
		{
			if ($fK != 0)
			{
				$arKeywords[0] = gw_array_exclude( $arKeywords[0], $arKeywords[$fK]);
			}
		}
		// Keywords were prepared
		//
		// Add keywords to database! Now!
#        prn_r($arStop);
#        prn_r ( $arKeywords );
#        prn_r($arPre);
		//
		gwAddNewKeywords($dictId, $qT['id'], $arKeywords, $id_old, $isCleanMap);
		//
		// Do automatic corrections for HTML
		$qT['term'] = gw_htmlentities($qT['term']);
		$qT['defn'] = gw_htmlentities($qT['defn']);
		// -------------------------------------------------
		// Turn on text parsers
		// -------------------------------------------------
		// Process automatic functions
		if (!empty($gw_this['vars']['funcnames'][UPDATE . GW_T_TERM]))
		{
			for (; list($k,$v) = each($gw_this['vars']['funcnames'][UPDATE . GW_T_TERM]);)
			{
				if (function_exists($v))
				{
					$v();
				}
			}
		}
		// can be changed to INSERTm but needs more testing
		$queryA[] = gw_sql_replace($qT, $arDictParam['tablename'], 1);
	} // is query allowed
	return $queryA;
}

/**
 * Adds a new keywords into TBL_WORDLIST and TBL_WORDMAP
 * @param int $dictId dictionary ID
 * @param int $termId term ID
 * @param array $arKeywords Array with keywords per field
 * @param int $termIdOld
 * @param int $isClean
 */
function gwAddNewKeywords($dictId, $termId, $arKeywords, $termIdOld, $isClean)
{
	global $arFields, $intFields, $arStop, $sys;
	global $oDb, $oSqlQ;
	//
	// Adding search keywords
	//
	$arKeywordsJoin = array();
	for (reset($arKeywords); list($k, $v) = each($arKeywords);)
	{
		$arKeywordsJoin = array_merge($arKeywordsJoin, $v); // common array with all keywords
	}
	// unique keywords only, have to run second time - wildcard disadvantage
	$arKeywordsJoin = array_values(array_unique($arKeywordsJoin));
	$arQuery = array();
	// prepage sql-query
	$word_text_sql = "'" . implode("', '", $arKeywordsJoin) . "'";
	//
	// Get the list of already known keywords
	$arSql = $oDb->sqlExec($oSqlQ->getQ('get-word', TBL_WORDLIST, $word_text_sql), '', 0);
	//
#    prn_r($arSql);
	//
	$q2 = $q1 = array();
	if (!empty($arSql)) // some keywords already exists
	{
		$cnt = 0;
		// for each founded keyword
		for (; list($arK, $arV) = each($arSql);)
		{
			if ($isClean) // overwrite mode
			{
				$arQuery[0] = $oSqlQ->getQ('del-wordmap-by-term-dict', $termIdOld, $dictId);
			}
			$q2['word_id'] = $arV['word_id'];
			$q2['term_id'] = $termId;
			$q2['dict_id'] = $dictId;
			// Set index ID per field
			for($i = 0; $i <= $intFields; $i++) // all fields are 1 to 10 by default
			{
				if (isset($arKeywords[$i]) && in_array($arV['word_text'], $arKeywords[$i]))
				{
					$q2['term_match'] = $i;
					$arQueryMapExist[] = prepareSQLqueryINSERT($q2, TBL_WORDMAP, 1, $cnt);
					$keyId = gw_array_value($arKeywordsJoin, $arV['word_text']);
					unset($arKeywordsJoin[$keyId]); // remove existent keyword
					$cnt++;
				}
			}
		}
	}
	//
	// Add new kewords
	//
#    prn_r($arKeywordsJoin);
#    prn_r($arKeywords);
	//
	$q2 = $q1 = array();
	$q1['word_id'] = $q2['word_id'] = ($oDb->MaxId(TBL_WORDLIST, 'word_id') - 1);
	$cnt = $cntMap = 0;
	// for each new keyword
	for (reset($arKeywordsJoin); list($k2, $v2) = each($arKeywordsJoin);)
	{
		$v2 = gw_stripslashes($v2);

		$q2['word_id']++;
		$q2['dict_id'] = $dictId;
		$q2['term_id'] = $termId;

		$q1['word_id']++;
		$q1['word_text'] = '';
		//
		// Set index ID per field
		//
		for ($i = 0; $i <= $intFields; $i++) // all fields are 1 to 10 by default
		{
			if (isset($arKeywords[$i]) && in_array($v2, $arKeywords[$i]))
			{
				$q2['term_match'] = $i;
				$q1['word_text'] = $v2;
				// add keyword into map
				$arQueryMap[] = prepareSQLqueryINSERT($q2, TBL_WORDMAP, 1, $cntMap);
				$keyId = gw_array_value($arKeywordsJoin, $v2);
				unset($arKeywordsJoin[$keyId]); // remove new keyword
				$cntMap++;
			}
		}
		// add new word into wordlist
		if ($q1['word_text'] != '')
		{
			$arQueryWord[] = prepareSQLqueryINSERT($q1, TBL_WORDLIST, 1, $cnt);
			$cnt++;
		}
	}
	if (isset($arQueryMapExist)) { $arQuery[] = implode('', $arQueryMapExist); }
	if (isset($arQueryWord)) { $arQuery[] = implode('', $arQueryWord); }
	if (isset($arQueryMap)) { $arQuery[] = implode('', $arQueryMap); }
	$cntQ = sizeof($arQuery);
	//
	// Displays queries for keywords map.
	if ($sys['isDebugQ'])
	{
		print '<span style="font:9pt \'courier new\'"><ul><li>'.implode(';</li><li>', $arQuery).';</li></ul></span>';
	}
	else
	{
		for ($i = 0; $i < $cntQ; $i++)
		{
			if (!$oDb->sqlExec($arQuery[$i], '', 0))
			{
				print '<li class="q">Error: cannot exec query: '.$arQuery[$i].';</li>';
			}
		}
	} // error level
} // end of gwAddNewKeywords();

/**
 *
 */
function sqlLock($tablename)
{
	return true; // disabled for a while
	global $oDb;
	if (!isset($oDb))
	{
		$oDb = new gwtkDb;
	}
	return $oDb->lock($tablename);
}
function sqlUnlock()
{
	return true; // disabled for a while
	global $oDb;
	if (!isset($oDb))
	{
		$oDb = new gwtkDb;
	}
	return $oDb->unlock();
}

function gwsql_get_databases()
{
	global $oDb, $sys;
	if (!defined('IS_CLASS_DB'))
	{
		include_once($sys['path_gwlib'] . '/class.db.mysql.php' );
	}
	if (!isset($oDb))
	{
		$oDb = new gwtkDb;
	}
	return $oDb->get_databases();
}

/**
 *
 */
function getTableInfo($tablename)
{
	global $oDb, $sys;
	if (!defined('IS_CLASS_DB'))
	{
		include_once($sys['path_gwlib'] . '/class.db.mysql.php' );
	}
	if (!isset($oDb))
	{
		$oDb = new gwtkDb;
	}
	return $oDb->table_info($tablename);
}

/**
 *
 * @param    string  $table       sql-table name // TODO: array
 * @param    int     $mode        [ 1 - structure | 2 - data | 3 - structure & data ]
 * @param    int     $func        [ 1 - insert | 2 - update ]
 * @param    string  $where       Dump only selected (28 may 2002)
 * @param    int     $isFields
 * @param    string  $limit       Apply "LIMIT n, n" to query
 */
function getTableStructure($tablename)
{
	global $oDb, $sys;
	if (!defined('IS_CLASS_DB'))
	{
		include_once( $sys['path_gwlib'] . '/class.db.mysql.php' );
	}
	if (!isset($oDb))
	{
		$oDb = new gwtkDb;
	}
	$strQ = '';
	$ar = array();
	// check for table name
	$isTable = 0;
	$arDbTables = $oDb->table_names($tablename);
	for (; list($k, $v) = each($arDbTables);)
	{
		if ($tablename == $v) { $isTable = 1; break; }
	}
	if (!$isTable) { return ''; }
#    if (!$isTable) { return 'ERROR: ' . $tablename . ' does not exist'; }
	// next
	$sql = 'SHOW CREATE TABLE '. $tablename;
	$arSql = $oDb->sqlExec($sql, '', 0);
	$arSql = isset($arSql[0]['Create Table']) ? $arSql[0] : array('Create Table');
	$strQ .= $arSql['Create Table'] . ';';
	return $strQ;
} // end of prepareSQLdum

#### -mySQL ####
// 1, Oct 2002
function gw_sql_replace($arData, $tbl_name, $isFields = 1)
{
	$arFields = $arValues = array();
	$strFields = '';
	if (is_array($arData))
	{
		for (reset($arData); list($k, $v) = each($arData);)
		{
			$v = gw_text_sql($v);
			if ($v == ''){ $vF = "''"; }
			else { $vF = "'" . $v . "'"; }
			$arFields[] = $k;
			$arValues[] = $vF;
		}
		if ($isFields) { $strFields = ' (' . implode(',', $arFields) . ')'; }
		$sql = CRLF . 'REPLACE INTO ' . $tbl_name . $strFields . ' VALUES ('.implode(',', $arValues).')';
		return $sql;
	}
	return false;
}

// 11 aug 2003: do not enclose with quotes all numerals
// 4 Oct 2002: short INSERT format (intCnt = [ 0, 1, 2 ... ]
// 24 May 2002 : $isFields -- include field names to query or not
function prepareSQLqueryINSERT($SQLnamesA, $table, $isFields = 1, $intCnt = 0)
{
	$query = '';
	$SQLfileds = '';
	$SQLfiledA = $SQLvalueA = array();
	if (is_array($SQLnamesA))
	{
		for (reset($SQLnamesA); list($k, $v) = each($SQLnamesA);)
		{
			$v = gw_text_sql($v);
			if ($v == '') { $vF = "''"; }
			// enum('0','1', .. ,'9'), timestamp(14)
			elseif ( preg_match("/^[0-9]{2,12}$/", $v) && !preg_match("/^[0-1]/", $v) )
			{
				$vF = $v;
			}
			else { $vF = "'" . $v . "'"; }
			$SQLfiledA[] = $k;
			$SQLvalueA[] = $vF;
		}
		$SQLvalues = implode(',', $SQLvalueA);
		if ($isFields) { $SQLfileds = ' (' . implode(',', $SQLfiledA) . ')'; }
		if (!$intCnt)
		{
			$query = CRLF.'INSERT INTO ' . $table . $SQLfileds . ' VALUES ('.$SQLvalues.')';
		}
		else
		{
			$query = ', ('.$SQLvalues.')';
		}
	}
	return $query;
} // end of function
/**
 *
 */
function gw_sql_update($SQLnamesA, $table, $where)
{
	$SQLratioA = array();
	for (reset($SQLnamesA); list($key, $val) = each($SQLnamesA);)
	{
		if (is_array($val)){
			$val = implode(',', $val);
		}
		$val = gw_text_sql($val);
		if ($val == ""){$valF = "'".$val."'";}
		else { $valF = "'".$val."'"; }
		$SQLratioA[] = "$key = $valF";
	}
	$SQLratioStr = implode(', ', $SQLratioA);
	$query = 'UPDATE '.$table.' SET '.$SQLratioStr.' WHERE '.$where;
	return $query;
}

#### /mySQL ####
?>