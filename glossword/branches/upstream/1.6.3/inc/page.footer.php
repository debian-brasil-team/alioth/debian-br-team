<?php

$gw_this['str_debug_stats'] = '';
if (defined("DEBUG_SQL_TIME") && DEBUG_SQL_TIME == 1)
{
    $cntCache = isset($oCh) ? sizeof($oCh->query_array) : 0;
    $totaltime = $oTimer->end();

    $sql_time = isset($oDb) ? $oDb->query_time + $oDb->connect_time : 0;
    $sql_time = isset($sess->that->db) ? ($sql_time + $sess->that->db->query_time) : $sql_time;
    $sql_time = isset($user->db) ? ($sql_time + $user->db->query_time) : $sql_time;
    $sql_time = isset($objDict->db) ? ($sql_time + $objDict->db->query_time) : $sql_time;

    $php_time = $totaltime - $sql_time;
    $query_count = isset($oDb) ? sizeof($oDb->query_array) : 0;
    $query_count = isset($sess->that->db) ? ($query_count + sizeof($sess->that->db->query_array)) : $query_count;
    $query_count = isset($user->db) ? ($query_count + sizeof($user->db->query_array)) : $query_count;
    if (isset($ch)) { $cntCache = sizeof($ch->query_array); }
    
    $gw_this['str_debug_stats'] .= '<div class="q" style="padding:4px;background:'.$theme['color_3'].'">';
    $gw_this['str_debug_stats'] .= sprintf('<span style="float:right">PHP=&#160;<b>%1.5f</b> SQL=&#160;<b>%1.5f</b> Total=&#160;<b>%1.5f</b></span>', $php_time, $sql_time, $totaltime);
    $gw_this['str_debug_stats'] .= sprintf('Queries=&#160;<b>%d</b> &#160;', $query_count);
    $gw_this['str_debug_stats'] .= sprintf('Cache SQL=&#160;<b>%s</b> &#160;', ($sys['is_cache_sql'] ? $cntCache : 'OFF'));
    $gw_this['str_debug_stats'] .= sprintf('Cache HTTP=&#160;<b>%s</b> &#160;', ($sys['is_cache_http'] ? 'ON' : 'OFF'));
    $gw_this['str_debug_stats'] .= '</div>';
}
if (defined("DEBUG_SQL_QUERY") && DEBUG_SQL_QUERY == 1)
{
    if (isset($oDb) && $query_count == 0){ $query_count = sizeof($oDb->query_array); }

    $gw_this['str_debug_stats'] .= '<div style="padding:5px;">';
    $gw_this['str_debug_stats'] .= '<div class="t">Number of queries: <b>'. $query_count . '</b></div>';

    if (isset($oDb))
    {
	    $gw_this['str_debug_stats'] .= '<ul class="q" title="general database" style="padding:0;margin:0 3em"><li>'.
	    								sprintf('<b>%1.5f</b> Connect', $oDb->connect_time). '</li><li>'.
	    								implode('</li><li>', $oDb->query_array) . '</li></ul>';
    }
    if (isset($sess->that->db))
    {
    	$gw_this['str_debug_stats'] .= '<ul class="q" title="sessions" style="padding:0;margin:0 3em"><li>' . implode('</li><li>', $sess->that->db->query_array) . '</li></ul>';
    }
    if (isset($user->db))
    {
	    $gw_this['str_debug_stats'] .= '<ul class="q" title="users" style="padding:0;margin:0 3em"><li>' . implode("</li><li>", $user->db->query_array) . "</li></ul>";
    }
    if (isset($objDict->db))
    {
	    $gw_this['str_debug_stats'] .= '<ul class="q" title="objDict" style="padding:0;margin:0 3em"><li>' . implode("</li><li>", $objDict->db->query_array) . "</li></ul>";
    }

    $gw_this['str_debug_stats'] .= '</div>';
}
if (defined("DEBUG_CACHE") && DEBUG_CACHE == 1)
{
    if (isset($oCh))
    {
	    $gw_this['str_debug_stats'] .= '<div style="padding:5px;">';
	    $gw_this['str_debug_stats'] .= '<div class="t">Cache usage: <b>'. sizeof($oCh->query_array). '</b></div>';
	    $gw_this['str_debug_stats'] .= '<ul class="q" style="padding:0;margin:0 3em"><li>' . implode('</li><li>', $oCh->query_array) . '</li></ul>';
	    $gw_this['str_debug_stats'] .= '</div>';
    }
}
if (defined("DEBUG_HTTP") && DEBUG_HTTP == 1)
{
	    $gw_this['str_debug_stats'] .= '<div style="padding:5px;">';
	    $gw_this['str_debug_stats'] .= '<div class="t">HTTP-headers: <b>'. sizeof($oHdr->get()). '</b> (except for Content-encoding)</div>';
	    $gw_this['str_debug_stats'] .= '<ul class="q" title="http headers" style="padding:0;margin:0 3em"><li>'.
	    								implode('</li><li>', $oHdr->get()) . '</li></ul>';
	    $gw_this['str_debug_stats'] .= '</div>';
}

$tpl->addVal( 'V_DEBUG',
				'<div style="width:100%; margin:0; padding:0; overflow: auto; background:'.$theme['color_2'].'">'.
				$gw_this['str_debug_stats'].
				'<br /></div>'
			);

/* */
?>