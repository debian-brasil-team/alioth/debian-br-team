<?php
if (!defined('IN_GW'))
{
	die('<!-- $Id: t.topic.inc.php,v 1.7 2004/07/06 14:18:14 yrtimd Exp $ -->');
}
/**
 *  Glossword - glossary compiler (http://glossword.info/dev/) 
 *  � 2002-2004 Dmitry N. Shilnikov <dev at glossword.info>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  (see `glossword/support/license.html' for details)
 */
// --------------------------------------------------------
/**
 *  Actions for a topic. Add, Browse, Edit, Remove, Update.
 *
 */
// --------------------------------------------------------
/**
 * HTML-form for topic
 *
 * @param    array   $vars       posted variables
 * @param    int     $runtime    is this form posted first time [ 0 - no | 1 - yes ] // todo: bolean
 * @param    array   $arBroken   the names of broken fields (after post)
 * @param    array   $arReq      the names of required fields (after post)
 * @return   string  complete HTML-code
 * @see array_walk(), ctlgGetTopicsRow()
 */
function getFormTopic($vars, $runtime = 0, $arBroken = array(), $arReq = array())
{
	global ${GW_ACTION}, $tid, $L, ${GW_SID}, $sys;
	$strForm = "";
	$trClass = "t";
	$form = new gwForms();

	$form->Set('submitdel', $L->m('3_remove'));
	$form->Set('action', $sys['page_admin']);
	$form->Set('submitok', $L->m('3_save'));
	$form->Set('submitcancel', $L->m('3_cancel'));
	$form->Set('formbgcolor', $GLOBALS['theme']['color_2']);
	$form->Set('formbordercolor', $GLOBALS['theme']['color_4']);
	$form->Set('formbordercolorL', $GLOBALS['theme']['color_1']);
	$form->Set('align_buttons', $GLOBALS['sys']['css_align_right']);	
	$form->Set('charset', $sys['internal_encoding']);
	
	## ----------------------------------------------------
	##
	// check vars
	// reverse array keys <-- values;
	$arReq = array_flip($arReq);
	// mark fields as "REQUIRED" and make error messages
	while(is_array($vars) && list($key, $val) = each($vars) )
	{
		$arReqMsg[$key] = $arBrokenMsg[$key] = "";
		if (isset($arReq[$key])) { $arReqMsg[$key] = ' <span class="red"><b>*</b></span>'; }
		if (isset($arBroken[$key])) { $arBrokenMsg[$key] = ' <span class="red" style="font: 70% verdana"><b>' . $L->m('reason_9') .'</b></span>'; }
	} // end of while
	##
	## ----------------------------------------------------
	$strForm .= '<table cellspacing="0" cellpadding="2" border="0" width="100%">';
	$strForm .= '<col width="25%"/><col width="75%"/>';
	$strForm .= '<tr>'.
				'<td class="' . $trClass . '">' . $L->m('dict_name') . ':' . $arReqMsg['name'] . '</td>'.
				'<td>' . $arBrokenMsg['name'] . $form->field("input","arPost[name]", textcodetoform($vars['name'])) . '</td>'.
				'</tr>';
	if (${GW_ACTION} == GW_A_ADD) // adding a topic
	{
		$topic_mode = "form";
		$strForm .= '<tr>'.
					'<td class="' . $trClass . '">' . $L->m('3_undertopic') . ':' . $arReqMsg['parent'] . '</td>'.
					'<td>' . $arBrokenMsg['parent']
						   . '<select name="arPost[parent]">'
						   . '<option value="0">'. $L->m('root_topic') .'</option>'
						   . ctlgGetTopicsRow($vars['ar'], 0, 1)
						   . '</select>' .
					'</td>'.
					'</tr>';
		$strForm .= '</table>';
		$strForm .= $form->field("hidden", GW_ACTION, GW_A_ADD);
		$strForm .= $form->field("hidden", GW_TARGET, TPCS);
	}
	else // updating
	{
		$strForm .= '<tr>'.
					'<td class="' . $trClass . '">' . $L->m('3_undertopic') . ':' . $arReqMsg['parent'] . '</td>'.
					'<td>' . $arBrokenMsg['parent']
						   . '<select name="arPost[parent]">'
						   . '<option value="0">'. $L->m('root_topic') .'</option>'
						   . ctlgGetTopicsRow($vars['ar'], 0, 1)
						   . '</select>' .
					'</td>'.
					'</tr>';
		$strForm .= '</table>';
		$strForm .= $form->field("hidden", "tid", $tid);
		$strForm .= $form->field("hidden", GW_ACTION, UPDATE);
		$strForm .= $form->field("hidden", GW_TARGET, TPCS);
	}
	$strForm .= $form->field("hidden", GW_SID, ${GW_SID});
	return $form->Output($strForm);
} // end of getFormTopic();
// --------------------------------------------------------
// action switcher
switch (${GW_ACTION})
{
case GW_A_ADD:
## --------------------------------------------------------
## Add a topic

	if (($post == '') && $auth->is_allow('topic')) // not saved
	{
		if (!isset($tid))
		{
			gwtk_header($sys['server_proto'].$sys['server_host'].$sys['page_admin']);
		}
		// set default values
		$vars['name']   = "";
		$topic_mode     = "form";
		$vars['parent'] = $tid;
		$vars['ar']     = ctlgGetTopics();
		$arBroken       = array();
		$strR .= getFormTopic($vars, 0, $arBroken, $arReq[$t]);
	}
	elseif($auth->is_allow('topic')) // parse posted values
	{
		$isPostError = 1;
		$queryA = array();
		if (isset($arReq[$t])) // check for broken fields
		{
			$arBroken = validatePostWalk($arPost, $arReq[$t]);
		}
		if (isset($arReq[$t]) && sizeof($arBroken) == 0) // no errors
		{
			$isPostError = 0;
		}
		else // on error
		{
			$isPostError = 1;
			$topic_mode = "form";
			$tid = $arPost['parent'];
			$arPost['ar'] = ctlgGetTopics();
			$strR .= getFormTopic($arPost, 1, $arBroken, $arReq[$t]);
		}
		if (!$isPostError)
		{
			// Check for duplicate name in the same chunk
			// Insert into TBL_TPCS
			// check term first
			$sql = sprintf('SELECT id
					FROM ' . TBL_TPCS . '
					WHERE UCASE(tpname) = UCASE("%s")
					AND p = "%d"',
					$arPost['name'], $arPost['parent']
				);
			$arSql = $oDb->sqlExec($sql, 'st', 0);
			$q = array();
			if (empty($arSql)) // no duplicates
			{
				$isPostError = 1;
				//
				$q['p'] = $arPost['parent'];
				$q['tpname'] = $arPost['name'];
				$sql = prepareSQLqueryINSERT($q, TBL_TPCS);
				$oDb->sqlExec($sql);
				$sql = sprintf('SELECT id
						FROM ' . TBL_TPCS . '
						WHERE p = "%d"
						ORDER BY tpname ASC',
						$arPost['parent']
					);
				$arSql = $oDb->sqlExec($sql, 'st', 0);
				$i = 10;
				for (; list($arK, $arV) = each($arSql);)
				{
					$queryA[] = "UPDATE " . TBL_TPCS . "
								 SET s = " . $i . "
								 WHERE id = " . $arV['id'];
						$i += 10;
				}
				$strR .= postQuery($queryA, 'a=' . BRWS . '&t=' . TPCS, $sys['isDebugQ'], 0);
			}
			else // topic already exist
			{
				for (; list($arK, $arV) = each($arSql);)
				{
					$tid = $arV['id']; // get ID from existed topic
				}
				$url_admin = append_url($sys['admin_proto'] . '://' . HTTP_HOST . $sys['admin_url']. '?'.GW_ACTION.'='.GW_A_EDIT.'&'.GW_TARGET.'='.TPCS.'&tid='.$tid);
				gwtk_header($url_admin, 0);
			}
		} // isPostError
	} // parse post
	else
	{
		$strR .= $L->m('reason_13');
	}
##
## --------------------------------------------------------
break;
case BRWS:
## --------------------------------------------------------
## Browse topics
	$ar = ctlgGetTopics();
	$topic_mode = "html";
	$strR .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">';
	$strR .= '<tr>';
	$strR .= '<td style="background:'.$GLOBALS['theme']['color_4'].'">';
	$strR .= '<table width="100%" cellspacing="1" cellpadding="0" border="0">';
	$strR .= '<tr style="text-align:center;height:20px;color:'.$GLOBALS['theme']['color_1'].';background:'.$GLOBALS['theme']['color_6'].'">';
	$strR .= '<th style="width:1%" class="gw">N</th>';
	$strR .= '<th style="width:33%" class="gw">' . $L->m('topic') . '</th>';
	$strR .= '<th style="width:26%" class="gw">' . $L->m('order') . '</th>';
	$strR .= '<th style="width:40%" class="gw">' . $L->m('action') . '</th>';
	$strR .= '</tr>';
	$strR .= ctlgGetTopicsRow($ar);
	$strR .= "</table>";
	$strR .= "</td></tr></table><br/>";
##
## --------------------------------------------------------
break;
case GW_A_EDIT:
## --------------------------------------------------------
## Edit a topic
	if ($tid == '')
	{
		gwtk_header($sys['server_proto'].$sys['server_host'].$sys['page_admin']);
	}
	if (($post == '') && $auth->is_allow('topic'))
	{
		$topic_mode = "form";
		$vars['parent'] = $tid;
		$vars['ar'] = ctlgGetTopics();
		$vars['name'] = $vars['ar'][$tid]['tpname'];
		$strR .= getFormTopic($vars, 0, 0, $arReq[$t]);
	}
	else
	{
		$strR .= $L->m('reason_13');
	}
##
## --------------------------------------------------------
break;
case REMOVE:
## --------------------------------------------------------
## Remove selected topic
	$ar = ctlgGetTopics();
	if (!isset($tid))
	{
		gwtk_header($sys['server_proto'].$sys['server_host'].$sys['page_admin']);
	}
	$isError = 0;
	$msgError = "";
	$sql_parent = "";
	$queryA = array();
	// can't remove topic already assigned to another dictionary
	$sql = sprintf('SELECT id, title FROM '.TBL_DICT.' WHERE id_tp = "%d"', $tid);
	$arSql = $oDb->sqlExec($sql);
	if (!empty($arSql))
	{
		$isError = 1;
		$msgError .= "<br/>" . $L->m('reason_3');
	}
	// warn if selected topic is a parent
	if (isset($ar[$tid]['ch']))
	{
		$msgError .= "<br/>" . $L->m('reason_2');
		$arKeys = ctlgGetTree($ar, $tid);
		while(is_array($arKeys) && list($k, $v) = each($arKeys))
		{
			$queryA[] = 'DELETE FROM ' . TBL_TPCS . ' WHERE p = "' . $v . '"';
		}
	} //
	// can't delete last root topic
	$sql = sprintf('SELECT count(*) AS n FROM ' . TBL_TPCS . ' WHERE p != "%d"', $tid);
	$arSql = $oDb->sqlExec($sql, 'st', 0);
	for (; list($arK, $arV) = each($arSql);)
	{
		if($arV['n'] == 1)
		{
			$isError = 1;
			$msgError .= "<br/>" . $L->m('reason_1');
		}
	} //
	if(!isset($ar[$tid]))
	{
		$isError = 1;
		$msgError .= "<br/>" . $L->m('reason_12');
	}
	if (!$auth->is_allow('topic'))
	{
		$isError = 1;
		$msgError .= "<br/>" . $L->m('reason_13');
	}     
	if(!$isError)
	{
		if(!$isConfirm) // if not confirmed
		{
			$Confirm = new gwConfirmWindow;
			$Confirm->action = $sys['page_admin'];
			$Confirm->submitok = $L->m('3_remove');
			$Confirm->submitcancel = $L->m('3_cancel');
			$Confirm->formbgcolor = $GLOBALS['theme']['color_2'];
			$Confirm->formbordercolor = $GLOBALS['theme']['color_4'];
			$Confirm->formbordercolorL = $GLOBALS['theme']['color_1'];            
			$Confirm->setQuestion('<p class="r"><span class="s"><b>' . $L->m('9_remove') .
										'</b>'.$msgError.'</span></p><p class="t"><span class="f">'. $L->m('3_remove').
										':</span><br/>' . $ar[$tid]['tpname'] . '</p>');
			$Confirm->tAlign = 'center';
			$Confirm->formwidth = '400';
			$Confirm->setField("hidden", "tid", $tid);
			$Confirm->setField("hidden", GW_ACTION, REMOVE);
			$Confirm->setField("hidden", GW_TARGET, ${GW_TARGET});
			$Confirm->setField("hidden", GW_SID, ${GW_SID});
			$strR .= $Confirm->Form();
		}
		else
		{
			$queryA[] = 'DELETE FROM ' . TBL_TPCS . ' WHERE id = "' . $tid . '"';
			$strR .= postQuery($queryA, 'a=' . BRWS . '&t=' . TPCS, $sys['isDebugQ'], 0);
		}
	} // isError
	else
	{
		$strR .= '<p class="r"><span class="s">' . $L->m('reason_11') . '</span> '.  $msgError . '</p>';
	}
##
## --------------------------------------------------------
break;
case UPDATE:
## --------------------------------------------------------
## Update topics
	$topic_mode = "form"; // global
	if ($auth->is_allow('topic'))
	{
		$ar = ctlgGetTopics();
		// Up or Down
		if ($mode == 'up' || $mode == 'dn')
		{
			$score = ($mode == "up") ? "- 15" : "+ 15";
			$queryA = array();
			$sql = sprintf('UPDATE ' . TBL_TPCS . ' SET s = s %s WHERE id = "%d"', $score, $tid);
			$oDb->sqlExec($sql);
			$sql = sprintf('SELECT id
					FROM ' . TBL_TPCS . '
					WHERE p = "%d"
					ORDER BY s ASC', 
					$ar[$tid]['p']
				);
			$arSql = $oDb->sqlExec($sql);
			$i = 10;
			for (; list($arK, $arV) = each($arSql);)
			{
				$queryA[] = 'UPDATE ' . TBL_TPCS . '
							 SET s = ' . $i . '
							 WHERE id = ' . $arV['id'];
				$i += 10;
			}
			postQuery($queryA, 'a=' . BRWS . '&t=' . TPCS, $sys['isDebugQ'], 0);
			exit($in); // stop!
		}
		elseif ($mode == 'reset')
		{
			$queryA = array();
			$i = 10;
			$sql = sprintf('SELECT id
					FROM ' . TBL_TPCS . '
					WHERE p = "%d"
					ORDER BY tpname', 
					$ar[$tid]['p']
				);
			$arSql = $oDb->sqlExec($sql);
			for (; list($arK, $arV) = each($arSql);)
			{
				$queryA[] = sprintf('UPDATE ' . TBL_TPCS . '
							SET s = "%d"
							WHERE id = "%d"',
							$i, $arV['id']
						);
				$i += 10;
			}
			postQuery($queryA, 'a=' . BRWS . '&t=' . TPCS, $sys['isDebugQ'], 0);
			exit($in); // stop!
		} // $in selected
		if(isset($arReq[$t])) // check for broken fields
		{
			$arBroken = validatePostWalk($arPost, $arReq[$t]);
		}
		if(isset($arReq[$t]) && sizeof($arBroken) == 0) // no broken fields
		{
			$isPostError = 0;
		}
		else // on error, call HTML-form with topic again
		{
			$isPostError = 1;
			$arPost['ar'] = $ar;
#      $tid = isset($arPost['parent']) ? $arPost['parent'] : 0;
		  $strR .= getFormTopic($arPost, 1, $arBroken, $arReq[$t]);
		}
		if (!$isPostError) // final update
		{
			$q = array();
			$q['tpname'] = $arPost['name'];
			$q['p'] = $arPost['parent'];
			$queryA[] = gw_sql_update($q, TBL_TPCS, 'id = ' . $tid);
			$strR .= postQuery($queryA, 'a=' . BRWS . '&t=' . TPCS, $sys['isDebugQ'], 0);
		} // isPostError
	}
	else
	{
		$strR .= $L->m('reason_13');    
	}
##
## --------------------------------------------------------
break;
default:
break;
} // end of switch
?>
