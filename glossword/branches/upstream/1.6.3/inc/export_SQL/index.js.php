<?php

$strForm .= '<script type="text/javascript">/*<![CDATA[*/';
$strForm .= '
checkSplit();
function selection() {
    if ("Text" == document.selection.type)
    {
        var tr = document.selection.createRange();
        tr.text = tr.text;
        tr.select();
    }
}
function checkSplit() {
    selection();
    if (document.forms[\'vbform\'].elements[\'split_list1\'].checked)
    {
		document.forms[\'vbform\'].elements[\'arPost_split1_\'].disabled = false;
		document.forms[\'vbform\'].elements[\'arPost_split2_\'].disabled = true;    	
        document.all[\'labelCustom\'].className = "f";
        document.all[\'labelList\'].className = "";
    }
    else
    {
        document.all[\'labelCustom\'].className = "";
        document.all[\'labelList\'].className = "f";
		document.forms[\'vbform\'].elements[\'arPost_split1_\'].disabled = true;
		document.forms[\'vbform\'].elements[\'arPost_split2_\'].disabled = false; 
        if ((typeof(document.layers) == \'undefined\') || typeof(slct) == \'undefined\')
        {
            slct = 1; document.all.vbform[\'arPost[\' + "split_m" + \']\'][1].select();
        }
    }
}
function setCheckboxesSQL(is_check) {
	var ch1 = document.forms[\'vbform\'].elements[\'arPost_is_dictdescr_\'];
	var ch2 = document.forms[\'vbform\'].elements[\'arPost_is_dictstats_\'];	
	var ch3 = document.forms[\'vbform\'].elements[\'arPost_is_droptable_\'];	
	var ch4 = document.forms[\'vbform\'].elements[\'arPost_is_keywords_\'];
	ch1.checked = ch2.checked = ch3.checked = ch4.checked = is_check;
}
';
$strForm .= '/*]]>*/</script>';
?>