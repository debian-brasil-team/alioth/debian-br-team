<?php
/**
 * Glossword plug-in: Export to SQL
 * Location: glossword/inc/export_SQL/index.inc.php
 * by Dmitry N. Shilnikov <dev at glossword.info>
 */
// --------------------------------------------------------
if (!defined('IN_GW'))
{
	die('<!-- $Id: index.inc.php,v 1.5 2004/07/06 14:18:15 yrtimd Exp $ -->');
}
// --------------------------------------------------------
define('FORMAT_NAME', str_replace('export_', '', $arPost['fmt']));
define('FORMAT_EXT', 'sql');
// --------------------------------------------------------
/**
 *
 */
function getFormSql($vars, $runtime = 0, $arBroken = array(), $arReq = array())
{
	global $sys, ${GW_ACTION}, $id, $L, $arPost, $arSplit, $arDictParam, ${GW_SID};
	$strForm = "";
	$trClass = "t";
	$form = new gwForms();
	$form->Set('action',           $sys['page_admin']);
	$form->Set('submitok',         $L->m('3_next').' &gt;');
	$form->Set('submitdel',        $L->m('3_remove'));
	$form->Set('submitcancel',     $L->m('3_cancel'));
	$form->Set('formbgcolor',      $GLOBALS['theme']['color_2']);
	$form->Set('formbordercolor',  $GLOBALS['theme']['color_4']);
	$form->Set('formbordercolorL', $GLOBALS['theme']['color_1']);
	$form->Set('align_buttons',    $sys['css_align_right']);
	$form->Set('arLtr',            array('arPost[split]'));
	$form->Set('charset', $sys['internal_encoding']);
	## ----------------------------------------------------
	##
	// reverse array keys <-- values;
	$arReq = array_flip($arReq);
	// mark fields as "REQUIRED" and make error messages
	while(is_array($vars) && list($key, $val) = each($vars) )
	{
		$arReqMsg[$key] = $arBrokenMsg[$key] = "";
		if (isset($arReq[$key])) { $arReqMsg[$key] = ' <span style="color:#E30"><b>*</b></span>'; }
		if (isset($arBroken[$key])) { $arBrokenMsg[$key] = ' <span class="'.$trClass.'" style="color:#E30"><b>' . $L->m('reason_9') .'</b></span>'; }
	} // end of while
	##
	## ----------------------------------------------------
	$strForm = '';
	$strForm .= '<table cellspacing="0" cellpadding="2" border="0" width="100%">';
	$strForm .= '<col width="50%"/><col width="50%"/>';
	$strForm .= '<tr valign="top"><td>';
			$strForm .= getFormTitleNav($L->m('3_export'), '<span class="r">'.FORMAT_NAME.'</span>');
			$strForm .= '<table cellspacing="0" cellpadding="2" border="0" width="100%">';
			$strForm .= '<col width="1%"/><col width="99%"/>';

			$arTmp['id'] = 'arPost_s';
			$strForm .= '<tr class="'.$trClass.'">'.
						'<td>' . $form->field("radio", "arPost[sd_mode]", 's', ($vars['sd_mode'] == 's'), $arTmp) . '</td>'.
						'<td><label for="arPost_s">'. $L->m('sql_structure') .'</label></td>'.
						'</tr>';
			$arTmp['id'] = 'arPost_d';
			$strForm .= '<tr class="'.$trClass.'">'.
						'<td>' . $form->field("radio", "arPost[sd_mode]", 'd', ($vars['sd_mode'] == 'd'), $arTmp) . '</td>'.
						'<td><label for="arPost_d">'. $L->m('sql_data') .'</label></td>'.
						'</tr>';
			$arTmp['id'] = 'arPost_sd';
			$strForm .= '<tr class="'.$trClass.'">'.
						'<td>' . $form->field("radio", "arPost[sd_mode]", 'sd', (($vars['sd_mode'] == 'sd') ? 1 : 0), $arTmp) . '</td>'.
						'<td><label for="arPost_sd">'. $L->m('sql_s_and_d') .'</label></td>'.
						'</tr>';

			$strForm .= '<tr class="'.$trClass.'">'.
						'<td>' . $form->field("checkbox", "arPost[is_dictdescr]", $vars['is_dictdescr']) . '</td>'.
						'<td><label for="arPost_is_dictdescr_">' . $L->m('sql_dict_descr') . '</label></td>'.
						'</tr>';
			$strForm .= '<tr class="'.$trClass.'">'.
						'<td>' . $form->field("checkbox", "arPost[is_dictstats]", $vars['is_dictstats']) . '</td>'.
						'<td><label for="arPost_is_dictstats_">' . $L->m('sql_dict_stats') . '</label></td>'.
						'</tr>';
			$strForm .= '<tr class="'.$trClass.'">'.
						'<td>' . $form->field("checkbox", "arPost[is_droptable]", $vars['is_droptable']) . '</td>'.
						'<td><label for="arPost_is_droptable_">' . $L->m('sql_droptable') . '</label></td>'.
						'</tr>';
			$strForm .= '<tr class="'.$trClass.'">'.
						'<td>' . $form->field("checkbox", "arPost[is_keywords]", $vars['is_keywords']) . '</td>'.
						'<td><label for="arPost_is_keywords_">' . $L->m('sql_keywords') . '</label></td>'.
						'</tr>';

			$strForm .= '</table>';
			// Check/Uncheck All
			$strForm .= '<span class="t"><a href="#" onclick="setCheckboxesSQL(true); return false;">'.$L->m('select_on').'</a>'.
						' | '.
						'<a href="#" onclick="setCheckboxesSQL(false); return false;">'.$L->m('select_off').'</a></span>';
			//
			$strForm .= '</td><td>';
			$strForm .= getFormTitleNav($L->m('dictdump_split'));
			$strForm .= '<table cellspacing="0" cellpadding="2" border="0" width="100%">';
			$strForm .= '<col width="1%"><col width="29%"><col width="70%">';
			$arBoxId['id'] = "split_list1";
			$arBoxId['onclick'] = 'checkSplit();';
			$strForm .= '<tr>';
			$strForm .= '<td>' . $form->field("radio", "arPost[split_m]", "list", $vars['is_list1'], $arBoxId) . '</td>';
			$strForm .= '<td class="t"><label id="labelList" for="split_list1">'.$L->m('dictdump_list').'</label></td>';
			$strForm .= '<td>' . $form->field("select", "arPost[split1]", $vars['intsplit'], "100%", $arSplit ) . '</td>';
			$strForm .= '</tr>';
			$arBoxId['id'] = "split_custom";
			$arBoxId['onclick'] = 'checkSplit();';
			$strForm .= '<tr>';
			$strForm .= '<td>' . $form->field("radio", "arPost[split_m]", "custom", $vars['is_list2'], $arBoxId) . '</td>';
			$strForm .= '<td class="t"><label id="labelCustom" for="split_custom">'.$L->m('dictdump_custom').'</label></td>';
			$strForm .= '<td>' . $form->field("input", "arPost[split2]", $vars['int_terms'] ) . '</td>';
			$strForm .= '</tr>';
			$strForm .= '</table>';
	$strForm .= '</td>';
	$strForm .= '</tr>';
	$strForm .= '</table>';
	include($sys['path_include'] . '/export_SQL/index.js.' . $GLOBALS['sys']['phpEx']);
	$strForm .= $form->field("hidden", 'arPost['.GW_TARGET.']', FORMAT_NAME);
	$strForm .= $form->field("hidden", 'arPost[fmt]', $vars['fmt']);
	$strForm .= $form->field("hidden", 'arPost[int_terms]', $vars['int_terms']);
	$strForm .= $form->field("hidden", 'arPost[min]', $vars['min']);
	$strForm .= $form->field("hidden", 'arPost[max]', $vars['max']);
	$strForm .= $form->field("hidden", 'id', $id);
	$strForm .= $form->field("hidden", GW_TARGET, GW_T_DICT);
	$strForm .= $form->field("hidden", GW_ACTION, EXPORT);
	$strForm .= $form->field("hidden", GW_SID, ${GW_SID});
	return $form->Output($strForm);
} //
// --------------------------------------------------------
$arReq = array('fmt');
// --------------------------------------------------------
// Language
$L->getCustom('export_sql', $sys['locale_name'], 'join');
// --------------------------------------------------------
// split per lines
$is_split       = isset($arPost['is_split']) ? $arPost['is_split'] : 100;
$is_list1       = (isset($arPost['is_list1']) && $arPost['is_list1'] == "list") ? 1 : 0;
$is_list2       = (isset($arPost['is_list2']) && $arPost['is_list2'] == "custom") ? 1 : 0;
$is_droptable   = isset($arPost['is_droptable']) ? $arPost['is_droptable'] : 0;
$is_keywords    = isset($arPost['is_keywords']) ? $arPost['is_keywords'] : 0;
$is_dictdescr   = isset($arPost['is_dictdescr']) ? $arPost['is_dictdescr'] : 0;
$is_dictstats   = isset($arPost['is_dictstats']) ? $arPost['is_dictstats'] : 0;
if (!($is_list1)&&!($is_list2) ) { $is_list1 = 1; }
//
$arSplit = array ('100' => '100', '500' => '500', '1000' => '1000', '2500' => '2500', '5000' => '5000');
$intSplit = 500;
//
if (!isset($arPost[GW_TARGET])) { $post = ''; }
if ($post == '') // not saved
{
	// get number of terms
	$vars['int_terms'] = 0;
	$vars['min'] = $arPost['date_minY'].$arPost['date_minM'].$arPost['date_minD'].str_replace(':', '', $arPost['date_minS']);
	$vars['max'] = $arPost['date_maxY'].$arPost['date_maxM'].$arPost['date_maxD'].str_replace(':', '', $arPost['date_maxS']);
	if (isset($arPost['date_minY']))
	{
		$arSql = $oDb->sqlRun($oSqlQ->getQ('cnt-dict-date', $arDictParam['tablename'], $vars['min'], $vars['max']));
		$vars['int_terms'] = isset($arSql['0']['n']) ? $arSql['0']['n'] : 0;
	}
	//
	$vars['fmt'] = $arPost['fmt'];
	$vars['intsplit'] = $intSplit;
	$vars['is_list1'] = $is_list1;
	$vars['is_list2'] = $is_list2;
	$vars['is_droptable'] = $is_droptable;
	$vars['is_keywords'] = $is_keywords;
	$vars['is_dictdescr'] = $is_dictdescr;
	$vars['is_dictstats'] = $is_dictstats;
	$vars['sd_mode'] = 'sd';
	$strR .= getFormSql($vars, 0, 0, $arReq);
}
else
{
	$arBroken = validatePostWalk($arPost, $arReq);
	if (sizeof($arBroken) == 0)
	{
		$isPostError = 0;
	}
	if (!$isPostError) // single check is ok
	{
		$i = 0;
		$q = array();
		$mode = 'w';
		$cntFiles = 1000;
		if ($arPost['split_m'] == 'list')
		{
			$int_split = $arPost['split1'];
		}
		else
		{
			$int_split = $arPost['split2'];
		}
		$cntFiles = $oFunc->math_divisor($arPost['int_terms'], $int_split);

		$fileS = getExportFilename(
				$sys['path_export'] . '/' . FORMAT_EXT . '/' . sprintf("%05s", $id) . '_' . $arDictParam['tablename'],
				$cntFiles,
				FORMAT_EXT
			 );
		$strR .= '<ul class="t">';
		$oHtml->setTag('a', 'target', '_blank');
		// Export in progress

		// export dictionary description, 27 july 2003
		if ($is_dictdescr)
		{
			$strQ = '';
			$filename = $sys['path_export'] . '/' . FORMAT_EXT . '/' . sprintf("%05s", $id) . '_' . $arDictParam['tablename'] . '_dict.' . FORMAT_EXT;
			$strR .= '<li><span class="f">';
			$strR .= $oHtml->a($filename, $filename) . '</span>... ';
			$strQ .= '# ' . $filename . CRLF;
			$sql = sprintf('SELECT * FROM ' . TBL_DICT . ' WHERE id = %d', $id);
			$arSql = $oDb->sqlExec($sql, '', 0);
			for (; list($arK, $arV) = each($arSql);)
			{
#					for (reset($arV); list($kV, $vV) = each($arV);)
#					{
#						$arV[$kV] = gw_addslashes($arV[$kV], 'runtime');
#					}
				$strQ .= gw_sql_replace($arV, TBL_DICT, 0) . ';';
			}
			$isWrite = $oFunc->file_put_contents( $filename, $strQ, $mode);
			$strR .= ( $isWrite ?  'ok (' . numfmt(strlen($strQ), 0) . " " . $L->m('bytes') . ')' : $L->m('error') ) . '</li>';
		}
		// Export dictionary statistics, 15 july 2003
		if ($is_dictstats)
		{
			$strQ = '';
			$filename = $sys['path_export'] . '/' . FORMAT_EXT . '/' . sprintf("%05s", $id) . '_' . $arDictParam['tablename'] . '_stats.' . FORMAT_EXT;
			$strR .= '<li><span class="f">';
			$strR .= $oHtml->a($filename, $filename) . '</span>... ';
			$strQ .= '# ' . $filename . CRLF;
			$sql = sprintf('SELECT * FROM ' . TBL_STAT_DICT . ' WHERE id = %d', $id);
			$arSql = $oDb->sqlExec($sql, '', 0);
			for (; list($arK, $arV) = each($arSql);)
			{
				for (reset($arV); list($kV, $vV) = each($arV);)
				{
					$arV[$kV] = gw_addslashes($arV[$kV], 'runtime');
				}
				$strQ .= gw_sql_replace($arV, TBL_STAT_DICT, 0) . ';';
			}
			$isWrite = $oFunc->file_put_contents( $filename, $strQ, $mode);
			$strR .= ( $isWrite ?  'ok (' . numfmt(strlen($strQ), 0) . " " . $L->m('bytes') . ')' : $L->m('error') ) . '</li>';
		}
		// Structure
		if (($arPost['sd_mode'] == 'sd') || ($arPost['sd_mode'] == 's'))
		{
			$strQ = '';
			$filename = $sys['path_export'] . '/' . FORMAT_EXT . '/' . sprintf("%05s", $id) . '_' . $arDictParam['tablename'] . '_structure.' . FORMAT_EXT;
			$strR .= '<li><span class="f">';
			$strR .= $oHtml->a($filename, $filename) . '</span>... ';
			$strQ .= '# ' . $filename . CRLF;
			if ($is_droptable) { $strQ .= 'DROP TABLE IF EXISTS ' . $arDictParam['tablename'] . ';' . CRLF; }
			$strQ .= getTableStructure($arDictParam['tablename']) . CRLF;
			$isWrite = $oFunc->file_put_contents( $filename, $strQ, $mode);
			$strR .= ( $isWrite ?  'ok (' . numfmt(strlen($strQ), 0) . " " . $L->m('bytes') . ')' : $L->m('error') ) . '</li>';
		}
		// Data
		if (($arPost['sd_mode'] == 'sd') || ($arPost['sd_mode'] == 'd'))
		{
			for ($i = 0; $i < $cntFiles; $i++)
			{
				$sqlWhereDate = 'date_created >= ' . $arPost['min'] . ' AND date_created <= ' . $arPost['max'];
				$limit = getSqlLimit($arDictParam['int_terms'], $i + 1, $int_split);
				$filename = sprintf($fileS, ($i + 1), $cntFiles);
				$strQ = '';
				$strR .= '<li><span class="f">';
				$strR .= $oHtml->a($filename, $filename) . '</span>... ';

				// Get SQL data for a terms and definitions
				$sql = sprintf('SELECT SQL_BIG_RESULT *
						FROM %s
						WHERE %s
						ORDER BY id %s',
						$arDictParam['tablename'], $sqlWhereDate, $limit
					);
				$arSql = $oDb->sqlExec($sql, '', 0);
				$strQ .= '# ' . $filename . CRLF;
				$strQ .= '# ' . sizeof($arSql) . ' record(s)';
				for (; list($arK, $arV) = each($arSql);)
				{
#					for (reset($arV); list($kV, $vV) = each($arV);)
#					{
#						$arV[$kV] = gw_addslashes(trim($arV[$kV]));
#					}
					if ($int_split != $arDictParam['int_terms']) // REPLACE for updated terms
					{
						$strQ .= gw_sql_replace($arV, $DBTABLE, 0) . ';';
					}
					else // INSERT for complete dictionary
					{
						$strQ .= prepareSQLqueryINSERT($arV, $DBTABLE, 0) . ';';
					}
				}
				$strQ .= CRLF . '# end of ' . $filename;
				$isWrite = $oFunc->file_put_contents( $filename, $strQ, $mode);
				$strR .= ( $isWrite ?  'ok (' . numfmt(strlen($strQ), 0) . " " . $L->m('bytes') . ')' : $L->m('error') ) . '</li>';
		   }
		} //
		if ($is_keywords)
		{
			// Create definition map
			//
			$sqlWhereDate = 'date_created >= ' . $arPost['min'] . ' AND date_created <= ' . $arPost['max'];
			$strQ = '';
			// count number of items in map
			$sql = sprintf('SELECT count(*) as n
					FROM %s AS t, ' . TBL_WORDMAP . ' AS m
					WHERE %s
					AND m.term_id = t.id
					AND m.dict_id = "%d"',
					$arDictParam['tablename'], $sqlWhereDate, $id
					);
			$arSql = $oDb->sqlExec($sql, 0, '');
			$int_map = isset($arSql[0]['n']) ? $arSql[0]['n'] : 0;
			for ($i = 0; $i < $cntFiles; $i++)
			{
				// file name for wordmap
				$fileS = getExportFilename(
					$sys['path_export'] . '/' . FORMAT_EXT . '/' . sprintf("%05s", $id) . '_' . $arDictParam['tablename'] . '_'.TBL_WORDMAP,
					$cntFiles,
					FORMAT_EXT
				 );
				$filename = sprintf($fileS, ($i + 1), $cntFiles) ;
				//
				$tt = new gw_timer('sql_exp');
				$int_perpage = $oFunc->math_divisor($int_map, $cntFiles);
				$limit = $oDb->prn_limit($int_map, $i + 1, $int_perpage);
				$sql = sprintf('SELECT SQL_BIG_RESULT m.*
						FROM %s AS t, ' . TBL_WORDMAP . ' AS m
						WHERE %s
						AND m.term_id = t.id
						AND m.dict_id = "%d"
						ORDER BY m.term_id %s',
						$arDictParam['tablename'], $sqlWhereDate, $id, $limit
					);
				$arSql = $arKeywordsId = $arTermIds = array();
				$arSql = $oDb->sqlExec($sql, '', 0);
				$strQ = $strQmap = $strQdelete = '';
				$strQ .= '# ' . $filename . CRLF;
				$strQ .= '# ' . sizeof($arSql) . ' record(s) of ' . $int_map;
				// clear map first
				if (($i == 0) && ($int_split == $arDictParam['int_terms']))
				{
					$strQdelete .= CRLF . 'DELETE FROM ' . TBL_WORDMAP . ' WHERE dict_id="' . $id . '";';
				}
				// collect data for wordmap
				for (; list($arK, $arV) = each($arSql);)
				{
					if ($int_split != $arDictParam['int_terms'])
					{
						$arTermIds[$arV['term_id']] = $arV['term_id'];
					}
					// 06 july 2003, optimized sql-export
					$arKeywordsId[$arV['word_id']] = $arV['word_id'];
					$strQmap .= prepareSQLqueryINSERT($arV, TBL_WORDMAP, 0) . ';';
				} // clear existent terms
				for (; list($kTerm, $vTerm) = each($arTermIds);)
				{
					$strQdelete .= CRLF . $oSqlQ->getQ('del-term_id-dict_d', TBL_WORDMAP, $vTerm, $id).';';
				}
				$strQ .= $strQdelete . $strQmap;
				$strQ .= CRLF . '# time spend, seconds: '. sprintf("%1.5f", $tt->end());
				$strQ .= CRLF . '# end of ' . $filename;
				$strR .= '<li><span class="f">';
				$strR .= $oHtml->a($filename, $filename) . '</span>... ';
				$isWrite = $oFunc->file_put_contents( $filename, $strQ, $mode);
				$strR .= ( $isWrite ?  'ok (' . numfmt(strlen($strQ), 0) . " " . $L->m('bytes') . ')' : $L->m('error') ) . '</li>';
				$tt = new gw_timer('sql_exp');
				//
				// File name for wordmap
				$fileS = getExportFilename(
					$sys['path_export'] . '/' . FORMAT_EXT . '/' . sprintf("%05s", $id) . '_' . $arDictParam['tablename'] . '_'.TBL_WORDLIST,
					$cntFiles,
					FORMAT_EXT
				 );
				$filename = sprintf($fileS, ($i + 1), $cntFiles);
				$arSql = array();
				if (!empty($arKeywordsId))
				{
					$sqlWhereWordlist = 'word_id IN('.implode(', ', $arKeywordsId) . ')';
					$sql = 'SELECT SQL_BIG_RESULT *
							FROM ' . TBL_WORDLIST . '
							WHERE ' . $sqlWhereWordlist;
					$arSql = $oDb->sqlExec($sql, '', 0);
				}
				$strQ = '';
				$strQ .= '# ' . $filename . CRLF;
				$strQ .= '# ' . sizeof($arSql) . ' records';
				for (; list($arK, $arV) = each($arSql);)
				{
					$strQ .= gw_sql_replace($arV, TBL_WORDLIST, 0) . ';';
				}
				$strQ .= CRLF . '# time spend, seconds: '. sprintf("%1.5f", $tt->end());
				$strQ .= CRLF . '# end of ' . $filename;
				$strR .= '<li><span class="f">';
				$strR .= $oHtml->a($filename, $filename) . '</span>... ';
				$isWrite = $oFunc->file_put_contents( $filename, $strQ, $mode);
				$strR .= ( $isWrite ?  'ok (' . numfmt(strlen($strQ), 0) . ' ' . $L->m('bytes') . ')' : $L->m('error') ) . '</li>';
			} // for each file
		} // $is_keywords
		$strR .= '</ul>';
		$oHtml->setTag('a', 'target', '');
	}
}
/* end o file */
?>