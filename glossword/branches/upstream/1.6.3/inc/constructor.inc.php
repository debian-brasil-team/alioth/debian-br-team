<?php
if (!defined('IN_GW'))
{
	die('<!-- $Id: constructor.inc.php,v 1.20 2004/09/25 11:07:10 yrtimd Exp $ -->');
}
/**
 *  Glossword - glossary compiler (http://glossword.info/dev/)
 *  � 2002-2004 Dmitry N. Shilnikov <dev at glossword.info>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  (see `glossword/support/license.html' for details)
 */
// --------------------------------------------------------
/**
 *  General page constructor.
 */
// --------------------------------------------------------
/* default template */
$tplConstructList = $tplConstructAdd = 'index.html';
$tmp['str_defn'] = '';
// --------------------------------------------------------
// same as ?a=home
if ( ($layout == 'list') && ($w1 == '') ){ $layout = 'home'; }
// because this is a basic constructor for the whole site,
// we need to separate templates between dictionary and website
if ($layout != '') // settings for all dictionary pages
{
	if ( ($layout == 'home') || ($layout == 'list') || ($layout == 'term') )
	{
		if ( empty($arDictParam) || !isset($arDictParam['title']) )
		{
			gwtk_header($sys['server_proto'].$sys['server_host'].$sys['page_index']);
		}
	}
	if ( $dictID > 0 ) // dictionary pages
	{
		// hits + 1 for non-localhost installations
		if (!IS_MYHOST)
		{
			$oDb->sqlExec($oSqlQ->getQ('up-dict-hits', $dictID), '', 0);
		}
		$mktCreated = $oFunc->date_Ts14toTime($arDictParam['date_created']);
		// 1.3.9
		// Sets last modified date for all dictionary pages (!)
		if ($sys['is_cache_http'])
		{
			// first header
			$oHdr->add("Last-Modified: " . gmdate("D, d M Y H:i:s", $mktCreated) . ' GMT');
		}
		// Dictionary table name, Depreciated variable
		$DBTABLE = $arDictParam['tablename'];
		// Do we need to link main page from main page? No.
		if ($layout == 'home')
		{
			$tpl->addVal("DICT_NAME_LINKED", $arDictParam['title']);
		}
		else
		{
			$tpl->addVal("DICT_NAME_LINKED", $oHtml->a($sys['page_index'] . '?a=list&d=' . $dictID, $arDictParam['title']));
		}
		$tpl->addVal("DICT_NAME",  $arDictParam['title']);
		$tpl->addVal("DICT_DESCR", $arDictParam['description']);
		$tpl->addVal("DICT_SIZE",  numfmt($arDictParam['int_terms']));
		// letters 0-Z
		if ($arDictParam['is_show_az'] == 1) // show or hide letters toolbar
		{
			// Always constructs A-Z toolbar (single letters)
			$ar0z = getLettersArray($dictID);
			// arrays, dictionary ID, current letter, isLinked, isLine;
			if ((${GW_ACTION} != GW_T_TERM) && (${GW_ACTION} != SEARCH))
			{
				$tpl->addVal("AZ", getLetterHtml($ar0z, $dictID, $w1 . ' ') );
			}
		}
		$tpl->addVal( "INT_D", $dictID );
	}
	else // non-dictionary pages (top10, feedback, title page)
	{
		if ($sys['is_cache_http'])
		{
			$oHdr->add("Last-Modified: " . gmdate("D, d M Y H:i:s") . ' GMT');
		}
	}
} // end of dictionary pages
// Language settings, from Translation Kit
$tpl->addVal( 'LANG',           $L->languagelist("0") );
$tpl->addVal( 'TEXT_DIRECTION', $L->languagelist("1") );
$tpl->addVal( 'CHARSET',        $L->languagelist("2") );
// 11 december 2002, r-t-l with l-t-r
$sys['css_align_right'] = 'right';
$sys['css_align_left'] = 'left';
if ($L->languagelist('1') == 'rtl')
{
	$sys['css_align_right'] = 'left';
	$sys['css_align_left'] = 'right';
}
$tpl->addVal( "CSS_ALIGN_RIGHT", $sys['css_align_right']);
$tpl->addVal( "CSS_ALIGN_LEFT",  $sys['css_align_left']);
// other variables
$tpl->addVal( "q",               '');
$tpl->addVal( "U_TO_MAIN",       $oHtml->url_normalize($sys['page_index']) );
$tpl->addVal( "PATH_DICT",       append_url($sys['page_index'].'?a=list&d='.$dictID) );
$tpl->addVal( "L_TO_MAIN",       $L->m('3_tomain'));
$tpl->addVal( "L_TOP_OF_PAGE",   $L->m('3_top'));
$tpl->addVal( "GLOSSWORDVERSION",$sys['version']);
$tpl->addVal( "FORM_ACTION",     $sys['page_index'] );
$tpl->addVal( "L_TERMS_AMOUNT",  $L->m('termsamount'));
$tpl->addVal( "URL_MAILTO",      $oFunc->text_mailto('<a href="mailto:'.$sys['y_email'].'">'. $sys['y_name'] . '</a>'));
$tpl->addVal( "NOURL_MAILTO",    $sys['y_name']);

$tpl->addVal( "SITE_NAME",       $sys['site_name'] );
$tpl->addVal( "L_SRCH_STRICT",   $L->m('srch_strict') );
$tmp['input_url_append'] = '';
/* Append URL for integration */
for (reset($sys['ar_url_append']); list($k, $v) = each($sys['ar_url_append']);)
{
	$tmp['input_url_append'] .= '<input type="hidden" name="'.$k.'" value="'.$v.'" />';
}
$tpl->addVal( "v:input_url_append", $tmp['input_url_append'] );
/* */
switch($layout)
{
	case 'home': // Dictionary title page
	{
		$strCurrentSection = '';
		$tplConstruct = array( "header.html", "search_form.html", "menu_letters_az.html",
								"dict_about.html", "footer_inside.html");
		$tpl->addVal( "T_LASTADDED",         getTop10('R_TERM_NEWEST') );
		$tpl->addVal( "SEARCHFORM",          textcodetoform($sys['searchformcode']) );
		$tpl->addVal( "SEARCHFORM_RENDER",   $sys['searchformcode'] );
		$tpl->addVal( "DICT_MODIFIED",       sprintf("%s: %s", $L->m('date_modif'), dateExtract($arDictParam['date_modified'], "%d %F %Y") ));
	}
	break;
	case 'list':
	{
		/* Build the list of terms for preview */
		$strCurrentSection = '';
		$tpl->addVal("L_PAGES",         '');
		$tpl->addVal("PAGE_SELECTION",  '');
		$tplConstruct = array( "header.html", "search_form.html", "menu_letters_az.html");
		if (($w1 != '') && ($w2 == ''))
		{
			/* 0-Z */
			$listA = getDictWordList($w1, $w2, $dictID, $p, 1, $arDictParam['is_show_full']);
			$tpl->addVal("block:term_list", $listA[0]);
			$sys['total'] = $listA[1];
			$gw_this['arTitle'][0] = $w1;
		}
		elseif (($w1 != '') && ($w2 != ''))  
		{
			/* 00-ZZ */
			$listA = getDictWordList($w1, $w2, $dictID, $p, 1, $arDictParam['is_show_full']);
			$tpl->addVal("block:term_list", $listA[0]);
			$sys['total'] = $listA[1];
			$gw_this['arTitle'][0] = $w2;
		}

		$intSumPages = $listA[3];
		if ($intSumPages > 1)
		{
			$tpl->addVal("L_PAGES", $L->m('L_pages').':');
		}
		// checking page numbers
		if ( ( $p < 1 ) || ( $p > $intSumPages) ){ $p = 1; }

		if ($arDictParam['is_show_az']) // show or hide letters toolbar
		{
			// $w1 already selected
			$tpl->addVal( "AAZZ", getLetterHtml( $ar0z, $dictID, $w1, $w2 . ' ') );
			$tplConstruct[] = "menu_letters_aazz.html";
		}
		if ($intSumPages > 1)
		{
			$tpl->addVal( "PAGE_SELECTION", getNavToolbar($intSumPages, $p, $sys['page_index'] . '?'.GW_ACTION.'='.$layout.'&strict='.$strict.'&d='.$dictID.'&w1='.urlencode($w1).'&w2='.urlencode($w2).'&p='));
		}
		$tplConstruct[] = "term_list.html";
		$tplConstruct[] = "menu_pages.html";
		$tplConstruct[] = "footer_inside.html";
	}
	break;
	case GW_T_TERM: 
	{
		/* Term selected */
		$strCurrentSection = '';
		$tpl->addVal( 'PAGE_SELECTION', '');
		$tpl->addVal( 'T_LASTADDED',    getTop10('R_TERM_NEWEST', 10, 1) );
		$tpl->addVal( 'PATH_IMG_DICT',  $sys['path_img'] . '/' . sprintf('%05d', $dictID) );
		$tpl->addVal( 'L_TERM_LINK',    $L->m('page_link_term'));
		$tpl->addVal( 'L_DICT_LINK',    $L->m('page_link_dict'));
		// -------------------------------------------------
		// Load HTML-templates
		if ($isPrint)
		{
			$tplConstruct = array("header_print.html", "search_form_print.html");
		}
		else
		{
			$tplConstruct = array("header.html", "search_form.html");
			if ($arDictParam['is_show_az']) // show or hide alphabetic toolbar
			{
				$tplConstruct[] = "menu_letters_az.html";
				$tplConstruct[] = "menu_letters_aazz.html";
			}
		}
		// -------------------------------------------------
		// Get term parameters
		$listA = getTermParam($t, $q);
		// Redirect to dictionary title page if no definition found for specified Term ID
		if (!isset($listA['defn']))
		{
			gwtk_header($sys['server_proto'].$sys['server_host'].$sys['page_index'].'?a=list&d=' . $dictID);
		}
		/* State: Term parameters were taken */

		/* 25 may 2002: ?a=term&d=55&t=1 */
		$gw_this['term_linked'] = strip_tags($listA['term']);
		$gw_this['term_linked'] = str_replace('/', ' ', $gw_this['term_linked']);
		$gw_this['term_linked'] = preg_replace("/\s{2,}/" , ' ', $gw_this['term_linked']);
		$gw_this['term_linked'] = urlencode($gw_this['term_linked']);
		$tpl->addVal( 'TERM_URL',
						$oHtml->a(
							$sys['server_proto'] . $sys['server_host'] . $sys['page_index']. '?a=term&d=' . $dictID . '&q=' . $gw_this['term_linked'],
							$sys['server_proto'] . $sys['server_host'] . $sys['page_index']. '?a=term&d=' . $dictID . '&q=' . $gw_this['term_linked']
						)
		);
		$tpl->addVal( 'DICT_URL',
						$oHtml->a(
							$sys['page_index']. '?a=list&d=' . $dictID,
							$sys['server_proto'] . $sys['server_host'] . $oHtml->url_normalize($sys['page_index']. '?a=list&d=' . $dictID)
						)
		);
		// -------------------------------------------------
		// Printable version
		$listA['term_length'] = $oFunc->mb_strlen( $listA['term'] );
		$gw_this['href_print'] = $oHtml->a($sys['page_index'].'?a='.A_PRINT.'&d='. $dictID.'&q='.$gw_this['term_linked'], $L->m('printversion'));
		// get stopwords
		$arStop = $L->getCustom('stop_words',  $arDictParam['lang'], 'return');
		// link to print version
		if ( ($listA['term_length'] < $arDictParam['min_srch_length']) ||
			(urlencode(gw_addslashes(str_replace('.','',$gw_this['term_linked']))) != $listA['term']) ||
			in_array(strtoupper($listA['term']), $arStop) )
		{
			$gw_this['href_print'] = $oHtml->a(append_url($sys['page_index'].'?a='.A_PRINT.'&d='. $dictID.'&t='.$listA['tid']), $L->m('printversion'));
			$tpl->addVal( 'TERM_URL',
				   $oHtml->a(
						$sys['page_index']. '?a=term&d=' . $dictID . '&t=' . $listA['tid'],
						'http://' . HTTP_HOST . $oHtml->url_normalize($sys['page_index']. '?a=term&d=' . $dictID . '&t=' . $listA['tid'])
				   )
			);
		}
		// -------------------------------------------------
		// Process automatic functions
		if (!empty($gw_this['vars']['funcnames'][GW_T_TERM]))
		{
			for (; list($k, $v) = each($gw_this['vars']['funcnames'][GW_T_TERM]);)
			{
				if (function_exists($v))
				{
					$v();
				}
			}
		}
		// -------------------------------------------------
		$tmp['cssTrClass'] = 't';
		$tmp['xref'] = $sys['page_index'] . '?a=term&amp;d=' . $dictID . '&amp;q=';
		$tmp['href_srch_term'] = $oHtml->url_normalize($sys['page_index'] . '?a=srch&amp;in=term&amp;d=%d&amp;q=%s&amp;strict=1');
		$tmp['href_link_term'] = $oHtml->url_normalize($sys['page_index'] . '?a=term&amp;d=%d&amp;q=%s');
		$arDictParam['lang'] = $gw_this['vars'][GW_T_LANGUAGE].'-utf8';
		//
		// Render HTML page, 25 apr 2003
		//
		$arPre = gw_Xml2Array($listA['defn']);
		$tmp['term'] = $listA['term'];
		$tmp['t1'] = $listA['term_1'];
		$tmp['t2'] = $listA['term_2'];
		$tmp['tid'] = $listA['tid'];
		//
		$objDom = new gw_domxml;
		$objDom->setCustomArray($arPre);
		$oRender = new $gw_this['vars']['class_render'];
		$oRender->Set('Gtmp', $tmp );
		$oRender->Set('Gsys', $sys );
		$oRender->Set('L', $L );
		$oRender->Set('objDom', $objDom );
		$oRender->Set('arDictParam', $arDictParam );
		$oRender->Set('arEl', $arPre );
		$oRender->Set('arFields', $arFields );
		$oRender->load_abbr_trns();
		//
		$tmp['str_defn'] = $oRender->array_to_html($arPre);
		/* Process text filters */
		while (!$sys['is_debug_output'] 
				&& is_array($sys['filters_defn']) 
				&& list($k, $v) = each($sys['filters_defn']) )
		{
			$tmp['str_defn'] = $v($tmp['str_defn']);
		}
		$tpl->addVal( "DEFN", $tmp['str_defn'] );
		// -------------------------------------------------
		/* $tag_stress_rule */
		$ar_pairs_src = explode("|", $oRender->tag_stress_rule);
		$listA['term'] = str_replace('<stress>', $ar_pairs_src[0], $listA['term']);
		$listA['term'] = str_replace('</stress>', $ar_pairs_src[1], $listA['term']);
		// --------------------------------------------------
		$tpl->addVal( 'TERM', $listA['term'] );
		$tpl->varXref = $sys['page_index'] . '?a=term&amp;d=' . $dictID . '&amp;q=';
		$tpl->layout = $layout;
		$tpl->addVal( "L_pages", "&#160;");
		//
		// linked term does not exist
		//
		if ($listA['term'] == '')
		{
			$tpl->addVal( 'AAZZ', '' );
			// avoid cross-scripting
			$tpl->addVal( 'DEFN', '<span class="term">'. textcodetoform($q) .'</span><br /><span class="r">' .
			htmlTagsA('?a=fb&name=Visitor&email=guest@'.HTTP_HOST.'&arPost[message]='.urlencode($q.' - '.CRLF.$L->m('reason_26')), $L->m('reason_26')) .'</span>');
		}
		//
		if ($isPrint) // HTML-code modifications for printable version
		{
			$tpl->addVal('PRINTVERSION', $L->m('printversion'));
			$tpl->tplPairs['DEFN'] = preg_replace("/<ol(.*[^>])>/", "<br />", $tpl->tplPairs['DEFN']);
			$tpl->tplPairs['DEFN'] = str_replace("</ol>", " ", $tpl->tplPairs['DEFN']);
			$tpl->tplPairs['DEFN'] = preg_replace("/<ul(.*[^>])>/", "<br />", $tpl->tplPairs['DEFN']);
			$tpl->tplPairs['DEFN'] = str_replace("</ul>", " ", $tpl->tplPairs['DEFN']);
			$tpl->tplPairs['DEFN'] = str_replace("<li>", "&#160;&#8226;", $tpl->tplPairs['DEFN']);
			$tpl->tplPairs['DEFN'] = str_replace("</li>", "<br/>", $tpl->tplPairs['DEFN']);
			$tpl->tplPairs['DEFN'] = str_replace('&#x25BA;', ' ', $tpl->tplPairs['DEFN']);
			//
			$tplConstruct[] = 'term_item_print.html';
			$tplConstruct[] = 'footer_print.html';
		}
		else
		{
			if ($arDictParam['is_show_az']) // show or hide letters toolbar
			{
				$tpl->addVal("AZ", getLetterHtml($ar0z, $dictID, $listA['term_1']));
				$tpl->addVal("AAZZ", getLetterHtml($ar0z, $dictID, $listA['term_1'], $listA['term_2']. ' '));
			}
			// Add to favorites
			$gw_this['url_fav'] = 'http://' . HTTP_HOST . $sys['page_index']. '?a=term&amp;d=' . $dictID . '&amp;q=' . $gw_this['term_linked'];
			$oHtml->setTag('a', 'onclick', 'window.external.AddFavorite(\''. strip_tags($gw_this['url_fav']) . "','". addslashes(strip_tags($listA['term'].' - '.$arDictParam['title'])) . "');return false;");
			$gw_this['href_fav'] = $oHtml->a('#', $L->m('page_fav'));
			$oHtml->setTag('a', 'onclick', '');
			// Refresh the page
			# 'http://' . HTTP_HOST . REQUEST_URI.'&r=' . rand(0, 9999);
			$gw_this['url_refresh'] = 'http://' . HTTP_HOST . $oHtml->url_normalize(append_url($sys['page_index'].'?a=term&d='. $dictID.'&t='.$listA['tid'].'&r='.rand(0,999)));
			$oHtml->setTag('a', 'onclick', "window.location.href('".$gw_this['url_refresh']."');return false;");
			$gw_this['href_refresh'] = $oHtml->a('#', $L->m('page_refresh'));
			$oHtml->setTag('a', 'onclick', '');

			$arTermActions = array(
				$gw_this['href_refresh'],
				$gw_this['href_fav'],
				$gw_this['href_print'],
			);
			$tpl->addVal( 'TERM_ACTIONS', implode(' | ', $arTermActions));
			//
			$tplConstruct[] = 'term_item.html';
			$tplConstruct[] = 'menu_pages.html';
			$tplConstruct[] = 'footer_inside.html';
		}
		$gw_this['arTitle'][] = strip_tags($listA['term']);
	}
	break;
	case SEARCH: // Search mode
	{
		$strCurrentSection = '';
		$tpl->addVal("DICT_NAME_LINKED", $L->m('2_page_18'));
		$tplConstruct = array("header.html", "search_form.html", "menu_letters_az.html");
		$intFound = 0;
		$q = trim($oFunc->mb_substr($q, 0, 254));
		//
		$gw_this['arSrchResults'] = array();
		$gw_this['arSrchResults']['found'] = 0;
		$gw_this['arSrchResults']['q'] = $q;
		if ($id_srch == '') // 1st search query
		{
			gw_search($q, $gw_this['arDictListSrch'], $srch);
		}
		else // list seach results
		{
			$gw_this['arSrchResults'] = gw_search_results($id_srch, $p, $dictID);
			/* Write search log */
			if (isset($gw_this['arSrchResults']['found']) && ($p == 1) && $sys['is_LogSearch'] )
			{
				if (!isset($oLog))
				{
					$oLog = new gw_logwriter($sys['path_logs']);
				}
				$arLogSrch = array();
				$arLogSrch[0] = GW_TIME_NOW_UNIX;
				$arLogSrch[1] = $oFunc->ip2int(REMOTE_IP);
				$arLogSrch[2] = 'd=' . urlencode($dictID) . '&in=' . $gw_this['arSrchResults']['in'] .
								'&found=' . $gw_this['arSrchResults']['found'] .
								'&q=' . urlencode($gw_this['arSrchResults']['q']);
				$oFunc->file_put_contents($oLog->get_filename('srch'), $oLog->make_str($arLogSrch), 'a');
			}
		}
		if ($gw_this['arSrchResults']['found'] == 0) // nothing was found
		{
			$gw_this['arSrchResults']['html'] =
			'<div class="y" style="font:75% verdana, helvetica">'.
			'<p>'. $L->m('reason_5') . '</p>'.
			'<p>' . sprintf(
							$L->m('srch_trydefn'),
							$oHtml->url_normalize(append_url($sys['page_index'] . '?'. GW_ACTION . '='.SEARCH.'&d='.$dictID."&amp;q=".urlencode($gw_this['arSrchResults']['q']).'&in=defn&strict=1')),
							$oHtml->url_normalize(('index.'. $sys['phpEx'] . '?'. GW_ACTION . '='.SEARCH.'&q='.urlencode($gw_this['arSrchResults']['q']))) ).
			'</p>'.
			'</div>';
		}
		$tplConstruct[] = 'term_list_search.html';
		$tpl->addVal( 'block:term_list_search', $gw_this['arSrchResults']['html']);
		$intSumPages = 1;
		if ( isset($gw_this['arSrchResults']['pages']) ){ $intSumPages = $gw_this['arSrchResults']['pages']; }
		// fix page number
		if ( ( $p < 1 ) || ( $p > $intSumPages) ){ $p = 1; }
		// show dictionary name on search results
		if ($dictID > 0)
		{
			$tpl->addVal("DICT_NAME_LINKED", $L->m('2_page_18') . '<br />' . $oHtml->a($sys['page_index'] . '?a=list&d=' . $dictID, $arDictParam['title']));
		}
		// redirect when only one word found
		if (($gw_this['arSrchResults']['found'] == 1) && ($dictID > 0) && isset($gw_this['arSrchResults']['results'][$dictID]))
		{
			gwtk_header($sys['server_proto'].$sys['server_host'].$sys['page_index']
						. '?a=term&d=' . $dictID
						. '&t=' . $gw_this['arSrchResults']['results'][$dictID], $sys['is_delay_redirect']
			);
		}

		$tpl->addVal( 'q', textcodetoform($gw_this['arSrchResults']['q']) );

		if ($intSumPages > 1) // enable page navigation
		{
			$tpl->addVal( 'L_PAGES', $L->m('L_pages').':');
			$tpl->addVal( 'PAGE_SELECTION',
				getNavToolbar($intSumPages, $p, $sys['page_index'] . '?'.GW_ACTION.'='.${GW_ACTION}.'&id_srch='.$id_srch.'&d='.$dictID.'&strict='.$strict.'&p=')
					);
		}
		$tplConstruct[] = "menu_pages.html";
		$tplConstruct[] = "footer_inside.html";

		$gw_this['arTitle'][] = textcodetoform($gw_this['arSrchResults']['q']);
		$gw_this['arTitle'][] = $L->m('2_page_18');
	}
	break;
	case "fb": // Feedback form
	{
		$gw_this['arTitle'][] = $strCurrentSection = $L->m('web_m_fb');
		$tpl->addVal( "AZ",       "");
		$tpl->addVal( "DICT_NAME",      $sys['site_name'] );
		$tpl->addVal( "DICT_NAME_LINK", $strCurrentSection);
		$tpl->addVal( "fb_fill",        $L->m('fb_fill') );
		$tpl->addVal( "fb_complete",    $L->m('fb_complete') );
		$tplConstruct = array("header.html", "search_form_empty.html");
		// required fields
		$arReqFields = array("name", "email", "message");
		$vars["name"]   = $name;
		$vars["email"]  = $email;
		$vars["url"]    = ( !empty($vars["url"]) && ($vars["url"] != "http://" ) ) ? $url : "http://";
		$vars["message"]= ( is_array($arPost) && isset($arPost['message'])
							&& (trim($arPost['message']) != "" ) ) ? $arPost['message'] : "";
		$strWebFormAdv  = "";
		if ($post == '') // not submitted
		{
			$tplConstruct[] = "web_feedback_before.html";
			$vars["url"]    = "http://";
			$strWebFormAdv .= scrFeedback($vars, 0, 0, $arReqFields);
		}
		else // after post
		{
			// Checking posted vars
			$isPostError = 1;
			$errorStr = "";
				$arBroken = validatePostWalk($vars, $arReqFields);
				if (sizeof($arBroken) == 0)
				{
					$isPostError = 0;
				}
				else
				{
					$tplConstruct[] = "web_feedback_before.html";
					$strWebFormAdv .= scrFeedback($vars, 1, $arBroken, $arReqFields);
				}
				if (!$isPostError)
				{
					$tplConstruct[] = "web_feedback_after.html";
					$vars["message"] = htmlspecialchars($vars["message"]);
					$vars['message'] = nl2br($vars['message']);
					for (reset($vars); list($k, $v) = each($vars);)
					{
						$$k = $oFunc->mb_substr($v, 0, 4096);
					}
					$mail_sig = $sys['site_name'] . ' - ' . $sys['site_desc'];
					$msgSubj = 'Feedback';
					//
					$tpl_mail = new gwTemplate();
					$tpl_mail->addVal( 'LANG',           $L->languagelist("0") );
					$tpl_mail->addVal( 'TEXT_DIRECTION', $L->languagelist("1") );
					$tpl_mail->addVal( 'CHARSET',        $L->languagelist("2") );
					$tpl_mail->addVal( 'EMAIL_SIG',      $mail_sig );
					$tpl_mail->addVal( 'SUBJECT',        $msgSubj );
					$tpl_mail->addVal( 'DATE',           date("Y-M-d H:i:s"));
					$tpl_mail->addVal( 'NAME',           $name);
					$tpl_mail->addVal( 'E-MAIL',         strip_tags($email));
					$tpl_mail->addVal( 'MESSAGE',        $oFunc->hardWrap($message, 60, CRLF));
					$tpl_mail->addVal( 'URL',            $url);
					$tpl_mail->addVal( 'REMOTE_IP',      REMOTE_IP);
					$tpl_mail->addVal( 'AGENT',          $oFunc->hardWrap(getenv('HTTP_USER_AGENT'), 60, ' '));

					$tpl_mail->setHandle(0, $sys['path_admin'] . '/' . $sys['path_tpl'] . '/' . 'mail_feedback.html');
					$tpl_mail->parse();
					$strMailBody = $tpl_mail->output();
					//
					if (get_magic_quotes_gpc()){ $strMailBody = gw_stripslashes($strMailBody); }
					//
					// to e-mail, from e-mail, subject, text, isDebug
					//
					kMailTo($sys['y_email'], $email,
							htmlspecialchars($sys['site_name']) . ' feedback',
							$strMailBody, 0
					);
				}
		} // end of After post
		$tpl->addVal( "webform_adv", $strWebFormAdv ); // HTML-form
		$tplConstruct[] = "footer_inside.html";
	}
	break;
	case "top10":
	{
		/* Top 10 */
		$gw_this['arTitle'][] = $strCurrentSection = sprintf($L->m('web_m_top10'), $sys['max_dict_top']);
		$tpl->addVal( "AZ",             '');
		$tpl->addVal( "L_PAGES",        '&#160;' );
		$tpl->addVal( "PAGE_SELECTION", '');
		$tpl->addVal( "DICT_NAME",      $sys['site_name'] );
		$tpl->addVal( "DICT_NAME_LINK", $L->m('web_m_top10') );
		$tplConstruct = array("header.html", "search_form_empty.html");
		$tplConstruct[] = "web_top10.html";
		/* Average hits per dictionary */
		$tpl->addVal( 'fHitsAvg', getTop10('R_DICT_AVERAGEHITS', $sys['max_dict_top']));
		/* Newest dictionaries */
		$tpl->addVal( "fNewDictionary", getTop10('R_DICT_NEWEST', $sys['max_dict_top']));
		$tplConstruct[] = "menu_pages.html";
		$tplConstruct[] = "footer_inside.html";
	}
	break;
	case "catalog": // dictionaries catalog
	{
		$gw_this['arTitle'][] = $strCurrentSection = $L->m('stat_dict');
		$tpl->addVal( "AZ", '');
		$tpl->addVal( "L_PAGES", '&#160;' );
		$tpl->addVal( "PAGE_SELECTION", '');
		$tpl->addVal( "legend_updated", sprintf($L->m('tip015'), '<span class="red">'.$L->m('mrk_new').'</span>', $sys['time_new']));
		$tpl->addVal( "legend_new",     sprintf($L->m('tip014'), '<span class="green">'.$L->m('mrk_upd').'</span>', $sys['time_upd']));
		$tpl->addVal( "DICT_NAME", $sys['site_name'] );
		$tpl->addVal( "DICT_NAME_LINK", $strCurrentSection);
		$tplConstruct = array("header.html", "search_form_empty.html", "menu_letters_az.html", "web_top10.html");
		// "", links per category, X, Y
		$tpl->addVal( "fNewDictionary", getDictList("", 99, 2, 99, "tp.tpname, d.lang, d.title") );
		## ------------------------------------------------
		## Global search
		$tpl->addVal( "block:allsearch", htmlBlockSmall($L->m('srch_title'), getDictSrch(), 't', 0, 0, 'center') );
		## ------------------------------------------------
		## Last updated dictionaries
		$tpl->addVal( "block:dict_updated", htmlBlockSmall($L->m('r_dict_updated'), getTop10('R_DICT_UPDATED', $sys['max_dict_updated'], 1), 't', 0, 0) );
		//
		$tplConstruct[] = "menu_pages.html";
		$tplConstruct[] = "footer_inside.html";
	}
	break;
	default: // title page
	{
		$strCurrentSection = '';
		$tpl->addVal( "SITE_DESCR", $sys['site_desc']);
		$tpl->addVal( "legend_updated", sprintf($L->m('tip015'), '<span class="red">'.$L->m('mrk_new').'</span>', $sys['time_new']));
		$tpl->addVal( "legend_new",     sprintf($L->m('tip014'), '<span class="green">'.$L->m('mrk_upd').'</span>', $sys['time_upd']));
		## ------------------------------------------------
		## Catalog object
		// "", links per category, X, Y, order by
		$tpl->addVal( "block:catalog", getDictList('', 99, 2, 99 ) );
		## ------------------------------------------------
		## Statistics
		$arStatCommon = getStat();
		$tpl->addVal("block:stats", htmlBlockSmall($L->m('web_stat'),
					'<span class="f">' . (dateExtract($arStatCommon['date'], "%d")/1) . dateExtract($arStatCommon['date'], (" %FL %Y")) . "</span>"
					.'<br/>' . $L->m('stat_dict') . ': ' . $oFunc->number_format($arStatCommon['num'], 0, $L->languagelist('4'))
					.'<br/>' . $L->m('stat_defn') . ': ' . $oFunc->number_format($arStatCommon['sum'], 0, $L->languagelist('4')), 0, 't', 0, 0)
			);
		## ------------------------------------------------
		## Global search
		$tpl->addVal( "block:allsearch", htmlBlockSmall($L->m('srch_title'), getDictSrch(), 't', 0, 0, 'center') );
		## ------------------------------------------------
		## Last updated dictionaries
		$tpl->addVal( "block:dict_updated", htmlBlockSmall($L->m('r_dict_updated'), getTop10('R_DICT_UPDATED', $sys['max_dict_updated'] , 1), 't', 0, 0, $sys['css_align_left'] ));
		$tplConstruct = array("header.html", "indexpage.html", "footer.html");
	}
	break;
}

## ------------------------------------------------
## Get random term
/*
$gw_this['vars']['arTermRandom'] = getTermRandom();
$gw_this['vars']['arDictParam'] = getDictParam( $gw_this['vars']['arTermRandom']['id'] );
if (isset($gw_this['vars']['arTermRandom']['defn']))
{
	$tmp['cssTrClass'] = 't';
	$tmp['xref'] = $sys['page_index'] . '?a=term&amp;d=' . $gw_this['vars']['arTermRandom']['id'] . '&amp;q=';
	// Render html page, 25 apr 2003 
	$arPre = gw_Xml2Array($gw_this['vars']['arTermRandom']['defn']);
	$objDom = new gw_domxml;
	$objDom->setCustomArray($arPre);
	$oRender = new gw_render;
	$oRender->Set('Gtmp', $tmp );
	$oRender->Set('Gsys', $sys );
	$oRender->Set('margin_top', '-1em' );
	$oRender->Set('L', $L );
	$oRender->Set('objDom', $objDom );
	$oRender->Set('arDictParam', $gw_this['vars']['arDictParam'] );
	$oRender->Set('arEl', $arPre );
	$oRender->Set('arFields', $arFields );
	$oRender->load_abbr_trns();
	$tpl->addVal( 'block:randomterm', htmlBlockSmall('Randomly choosed term',
				'<span class="r">'.$gw_this['vars']['arTermRandom']['term'].'</span>'.$oRender->array_to_html($arPre), 't' ) );
	$gw_this['vars']['arTermRandom'] = $gw_this['vars']['arDictParam'] = array();
}
*/
## --------------------------------------------------------
## Site navigation
if (!isset($sys['split_nav'])){ $sys['split_nav'] = ' &#160; '; }
if (!isset($arNavBar))
{
	/* <a href="index.php?a=key">value</a> */
	$arNavText = array(
		'top10' => sprintf($L->m('web_m_top10'), $sys['max_dict_top']), 
		'fb' => $L->m('web_m_fb')
	);
	$arNavBar = $arNavIsActive = array();
	for (; list($k, $v) = each($arNavText);)
	{
		$icon = '';
		$file_icon = $sys['path_tpl'] . '/' . $gw_this['vars']['themename'] . '/icon_' . $k . '.gif';
		if (file_exists($file_icon))
		{
			$icon = '<img style="vertical-align:top;margin:1px" src="' . $sys['server_dir'] . '/' . $file_icon . '" ' .
				' width="13" height="13" alt="'.$L->m('web_m_'.$v).'" />&#160;';
		}
		$arNavBarTop[] = $icon . $oHtml->a($sys['page_index'] . '?a=' . $k, $v, ($layout !=  $v));
		$arNavBarBottom[] = $oHtml->a($sys['page_index'] . '?a=' . $k, $v, ($layout !=  $v));
	}
}
$tpl->addVal("NAV_TOP",     implode($sys['split_nav'], $arNavBarTop));
$tpl->addVal("NAV_BOTTOM",  implode(' | ', $arNavBarBottom));

$tpl->addVal( "block:current_section",  $strCurrentSection);
## Site navigation
## --------------------------------------------------------

## --------------------------------------------------------
## Keywords section
if (!is_array($k)){ $k = array(); }
if (!isset($kw) || !is_array($kw)){ $kw = array('0' => '0'); }
if ($w != '') // if letter selected
{
	$k[] = $w;
}
if ( !empty($listA['term']) ) // if term selected
{
	$listA['term'] = htmlspecialchars(strip_tags($listA['term']));
	// fix for special chars (a term &amp; a term)
	$listA['term'] = str_replace("&amp;", "&", $listA['term']);
	// meta keywords
	$k[] = $listA['term'];
}
if ( !empty($arDictParam['title']) ) // call $arDictParam safely
{
	$gw_this['arTitle'][] = $k[] = htmlspecialchars(strip_tags($arDictParam['title']));
}
if (empty($k))
{
	$gw_this['arTitle'][] = $sys['site_name'];
	$k[] = $sys['site_desc'];
	if ($strCurrentSection != '')
	{
		$k[] = $strCurrentSection;
	}
	$metaDescr = $sys['site_desc'];
}
else
{
	$metaDescr = $sys['site_name'] . ' - ' . $sys['site_desc'];
}
if (empty($metaDescr)) { $metaDescr = $strCurrentSection; }
if (isset($arDictParam['keywords']))
{
	if (!empty($arDictParam['keywords'])){ $kw[] = $arDictParam['keywords']; }
}
if (!empty($sys['keywords'])){ $kw[] = $sys['keywords']; }

$tpl->addVal( "TITLE",  implode(' - ', $gw_this['arTitle']) );
$tpl->addVal( "htmlKeywords", strip_tags(searchkeys(array_merge($k, $kw))) );
$tpl->addVal( "htmlDescription", trim(strip_tags($metaDescr)) );
## Keywords section
## --------------------------------------------------------

// Add previously defined template variables
for (reset($arTplVars['srch']); list($k, $v) = each($arTplVars['srch']);)
{
	while (is_array($v) && list($k2, $v2) = each($v))
	{
		$tpl->AddVal($k2, $v2);
	}
}
// Add previously defined template files
if (isset($tplConstruct) && is_array($tplConstruct))
{
	for (reset($tplConstruct); list($k, $v) = each($tplConstruct);)
	{
		$tpl->setHandle( $k , $sys['path_tpl'] . '/' . $sys['path_theme'] . '/' . $v );
	}
}

/* end of file */
?>