<?php
if (!defined('IN_GW'))
{
	die('<!-- $Id: func.admin.inc.php,v 1.9 2004/07/06 14:18:14 yrtimd Exp $ -->');
}
/**
 *  Glossword - glossary compiler (http://glossword.info/dev/)
 *  � 2002-2004 Dmitry N. Shilnikov <dev at glossword.info>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  (see `glossword/support/license.html' for details)
 */
// --------------------------------------------------------
/**
 *  Functions for administrative interface.
 */
// --------------------------------------------------------

function gw_after_redirect_url($action)
{
	global $arDictParam, $user, $id, $oDb;
	switch ($action)
	{
		case GW_AFTER_DICT_UPDATE:
			/* Redirect to "Editing dictionary settings" page */
			$str_url = GW_ACTION . '=' . GW_A_EDIT . '&' . GW_TARGET . '=' . GW_T_DICT .'&id=' . $id;
		break;
		case GW_AFTER_SRCH_BACK:
			/* Search again */
			if ($user->get('q'))
			{
				$str_url = GW_ACTION . '=' . SEARCH . '&q=' . $user->get('q') . '&d=' . $id;
			}
		break;
		case GW_AFTER_TERM_ADD:
			/* Redirect to "Add a term" */
			$str_url = GW_ACTION . '=' . GW_A_ADD . '&' . GW_TARGET . '=' . GW_T_TERM . '&id=' . $id;
		break;
		case GW_AFTER_TERM_IMPORT:
			/* Import terms page */
			$str_url = GW_ACTION . '=' . IMPORT. '&' . GW_TARGET . '=' . GW_T_DICT .'&id=' . $id;
		break;
	}
	return $str_url;
}

/* Update dictionary settings */
function gw_sys_dict_update()
{
	global $arDictParam, $oDb;
	$qDict['int_terms'] = gw_sys_dict_count_terms();
	$qDict['int_bytes'] = gw_sys_dict_count_kb();
	$sql = gw_sql_update($qDict, TBL_DICT, "id = '".$arDictParam['id']."'");
	$oDb->sqlExec($sql);
}
/* Count number of terms */
function gw_sys_dict_count_terms()
{
	global $arDictParam, $oDb;
	$sql = 'SELECT count(*) AS n FROM ' . $arDictParam['tablename'];
	$arSql = $oDb->sqlExec($sql);
	return isset($arSql[0]['n']) ? $arSql[0]['n'] : 0;
}
/* Count bytes */
function gw_sys_dict_count_kb()
{
	global $arDictParam;
	$arSize = getTableInfo($arDictParam['tablename']);
	return ($arSize['Data_length'] + $arSize['Index_length']);
}
/* Check & Optimize table */
function gw_sys_dict_check()
{
	global $arDictParam, $oDb;
	$sql = 'CHECK TABLE ' . $arDictParam['tablename'];
	$arSql = $oDb->sqlExec($sql);
	$sql = 'OPTIMIZE TABLE ' . $arDictParam['tablename'];
	$arSql = $oDb->sqlExec($sql);
}


/**
 * Builds an automatically generated navigation toolbar,
 * depends on currect action.
 *
 * @param   array   $at navigation map
 * @param   string  $t currect action
 * @param   string  $a currect target
 * @return  string  complete HTML-code
 * @globals  object  $L Translation kit phrases
 */
function admGetNavbar($ar, $a, $t)
{
	global $L, $tid, $id, $auth, $arDictParam, $sys;
	$isShowA = 0;
	$is_allow['topic'] = $auth->is_allow('topic');
	$is_allow['dict'] = $auth->is_allow('dict', $id);
	$is_allow['term'] = $auth->is_allow('term', $id);
	$is_allow['sys'] = $auth->is_allow('sys');
	$is_allow['user'] = $auth->is_allow('user');
	$is_allow['user_brws'] = $auth->have_perm('admin', PERMLEVEL);
	$is_allow['dict_edit'] = ( ( $auth->have_perm('author', PERMLEVEL) || (isset($arDictParam['id_user']) && ($arDictParam['id_user'] == $auth->auth['id']) && $is_allow['dict'])) ? 1 : 0 );
	$is_allow['export']    = ( ( $auth->have_perm('admin', PERMLEVEL) || (isset($arDictParam['id_user']) && $auth->is_allow('export'))) ? 1 : 0 );
	$is_allow['dict_add']  = ( ( $auth->have_perm('author', PERMLEVEL) && $is_allow['dict']) ? 1 : 0 );
	$is_allow['user_reg']  = ( $auth->have_perm('admin', PERMLEVEL) && $is_allow['user'] ? 1 : 0 );

	// map
	$arMap[REMOVE][GW_T_TERM] =
	$arMap[GW_A_EDIT][GW_T_TERM] = array(
		BRWS =>   array(USER=>$is_allow['user_brws'], TPCS=>1, GW_T_DICT =>1, TPL=>1),
		GW_A_ADD =>    array(TPCS=> $is_allow['topic'], GW_T_DICT =>$is_allow['dict_add'], GW_T_TERM=>$is_allow['term'], TPL=>1),
		GW_A_EDIT =>   array(GW_T_DICT => $is_allow['dict']),
		REMOVE => array(GW_T_DICT =>$is_allow['dict_edit'], GW_T_TERM=>$is_allow['term']),
		CLEAN =>  array(GW_T_DICT =>$is_allow['dict']),
		IMPORT => array(GW_T_DICT =>$is_allow['dict']),
		EXPORT => array(GW_T_DICT =>$is_allow['export']),
		CFG =>    array(SYS=>$is_allow['sys']),
		STATS =>  array(SYS=>$is_allow['sys']),
		REG =>    array(USER=>$is_allow['user_reg']),
		PROFILE =>array(USER=>$is_allow['user'])
	);
		$arMap[SEARCH][GW_T_TERM] =
		$arMap[SEARCH][GW_T_DICT] =
		$arMap[SEARCH][''] =
		$arMap[GW_A_ADD][GW_T_TERM] =
		$arMap[UPDATE][GW_T_TERM] =
		$arMap[REMOVE][GW_T_DICT] =
		$arMap[CLEAN][GW_T_DICT] =
		$arMap[UPDATE][GW_T_DICT] =
		$arMap[IMPORT][GW_T_DICT] =
		$arMap[EXPORT][GW_T_DICT] =
	$arMap[GW_A_EDIT][GW_T_DICT] = array(
		BRWS =>   array(USER=>$is_allow['user_brws'], TPCS=>1, GW_T_DICT =>1, TPL=>1),
		GW_A_ADD =>    array(TPCS=>$is_allow['topic'], GW_T_DICT =>$is_allow['dict_add'], GW_T_TERM=>$is_allow['term'], TPL=>1),
		GW_A_EDIT =>   array(GW_T_DICT =>$is_allow['dict_edit']),
		REMOVE => array(GW_T_DICT =>$is_allow['dict_edit']),
		CLEAN =>  array(GW_T_DICT =>$is_allow['dict_edit']),
		IMPORT => array(GW_T_DICT =>$is_allow['dict']),
		EXPORT => array(GW_T_DICT =>$is_allow['export']),
		CFG =>    array(SYS=>$is_allow['sys']),
		STATS =>  array(SYS=>$is_allow['sys']),
		REG =>    array(USER=>$is_allow['user_reg']),
		PROFILE =>array(USER=>$is_allow['user'])
	);
	$arMap[GW_A_EDIT][TPCS] = array(
		BRWS =>   array(USER=>$is_allow['user_brws'], TPCS=>1, GW_T_DICT =>1, TPL=>1),
		GW_A_ADD =>    array(TPCS=>$is_allow['topic'], GW_T_DICT =>$is_allow['dict_add'], TPL=>1),
		REMOVE => array(TPCS=>$is_allow['dict_edit']),
		CFG =>    array(SYS=>$is_allow['sys']),
		STATS =>  array(SYS=>$is_allow['sys']),
		REG =>    array(USER=>$is_allow['user_reg']),
		PROFILE =>array(USER=>$is_allow['user'])
	);
	$arMap[''][''] =
		$arMap[NEWPASS][USER] =
		$arMap[REG][USER] =
		$arMap[PROFILE][USER] =
		$arMap[CFG][SYS] =
		$arMap[CFG][UPDATE] =
		$arMap[GW_A_ADD][GW_T_DICT] =
		$arMap[GW_A_ADD][TPCS] =
		$arMap[GW_A_ADD][TPL] =
		$arMap[UPDATE][TPCS] =
		$arMap[UPDATE][TPL] =
		$arMap[UPDATE][USER] =
		$arMap[REMOVE][TPCS] =
		$arMap[REMOVE][TPL] =
		$arMap[BRWS][USER] =
		$arMap[BRWS][GW_T_DICT] =
		$arMap[BRWS][TPCS] =
		$arMap[BRWS][TPL] = array(
			BRWS =>   array(USER=>$is_allow['user_brws'], TPCS=>1, GW_T_DICT =>1, TPL=>1),
			GW_A_ADD =>    array(TPCS=>$is_allow['topic'], GW_T_DICT =>$is_allow['dict_add'], TPL=>1),
			CFG =>    array(SYS=>$is_allow['sys']),
			STATS =>  array(SYS=>$is_allow['sys']),
			REG =>    array(USER=>$is_allow['user_reg']),
			PROFILE =>array(USER=>$is_allow['user'])
	);

	// html
	$str = '<table cellspacing="0" cellpadding="2" border="0" width="98%">';
	$str .= '<col width="14%"/><col width="1%"/><col width="85%"/>';
	for(; list($k1, $arV) = each($ar);)
	{
		$isShowT = 1;
		// Check permission level, access to system configuration, user registration

		if (($k1 == 'sys') && !$is_allow['sys']) { $isShowT = 0; }
		if (($k1 == 'user') && !$is_allow['user']) { $isShowT = 0; }

		if ($isShowT)
		{
			$str .= '<tr class="m" valign="top">';
			$str .= '<td style="text-align:' . $sys['css_align_right'] . '">' . $L->m($k1) . '</td>';
			$str .= '<td class="f">:</td>';
			$str .= '<td class="f" style="text-align:' . $sys['css_align_left'] . '">';
			$arStr = array();

			for (; list($k2, $v2) = each($arV);)
			{
				if (isset($arMap[$a][$t][$v2][$k1]) &&  $arMap[$a][$t][$v2][$k1] == 1)
				{
					$tidS = $idS = $markS = '';
					//
					// It is the most beautiful IF's I have ever seen...
					//
					if($v2 == REMOVE) { $markS = '&#215;&#160;'; $tidS = '&tid='. $tid; $idS = '&id='. $id; }
					if(($v2 == GW_A_EDIT)||($v2 == CLEAN)||($v2 == IMPORT)||($v2 == EXPORT)) { $idS = '&id='. $id; }
					if($v2 == GW_A_ADD) { $markS = '+&#160;'; if($k1 == GW_T_TERM) { $idS = '&id='. $id; } }
					if($v2 == CLEAN) { $markS = '&#215;&#160;'; }
					if($v2 == BRWS) { $markS = '&#8230;&#160;'; }
					if($v2 == GW_A_EDIT) { $markS = '&#177;&#160;'; }
					if(($v2 == IMPORT)||($v2 == EXPORT)){ $markS = '&#8226;&#160;'; }
					if($id == '') { $idS = ''; }
					if($tid == '') { $tidS = ''; }
					$arStr[] = $markS . htmlTagsA( $GLOBALS['sys']['page_admin'] . '?' .
								GW_TARGET . "=" . $k1 . "&" . GW_ACTION . "=" . $v2 . $tidS . $idS,
								$L->m("3_" . $v2));
				}
			}
			if (sizeof($arStr) > 0)
			{
				$str .= '['.implode("] [", $arStr).']';
#                $str .= implode(" | ", $arStr);
			}
			$str .= '</td>';
			$str .= '</tr>';
		} // $isShowT
	}
	$str .= '</table>';
	return $str;
}




/**
 * Post query to database
 *
 * @param    array  $arQuery   all database queries
 * @param    string $url       redirect to path if success
 * @param    bool   $debug     if true, display query and errors
 * @return   string html-code for redirect or an error
 * @access   public
 */
function postQuery($arQuery, $url = "", $isDebug = 0, $isPause = 1, $lock = '')
{
	global $oDb, $oSqlQ, $sys;
	
	$isPostError = true;
	$str_status = isset($GLOBALS['L']) ? $GLOBALS['L']->m('2_success') : 'ok';
	$str_continue = isset($GLOBALS['L']) ? $GLOBALS['L']->m('2_continue') : 'Continue';
	/**
	 * Outputs all database queries in readable format
	 * @param    array   $arQuery
	 * @return   string  debug information
	 * @access   private
	 * @see  htmlspecialchars2()
	 */
	function _gw_showhtml($arQuery)
	{
		array_walk($arQuery, "htmlspecialchars_ltgt");
		return '<span style="font:9pt \'courier new\'"><ul><li>' . implode(';</li><li>', $arQuery). ';</li></ul></span>';
	}
	$url_to = $sys['page_admin'] . '?' . $url;

	if ($isDebug)
	{
		return _gw_showhtml($arQuery).
			   '<p>' . htmlTagsA($url_to, $str_continue) . '</p>';
	}
	## ----------------------------------------------------
	## Insert into database
	if ($lock != '')
	{
		sqlLock($lock);
	}
	$cntQ = sizeof($arQuery);
	for ($i=0; $i < $cntQ; $i++)
	{
		if ($oDb->sqlExec($arQuery[$i])){ $isPostError = false; }
		if ($isPostError)
		{
			$isPostError = preg_match("/^SELECT/", $arQuery[$i]) ? true : false;
		}
	}
	if ($lock != '')
	{
		sqlUnlock();
	}
	##
	## ----------------------------------------------------
	// 12 jan 2003, No data
	if ($cntQ == 0)
	{
		$isPostError = 0;
	}
	// Return status messages or redirect ofter post
	if ($isPostError)
	{
		return '<span class="t" class="red">ERROR:</span>' . $cntQ . htmlspecialchars3($arQuery);
	}
	/* Try to update dictionary settings */
	global $arDictParam, $arPost;
	if (isset($arDictParam) && isset($arPost['after']) && ($arPost['after'] == GW_AFTER_DICT_UPDATE))
	{
		gw_sys_dict_update();
	}
	if ($isPause)
	{
		global $strR;
		$strR .= '<p>' .htmlTagsA($url_to, $str_continue). '</p>';
		return;
	}
	else
	{
		page_close();
		gwtk_header(append_url($url_to), $sys['is_delay_redirect']);
	}
	return true;
}



/**
 * Table template, used for all headers in HTML-form
 *
 * @param    string  $title      text for header, 1st column
 * @param    string  $funcnav    text for header, 2nd column
 * @return   string  string      HTML-code for table header
 * @see getFormDefn()
 */
function getFormTitleNav($title = "title", $funcnav = "")
{
	global $sys;
	$str = "";
	$str .= '<table cellspacing="0" cellpadding="2" border="0" width="100%">';
	$str .= '<tr style="background:'.$GLOBALS['theme']['color_4'].'">';
	if ($funcnav != '')
	{
		$str .= '<td style="width:50%;font-weight:bold;color:#000" class="m">&#160;' . $title . '</td>';
		$str .= '<td style="width:50%;text-align:'.$sys['css_align_right'].';height:25px">' . $funcnav . '</td>';
	}
	else
	{
		$str .= '<td style="height:26px;font-weight:bold;color:#000" class="m">&#160;' . $title . '</td>';
	}
	$str .= '</tr>';
	$str .= '</table>';
	return $str;
}

/**
 * Join posted variables with content structure
 *
 * @param    array   $arParsed   fields content structure
 * @param    array   $arPre      additional actions
 * @return   array   new $arParsed;
 * @see ParseFieldDbToInput()
 */
function gw_ParsePre($arParsed, $arPre)
{
	global $arControl, $arDictParam;
	if (!is_array($arParsed) || !is_array($arPre))
	{
		return $arParsed;
	}
	// go for $arPre
	//
	// update some arrays and tags...
	//
	//
	if (isset($arPre['trsp'][0][0]['value']))
	{
		$tmp['arTrsp'] = explode(CRLF, trim($arPre['trsp'][0]['value']));
		while(is_array($tmp['arTrsp']) && list($k, $v) = each($tmp['arTrsp']))
		{
			$arPre['trsp'][0][$k]['value'] = $v;
		}
	}
	if (isset($arPre['see'][0][0]['value']))
	{
#        $tmp['arSyn'] = explode(CRLF, trim($arPre['syn'][0]['value']));
#        while(is_array($tmp['arSyn']) && list($k, $v) = each($tmp['arSyn']))
#        {
#            $tmp['synText'] = preg_replace("'(.*)\[\[(.*?)\]\]'", ' \\2', $v);
#            $v = preg_replace("'\[\[(.*?)\]\]'", '', $v );
#            $tmp['synText'] = str_replace($v, '', $tmp['synText']);
#            $arPre['syn'][$k]['value'] = $v;
#            $arPre['syn'][$k]['attributes']['text'] = $tmp['synText'];
#        }
#        prn_r($arParsed['syn'], __LINE__.__FILE__);
	}
	#prn_r($arPre['usg']);
	//
	//
	for (reset($arPre); list($target_name, $arTarget) = each($arPre);) // for each target [ abbr | trns | defn | syn | .. ]
	{
		// replace structures
		$arParsed[$target_name] = $arPre[$target_name];
	}
	for (reset($arPre); list($target_name, $arTarget) = each($arPre);) // for each target [ abbr | trns | defn | syn | .. ]
	{
		// is there any direct instructions for this tag?
		if (isset($arControl[$target_name])) // defn | abbr | trns
		{

			// Get ID from current tag followed by direct instructions
			foreach ($arControl[$target_name] as $action => $arId)
			{
				$tmp['action'] = $action;
				foreach ($arId as $elK => $arCh)
				{
					foreach ($arCh as $chK => $ChV)
					{
						$tmp['chK'] = $chK;
						$tmp['elK'] = $elK;
					}
				}
			}
			// Now script knows what are `chK' and `ehK' for current tag

			// How much keys (definitions) in current tag
			$tmp['intCurChilds'] = (sizeof($arParsed[$target_name][$tmp['elK']]) - 1); // -1 because array

			if ($tmp['action'] == GW_A_ADD)
			{
				// add empty values
				if (!isset($arParsed['syn']) && $arDictParam['is_syn'] ) { $arParsed['syn'] = array(); }
				if (!isset($arParsed['see']) && $arDictParam['is_see']){ $arParsed['see'] = array(); }
				if (!isset($arParsed['usg']) && $arDictParam['is_usg']){ $arParsed['usg'] = array(); }
				if (!isset($arParsed['src']) && $arDictParam['is_src']){ $arParsed['src'] = array(); }
				if (!isset($arParsed['phone']) && $arDictParam['is_phone']){ $arParsed['phone'] = array(); }
				if (!isset($arParsed['address']) && $arDictParam['is_address']){ $arParsed['address'] = array(); }
				//
				if ( ($target_name == 'abbr') || ($target_name == 'trns') )
				{
					// do not add empty attributes
					if ( ($arParsed[$target_name][$tmp['elK']][$tmp['intCurChilds']]['value'] != '') ||
						 ($arParsed[$target_name][$tmp['elK']][$tmp['intCurChilds']]['attributes']['lang'] != '--')
					   )
					{
						$arParsed[$target_name][$tmp['elK']][($tmp['intCurChilds']+1)]['value'] = '';
						$arParsed[$target_name][$tmp['elK']][($tmp['intCurChilds']+1)]['attributes']['lang'] = '--';
					}
				}
				elseif ($target_name == 'defn')
				{
					//
					gw_array_insert($arParsed[$target_name], $tmp['elK'],
							array('value' => '')
					);
					//
					gw_array_insert($arParsed['abbr'], $tmp['elK'],
							array(0 => array('value' => '', 'attributes' => array('lang' => '--')))
					);
					gw_array_insert($arParsed['trns'], $tmp['elK'],
							array(0 => array('value' => '', 'attributes' => array('lang' => '--')))
					);
					//
					gw_array_insert($arParsed['usg'], $tmp['elK'], array('value' => '') );
					//
					gw_array_insert($arParsed['address'], $tmp['elK'], array(0 => array('value' => '')) );
					gw_array_insert($arParsed['phone'], $tmp['elK'], array(0 => array('value' => '')) );
					gw_array_insert($arParsed['src'], $tmp['elK'], array(0 => array('value' => '')) );
					gw_array_insert($arParsed['see'], $tmp['elK'], array(0 => array('value' => '')) );
					gw_array_insert($arParsed['syn'], $tmp['elK'], array(0 => array('value' => '')) );

					#prn_r($arParsed['usg'], strval($tmp['elK']));

				 }
			}
			elseif ($tmp['action'] == REMOVE)
			{
				// `Remove' pressed
				//
				if ( ($target_name == 'abbr') || ($target_name == 'trns') )
				{
					// do not remove empty attributes
					if ( ($arParsed[$target_name][$tmp['elK']][$tmp['chK']]['value'] != '') ||
						 ($arParsed[$target_name][$tmp['elK']][$tmp['chK']]['attributes']['lang'] != '--')
					   )
					{
						unset($arParsed[$target_name][$tmp['elK']][$tmp['chK']]);
					}
					unset($arParsed[$target_name][$tmp['elK']][$tmp['chK']]);
				}
				else
				{
					// Remove current key from definition and all related to key tags
					//
					for (reset($arParsed); list($targetK, $targetV) = each($arParsed);)
					{
						if (isset($targetV[$tmp['elK']])) // unset only existed keys
						{
							unset( $arParsed[$targetK][$tmp['elK']] );
						}
					}
				} // end of target


			} // end of action
#            prn_r($arPre);
#            prn_r($arParsed);
		} // target_name
		//
		#prn_r($arParsed['abbr'], $target_name);
	} // end of root elements, target
	return $arParsed;
}

/**
 * Clears all cached files for selected dictionary
 *
 * @return   str report with information of deleted (or not) files
 */
function gw_tmp_clear($prefix = 'st')
{
	$str = $d1 = $d2 = '';
	$strDir = $GLOBALS['sys']['path_cache_sql'];
	$str .= '<span class="t">Cache...';
	$prefix = sprintf("%05d", $prefix);
	if (is_dir($strDir))
	{
		$dir = opendir($strDir);
		while (($f = readdir($dir)) !== false)
		{
			if ($f != '.' && $f != '..' && is_file($strDir.'/'.$f) && (preg_match("/^".$prefix."_/", $f)))
			{
				$d1 .= "<li>".$GLOBALS['sys']['path_cache_sql'].'/'.$f;
				unlink($GLOBALS['sys']['path_cache_sql'].'/'.$f);
			}
		}
	}
	$prefix = 'st';
	if (is_dir($strDir))
	{
		$dir = opendir($strDir);
		while (($f = readdir($dir)) !== false)
		{
			if ($f != '.' && $f != '..' && is_file($strDir.'/'.$f) && (preg_match("/^".$prefix."_/", $f)))
			{
				$d1 .= "<li>".$GLOBALS['sys']['path_cache_sql'].'/'.$f;
				unlink($GLOBALS['sys']['path_cache_sql'].'/'.$f);
			}
		}
	}
	$str .= ($d1) ? ('<ul class="red">' . $d1 . '</ul>') : false;
	$str .= ($d2) ? ('<ul class="red">' . $d2 . '</ul>') : false;
	$str .= ' finished.</span>';
	// No cache found
	if (($d1 == $d2) && ($d1 == ''))
	{
		$str = '';
	}
	return $str;
}

/* end of file */
?>