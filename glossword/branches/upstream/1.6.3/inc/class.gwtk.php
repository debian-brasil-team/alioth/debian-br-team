<?php
/**
 * Translation Kit class.
 * © 2003-2004 Dmitry N. Shilnikov <dev at glossword.info>
 * http://glossword.info/dev/
 */
/**
 * Core Translation Kit functions.
 *
 * Usage:
 *      $oL = new gwtk;
 *      $oL->setHomeDir("locale");  // path to ./your_project/locale
 *      $oL->setLocale("ru-utf8");  // path to ./your_project/locale/ru-utf8/
 *      print $oL->m("variable_name");
 *      // set another language
 *      $L->setLocale("en-utf8");
 *      print $oL->m(1);
 *
 *      print_r( $oL->getLanguages() );
 *
 */
/* ------------------------------------------------------ */
if (!defined('IN_GW'))
{
	die('<!-- $Id: class.gwtk.php,v 1.10 2004/07/09 13:05:17 yrtimd Exp $ -->');
}
/* ------------------------------------------------------ */
if (!defined('IS_CLASS_GWTK'))
{
	define('IS_CLASS_GWTK', 1);

	$tmp['mtime'] = explode(' ', microtime());
	$tmp['start_time'] = (float)$tmp['mtime'][1] + (float)$tmp['mtime'][0];

class gwtk
{
	var $pathHomeDir = 'locale';
	var $pathLocale  = 'en';
	var $pathFile    = 'global';
	var $pathEx      = 'php';
	var $pathRoot    = '.';
	var $varMode     = 'names'; /* [ names | constants ], `names' is default */
	var $varName     = 'lang'; /* for "names" mode only */
	var $url_prefix  = 'il'; /* url parameter `&lang=' */
	var $arMessages  = array();
	/**
	 * Sets directory name for ./your_project/$path
	 */
	function setHomeDir($path)
	{
		$this->pathHomeDir = $path;
	}
	function getLocale()
	{
		return $this->pathLocale;
	}
	function getHomeDir()
	{
		return $this->pathHomeDir;
	}
	/**
	 * Sets new url parameter instead of `&lang='
	 */
	function setPrefix($str)
	{
		$this->url_prefix = $str;
	}
	/**
	 * Appends '&lang=' to any url
	 */
	function url($url, $locale_id)
	{
		if ($locale_id != '')
		{
			$url = preg_replace("/[&?]" . $this->url_prefix . "=([0-9a-z]+)/", '', $url);
			$url = preg_replace("/[&?]+$/", '', $url);
			$url .= ((strpos($url, "?") != false) ?  "&" : "?") . $this->url_prefix . '=' . $locale_id;
		}
		return $url;
	}
	/**
	 * Sets directory name for ./your_project/locale/$localename
	 *
	 * @param	string	$localename Directory path name to phrases
	 * @see _getFileName();
	 */
	function setLocale($localename)
	{
		$this->lang = array();
		$this->pathLocale = $localename;
		$f = $this->_getFileName();
		/* load language configuration */
		if (file_exists($f))
		{
			global $tmp;
			include($f);
			if ($this->varMode == "names")
			{
				$this->lang = ${$this->varName};
			}
		}
	}
	/**
	 * Creates new array from custom file
	 * ./your_project/locale/$localename/$f_name
	 *
	 * @param	string	$f_name File name with phrases
	 * @param	string	$localename Directory path name to phrases
	 * @access	private
	 * @return	array	Phrases
	 * @see _getFileName();
	 */
	function _loadCustom($f_name, $localename)
	{
		/* custom language file */
		$f = $this->_getFileName();
		$f_custom = str_replace($this->pathFile, $f_name, $f);
		$f_custom = str_replace($this->pathLocale , $localename, $f_custom);
		$f_name = $this->varName.'_'.$f_name;
		if (file_exists($f_custom))
		{
			include($f_custom);
			if (isset(${$this->varName}))
			{
				$this->$f_name = ${$this->varName};
			}
#            asort($this->$f_name);
		}
	}
	/**
	 * Gets additional phrases in two ways:
	 * "join"   : merge custom data with existent phrases
	 * "return" : do not merge, just return new array
	 */
	function getCustom($f_name, $localename, $mode = 'join')
	{
		$this->_loadCustom($f_name, $localename);
		$f_name = $this->varName.'_'.$f_name;
		if ((isset($this->$f_name)) && ($mode == 'join'))
		{
			/* array_merge_clobber */
			$arNew = $this->lang;
			while (list($key, $val) = each($this->$f_name))
			{
				$arNew[$key] = $val;
			}
			$this->lang = $arNew;
			unset($this->$f_name);
		}
		elseif ($mode == 'return')
		{
			return isset($this->$f_name) ? $this->$f_name : array();
		}
	}
	/**
	 * Shows untranslated phrases
	 */
	function getMsgNames()
	{
		$this->arMessages = array_flip(array_values(array_unique($this->arMessages)));
		while (list($k, $v) = each($this->arMessages)) /* all phrases used on page */
		{
			if (isset($this->lang[$k])) /* global phrase list */
			{
				unset($this->arMessages[$k]); /* remove from used */
			}
		}
		return array_flip($this->arMessages);
	}
	/**
	 * Constructs filename to translation file
	 * @access	private
	 */
	function _getFileName()
	{
		$f = $this->pathRoot . '/' . $this->pathHomeDir . '/' . $this->pathLocale . '/' . $this->pathFile . "." . $this->pathEx;
		return $f;
	}
	/**
	 * Main translation function.
	 * Last updated: 30 march 2003
	 */
	function m($t, $key = '')
	{
		$this->arMessages[] = $t;
		if ($this->varMode == 'names')
		{
			return (isset($this->lang[$t]) && is_array($this->lang[$t]) && isset($this->lang[$t][$key]))
					? $this->lang[$t][$key]
					: (isset($this->lang[$t]) ? $this->lang[$t] : $t);
		}
		else
		{
			return defined($t) ? eval("print $t;") : $t;
		}
	}
	/* */
	function getLanguages()
	{
		$dirname = $this->pathRoot . '/' . $this->pathHomeDir;
		$dir = opendir($dirname);
		$arLang = array();
		$arSysLang = $this->languagelist();
		while (($file = readdir($dir)) !== false)
		{
			if (isset($arSysLang[$file])
				&& !is_file($dirname . '/' . $file)
				&& !is_link($dirname . '/' . $file)
				&& file_exists($dirname . '/' . $file . '/' . $this->pathFile . "." . $this->pathEx )
			   )
			{
				$arReturn[$file] = $arSysLang[$file];
			}
		}
		closedir($dir);
		@asort($arReturn);
		@reset($arReturn);
		return $arReturn;
	}
	/* */
	function languagelist($param = '')
	{
		/*
			'Locale directory name' => array(
				"ISO 639-2 or ISO 639-3 code",
				"Text direction",
				"ISO charset",
				"Display name",
				another settings
			);
		*/
		$a = array(
			'af-utf8'   => array('af',   'ltr', 'UTF-8', 'Afrikaans',                  array('thousands_separator'=>',', 'decimal_separator'=>'.', 'part_separator'=>' ')),
			'sq-utf8'   => array('sq',   'ltr', 'UTF-8', 'Albanian',                   array('thousands_separator'=>',', 'decimal_separator'=>'.', 'part_separator'=>' ')),
			'ar-utf8'   => array('ar',   'rtl', 'UTF-8', 'Arabic - عربي',              array('thousands_separator'=>',', 'decimal_separator'=>'.', 'part_separator'=>' ')),
			'eu-utf8'   => array('eu',   'ltr', 'UTF-8', 'Basque - Euskara',           array('thousands_separator'=>',', 'decimal_separator'=>'.', 'part_separator'=>' ')),
			'bg-utf8'   => array('bg',   'ltr', 'UTF-8', 'Bulgarian - Българска',      array('thousands_separator'=>',', 'decimal_separator'=>'.', 'part_separator'=>' ')),
			'by-utf8'   => array('by',   'ltr', 'UTF-8', 'Byelorussian - Беларуски',   array('thousands_separator'=>' ', 'decimal_separator'=>',', 'part_separator'=>'. ')),
			'km-utf8'   => array('km',   'ltr', 'UTF-8', 'Cambodian (Khmer)',          array('thousands_separator'=>',', 'decimal_separator'=>'.', 'part_separator'=>' ')),
			'ca-utf8'   => array('ca',   'ltr', 'UTF-8', 'Catalan - Català',           array('thousands_separator'=>',', 'decimal_separator'=>'.', 'part_separator'=>' ')),
			'chr-utf8'  => array('chr',  'ltr', 'UTF-8', 'Cherokee - ᏣᎳᎩ Tsalagi',     array('thousands_separator'=>',', 'decimal_separator'=>'.', 'part_separator'=>' ')),
			'hr-utf8'   => array('hr',   'ltr', 'UTF-8', 'Croatian - Hrvatska',        array('thousands_separator'=>' ', 'decimal_separator'=>'.', 'part_separator'=>' ')),
			'cs-utf8'   => array('cs',   'ltr', 'UTF-8', 'Czech - Česká',              array('thousands_separator'=>' ', 'decimal_separator'=>'.', 'part_separator'=>' ')),
			'da-utf8'   => array('da',   'ltr', 'UTF-8', 'Danish - Dansk',             array('thousands_separator'=>',', 'decimal_separator'=>'.', 'part_separator'=>' ')),
			'du-utf8'   => array('du',   'ltr', 'UTF-8', 'Dutch - Nederlands',         array('thousands_separator'=>',', 'decimal_separator'=>'.', 'part_separator'=>' ')),
			'de-utf8'   => array('de',   'ltr', 'UTF-8', 'German - Deutsch',           array('thousands_separator'=>',', 'decimal_separator'=>'.', 'part_separator'=>' ')),
			'el-utf8'   => array('el',   'ltr', 'UTF-8', 'Greek - Ελληνικά',           array('thousands_separator'=>',', 'decimal_separator'=>'.', 'part_separator'=>' ')),
			'en-utf8'   => array('en',   'ltr', 'UTF-8', 'English US',                 array('thousands_separator'=>',', 'decimal_separator'=>'.', 'part_separator'=>' ')),
			'es-utf8'   => array('es',   'ltr', 'UTF-8', 'Spanish - Español',          array('thousands_separator'=>',', 'decimal_separator'=>'.', 'part_separator'=>' ')),
			'et-utf8'   => array('et',   'ltr', 'UTF-8', 'Estonian',                   array('thousands_separator'=>',', 'decimal_separator'=>'.', 'part_separator'=>' ')),
			'fi-utf8'   => array('fi',   'ltr', 'UTF-8', 'Finnish - Suomi',            array('thousands_separator'=>',', 'decimal_separator'=>'.', 'part_separator'=>' ')),
			'fr-utf8'   => array('fr',   'ltr', 'UTF-8', 'French - Français',          array('thousands_separator'=>' ', 'decimal_separator'=>',', 'part_separator'=>'. ')),
			'he-utf8'   => array('he',   'rtl', 'UTF-8', 'Hebrew - עברית',              array('thousands_separator'=>' ', 'decimal_separator'=>',', 'part_separator'=>' ')),
			'he-iso'    => array('he',   'rtl', 'ISO-8859-8-I', 'Hebrew - עברית (ISO-Logical)', array('thousands_separator'=>' ', 'decimal_separator'=>',', 'part_separator'=>' ')),
			'hi-utf8'   => array('hi',   'ltr', 'UTF-8', 'Hindi - हिन्दी',                array('thousands_separator'=>',', 'decimal_separator'=>'.', 'part_separator'=>' ')),
			'hu-utf8'   => array('hu',   'ltr', 'UTF-8', 'Hungarian - Magyar',           array('thousands_separator'=>',', 'decimal_separator'=>'.', 'part_separator'=>' ')),
			'ia-utf8'   => array('ia',   'ltr', 'UTF-8', 'Interlingua',                  array('thousands_separator'=>',', 'decimal_separator'=>'.', 'part_separator'=>' ')),
			'id-utf8'   => array('id',   'ltr', 'UTF-8', 'Indonesian',                   array('thousands_separator'=>',', 'decimal_separator'=>'.', 'part_separator'=>' ')),
			'it-utf8'   => array('it',   'ltr', 'UTF-8', 'Italian - Italiano',           array('thousands_separator'=>',', 'decimal_separator'=>'.', 'part_separator'=>' ')),
			'ja-utf8'   => array('ja',   'ltr', 'UTF-8', 'Japanese',                     array('thousands_separator'=>',', 'decimal_separator'=>'.', 'part_separator'=>' ')),
			'ka-utf8'   => array('ka',   'ltr', 'UTF-8', 'Georgian',                     array('thousands_separator'=>',', 'decimal_separator'=>'.', 'part_separator'=>' ')),
			'ko-utf8'   => array('ko',   'ltr', 'UTF-8', 'Korean - 한국어',              array('thousands_separator'=>',', 'decimal_separator'=>'.', 'part_separator'=>' ')),
			'lt-utf8'   => array('lt',   'ltr', 'UTF-8', 'Lithuanian - Lietuviø',        array('thousands_separator'=>',', 'decimal_separator'=>'.', 'part_separator'=>' ')),
			'lv-utf8'   => array('lv',   'ltr', 'UTF-8', 'Latvian',                      array('thousands_separator'=>',', 'decimal_separator'=>'.', 'part_separator'=>'. ')),
			'mk-utf8'   => array('mk',   'ltr', 'UTF-8', 'Macedonian',                   array('thousands_separator'=>'.', 'decimal_separator'=>',', 'part_separator'=>'. ')),
			'ms-utf8'   => array('ms',   'ltr', 'UTF-8', 'Malay - Melayu',               array('thousands_separator'=>',', 'decimal_separator'=>'.', 'part_separator'=>' ')),
			'ml-utf8'   => array('ml',   'ltr', 'UTF-8', 'Malayalam',                    array('thousands_separator'=>',', 'decimal_separator'=>'.', 'part_separator'=>' ')),
			'mn-utf8'   => array('mn',   'ltr', 'UTF-8', 'Mongolian - Монгол',           array('thousands_separator'=>',', 'decimal_separator'=>'.', 'part_separator'=>' ')),
			'no-utf8'   => array('no',   'ltr', 'UTF-8', 'Norwegian - Norsk',            array('thousands_separator'=>',', 'decimal_separator'=>'.', 'part_separator'=>' ')),
			'ps-utf8'   => array('ps',   'rtl', 'UTF-8', 'Pashto (Afghan)',              array('thousands_separator'=>',', 'decimal_separator'=>'.', 'part_separator'=>' ')),
			'pl-utf8'   => array('pl',   'ltr', 'UTF-8', 'Polish - Polski',              array('thousands_separator'=>'.', 'decimal_separator'=>',', 'part_separator'=>'. ')),
			'pt-utf8'   => array('pt',   'ltr', 'UTF-8', 'Portuguese - Português',       array('thousands_separator'=>',', 'decimal_separator'=>'.', 'part_separator'=>' ')),
			'pt-br-utf8'=> array('pt-br','ltr', 'UTF-8', 'Brazilian Portuguese',         array('thousands_separator'=>',', 'decimal_separator'=>'.', 'part_separator'=>' ')),
			'ro-utf8'   => array('ro',   'ltr', 'UTF-8', 'Romanian - Română',            array('thousands_separator'=>',', 'decimal_separator'=>'.', 'part_separator'=>'. ')),
			'ia-utf8'   => array('ia',   'ltr', 'UTF-8', 'Romanica',                     array('thousands_separator'=>',', 'decimal_separator'=>'.', 'part_separator'=>'. ')),
			'ru-utf8'   => array('ru',   'ltr', 'UTF-8', 'Russian - Русский',            array('thousands_separator'=>' ', 'decimal_separator'=>',', 'part_separator'=>'. ')),
			'sk-utf8'   => array('sk',   'ltr', 'UTF-8', 'Slovak',                       array('thousands_separator'=>'.', 'decimal_separator'=>',', 'part_separator'=>'. ')),
			'ia-utf8'   => array('ia',   'ltr', 'UTF-8', 'Slovio',                       array('thousands_separator'=>',', 'decimal_separator'=>'.', 'part_separator'=>'. ')),
			'sl-utf8'   => array('sl',   'ltr', 'UTF-8', 'Slovenian',                    array('thousands_separator'=>'.', 'decimal_separator'=>',', 'part_separator'=>'. ')),
			'sv-utf8'   => array('sv',   'ltr', 'UTF-8', 'Swedish',                      array('thousands_separator'=>',', 'decimal_separator'=>'.', 'part_separator'=>' ')),
			'th-utf8'   => array('th',   'ltr', 'UTF-8', 'Thai - ไทย',                    array('thousands_separator'=>',', 'decimal_separator'=>'.', 'part_separator'=>' ')),
			'tr-utf8'   => array('tr',   'ltr', 'UTF-8', 'Turkish - Türkçe',              array('thousands_separator'=>' ', 'decimal_separator'=>',', 'part_separator'=>'. ')),
			'tt-utf8'   => array('tt',   'ltr', 'UTF-8', 'Tatar',                         array('thousands_separator'=>' ', 'decimal_separator'=>',', 'part_separator'=>'. ')),
			'uz-utf8'   => array('uz',   'ltr', 'UTF-8', 'Uzbek',                         array('thousands_separator'=>' ', 'decimal_separator'=>',', 'part_separator'=>'. ')),
			'uk-utf8'   => array('uk',   'ltr', 'UTF-8', 'Ukraine - Українська',          array('thousands_separator'=>' ', 'decimal_separator'=>',', 'part_separator'=>'. ')),
			'vi-utf8'   => array('vi',   'ltr', 'UTF-8', 'Vietnamese - Tiếng Việt Nam',   array('thousands_separator'=>',', 'decimal_separator'=>'.', 'part_separator'=>' ')),
			'zh-utf8'   => array('zh',   'ltr', 'UTF-8', 'Chinese Simplified - 中文(简体)', array('thousands_separator'=>'.', 'decimal_separator'=>',', 'part_separator'=>'. ')),
			'zh-tw-utf8'=> array('zh-tw','ltr', 'UTF-8', 'Chinese Traditional - 中文(繁体)', array('thousands_separator'=>'.', 'decimal_separator'=>',', 'part_separator'=>'. ')),
		);
		if ($param != '')
		{
			/* return selected parameter */
			return isset($a[$this->pathLocale][$param]) ? $a[$this->pathLocale][$param] : false;
		}
		else
		{
			/* returns array [ISO 639 code] = "Display name" */
			while (list($k, $v) = each($a))
			{
				$ar[$k] = $a[$k][3];
			}
			return $ar;
		}
	}
} /* end of class */
$tmp['mtime'] = explode(' ', microtime());
$tmp['endtime'] = (float)$tmp['mtime'][1] + (float)$tmp['mtime'][0];
$tmp['time'][__FILE__] = ($tmp['endtime'] - $tmp['start_time']);
}
/* defined IS_CLASS_GWTK */

/* end of file */
?>