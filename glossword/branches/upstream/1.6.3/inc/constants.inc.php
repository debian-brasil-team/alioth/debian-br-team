<?php
if (!defined('IN_GW'))
{
	die('<!-- $Id: constants.inc.php,v 1.9 2004/07/08 18:32:30 yrtimd Exp $ -->');
}
/**
 *  Glossword - glossary compiler (http://glossword.info/dev/) 
 *  � 2002-2004 Dmitry N. Shilnikov <dev at glossword.info>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  (see `glossword/support/license.html' for details)
 */
// --------------------------------------------------------
/**
 *  page numbers, url parameters, navigation map, dictionary fields
 *   
 *  Configuration scheme:
 *  index -> config.inc -> lib.prepend, constants.inc -> custom.inc
 *                                      ^^^^^^^^^^^^^
 */
if (!defined('GW_DB_HOST')){ define('GW_DB_HOST', 'localhost'); }
if (!defined('GW_DB_DATABASE')){ define('GW_DB_DATABASE',  'glossword1'); }
if (!defined('GW_DB_USER')){ define('GW_DB_USER',  'root'); }
if (!defined('GW_DB_PASSWORD')){ define('GW_DB_PASSWORD',  'root'); }
if (!defined('GW_TBL_PREFIX')){ define('GW_TBL_PREFIX',  'gw_'); }
// --------------------------------------------------------
// Database table names
define('TBL_AUTH',        GW_TBL_PREFIX . 'auth');
define('TBL_DICT',        GW_TBL_PREFIX . 'dict');
define('TBL_TPCS',        GW_TBL_PREFIX . 'tpcs');
define('TBL_STAT_DICT',   GW_TBL_PREFIX . 'stat_dict');
define('TBL_SETTINGS',    GW_TBL_PREFIX . 'settings');
define('TBL_SESS',        GW_TBL_PREFIX . 'sessions');
define('TBL_USERS',       GW_TBL_PREFIX . 'users');
define('TBL_USERS_MAP',   GW_TBL_PREFIX . 'users_map');
define('TBL_WORDLIST',    GW_TBL_PREFIX . 'wordlist');
define('TBL_WORDMAP',     GW_TBL_PREFIX . 'wordmap');
define('TBL_SRCH_RESULTS',GW_TBL_PREFIX . 'search_results');
// --------------------------------------------------------
// Critical system constants. DO NOT EDIT.
// --------------------------------------------------------
// Action URL parameters
// '?index.php?a=add&t=topics' calls file 'add_topic.at.php'
define('GW_ACTION', 'a');
define('GW_SID',    'sid');
define('UPDATE',    'update');
define('GW_A_ADD',  'add');
define('GW_A_EDIT', 'edit');
define('REMOVE',    'remove');
define('CLEAN',     'clean');
define('IMPORT',    'import');
define('EXPORT',    'export');
define('CFG',       'cfg');
define('STATS',     'stats');
define('REG',       'register');
define('PROFILE',   'profile');
define('SEARCH',    'srch');
define('NEWPASS',   'newpass');
define('A_PRINT',   'print');
// --------------------------------------------------------
// Target URL parameters
// '?index.php?a=import&t=dict' calls file 'import_dict.at.php'
define('GW_TARGET', 't');
define('BRWS',      'browse');
define('TPCS',      'topic');
define('GW_T_DICT', 'dict');
define('GW_T_TERM', 'term');
define('TPL',       'template');
define('SYS',       'sys');
define('USER',      'user');
// --------------------------------------------------------
define('GW_T_LANGUAGE', 'il');
define('G_DLANGUAGE', 'dl');
// --------------------------------------------------------
/* -------------------------------------------------------- */
/* Switches */
define('GW_AFTER_DICT_UPDATE', 1);
define('GW_AFTER_TERM_ADD', 2);
define('GW_AFTER_SRCH_BACK', 3);
define('GW_AFTER_TERM_IMPORT', 4);
// Page numbers
$arPageNumbers = array(
	'index' => 1,
	TPCS.'_'.BRWS => 2,
	TPCS.'_'.GW_A_ADD => 3,
	TPCS.'_'.GW_A_EDIT => 4,
	TPCS.'_'.REMOVE => 5,
	GW_T_DICT.'_'.BRWS => 6,
	GW_T_DICT.'_'.GW_A_ADD => 7,
	GW_T_DICT.'_'.GW_A_EDIT => 8,
	GW_T_DICT.'_'.REMOVE => 9,
	GW_T_DICT.'_'.UPDATE => 10,
	GW_T_DICT.'_'.CLEAN => 11,
	GW_T_DICT.'_'.IMPORT => 12,
	GW_T_DICT.'_'.EXPORT => 13,
	GW_T_TERM.'_'.GW_A_ADD => 14,
	GW_T_TERM.'_'.GW_A_EDIT => 15,
	GW_T_TERM.'_'.REMOVE => 16,
	GW_T_TERM.'_'.UPDATE => 17,
	'_'.SEARCH => 18,
	SYS.'_'.CFG => 19,
	USER.'_'.REG => 20,
	USER.'_'.PROFILE => 21,
	USER.'_'.NEWPASS => 22,
	USER.'_'.UPDATE => 23,
	USER.'_'.BRWS => 24
);
// --------------------------------------------------------
// Navigation
$arNav = array(
	TPCS => array(BRWS, GW_A_ADD, GW_A_EDIT, REMOVE),
	GW_T_DICT => array(BRWS, GW_A_ADD, GW_A_EDIT, REMOVE, CLEAN, IMPORT, EXPORT),
	GW_T_TERM => array(BRWS, GW_A_ADD, GW_A_EDIT, REMOVE),
	SYS  => array(CFG),
	USER => array(BRWS, REG, PROFILE)
);
// --------------------------------------------------------
// Dictionary fields:
// - Field name (latin characters only)
// - Type [ textarea | input ]
// - Search index length [ 0 - off | 1..9 | auto ]
// - Is multiple records (split each item with a new line)
// - Is root element (<line><term>..<trsp>..<defn>..</line>)
// Array keys cannot be changed, i.e. 2 always transcription.
$arFields = array(
				1 => array('term',    'input',    'auto', 0, 1),
				2 => array('trsp',    'textarea', 'auto', 1, 1),
				0 => array('defn',    'textarea', 'auto', 1, 1),
				3 => array('abbr'),
				4 => array('trns'),
				5 => array('usg',     'textarea', 'auto', 1),
				6 => array('src',     'textarea', '0', 0),
				7 => array('syn',     'textarea', 1),
				8 => array('see',     'textarea', 1),
				9 => array('address', 'textarea', 2, 0),
				10 => array('phone',  'textarea', 2, 0),
			);
$intFields = count($arFields);
// --------------------------------------------------------

/* end of file */
?>