<?php
/**
 * $Id: lib.page.php,v 1.7 2004/09/25 11:07:10 yrtimd Exp $
 */
/**
 *  Glossword - glossary compiler (http://glossword.info/dev/) 
 *  � 2002-2004 Dmitry N. Shilnikov <dev at glossword.info>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  (see `glossword/support/license.html' for details)
 */
// --------------------------------------------------------
/**
 * Authentication.
 * Based on PHPlib concepts
 * http://phplib.shonline.de/
 */
// --------------------------------------------------------
/**
 *
 */
function page_open($feature)
{
    if (isset($feature["sess"]))
    {
        global $sess;
        $sess = new $feature["sess"];
        $sess->start();

        $sess->session_start = date("YmdHis");
        $sess->unregister("session_start");
        /**
         *
         */
        if (isset($feature["auth"]))
        {
            global $auth;
            if (!isset($auth))
            {
                $auth = new $feature["auth"];
            }
            $auth->start();
            if (isset($feature["user"]))
            {
                global $user;
                if (!isset($user))
                {
                    $user = new $feature["user"];
                }
                $user->start($auth->auth["id"]);
            } // end of $user
        } // end of $auth
    } // isset($sess)
} // end of page_open();
/**
 *
 */
function page_close()
{
    global $sess, $user, $objDict;
    if (isset($sess))
    {
        $sess->freeze();
        if (isset($user))
        {
            $user->freeze();
        }
        if (isset($objDict))
        {
            $objDict->freeze();
        }
    }
} // end of page_close();
/**
 *
 */
function sess_load($session)
{
    reset($session);
    while (list($k,$v) = each($session))
    {
        $GLOBALS[$k] = new $v;
        $GLOBALS[$k]->start();
    }
} // end of sess_load();
/**
 *
 */
function sess_save($session)
{
    reset($session);
    while (list(,$v) = each($session))
    {
        $GLOBALS[$v]->freeze();
    }
} // end of sess_save();
?>
