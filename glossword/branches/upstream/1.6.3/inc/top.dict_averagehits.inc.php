<?php
if (!defined('IN_GW'))
{
	die('<!-- $Id: top.dict_averagehits.inc.php,v 1.6 2004/06/15 15:27:54 yrtimd Exp $ -->');
}
/**
 *  Glossword - glossary compiler (http://glossword.info/dev/) 
 *  � 2002-2004 Dmitry N. Shilnikov <dev at glossword.info>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  (see `glossword/support/license.html' for details)
 */
// --------------------------------------------------------
/**
 *  Calculates the average hits per dictionary.
 *  This code runs inside function getTop10();
 *  switch keyname: R_DICT_AVERAGEHITS
 */
// --------------------------------------------------------
/**
 * Variables:
 * $amount, $arThText,  $arThAlign, $strTopicName, $strData, $curDateMk
 */
	// do not cache, $curDateMk used
	$arSql = $oDb->sqlExec(sprintf($oSqlQ->getQ('top-dict-hits-avg', $curDateMk, $amount)), 'st', 0);
	//
	$sumHits = $sumAvg = 0;
	$arThText = array($L->m('th_1'), $L->m('th_3'), $L->m('th_4'));
	// Sets width for colums
	$arThWidth = array('59%', '15%', '15%');
	// Sets alignment for colums
	$arThAlign = array($sys['css_align_left'], $sys['css_align_right'], $sys['css_align_right']);
	$strTopicName = $L->m('r_dict_averagehits');
	// for each dictionary
	for (; list($arK, $arV) = each($arSql);)
	{
		$cnt % 2 ? ($bgcolor = $GLOBALS['theme']['color_3']) : ($bgcolor = $GLOBALS['theme']['color_2']);
		$cnt++;        
		$strData .= '<tr valign="top" class="t" style="background:'.$bgcolor.'">';
		$strData .= '<td>' . $cnt . '</td>';
		$strData .= '<td>' . $oHtml->a($sys['page_index']. "?a=list&d=" . $arV['id'], $arV['title']) . '</td>';
		$strData .= '<td>' . numfmt($arV['hits_avg'], 1) . '</td>';
		$strData .= '<td>' . numfmt($arV['hits'], 0) . '</td>';
		$strData .= '</tr>';
		$sumAvg += $arV['hits_avg'];
		$sumHits += $arV['hits'];
	}
	// table footer
	$strData .= '<tr valign="top" class="t" style="background:' . $GLOBALS['theme']['color_3'] . '">';
	$strData .= '<td></td>';
	$strData .= '<td>' . $oHtml->a($sys['page_index'], '<b>'.$L->m('web_m_catalog').'&#160;&gt;</b>') . '</td>';
	$strData .= '<td>' . numfmt($sumAvg, 1) . '</td>';
	$strData .= '<td>' . numfmt($sumHits, 0) . '</td>';
	$strData .= '</tr>';
/**
 * end of R_DICT_AVERAGEHITS
 */
?>