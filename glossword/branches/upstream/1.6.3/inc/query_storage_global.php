<?php
/*
 * Query storage
 * � 2004 Dmitry N. Shilnikov <dev at glossword.info>
 * $Id: query_storage_global.php,v 1.2 2004/07/09 12:44:23 yrtimd Exp $
 */
$tmp['ar_queries'] = array(
	'get-settings' => 'SELECT settings_key, settings_val
						FROM ' . TBL_SETTINGS.'
					',
	'get-topics' => 'SELECT id, s, p, tpname
						FROM ' . TBL_TPCS . '
						ORDER BY p ASC, s ASC, tpname
					',
	'get-dicts-admin' => 'SELECT tp.tpname, tp.id AS p, d.*
						FROM ' . TBL_DICT . ' AS d, ' . TBL_TPCS . ' AS tp
						WHERE tp.id = d.id_tp
						ORDER BY d.title, d.is_active, d.lang
					',
	'get-dicts-web' => 'SELECT tp.tpname, tp.id AS p, d.*
						FROM ' . TBL_DICT . ' AS d, ' . TBL_TPCS . ' AS tp
						WHERE d.is_active = "1"
						AND tp.id = d.id_tp
						ORDER BY tp.tpname, d.lang, d.title
					',
	'get-dict' => 'SELECT u.user_name, u.user_email, u.location, u.is_showcontact, d.*
						FROM gw_dict AS d, gw_users AS u
						WHERE d.id = "%d"
						AND d.id_user = u.auth_id
						AND d.is_active = "1"
					',
	'get-terms-total' => 'SELECT count(*) AS num, SUM(int_terms) AS sum
						FROM ' . TBL_DICT . '
						WHERE is_active = "1"
					',
	'top-dict-updated' => 'SELECT d.id, d.date_created, s.hits, d.title
						FROM ' . TBL_DICT . ' AS d, ' . TBL_STAT_DICT . ' AS s
						WHERE d.is_active = "1"
						AND d.id = s.id
						ORDER BY d.date_modified DESC LIMIT 0, %d
					',
	'top-dict-hits-avg' => 'SELECT d.id, s.hits, d.title, (s.hits / ((%d - UNIX_TIMESTAMP(d.date_created)) / 86400) ) AS hits_avg
						FROM ' . TBL_DICT . ' AS d, ' . TBL_STAT_DICT . ' AS s
						WHERE d.is_active = "1"
						AND d.id = s.id
						ORDER BY hits_avg DESC, s.hits DESC, d.int_terms ASC, d.title ASC LIMIT 0, %d
					',
	'top-dict-new' => 'SELECT d.id, d.date_created, s.hits, d.title
						FROM ' . TBL_DICT . ' AS d, ' . TBL_STAT_DICT . ' AS s
						WHERE d.is_active = "1"
						AND d.id = s.id
						ORDER BY d.date_created DESC LIMIT 0, %d
					',
	'top-term-new' => 'SELECT id, date_created, term
						FROM %s
						ORDER BY date_created DESC, term LIMIT 0, %d
					',
	'up-dict-hits' => 'UPDATE ' . TBL_STAT_DICT . '
						SET hits = hits + 1
						WHERE id = "%d"
					',
	'get-az' => 'SELECT t.term_1 AS L1, t.term_2 AS L2
						FROM ' . TBL_DICT . ' AS d, %s AS t
						WHERE d.id = t.id_d
						GROUP BY t.term_1, t.term_2
						ORDER BY t.term_1, t.term_2, t.term
					',
	'get-term-by-id' => 'SELECT id AS tid, term_1, term_2, term, defn
						FROM %s
						WHERE id = "%d"
						',
	'get-term-by-id-adm' => 'SELECT id AS tid, term_1, term_2, term, defn
						FROM %s
						WHERE id = "%d"
						AND id_user = "%d"
					',
	'get-term-by-name' => 'SELECT t.id as tid, t.term_1, t.term_2, t.term, t.defn
						FROM %s AS k, %s AS m, %s AS t
						WHERE m.dict_id = "%d"
						AND m.term_match = "1"
						AND k.word_id = m.word_id
						AND t.id = m.term_id
						AND k.word_text IN (%s)
						GROUP BY m.term_id
					',
	'cnt-term-by-t1' => 'SELECT count(*) AS n
						FROM %s
						WHERE term_1 = "%s"
					',
	'cnt-term-by-t1t2' => 'SELECT count(*) AS n
						FROM %s
						WHERE term_1 = "%s"
						AND term_2 = "%s"
					',
	'get-term-by-t1' => 'SELECT id AS t_id, term, LENGTH(defn) AS len, %s
						FROM %s
						WHERE term_1 = "%s"
						ORDER BY term_2 ASC, term ASC
					',
	'get-term-by-t1t2' => 'SELECT id AS t_id, term, LENGTH(defn) AS len, %s
						FROM %s
						WHERE term_1 = "%s"
						AND term_2 = "%s"
						ORDER BY term_2 ASC, term ASC
					',
	'srch-word-cnt' => 'SELECT m.term_id, m.dict_id
						FROM ' . TBL_WORDLIST . ' AS k, ' . TBL_WORDMAP . ' AS m
						WHERE m.dict_id IN (%s) %s
						AND k.word_id = m.word_id
						AND %s
						GROUP BY m.term_id
					',
	'srch-result-cnt' => 'SELECT hits FROM '.TBL_SRCH_RESULTS.'
						WHERE id_srch = "%s"
					',
	'srch-result-id' => 'SELECT * FROM '.TBL_SRCH_RESULTS.'
						WHERE id_srch = "%s"
					',
	'get-terms-in' => 'SELECT id as id_t, id_d, term, defn FROM %s
						WHERE id IN (%s)
						ORDER BY term_2, term_1, term %s
					',
	'upd-srch-results++' => 'UPDATE '.TBL_SRCH_RESULTS.'
						SET hits = "%d"
						WHERE id_srch = "%s"
					',
	'upd-srch-q' => 'UPDATE '.TBL_SRCH_RESULTS.'
						SET hits = "%d", q = "%s"
						WHERE id_srch = "%s"
					',
	'create-dict' => "CREATE TABLE %s (
						id int(10) unsigned NOT NULL default '0',
						id_d mediumint(5) unsigned NOT NULL default '0',
						id_user smallint(5) unsigned NOT NULL default '1',
						date_created timestamp(14) NOT NULL,
						hits int(10) unsigned NOT NULL default '0',
						term_1 varchar(6) binary NOT NULL default '',
						term_2 varchar(12) binary NOT NULL default '',
						term varchar(255) binary NOT NULL default '',
						defn text NOT NULL,
						PRIMARY KEY (id)
					 ) TYPE=MyISAM
					",
	'get-term-exists' => 'SELECT t.id, t.term
						FROM %s AS k, '.TBL_WORDMAP.' AS m, %s AS t
						WHERE m.dict_id = %d
						AND m.term_match = "%d"
						AND k.word_id = m.word_id
						AND t.id = m.term_id
						AND %s
						GROUP BY m.term_id
					',
	'get-term-exists-spec' => 'SELECT t.id, t.term
						FROM %s AS t
						WHERE t.id_d = "%d"
						AND t.term LIKE "%s"
						GROUP BY t.id
					',
	'get-term-rand' => 'SELECT t.term, t.defn
						FROM %s as t
						WHERE LENGTH(t.defn) < 4096
						ORDER BY RAND()
						LIMIT 0, 1
					',
	'get-word' => 'SELECT word_text, word_id
						FROM %s
						WHERE word_text IN (%s)
					',
	'del-wordmap-by-term-dict' => 'DELETE FROM '.TBL_WORDMAP.'
						WHERE term_id = "%d"
						AND dict_id = "%d"
					',
	'get-terms-all' => 'SELECT t.id as term_id, t.id_d as dict_id
						FROM %s AS t
						WHERE t.id_d = "%d"
						GROUP BY id
						ORDER BY t.term_1 ASC, t.term_2 ASC, t.term ASC
					',
	'get-date-mm' => 'SELECT MIN(date_created) AS min, MAX(date_created) AS max
						FROM %s
					',
	'cnt-dict-date' => 'SELECT count(*) AS n
						FROM %s
						WHERE date_created >= %s
						AND date_created <= %s
					',
	'del-srch-by-date' => 'DELETE
						FROM '.TBL_SRCH_RESULTS.'
						WHERE UNIX_TIMESTAMP(srch_date) < (UNIX_TIMESTAMP(NOW())-%d)
					',
	'del-by-id' => 'DELETE FROM %s WHERE id = %d',
	'del-by-dict_id' => 'DELETE FROM %s WHERE dict_id = "%d"',
	'del-id-id_d' => 'DELETE FROM %s WHERE id = "%s" AND id_d = "%d"',
	'del-term_id-dict_d' => 'DELETE FROM %s WHERE term_id = "%s" AND dict_id = "%d"',
	'del-term_id-id_d' => 'DELETE FROM %s WHERE term_id = "%s" AND id_d = "%d"',
	'del-srch-by-dict' => 'DELETE FROM '.TBL_SRCH_RESULTS.' WHERE id_d = "%d"',
	'del-table' => 'DELETE FROM %s',
	'del-wordmap-by-dict' => 'DELETE FROM '.TBL_WORDMAP.' WHERE dict_id = "%d"',
	'drop-table' => 'DROP TABLE IF EXISTS %s'
);

?>