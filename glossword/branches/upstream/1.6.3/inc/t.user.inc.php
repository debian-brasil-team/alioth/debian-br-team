<?php
if (!defined('IN_GW'))
{
	die('<!-- $Id: t.user.inc.php,v 1.10 2004/11/01 13:05:00 yrtimd Exp $ -->');
}
/**
 *  Glossword - glossary compiler (http://glossword.info/dev/)
 *  � 2002-2004 Dmitry N. Shilnikov <dev at glossword.info>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  (see `glossword/support/license.html' for details)
 */
// --------------------------------------------------------
/**
 *  Actions for a user. New registration, Profile.
 */
// --------------------------------------------------------
/**
 * Authors list
 */
function getFormUsers()
{
	global $auth, $user, ${GW_ACTION}, $L, ${GW_SID}, $sys;
	global $oDb, $oHtml;
	$strForm = '';
	$trClass = 't';
	$sql = 'SELECT *
			FROM ' . TBL_USERS . '
			WHERE auth_level < 16
			AND auth_level >= 8
			ORDER BY date_reg';
	$arSql = $oDb->sqlExec($sql, '', 0);
	// common part
	$strForm .= '<table cellspacing="1" cellpadding="3" border="0" width="100%">';
	$strForm .= '<col width="1%"/><col width="4%"/><col width="70%"/><col width="25%"/>';
	//
	$cnt = 0;
		$strForm .= '<tr class="f" style="color:'.$GLOBALS['theme']['color_1'].';background:'.$GLOBALS['theme']['color_6'].'">';
		$strForm .= '<th class="gw">N</th>';
		$strForm .= '<th class="gw">'.$L->m('action').'</th>';
		$strForm .= '<th class="gw">'.$L->m('contact_name').'</th>';
		$strForm .= '<th class="gw">'.$L->m('contact_email').'</th>';
		$strForm .= '</tr>';
	for (; list($k, $v) = each($arSql);)
	{
		$cnt % 2 ? ($bgcolor = $GLOBALS['theme']['color_6']) : ($bgcolor = $GLOBALS['theme']['color_1']);
		$cnt++;
		$strForm .= '<tr valign="top" class="t" style="background-color:'.$bgcolor.'">';
		$strForm .= '<td>' . $cnt . '.</td>';
		$strForm .= '<td>' . htmlTagsA( $GLOBALS['sys']['page_admin'] . '?' . GW_TARGET . '=' . USER . '&' .  GW_ACTION . '=' . PROFILE . '&tid='. $v['auth_id'],
						$L->m('3_profile') ). '</td>'.
					'<td>' . $v['user_name'] . '</td>'.
					'<td><a href="mailto:' . $v['user_email'] . '">'. $v['user_email'] . '</a></td>';
		$strForm .= '</tr>';
	}
	$strForm .= '</table>';
	return $strForm;
}
/**
 * HTML-form for profile
 *
 * @param    array   $vars       posted variables
 * @param    int     $runtime    is this form posted first time [ 0 - no | 1 - yes ] // todo: bolean
 * @param    array   $arBroken   the names of broken fields (after post)
 * @param    array   $arReq      the names of required fields (after post)
 * @return   string  complete HTML-code
 * @see textcodetoform(), getFormHeight()
 */
function getFormProfile($vars, $runtime = 0, $arBroken = array(), $arReq = array())
{
	global $auth, $user, ${GW_ACTION}, $L, ${GW_SID}, $tid, $sys, $oHtml;
	$strForm = "";
	$trClass = "t";
	$trDisabled = "disabled";
	$form = new gwForms();

	// TODO: Remove user:
	//  1. remove user id from gw_auth
	//  2. remove user id from gw_users
	//  3. remove user id from gw_users_map
	//  4. re-assign dictionary IDs to admin
	//  5. re-assing term IDs to admin (hardest thing)
	/*
	if (($tid != '') && $auth->have_perm('admin', PERMLEVEL))
	{
		$form->Set('isButtonDel', 1);
		$form->Set('submitdel', $L->m('3_remove'));
	}
	*/

	$form->Set('action', $GLOBALS['sys']['page_admin']);
	$form->Set('submitok', $L->m('3_save'));
	$form->Set('submitcancel', $L->m('3_cancel'));
	$form->Set('formbgcolor', $GLOBALS['theme']['color_2']);
	$form->Set('formbordercolor', $GLOBALS['theme']['color_4']);
	$form->Set('formbordercolorL', $GLOBALS['theme']['color_1']);
	$form->Set('align_buttons', $GLOBALS['sys']['css_align_right']);
	$form->Set('charset', $sys['internal_encoding']);
	$form->arLtr = array('arPost[user_email]');
	## ----------------------------------------------------
	##
	// check vars
	// reverse array keys <-- values;
	$arReq = array_flip($arReq);
	// mark fields as "REQUIRED" and make error messages
	while (is_array($vars) && list($key, $val) = each($vars) )
	{
		$arReqMsg[$key] = $arBrokenMsg[$key] = "";
		if (isset($arReq[$key])) { $arReqMsg[$key] = ' <span style="color:#E30"><b>*</b></span>'; }
		if (isset($arBroken[$key])) { $arBrokenMsg[$key] = ' <span class="'.$trClass.'" style="color:#E30"><b>' . $L->m('reason_9') .'</b></span>'; }
	} // end of while
	##
	## ----------------------------------------------------
	//
	if ((${GW_ACTION} == PROFILE) || (${GW_ACTION} == UPDATE)) // change profile
	{
		$strForm .= getFormTitleNav($L->m('user_reginfo'));
		$strForm .= '<table cellspacing="3" cellpadding="0" border="0" width="100%">';
		$strForm .= '<col width="20%" align="'.$GLOBALS['sys']['css_align_right'].'"/><col width="80%"/>';
		if ( $vars['auth_level'] == PERMLEVEL ) // owner changes profile
		{
			$strForm .= '<tr valign="top">'.
						'<td class="' . $trClass . '">' . $L->m('login') . ':' . $arReqMsg['username'] . '</td>'.
						'<td>' . $arBrokenMsg['username'] . $form->field("input","arPost[username]", textcodetoform($vars['username'])) . '</td>'.
						'</tr>';
		}
		else
		{
			$strForm .= '<tr valign="top">'.
						'<td class="' . $trClass . '">' . $L->m('login') . ':</td>'.
						'<td class="' . $trDisabled . '">' . $vars['username'] . '</td>'.
						'</tr>';
		}

		//
        if ($tid == '') /* admin changes profile of another user */
        {
			$strForm .= '<tr valign="top">'.
							'<td class="' . $trClass . '">' . $L->m('pass_current') . ': </td>'.
							'<td>' . $arBrokenMsg['pass_current'] . $form->field("pass", "arPost[pass_current]", textcodetoform($vars['pass_current']), 16) . '</td>'.
							'</tr>';
        }
		$strForm .= '<tr valign="top">'.
						'<td class="' . $trClass . '">' . $L->m('pass_new') . ': </td>'.
						'<td>' . $arBrokenMsg['pass_new'] . $form->field("pass", "arPost[pass_new]", textcodetoform($vars['pass_new']), 16) . '</td>'.
						'</tr>';
		$strForm .= '<tr valign="top">'.
						'<td class="' . $trClass . '">' . $L->m('pass_confirm') . ': </td>'.
						'<td>' . $arBrokenMsg['pass_new'] . $form->field("pass","arPost[pass_confirm]", textcodetoform($vars['pass_confirm']), 16) . '</td>'.
						'</tr>';
#        $strForm .= '<tr valign="top">'.
#                        '<td class="' . $trClass . '">' . $L->m('password')
#                        . ': ' . htmlTagsA( $GLOBALS['sys']['index'] . '?' . GW_TARGET . '=' . USER . '&' .  GW_ACTION . '=newpass',
#                            $L->m('3_change'))
#                        . '</td>'.
#                        '<td>' . $arBrokenMsg['username'] . $form->field("pass","arPost[username]", textcodetoform($vars['username'])) . '</td>'.
#                        '</tr>';

		$strForm .= '<tr valign="top">'.
					'<td class="' . $trClass . '">' . $L->m('date_register') . ':</td>'.
					'<td class="' . $trDisabled . '">' . dateExtract($vars['date_reg'], "%d %FL %Y %H:%i:%s") . '</td>'.
					'</tr>';
		$strForm .= '<tr valign="top">'.
					'<td class="' . $trClass . '">' . $L->m('date_logged') . ':</td>'.
					'<td class="' . $trDisabled . '">' . ((($vars['date_login'] / 1) != 0) ? dateExtract($vars['date_login'], "%d %FL %Y %H:%i:%s") : '&#160;') . '</td>'.
					'</tr>';
		$strForm .= '</table>';
	}
	$strForm .= getFormTitleNav($L->m('user_profile'));
	$strForm .= '<table cellspacing="3" cellpadding="0" border="0" width="100%">';
	$strForm .= '<col width="20%" align="'.$GLOBALS['sys']['css_align_right'].'"/><col width="80%"/>';
	//
	if((${GW_ACTION} == PROFILE) && ($vars['auth_level'] == PERMLEVEL)) // owner changes profile
	{
	   $strForm .= '<tr valign="top">'.
					'<td class="' . $trClass . '">' . $L->m('y_name') . ':' . $arReqMsg['user_name'] . '</td>'.
					'<td>' . $arBrokenMsg['user_name'] . $form->field("input","arPost[user_name]", textcodetoform($vars['user_name'])) . '</td>'.
					'</tr>';
		$strForm .= '<tr valign="top">'.
					'<td class="' . $trClass . '">' . $L->m('y_email') . ':' . $arReqMsg['user_email'] . '</td>'.
					'<td>' . $arBrokenMsg['user_email'] . $form->field("input","arPost[user_email]", textcodetoform($vars['user_email'])) . '</td>'.
					'</tr>';
	}
	else
	{
	   $strForm .= '<tr valign="top">'.
					'<td class="' . $trClass . '">' . $L->m('contact_name') . ':' . $arReqMsg['user_name'] . '</td>'.
					'<td>' . $arBrokenMsg['user_name'] . $form->field("input","arPost[user_name]", textcodetoform($vars['user_name'])) . '</td>'.
					'</tr>';
		$strForm .= '<tr valign="top">'.
					'<td class="' . $trClass . '">' . $L->m('contact_email') . ':' . $arReqMsg['user_email'] . '</td>'.
					'<td>' . $arBrokenMsg['user_email'] . $form->field("input","arPost[user_email]", textcodetoform($vars['user_email'])) . '</td>'.
					'</tr>';
	}
	// common part
#		$strForm .= '<tr valign="top">'.
#					'<td class="' . $trClass . '">' . $L->m('user_location') . ':' . $arReqMsg['location'] . '</td>'.
#					'<td>' . $arBrokenMsg['location'] . $form->field("input","arPost[location]", textcodetoform($vars['location'])) . '</td>'.
#					'</tr>';
		$strForm .= '<tr valign="top">'.
					'<td class="'.$trClass .'">' . $L->m('options') . ':</td>'.
					'<td>';
			$strForm .= '<table cellspacing="0" cellpadding="1" border="0" width="100%">';
			$strForm .= '<col width="1%"/><col width="99%"/>';
			$strForm .= '<tr class="t">';
			$strForm .= '<td>' . $form->field("checkbox", "arPost[is_showcontact]", $vars['is_showcontact']) . '</td>';
			$strForm .= '<td><label for="arPost_is_showcontact_">' . $L->m('allow_showcontact') . '</label></td>';
			$strForm .= '</tr>';
		if (($tid != '') && $auth->have_perm('admin', PERMLEVEL) )
		{
			$strForm .= '<tr class="t">';
			$strForm .= '<td>' . $form->field("checkbox", "arPost[is_active]", $vars['is_active']) . '</td>';
			$strForm .= '<td><label for="arPost_is_active_">' . $L->m('allow_user') . '</label></td>';
			$strForm .= '</tr>';
		}
		if (${GW_ACTION} == REG)
		{
			$strForm .= '<tr class="t">';
			$strForm .= '<td>' . $form->field("checkbox", "arPost[is_notice]", $vars['is_notice']) . '</td>';
			$strForm .= '<td><label for="arPost_is_notice_">' . $L->m('user_notice') . '</label></td>';
			$strForm .= '</tr>';
		}
			$strForm .= '</table>';
		$strForm .= '</td>';
		$strForm .= '</tr>';
	//
	// do not show assigned dictionaries for Admin
	// because Admin can access to any dictionary
	if( ((${GW_ACTION} == PROFILE) || (${GW_ACTION} == UPDATE)) && !$auth->have_perm('admin', PERMLEVEL)) // non-admin changes profile
	{
		$strForm .= '<tr valign="top">'.
					'<td class="' . $trClass . '">' . $L->m('user_dictionaries') . ':</td><td>';
		$strForm .= '<table cellspacing="1" cellpadding="0" border="0" width="100%">';
		//
		$arDictMap = getDictArray();
		for (; list($k, $v) = each($arDictMap);)
		{
			$is_active = 0;
			if (isset($vars['dictionaries'][$v['id']]))
			{
				$strForm .= '<tr><td class="' . $trClass . '">'
							. $oHtml->a( $sys['page_admin'] . '?'
							. GW_ACTION . '=' . GW_A_EDIT . '&' . GW_TARGET . '=' . GW_T_DICT . '&id=' . $v['id'], $v['title'])
							. '</td></tr>';
			}
		}
		$strForm .= '</table>';
		$strForm .= '</td></tr>';
	}
	elseif ((${GW_ACTION} == REG)
			|| ( (${GW_ACTION} == PROFILE) && !$auth->have_perm('admin', $vars['auth_level']))
			|| ($tid != '') && !$auth->have_perm('admin', $vars['auth_level']))
	{
		// new user registration or admin changes profile of another user
		$strForm .= '<tr valign="top">'.
					'<td class="' . $trClass . '">' . $L->m('user_dictionaries') . ':</td><td>';
		$strForm .= '<table cellspacing="1" cellpadding="0" border="0" width="100%">';
		$strForm .= '<col width="1%"/><col width="99%"/>';
		//
		$arDictMap = getDictArray();
		/* Per each dictionary */
		for (; list($k, $arDictParam) = each($arDictMap);)
		{
			$is_assigned = 0;
			/* $vars['dictionaries'] is flipped */
			if (isset($vars['dictionaries'][$arDictParam['id']]))
			{
				$is_assigned = 1;
			}
			$str_external_link = $arDictParam['is_active'] ? '<a href="'.$sys['page_index'].'?a=list&amp;d='. $arDictParam['id'] .'" onclick="window.open(this.href);return false;">&gt;&gt;&gt;</a> ' : '';
			$strForm .= '<tr>'.
						'<td class="' . $trClass . '">' .
						$form->field('checkbox', "arPost[dictionaries][". $arDictParam['id'] . "]", $is_assigned) .
						'</td><td class="' . $trClass . '">'.
						$str_external_link .
						'<label for="arPost_dictionaries_' . $arDictParam['id'] . '_">'.
						$arDictParam['title'] .
						'</label></td>'.
						'</tr>';
		}
		$strForm .= '</table>';
		$strForm .= '</td></tr>';
	}
	// common part
		$strForm .= '</table>';
	//
	if (${GW_ACTION} == REG) // change profile
	{
		$strForm .= $form->field("hidden", GW_ACTION, REG);
	}
	else
	{
		$strForm .= $form->field("hidden", GW_ACTION, UPDATE);
		$strForm .= $form->field("hidden", 'tid', $tid);
	}
	$strForm .= $form->field("hidden", GW_TARGET, USER);
	$strForm .= $form->field("hidden", GW_SID, ${GW_SID});
	return $form->Output($strForm);
} // end of getFormProfile();
$arReq = array('user_name', 'user_email', 'username');
// --------------------------------------------------------
// Check permission
if ($auth->is_allow('user'))
{
// --------------------------------------------------------
// action switcher
switch (${GW_ACTION})
{
case BRWS:
## --------------------------------------------------------
##
	$strR .= getFormUsers();
##
## --------------------------------------------------------
break;
case PROFILE:
## --------------------------------------------------------
##
	if ($post == '') // not saved
	{
		$arBroken   = array();
		if (($tid != '') && $auth->have_perm('admin', PERMLEVEL))
		{
			/* admin changes profile of another user */
			$vars = $user->get_by_id($tid);
			$vars = array_merge($auth->getAuthUserInfo($tid), $vars);
		}
		else
		{
			$vars = $user->get();
			$vars['username'] = $auth->auth['uname'];
		}
		$vars['dictionaries'] = array_flip($vars['dictionaries']);
		$vars['is_notice'] = 0;
		$vars['pass_current'] = $vars['pass_new'] = $vars['pass_confirm'] = '';

		$strR .= getFormProfile($vars, 0, $arBroken, $arReq);
		$arHelpMap = array(
					'login'        => 'tip025',
					'pass_current' => 'tip022',
					'pass_new'     => 'tip023',
					'pass_confirm' => 'tip024',
				 );
		if (($tid != '') && $auth->have_perm('admin', PERMLEVEL)) // admin changes profile of another user
		{
			unset($arHelpMap['pass_current']);
		}
		$strHelp = '';
		$strHelp .= '<dl>';
		for (; list($k, $v) = each($arHelpMap);)
		{
			$strHelp .= '<dt><b>' . $L->m($k) . '</b></dt>';
			$strHelp .= '<dd>' . $L->m($v) . '</dd>';
		}
		$strHelp .= '</dl>';
		$strR .= '<br />'.kTbHelp($L->m('2_tip'), $strHelp);
	}
##
## --------------------------------------------------------
break;
case REG:
## --------------------------------------------------------
##
	if ($post == '') // not saved
	{
		$arBroken   = array();
		$vars['is_showcontact'] = 0;
		$vars['is_notice'] = 1;
		$vars['user_name'] = $vars['user_email'] = $vars['location'] = '';
		$vars['dictionaries'] = array();
		$strR .= getFormProfile($vars, 0, $arBroken, $arReq);
	}
	else
	{
		if (isset($arReq)) // check for broken fields
		{
			$arBroken = validatePostWalk($arPost, $arReq);
		}
		// fix on/off options
		$arPost['is_showcontact'] = isset($arPost['is_showcontact']) ? $arPost['is_showcontact'] : 0;
		$arPost['is_notice'] = isset($arPost['is_notice']) ? $arPost['is_notice'] : 0;
		//
		// check e-mail
		if (sizeof( $auth->getAuthUserInfo('', '', $arPost['user_email'] )) > 0)
		{
			$arBroken['user_email'] = true;
			$isPostError = 1;
			$strR .= '<p class="r"><span class="s">' . $L->m("reason_18") . '</span></p>';
		}
		//
		if (isset($arReq) && sizeof($arBroken) == 0) // no errors
		{
			$isPostError = 0;
		}
		else // on error
		{
			if (!isset($arPost['dictionaries']))
			{
				$arPost['dictionaries'] = array();
			}
			$strR .= getFormProfile($arPost, 0, $arBroken, $arReq);
		}
		if (!$isPostError)
		{
#			$sys['isDebugQ'] = 0;
			$queryA = $q = array();
			$arDicts = isset($arPost['dictionaries']) ? $arPost['dictionaries'] : array();
			unset($arPost['dictionaries']);
			//
			// create new password and login
			$password = kMakeUid('', $maxchar = 10, 1);
			$login = kMakeUid('a', $maxchar = 8);
			$login_url = $sys['server_proto'] . $sys['server_host'] . $sys['page_login'];
			$login_url = '<a href="'.$login_url.'" onclick="window.open(this.href);return false;">'.$login_url.'</a>';
			// Prepare message
			// User name, URL, login, password, message template
			$msgBody = gwCreateMessage($arPost['user_name'], $login_url, $login, $password, 'mail_newuser.html');
			//
			if ($arPost['is_notice']) // send mail to user
			{
				if (!kMailTo($arPost['user_email'], $sys['y_email'], '', $msgBody, 0))
				{
					$strR .= '<p class="r"><span class="s">'.sprintf($L->m("reason_17"), $arPost['user_email']).'</span></p>';
				}
			}
			// display login and password
			$strR .= '<p class="u">'.$L->m('login'). ': <b>' . $login . '</b> <br />'.
					 $L->m('password'). ': <b>' . $password . '</b><br />'.
					 $login_url . '</p>';

				$q1 = $q2 = array();
				unset($arPost['is_notice']);
				/* Auth table */
				$q1['id'] = $oDb->MaxId(TBL_AUTH);
				$q1['user_id'] = md5($login);
				$q1['username'] = $login;
				$q1['password'] = md5($password);
				$q1['perms'] = '01110010';
				$queryA[] = gw_sql_replace($q1, TBL_AUTH);
				/* Users table */
				$arPost['auth_id'] = $q1['id'];
				$q1 = array();
				$arPost['date_reg'] = date("YmdHis");
				$arPost['date_login'] = '00000000000000';
				$arPost['is_active'] = '1';
				$arPost['auth_level'] = '8';
				$arPost['user_settings'] = serialize(array());
				$queryA[] = gw_sql_replace($arPost, TBL_USERS);
				/* Assign dictionaries map */
				for (; list($k, $v) = each($arDicts);)
				{
					$q2 = array();
					$q2['user_id'] = $arPost['auth_id'];
					$q2['dict_id'] = $k;
					$queryA[] = gw_sql_replace($q2, TBL_USERS_MAP);
				}
				/* Redirect */
				$url = GW_ACTION . '=' . PROFILE . '&' . GW_TARGET . '=' . USER;
				$strR .= postQuery($queryA, $url, $sys['isDebugQ']);
		} /* final update */
	} /* submitted */
##
## --------------------------------------------------------
break;
case NEWPASS:
## --------------------------------------------------------
##
	// TODO: confirm
	// 2) create activation key
	// 3) create mail message
	// 4) send mail
	// 5) update database
	// 6) unauth
	//
	$actkey = kMakeUid('k', $maxchar = 16, 1);
	$actkey_url = $sys['server_proto'] . $sys['server_host'] . $sys['page_login'] . '?' . 'actkey=' . $actkey;
	$actkey_url = '<a href="'.$actkey_url.'">'.$actkey_url.'</a>';
	$mail_sig = $sys['site_name'] . ' - ' . $sys['site_desc'];
	$arSrc = array('{USERNAME}', '{SITENAME}', '{U_ACTIVATE}');
	$arTrg = array($user->get('user_name'), $sys['site_name'], $actkey_url);
	$msgBody = str_replace($arSrc, $arTrg, $L->m('mail_newpass'));
	$msgSubj = str_replace($arSrc, $arTrg, $L->m('mail_newpass_subj'));
	//
	$tpl_mail = new gwTemplate();
	$tpl_mail->addVal( 'LANG',           $L->languagelist("0") );
	$tpl_mail->addVal( 'TEXT_DIRECTION', $L->languagelist("1") );
	$tpl_mail->addVal( 'CHARSET',        $L->languagelist("2") );
	$tpl_mail->addVal( 'EMAIL_SIG',      $mail_sig );
	$tpl_mail->addVal( 'MSG_NEWPASS',    $msgBody );
	$tpl_mail->addVal( 'SUBJECT',        $msgSubj );
	$tpl_mail->setHandle(0, $sys['path_admin'] . '/' . $sys['path_tpl'] . '/mail_newpass.html');
	$tpl_mail->parse();
	$msgBody = $tpl_mail->output();
	//
	if (kMailTo($user->get('user_email'), $sys['y_email'], $msgSubj, $msgBody, 0))
	{
		// mail was send, now add activation key
		$sql = 'UPDATE %s
				SET date_reg=date_reg, date_login=date_login, is_active = "0", user_actkey = "%s"
				WHERE auth_id ="%d"';
		$sql = sprintf($sql, TBL_USERS, $actkey, $auth->auth['id']);
		if(sqlExec($sql, '', 0))
		{
			$strR .= $L->m('2_success');
			$auth->logout();
			page_close();
		}
	}
	else
	{
		$strR .= 'mail() error';
	}
##
## --------------------------------------------------------
break;
case UPDATE:
## --------------------------------------------------------
##
	if(isset($arReq)) // check for broken fields
	{
		$arBroken = validatePostWalk($arPost, $arReq);
	}
	// fix on/off options
	$arPost['is_showcontact'] = isset($arPost['is_showcontact']) ? $arPost['is_showcontact'] : 0;
	$arPost['is_active'] = isset($arPost['is_active']) ? $arPost['is_active'] : 0;
	$arPost['is_notice'] = isset($arPost['is_notice']) ? $arPost['is_active'] : 1;
	//
	//
	// protect other's username from changing
	if (isset($arPost['username']) && $tid != '')
	{
		unset($arPost['username']);
	}
	// compare changed username with existent
	if (isset($arPost['username']))
	{
		$arExistent = $auth->getAuthUserInfo('', $arPost['username']);
		if (sizeof($arExistent) > 0)
		{
			if (isset($arExistent['id']) && ($arExistent['id'] != $auth->auth['id']))
			{
				$arBroken['username'] = true;
				$strR .= '<p class="r"><span class="s">'. $L->m("reason_15") . '</span></p>';
				$isPostError = 1;
			}
		}
	}
	$isPassChanged = 0; // Switch for automatic logout
	//
	$user_id = ($tid != '') ? $tid : $user->id;
	//
	// check e-mail
	if ($arPost['user_email'] != '')
	{
		$arExistent = $auth->getAuthUserInfo('', '', $arPost['user_email']);
		if (sizeof($arExistent) > 0)
		{
			if (isset($arExistent['id']) && ($arExistent['id'] != $user_id))
			{
				$arBroken['user_email'] = true;
				$isPostError = 1;
				$strR .= '<p class="r"><span class="s">' . $L->m("reason_18") . '</span></p>';
				if (($tid != '') && $auth->have_perm('admin', PERMLEVEL)) // admin changes profile of another user
				{
					$arPost = array_merge( $auth->getAuthUserInfo($tid), $arPost);
					$arPost['auth_level'] = $user->get_by_id($tid, 'auth_level');
				}
			}
		}
	}
	//
	// parse password change
	if (!empty($arPost['pass_new']) && !empty($arPost['pass_confirm']))
	{
		if ( $arPost['pass_new'] != $arPost['pass_confirm'] )
		{
			// error
			$arBroken['pass_new'] = $arBroken['pass_confirm'] = true;
			$strR .= '<p class="r"><span class="s">'. $L->m("reason_14") . '</span></p>';
			$isPostError = 1;
		}
		elseif ( strlen($arPost['pass_new']) > 32 )
		{
			$arBroken['pass_new'] = $arBroken['pass_confirm'] = true;
			$strR .= '<p class="r"><span class="s">'. $L->m("reason_19") . '</span></p>';
			$isPostError = 1;
		}
		else if ($tid == '')
		{
			/* check current password */
			$arExistent = $auth->getAuthUserInfo('', '', '', $arPost['pass_current']);
			/* do change password */
			if (sizeof($arExistent) != 1)
			{
				$arBroken['pass_current'] = true;
				$isPostError = 1;
			}
			if ($user_id == $user->id) /* password changed by current user */
			{
				$isPassChanged = 1;
			}
		}
	}
	// new login correct, current password is ok, both passwords is correct
	if(isset($arReq) && sizeof($arBroken) == 0) // no errors
	{
		$isPostError = 0;
	}
	else // on error
	{
		$isPostError = 1;
		if (($tid != '') && $auth->have_perm('admin', PERMLEVEL)) // admin changes profile of another user
		{
			$arPost['date_reg'] = $user->get_by_id($tid, 'date_reg');
			$arPost['date_login'] = $user->get_by_id($tid, 'date_login');
		}
		else
		{
			$arPost['dictionaries'] = $user->get('dictionaries');
			$arPost['date_reg'] = $user->get('date_reg');
			$arPost['date_login'] = $user->get('date_login');
		}
		$arPost['username'] = $user->get_by_id($tid, 'username');
		$arPost['auth_level'] = $user->get_by_id($user_id, 'auth_level');
		$arPost['username'] = $auth->auth['uname'];
		$strR .= getFormProfile($arPost, 0, $arBroken, $arReq);
	}
	if (!$isPostError) // final update
	{
		$queryA = $q = array();
		//
		$where = 'auth_id = "%s"';
		unset($arPost['is_notice']);
		//
		$q2 = array();
		if (isset($arPost['username']))
		{
			$q2['username'] = $arPost['username'];
		}
		if (isset($arPost['pass_confirm']) && ($arPost['pass_confirm'] != ''))
		{
			$q2['password'] = md5($arPost['pass_confirm']);
		}
		unset($arPost['username']);
		unset($arPost['pass_current']);
		unset($arPost['pass_new']);
		unset($arPost['pass_confirm']);
		if (sizeof($q2) > 0)
		{
			$queryA[] = gw_sql_update($q2, TBL_AUTH, 'id = "'.$user_id.'"');
		}
		//
		$q = $arPost;
		unset($q['dictionaries']);
		if ($tid != '') //
		{
			$where = sprintf($where, $user_id);
			$q['date_reg'] = $user->get_by_id($user_id, 'date_reg');
			$q['date_login'] = $user->get_by_id($user_id, 'date_login');
		}
		else
		{
			unset($q['is_active']);
			$where = sprintf($where, $user_id);
			$q['date_reg'] = $user->get('date_reg');
			$q['date_login'] = $user->get('date_login');
		}
		$queryA[] = gw_sql_update($q, TBL_USERS, $where);
		// assign dictionaries, map

		$arPost['dictionaries'] = isset($arPost['dictionaries']) ? $arPost['dictionaries'] : array();
		if ($auth->have_perm('admin', PERMLEVEL))
		{
			$queryA[] = sprintf('DELETE FROM %s WHERE user_id = "%d"', TBL_USERS_MAP, $user_id);
		}
		for (; list($k, $v) = each($arPost['dictionaries']);)
		{
			$q2 = array();
			$q2['user_id'] = $user_id;
			$q2['dict_id'] = $k;
			$queryA[] = prepareSQLqueryINSERT($q2, TBL_USERS_MAP);
		}
		//
		$url = GW_ACTION . '=' . PROFILE . '&' . GW_TARGET . '=' . USER;
		if ($tid)
		{
			$url .= '&tid=' . $tid;
		}
		$strR .= postQuery($queryA, $url, $sys['isDebugQ'], 0);
		if ($isPassChanged)
		{
			$auth->logout();
			page_close();
		}
	}
##
## --------------------------------------------------------
break;
default:
break;
} // end of switch
// --------------------------------------------------------
// End check permission
}
else
{
	$strR .= $L->m('reason_13');
}

?>