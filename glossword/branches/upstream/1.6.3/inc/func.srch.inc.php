<?php
if (!defined('IN_GW'))
{
	die('<!-- $Id: func.srch.inc.php,v 1.13 2004/10/17 01:30:18 yrtimd Exp $ -->');
}
/**
 *  Glossword - glossary compiler (http://glossword.info/dev/)
 *  � 2002-2004 Dmitry N. Shilnikov <dev at glossword.info>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  (see `glossword/support/license.html' for details)
 */
// --------------------------------------------------------
/**
 *  Search functions
 *
 *  @package gw_search
 *  @version $Id: func.srch.inc.php,v 1.13 2004/10/17 01:30:18 yrtimd Exp $
 */
// --------------------------------------------------------


/**
 * Main function for searching terms
 *
 * @param    string  $q Search query string
 * @param    array   $arDict_Ids Dictionary IDs
 * @param    array   $srch Search parameters
 * @return   array   string term items, int total terms
 */
function gw_search($q, $arDict_Ids, $srch)
{
	global $arDictParam, $L, $sys;
	global $oSqlQ, $oDb, $oHtml, $oFunc;
	global $strict;
	// Clean old search results
	gw_search_cleanup();
	//
	$arSql = array();
	$strA = array(0 => '', 1 => '');
	$tmp['arIn'] = $tmp['redirect_url'] = $tmp['arCache'] = $tmp['arResultsTmp'] = $tmp['arResults'] = array();
	$i = 0;
	// Collect data for `gw_search_results' table
	$tmp['arCache']['q'] = $q;
	$tmp['arCache']['found'] = 0;
	//
	if (!is_array($srch['in']))
	{
		$srch['in'] = array($srch['in']);
	}
	// Save search parameters
	$tmp['arCache']['srch_settings']['in'] = implode(',', $srch['in']);
	$tmp['arCache']['srch_settings']['srch_adv'] = $srch['adv'];
	$tmp['arCache']['srch_settings']['srch_by'] = $srch['by'];
	//
	// Use minimum search length
	$arDictParam['min_srch_length'] = isset($arDictParam['min_srch_length']) ? $arDictParam['min_srch_length'] : 1;
	if (GW_IS_BROWSE_ADMIN)
	{
		$page_index = $sys['page_admin'];
		$arKeywords = text2keywords( text2term_uniq($q), 1, 25, $sys['internal_encoding']);
		if ($q == '')
		{
			$tmp['arCache']['html'] = $L->m('reason_5');
			return $tmp['arCache'];
		}		
	}
	else
	{
		$page_index = $sys['page_index'];
		$tmp['tpl_srch_name'] = 'tpl_term_list_search.html';
		$arKeywords = text2keywords( text2term_uniq($q), $arDictParam['min_srch_length'], 25, $sys['internal_encoding']);
		if ($oFunc->mb_strlen( str_replace('*', '', $q) ) < $arDictParam['min_srch_length'] )
		{
			$strA[0] = $L->m('error');
			return $strA;
		}
	}
	// Add `Dictionary ID' to SQL-request and to URL for redirect
	$tmp['redirect_url'][0] = 'd=0';
	/* Search in single dictionary */
	if (sizeof($arDict_Ids) == 1)
	{
		$tmp['arCache']['id_d'] = $arDict_Ids[0];
		$tmp['redirect_url'][0] = 'd=' . $arDict_Ids[0];
		/* Get stopwords */
		/* to fix */
		global $d;
		$arDictParam = getDictParam($d);
		$arStop = $L->getCustom('stop_words', $arDictParam['lang'], 'return');
		$arKeywords = gw_array_exclude($arKeywords, $arStop);
	}
#	prn_r($arKeywords);
	$tmp['intKeywords'] = sizeof($arKeywords);
	sort($arKeywords);
	//
	// Assign `Unique ID' to query.
	// Already uppercased, without stopwords, without special symbols, alphabetically sorted. Nice.
	$tmp['arCache']['id_srch'] = md5(implode('',$arKeywords).implode('',$arDict_Ids).$tmp['arCache']['srch_settings']['in'].$srch['adv']);
	// -----------------------------------------------
	// Check for an existent query, if exists, get results and add hits + 1
	$arSql = $oDb->sqlExec($oSqlQ->getQ('srch-result-cnt', $tmp['arCache']['id_srch']), '', 0);
	$tmp['arCache']['hits'] = isset($arSql[0]['hits']) ? $arSql[0]['hits'] : 1;
	//
	if (GW_IS_BROWSE_WEB && !empty($arSql)) // prev results found, web only
	{
		$arSql = isset($arSql[0]) ? $arSql[0] : array();
		// Increase hits, web only
		// Can't place `hits' field into `srch_settings' because it is used in SQL-requests
		$tmp['arCache']['hits'] = isset($arSql['hits']) ? $arSql['hits'] + 1 : 1;
		// Update query text to show for user.
		$oDb->sqlExec($oSqlQ->getQ('upd-srch-q', $tmp['arCache']['hits'], gw_text_sql($q), $tmp['arCache']['id_srch']), '', 0);
	}
	else
	{
		$sql_term_match = '';
		// Search in specified fields
		if ( is_array($srch['in']) && ( isset($srch['in'][0]) && $srch['in'][0] != -1) )
		{
			for (reset($srch['in']); list($inK, $inV) = each($srch['in']);)
			{
				$tmp['arIn'][] = $inV;
			}
			$sql_term_match = 'AND m.term_match IN (' . implode(', ', $tmp['arIn']) . ')';
		}
		// Search in definitions only
		if ( isset($srch['in'][0]) && $srch['in'][0] == 0)
		{
			$sql_term_match = 'AND m.term_match != "1"';
		}
		// Configure to search for 'all', 'phrase', 'any';
		$t = new gw_timer('srch');
		if ( ($srch['adv'] == 'all') || ($srch['adv'] == 'phrase') )
		{
			if (GW_IS_BROWSE_ADMIN)
			{
				$arKeywords = gw_text_wildcars($arKeywords, 'sql');
			}
			else // will be removed after testing, in future 1.6.2 or so
			{
				$arKeywords = gw_text_wildcars($arKeywords, 'sql');
			}
			// List all terms from one dictionary, for admin only
			if (str_replace('*', '', $q) == '')
			{
				$arSql = $oDb->sqlExec($oSqlQ->getQ('get-terms-all', $arDictParam['tablename'], $arDict_Ids[0]), '', 0);
				// re-organize search results per dictionary
				while (list($sqlK, $sqlV) = each($arSql)) // always one item in array
				{
					$tmp['arResultsTmp'][$sqlV['dict_id']][$sqlV['term_id']][] = $sqlV['term_id'];
				}
				for (reset($tmp['arResultsTmp']); list($dictK, $resultsV) = each($tmp['arResultsTmp']);)
				{
					while (list($rK, $rV) = each($resultsV))
					{
						$tmp['arResults'][$dictK][] = $rK;
						$tmp['arCache']['found']++;
						$i++;
					}
					$tmp['arResults'][$dictK] = implode(',', $tmp['arResults'][$dictK]);
				}
			}
			else // usual search
			{
				// Go for each word
				for (reset($arKeywords); list($k, $v) = each($arKeywords);)
				{
					$sql_word_srch = "k.word_text LIKE '" . $v . "'";
					// Main search query
					$sql = $oSqlQ->getQ('srch-word-cnt', implode(',',$arDict_Ids), $sql_term_match, $sql_word_srch);
					$arSql = $oDb->sqlExec($sql, '', 0);
					// re-organize search results per dictionary
					while (list($sqlK, $sqlV) = each($arSql)) // always one item in array
					{
						$tmp['arResultsTmp'][$sqlV['dict_id']][$sqlV['term_id']][] = $sqlV['term_id'];
					}
				}
#				prn_r($sql);
				// Now sort search results
				for (reset($tmp['arResultsTmp']); list($dictK, $resultsV) = each($tmp['arResultsTmp']);)
				{
					// Get stopwords per dictionary
					$arThisDictParam = getDictParam($dictK);
					$arStop = $L->getCustom('stop_words', $arThisDictParam['lang'], 'return');
					$arKeywordsD = gw_array_exclude($arKeywords, $arStop);
					$tmp['intKeywords'] = sizeof($arKeywordsD);
					$tmp['arResults'][$dictK] = array();
					while (list($rK, $rV) = each($resultsV))
					{
						if (sizeof($rV) != $tmp['intKeywords'])
						{
							// not required, but it cleans some memory, I hope
							unset($tmp['arResultsTmp'][$dictK][$rK]);
						}
						else
						{
							// collect matched term IDs
							$tmp['arResults'][$dictK][] = $rK;
							$tmp['arCache']['found']++;
							$i++;
						}
					}
					$tmp['arResults'][$dictK] = implode(', ', $tmp['arResults'][$dictK]);
					if (empty($tmp['arResults'][$dictK]))
					{
						unset($tmp['arResults'][$dictK]);
					}
				} // for
				// Seach for each keyword completed
			} // q = *
		} // TODO: 'any of words'
		//
		// Save collected Term IDs
		$tmp['arCache']['srch_settings']['results'] = $tmp['arResults'];
		// Save real search time (SQL + PHP)
		$tmp['arCache']['srch_settings']['time'] = sprintf("%1.5f", $t->end()); // will be changed to 3
		// view uncoded search results
		$tmp['arCache']['srch_settings'] = serialize($tmp['arCache']['srch_settings']);
		//
		// -----------------------------------------------
		// Add search results
#		prn_r( $tmp['arCache'] );
		$oDb->sqlExec(gw_sql_replace($tmp['arCache'], TBL_SRCH_RESULTS), '', 0);
	}
		
#	prn_r($tmp['arCache'], __LINE__.' '.__FILE__);	
#	prn_r($tmp['arResults'], __LINE__.' '.__FILE__);	
#	prn_r($oDb->query_array);
	$tmp['redirect_url'][] = 'id_srch='.$tmp['arCache']['id_srch'];
	$tmp['redirect_url'][] = 'a=srch';
	$tmp['redirect_url'][] = 'p=1';
	$tmp['redirect_url'][] = 'strict='.$strict;
	for (reset($sys['ar_url_append']); list($k, $v) = each($sys['ar_url_append']);)
	{
			$tmp['redirect_url'][] = $k.'='.$v;
	}
	sort($tmp['redirect_url']);
	gwtk_header( append_url($sys['server_proto'].$sys['server_host'].$page_index . '?'. implode('&', $tmp['redirect_url'])), $sys['is_delay_redirect']);
}


/**
 * 
 * 
 */
function gw_search_results($id_srch, $p, $id_dict = 0)
{
	global $oDb, $oSqlQ, $oHtml, $oFunc;
	global $sys, $L, $strict, $gw_this;
	global $arTplVars, $user;
	// -----------------------------------------------
	// Select existent query
	$arQ = $oDb->sqlExec($oSqlQ->getQ('srch-result-id', $id_srch), '', 0);
	$strA = array('html' => '', 'found' => 0, 'q' => '');
	// No results
	if (empty($arQ))
	{
		$arQ['found'] = 0;
		$arQ['q'] = '';
		return $strA;
	}
	$arQ = isset($arQ[0]) ? $arQ[0] : array();
	$arQ = array_merge($arQ, unserialize($arQ['srch_settings']));
	unset($arQ['srch_settings']);
	$arQ['found_total'] = 0;
	//
#	prn_r($arQ, __LINE__.' '.__FILE__);
	//
	$sys['page_srch_limit'] = 10;		
	$arResults = array();
	$arQ['arDictIds'] = array_keys($arQ['results']);
	// if requested dictionary is not in search results, then reset to 0
	$id_dict = in_array($id_dict, $arQ['arDictIds']) ? $id_dict : 0;
	// go for each search results
	for (reset($arQ['results']); list($dictK, $id_terms) = each($arQ['results']);)
	{
		// selected first dictionary from seach results
		$id_dict = ($id_dict == 0) ? $dictK : $id_dict;
		$tmp['arDictParam'] = getDictParam($id_dict);
		$arQ['found_total'] = $arQ['found'];
		$arQ['found'] = sizeof(explode(",", $arQ['results'][$id_dict]));
		$str_terms = $arQ['results'][$id_dict];
		// Get founded terms in selected dictionary
		$arResults = $oDb->sqlExec(
					$oSqlQ->getQ('get-terms-in', $tmp['arDictParam']['tablename'], $str_terms,
					$oDb->prn_limit($arQ['found'], $p, $sys['page_srch_limit'])), '', 0);
		break;
	}
	// --------------------------------------------------------
	// Settings for HTML-form
	// --------------------------------------------------------
	global $tpl_srch;
	$tpl_srch = new gwTemplate;
	$tpl_srch->unknownTagsT = $tpl_srch->unknownTags = 'keep';
	// allow multilingual_vars
	gw_addon_multilingual_vars('', 'tpl_srch');
	//	
	if (GW_IS_BROWSE_ADMIN)
	{
		/* Save search query */
		$user->save('id_search', $id_srch);
		$user->save('q', $arQ['q']);
		$page_index = $sys['page_admin'];
		$tpl_srch->addVal( "INT_TIME",   sprintf("%1.5f", $arQ['time']) );
		$tpl_srch->addVal( "TABLEWIDTH", "100%");
		$tmp['tpl_srch_name'] = './'.$GLOBALS['sys']['path_admin'].'/'.$GLOBALS['sys']['path_tpl'].'/tpl_term_list_search_admin.html';
	}
	else
	{		
		$page_index = $sys['page_index'];
		$tpl_srch->addVal( "INT_TIME",   sprintf("%1.3f", $arQ['time']) );
		$tpl_srch->addVal( "TABLEWIDTH", $GLOBALS['sys']['tblwidth']);
		$tmp['tpl_srch_name'] = $GLOBALS['sys']['path_tpl']."/".$GLOBALS['sys']['path_theme'].'/tpl_term_list_search.html';
	}
	$srch['adv'] = $arQ['srch_adv'];
	$srch['by'] = $arQ['srch_by']; 
	$arTplVars['srch'][] = array('chk_srch_in_term' => '' );
	$arTplVars['srch'][] = array('chk_srch_in_defn' => '' );
	$arTplVars['srch'][] = array('chk_srch_in_both' => '' );
	if ($arQ['in'] == -1)
	{
		$arTplVars['srch'][] = array('chk_srch_in_both' => ' checked="checked"' );
	}
	elseif ($arQ['in'] == 0)
	{
		$arTplVars['srch'][] = array('chk_srch_in_defn' => ' checked="checked"' );
	}
	elseif ($arQ['in'] == 1)
	{
		$arTplVars['srch'][] = array('chk_srch_in_term' => ' checked="checked"' );
	} // more to do
	//
	// --------------------------------------------------------
	// Rendering
	// --------------------------------------------------------
	$arA = array();
	for (; list($arK, $arV) = each($arResults);)
	{
		$arA[$arK]['term'] = $strA[2] = $arV['term'];
		$arA[$arK]['id_t'] = $arV['id_t'];
		$arA[$arK]['id_d'] = $arV['id_d'];
		if (isset($arV['defn'])) // prepare for output
		{
			$arV['defn'] = str_replace("/abbr>", "/abbr>&#032;", $arV['defn']);
			$arV['defn'] = str_replace("/trns>", "/trns>&#032;", $arV['defn']);
			$arV['defn'] = str_replace('</defn><defn>', ' &#8226;&#160;', $arV['defn']);
			$arV['defn'] = str_replace("><", ">&#032;<", $arV['defn']);
			$arV['defn'] = str_replace('&#160;', ' ', $arV['defn']);
			// if tag inside link="" found, 18 july 2003
			$arV['defn'] = preg_replace("/<(\/){0,1}(\w+)>/", '', $arV['defn']);
			$arV['defn'] = strip_tags($arV['defn']);
			$arA[$arK]['defn'] = preg_replace("/\{([A-Za-z0-9:\-_]+)\}/", '', $arV['defn']);
		}
	}
	$pairsA = array();

	$tpl_srch->setHandle( 0 , $tmp['tpl_srch_name'] );

	$tpl_srch->addVal( "KB", '');
	$tpl_srch->addVal( "DICT_NAME", '');
	$tpl_srch->addVal( "CSS_ALIGN_RIGHT", $sys['css_align_right']);
	$tpl_srch->addVal( "CSS_ALIGN_LEFT",  $sys['css_align_left']);

	// TODO: new highlight code,
	// TODO: highlight every keywords in search results
	$q = $arQ['q'];
	$q = str_replace("*", "", $q);
	$q = str_replace(")", "\)", $q);
	$q = str_replace("(", "\(", $q);
	$q = str_replace("]", "\]", $q);
	$q = str_replace("[", "\[", $q);
	$q = str_replace("/", "\/", $q);
	$q = str_replace("\\"," ", $q);
	$q = str_replace("/", " ", $q);
	$q = str_replace("?", "\?", $q);
	$srch = $q;

	// for each dictionary
	$arPairDict = array();
	$oHtml->setTag('a', 'style', 'text-decoration:underline');
	// resort dictionaries in alphabetic order
	$arQ['arDictIdsSorted'] = array();
	for (reset($arQ['arDictIds']); list($k, $v_id_dict) = each($arQ['arDictIds']);)
	{
		$arThisDict = getDictParam($v_id_dict);
		$arQ['arDictIdsSorted'][$arThisDict['title']] = $arThisDict;
	}
	unset($arQ['arDictIds']);
	ksort($arQ['arDictIdsSorted']);
	for (reset($arQ['arDictIdsSorted']); list($k, $arV) = each($arQ['arDictIdsSorted']);)
	{
		$str_found_dict = $k;
		$arPairDict[$k]['INT_FOUND_DICT'] = sizeof(explode(",", $arQ['results'][$arV['id']]));
		$arPairDict[$k]['DICT_NAME_URL'] = $oHtml->a($page_index.'?a=srch&id_srch='.$id_srch.'&d='.$arV['id'].'&p=1&strict=0', $str_found_dict);
		if ($arV['id'] == $id_dict)
		{
			$arPairDict[$k]['DICT_NAME_URL'] = '&gt;'.$str_found_dict;
		}
	}
	$oHtml->setTag('a', 'style', '');
	$tpl_srch->setBlock(0, 'found_dict', $arPairDict);
		
	
	// for each re-formated results
	for (reset($arA); list($k1, $v1) = each($arA);)
	{
		$pairsA[$k1]['intListNum'] = (($p-1) * $sys['page_srch_limit']) + $k1 + 1;
			
		// collect data for template
		if (GW_IS_BROWSE_ADMIN)
		{
			$pairsA[$k1]['TERM'] = htmlTagsA( $page_index
											. '?'. GW_ACTION . '=' . GW_A_EDIT . '&' . GW_TARGET . '=' . GW_T_TERM . '&id=' . $v1['id_d']
											. '&tid='.$v1['id_t'],
											(($q != '') ? @preg_replace("/($q)/si", '<span class="highlight">\\1</span>', $v1['term']) :  $v1['term'])
											);
			$pairsA[$k1]['URL_TERM_EDIT'] = htmlTagsA($page_index
											. '?'. GW_ACTION . '=' . GW_A_EDIT . '&' . GW_TARGET . '=' . GW_T_TERM . '&id=' . $v1['id_d']
											. '&tid='.$v1['id_t'], $L->m('3_edit'));
			$pairsA[$k1]['URL_TERM_REMOVE'] = htmlTagsA($page_index
											. '?'. GW_ACTION . '=' . REMOVE . '&' . GW_TARGET . '=' . GW_T_TERM . '&id=' . $v1['id_d']
											. '&tid='.$v1['id_t'], $L->m('3_remove'));
		}
		else
		{
			/* link from search results */
			$pairsA[$k1]['TERM'] = $oHtml->a($page_index
											. '?'.GW_ACTION.'=' . GW_T_TERM.'&d=' . $v1['id_d'] . '&t=' . $v1['id_t'],
									preg_replace("/^(" . $q . ")/i",
									'<span class="highlight">\\1</span>', $v1['term']));
		}
		if (isset($v1['defn']))
		{
			$pairsA[$k1]['DEFN'] = $v1['defn'];
			$pairsA[$k1]['DEFN'] = str_replace("\"", " ", $pairsA[$k1]['DEFN']);
			$pairsA[$k1]['DEFN'] = preg_replace('/&#[0-9]+;/', ' ', $pairsA[$k1]['DEFN']); // fix
			$pairsA[$k1]['DEFN'] = preg_replace('/&[a-z]+;/', ' ', $pairsA[$k1]['DEFN']);  // fix
			$pos = ($srch != '') ? $oFunc->mb_strpos($pairsA[$k1]['DEFN'], $srch) : 0;
			$posStart = $pos - 75;
			if ($posStart < 0 ) { $posStart = 0; }
			$pairsA[$k1]['DEFN'] = '&#8230;' . $oFunc->mb_substr($pairsA[$k1]['DEFN'], $posStart, 140) . '&#8230;';
		#	$pairsA[$k1]['DEFN'] = ($srch != '') ? preg_replace("/($srch)/si", '<span class="highlight">\\1</span>', $pairsA[$k1]['DEFN']) : $pairsA[$k1]['DEFN'];
		}
	}
	if (($arQ['found_total'] == 0) && $sys['dplayout'] > 0)
	{
		$pairsA = array();
	}
	$tpl_srch->dplayout = 0;
	$tpl_srch->setBlock(0, 'search_item', $pairsA);

	$tpl_srch->addVal( "QUERY",  textcodetoform($arQ['q']));
	$tpl_srch->addVal( "INT_FOUND",  $arQ['found']);
	$tpl_srch->addVal( "INT_FOUND_TOTAL",  $arQ['found_total']);
		
	$tpl_srch->addVal( "INT_REQUESTS",  $arQ['hits']);		
	$tpl_srch->addVal( "L_SRCH_TIME", $L->m('srch_6') );
	$tpl_srch->addVal( "L_SRCH_MATCHES", $L->m('srch_matches') );
	$tpl_srch->addVal( "L_SRCH_PHRASE", $L->m('srch_phrase') );
	$tpl_srch->addVal( "L_SRCH_TOTAL", $L->m('srch_5') );	
	$tpl_srch->addVal( "L_SRCH_FOUND", $L->m('srch_3') );	
	$tpl_srch->addVal( "L_SRCH_FOUND_DICT", $L->m('srch_2') );	
	$tpl_srch->addVal( "SRCH_ALSO", '' );

	$tpl_srch->parse();
	$arQ['pages'] = ceil($arQ['found'] / $sys['page_srch_limit']);
	$arQ['html'] = $tpl_srch->output();
	return $arQ;
}


/**
 * Cleans search results table.
 * Removes `not found', rare, old queries
 */
function gw_search_cleanup()
{
	global $oDb, $oSqlQ, $sys;
	$oDb->sqlExec($oSqlQ->getQ('del-srch-by-date', ($sys['search_lifetime'] * 60) ));
}


?>