<?php
/**********************************************************
*  HTML tools
*  =============================
*  Copyright (c) 2002 Dmitry Shilnikov <dev at glossword.info>
*
*  $Id: class.rendercells.php,v 1.6 2004/07/06 14:18:14 yrtimd Exp $
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
*
* @author   Dmitry Shilnikov <dev at glossword.info>
*
* Usage:
*   $class = new htmlRenderCells();
*   $class->ar = $somearray; // $ar[] = 'cell 1';
*   $class->X = $x;
*   $class->Y = $y;
*   print $class->RenderCells();
*
*/

class htmlRenderCells
{
    var $cellAlign = "";
    var $cellClass = "";
    var $tBorder = 0;
    var $X = 1;
    var $Y = 99;
    var $ar = array();
    var $totalItems = "";
    var $page = 1;

    function RowsCols($numCols, $numRows, $col, $row, $pages=0)
    {
        $numStart = $num = "0";
        for ($i=1; $i <= $numRows; $i++)
        {
            if ($i == $row)
            {
                $numStart = ($i * $numCols) + $col - $numCols;
            }
        }
        $numStart = $numStart + ($numCols*$numRows) * $pages - ($numCols*$numRows);
        return $numStart;
    } // end of function


function RenderCells()
{
    $href = "";
    $str = "";
    $navPagesA = array();
    $linkNext = $linkPrev = $linkCur = "";
    $cellAlign = ($this->cellAlign != "") ? ' align="' . $this->cellAlign . '"' : '';
    $cellClass = ($this->cellClass != "") ? ' class="' . $this->cellClass . '"' : '';
    if ($this->totalItems == "") { $this->totalItems = count($this->ar); }

    $NumberOfAllThumbs = ($this->X * $this->Y);
    $NumberOfPages = ceil($this->totalItems / $NumberOfAllThumbs);
    $ColsTotalOne = intval($NumberOfAllThumbs / $this->X);
    $ColsTotalTwo = ($NumberOfAllThumbs / $this->X);
    if ($ColsTotalTwo > $ColsTotalOne){ $ColsTotalOne += 1; }
    $Y = $ColsTotalOne;
    $NumberOfEmpty = 0;
    if (($this->totalItems - ($NumberOfAllThumbs * $this->page) ) < 0)
    {
        $NumberOfEmpty = ( ( ($this->X * $this->Y) * $NumberOfPages) - $this->totalItems );
    }
    if ($NumberOfEmpty > 0)
    {
        $Yauto = intval(($NumberOfAllThumbs - $NumberOfEmpty) / $this->X);
        $Yauto2 = (($NumberOfAllThumbs - $NumberOfEmpty) / $this->X);
        if ($Yauto2 > $Yauto){ $Yauto += 1; }
        $this->Y = $ColsTotalOne = $Yauto;
    }

    for ($i = 1; $i <= $NumberOfPages; $i++)
    {
        $linkNext = ($this->page + 1);
        $linkPrev = ($this->page - 1);
        if ($i == 1){ $link = "/$href.index"; }
        else { $link = "/$href.$i"; }
        if ($i == $this->page){ $navPagesA[] = '<span id="Lred"><b>'.$i.'</b></span>'; }
        else {$navPagesA[] = '<a href="'.$link.'"><u>'.$i.'</u></a>';}
    }
    if ($this->page == 2){ $linkPrev = "index";}
    if (($this->page - 1) != 0){ $linkPrev = '<a href="/'.$href.".".$linkPrev.'">�&nbsp;<b>Prev</b></a>'; }
    else {$linkPrev = "";}
    if (($this->page + 1) <= $NumberOfPages){$linkNext = '<a href="/'.$href.".".$linkNext.".".$GLOBALS["pathFileNameExt"].'"><b>Next&nbsp;�</b></a>';}
    else {$linkNext = "";}
    $delim = "&nbsp;";
    if (count($navPagesA) > 1)
    {
        $strPages = "pages:&#160;" . implode("&$160;| ", $navPagesA);
        $navStr = $linkPrev . $delim . $delim . $linkNext;
    }
    else {
        $strPages = "";
        $navStr = "";
    }

    if (count($navPagesA) > 1)
    {
        $str.= '<table border="0" cellspacing="0" cellpadding="2" width="100%">';
        $str.= '<tr valign="top" class="net">';
        $str.= '<td width="50%" bgcolor="#F6F6F0">'.$strPages.'</td>';
        $str.= '<td width="50%" bgcolor="#F6F6F0" align="right">'.$navStr.'</td>';
        $str.= '</tr>';
        $str.= '</table>';
    }
    $cellwidth = intval(100 / $this->X) . "%";

    $str .= '<table border="'.$this->tBorder.'" cellspacing="1" cellpadding="0" width="100%">';

    for($ThumbCols = 1; $ThumbCols <= $ColsTotalOne; $ThumbCols++)
    {
        // add <col width=""> after the first <tr>
        if ($ThumbCols == 1)
        {
            $intCellwidth = 0;
            for($ThumbRows = 1; $ThumbRows <= $this->X; $ThumbRows++)
            {
                $intCellwidth += intval(100 / $this->X);
                if ($ThumbRows == $this->X)
                {
                    $cellwidth = $cellwidth + (100 - $intCellwidth) . '%';
                }
                $str.= '<col width="'.$cellwidth.'"/>';
            }
        }
        $str.= '<tr valign="top"' . $cellClass . $cellAlign . '>';
        // render <td>
        for($ThumbRows = 1; $ThumbRows <= $this->X; $ThumbRows++)
        {
            $NumberOfCell = ( $this->RowsCols($this->Y, $this->X, $ThumbCols, $ThumbRows, 1) - 1 );
            $str .= '<td>';
            $str .= isset($this->ar[$NumberOfCell]) ? $this->ar[$NumberOfCell] : '&#160;';
            $str.= '</td>';
        }
        $str.=  '</tr>';
    }
    $str .= '</table>';
    return $str;
}

} // end of class


?>