<?php
if (!defined('IN_GW'))
{
	die('<!-- $Id: config.inc.php,v 1.18 2004/10/17 01:30:18 yrtimd Exp $ -->');
}
/**
 *  Glossword - glossary compiler (http://glossword.info/dev/)
 *  � 2002-2004 Dmitry N. Shilnikov <dev at glossword.info>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  (see `glossword/support/license.html' for details)
 */
/* ------------------------------------------------------- */
/**
 *  Configuration scheme:
 *  index -> config.inc -> lib.prepend -> constants.inc -> custom
 *           ^^^^^^^^^^
 */
/* ------------------------------------------------------- */
// Database settings
// see `glossword/db_config.php'
/* ------------------------------------------------------- */
// Debug Level
define('DEBUG',           0); /* [ 1 - on | 0 - off ] Debug mode */
define('DEBUG_SQL_TIME',  0); /* displays Total execution time */
define('DEBUG_SQL_QUERY', 0); /* displays SQL-queries */
define('DEBUG_CACHE',     0); /* displays Cache usage */
define('DEBUG_HTTP',      0); /* displays HTTP headers */
/* --------------------------------------------------------
 * System settings
 * ----------------------------------------------------- */
## is_check_ip          [ 1 - on | 0 - off ] Set to 0 if you have dynamic IP or use DSL/ADSL connection
## is_mod_rewrite       [ 1 - on | 0 - off ] Set to 1 if site uses Apache mod_rewrite configuration. See also .htaccess
## is_cache_sql         [ 1 - on | 0 - off ] Enables SQL-query caching.
## is_cache_http        [ 1 - on | 0 - off ] Enables caching pages for browsers. Reduces the bandwidth up to 70% for second requests.
## is_use_gzip          [ 1 - on | 0 - off ] Enables compresed output. Reduces the bandwidth up to 80%.
## cache_zlib           [ 1 - on | 0 - off ] Use Zlib compression to store/read cache files. It saves some of space on HDD (Clear cache before to switch)
## cache_lifetime       [ 1440 - day | 10080 - week | 43200 - 30 days ] Cache expires after `n' minutes.
## refreshtime          [ 1..9 ] Time in seconds before redirect (Admin mode)
## phpEx                [ php - default | php3 | php4 ] PHP-files extension
## dplayput             [ 0 - default | 1 - single | 2 - twoside | 3..9 - multicolumn ] Mode for definition preview layout.
## srchlen              [ 1 - default | 2..9 ] Minimum length for search string.
## path_img             Path to directory where images are stored.
## path_tpl             Path to directory where templates are stored.
## path_admin           Path to login directory.
/* 1.4.2 */
## is_LogMail           [ 1 - on | 0 - off ] Save mail traffic.
## is_LogRef            [ 1 - on | 0 - off ] Save referrers.
## is_LogSearch         [ 1 - on | 0 - off ] Save search queries.
/* 1.5.3 */
## leech_factor         [ 1..99 ] Increases amount of pages in `n' times when anti-leecher is turned on.
/* 1.6 */
## filters_output       [ array ] Additional function names to use for HTML-output
## filters_output_defn  [ array ] Additional function names to use for HTML-code of a definition
## gzip_level           [ 1-9 ] 9 is the best compression mode. Requires more CPU.
## is_debug_output      [ 1 - on | 0 - off ] Set to 1 to disable all applied filters for HTML-output.
## is_delay_redirect    [ 1 - on | 0 - off ] Pause between redirect. For debug purposes.
## is_tpl_show_names    [ 1 - on | 0 - off ] Show teplate file names. Useful for customizing themes.
## path_cache_sql       Path to directory where cached results are stored.
## path_export          Path to directory where exported files are stored.
## search_lifetime      [ 1440 - day | 10080 - week | 43200 - 30 days ] Keep search results cached `n' minutes.
## is_LogGzip           [ 1 - on | 0 - off ] Save Gzip optimization results.
## path_logs            Path to directory where log-files are stored.
## path_gwlib           Path to directory where PHP-libraries are stored.
## max_dict_updated     [ 1-9 ] Maximum number of recently updated dictionaries
## max_dict_top         [ 1-9 ] Maximum number of dictionaries in the Top 10
## is_list_numbers']    [ 1 - on | 0 - off ] Catalog: show number of terms
## is_list_images']     [ 1 - on | 0 - off ] Catalog: show icons
## is_list_announce']   [ 1 - on | 0 - off ] Catalog: show announce

/* ------------------------------------------------------- */
$sys['is_check_ip']       = 0;
$sys['is_mod_rewrite']    = 0;
$sys['is_cache_sql']      = 0;
$sys['is_cache_http']     = 0;
$sys['is_use_gzip']       = 1;
$sys['gzip_level']        = 5;
$sys['cache_zlib']        = 0;
$sys['cache_lifetime']    = 10080;
$sys['path_cache_sql']    = 'gw_temp/cache_sql';
$sys['path_export']       = 'gw_temp/export';
$sys['path_logs']         = 'gw_temp/logs';
$sys['path_admin']        = 'gw_admin';
$sys['path_gwlib']        = 'lib';
$sys['refreshtime']       = 2;
$sys['phpEx']             = 'php';
$sys['dplayout']          = 0;
$sys['srchlen']           = 1;
$sys['path_img']          = 'img';
$sys['path_tpl']          = 'templates';
$sys['is_LogMail']        = 0;
$sys['is_LogRef']         = 0;
$sys['is_LogSearch']      = 0;
$sys['is_LogGZip']        = 0;
$sys['leech_factor']      = 3;
$sys['is_tpl_show_names'] = 0;
$sys['is_delay_redirect'] = 0;
$sys['search_lifetime']   = 43200;
$sys['filters_output']    = array('gw_text_smooth');
$sys['filters_defn']      = array('gw_text_smooth_defn');
$sys['is_debug_output']   = 0;
$sys['max_dict_updated']  = 5;
$sys['max_dict_top']      = 10;
$sys['is_list_numbers']   = 1;
$sys['is_list_images']    = 1;
$sys['is_list_announce']  = 0;
$sys['ar_url_append']     = array();

include_once( $sys['path_gwlib'] .'/class.func.php' );

/* ------------------------------------------------------- */
/* Autoexec */
if (isset($sys['is_prepend']) && $sys['is_prepend'])
{
	include_once( $sys['path_include'] . '/lib.prepend.'. $sys['phpEx']);
}
$sys['file_lock']   = 'gw_temp/gw_install.lock';
	
/* end of file */
?>