<?php
/**
 *  Glossword - glossary compiler (http://glossword.info/dev/)
 *  � 2002-2004 Dmitry N. Shilnikov <dev at glossword.info>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  (see `glossword/support/license.html' for details)
 */
if (!defined('IS_CLASS_DBQ'))
{
	define('IS_CLASS_DBQ', 1);

class gw_query_storage {
	/*
		usage:
		function setQ()
		{
			...
			$arSql = array_merge($arSql, $this->import(array('file1','file2','...')));
			return $arSql;
		}
	*/
	function import($ar = array())
	{
		global $sys;
		$arSql = array();
		while (is_array($ar) && list($k, $v) = each($ar))
		{
			if (file_exists($sys['path_include']. '/' . $v))
			{
				include($sys['path_include']. '/' . $v);
				$arSql = array_merge($arSql, $tmp['ar_queries']);
			}
		}
		return $arSql;
	}
	/* */
	function setCustomQ()
	{
		return array();
	}
	/* */
	function setQ()
	{
		$arSql = $this->import(array('query_storage_global.php'));
		return $arSql;
	}
	/**/
	function getQ()
	{
		$args = func_get_args();
		$ar = array();
		/* 8 parameters allowed, */
		/* see also `return sprintf' at the end of function */
		for ($i = 0; $i <= 8; $i++)
		{
			$ar[] = isset($args[$i]) ? $args[$i] : '';
		}
		$arSql = array_merge($this->setQ(), $this->setCustomQ());
		if (isset($arSql[$ar[0]]))
		{
			$arSql[$ar[0]] = preg_replace("/[ ]{2,}/", ' ', $arSql[$ar[0]]);
			return sprintf( $arSql[$ar[0]], $ar[1], $ar[2], $ar[3], $ar[4], $ar[5], $ar[6], $ar[7], $ar[8] );
		}
	}
} /* end of class */
}

?>