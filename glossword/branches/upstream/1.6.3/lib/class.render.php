<?php
if (!defined('IN_GW'))
{
	die('<!-- $Id: class.render.php,v 1.8 2004/09/10 07:51:26 yrtimd Exp $ -->');
}
/**
 *  Glossword - glossary compiler (http://glossword.info/dev/)
 *  � 2002-2004 Dmitry N. Shilnikov <dev at glossword.info>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  (see `glossword/support/license.html' for details)
 */
// --------------------------------------------------------
/**
 *  Coverts array structure into XML or HTML code.
 *
 *  @todo   normalize raw functions.
 */
// --------------------------------------------------------


class gw_render extends gw_htmlforms
{
	var $is_html_preview = 0;
	var $tag_abbr   = 'acronym'; // 'abbr' for XHTML 2.0
	var $tag_trns   = 'acronym'; // 'abbr' for XHTML 2.0
	// <trsp> tag
	var $prepend_trsp  = '<br /><span class="trsp">[';
	var $split_trsp    = ', ';
	var $append_trsp  = ']</span><br />';
	// <abbr> tag
	var $prepend_abbr  = '';
	var $postlang_abbr = '&#32;';
	var $split_abbr    = '; <br />';
	var $append_abbr   = '<br />';
	// <trns> tag
	var $prepend_trns  = '';
	var $postlang_trns = '&#32;';
	var $split_trns    = '; <br />';
	var $append_trns   = '';
	// <usg> tag
	var $prepend_usg   = '<dl><dt>';
	var $split_usg     = '</dt><dt>';
	var $append_usg    = '</dt></dl>';
	// <see> and <syn> tags
	var $prepend_see   = '';
	var $split_see     = ', ';
	var $append_see    = '';	
	// other tags
	var $split_src     = ', ';
	var $split_address = ', ';
	var $split_phone   = ', ';
	var $tag_stress_rule  = '<span class="stress">|</span>';
	// internal switches, do not change
	var $is_br_begin   = 0;
	var $is_plural     = 0;
	// available functions
	var $arFuncList = array(
					   'make_xml_term' => 1, 'make_xml_trsp' => 1, 'make_xml_defn' => 1,
					   'make_xml_abbr' => 1, 'make_xml_trns' => 1,
					   'make_xml_syn'  => 1, 'make_xml_see'  => 1,
					   'make_xml_usg'  => 1, 'make_xml_src'  => 1, 'make_xml_address' => 1, 'make_xml_phone'  => 1
					  );
	var $arFuncList_html = array(
					   'make_html_term' => 1, 'make_html_trsp' => 1, 'make_html_defn' => 1,
					   'make_html_abbr' => 1, 'make_html_trns' => 1,
					   'make_html_syn'  => 1, 'make_html_see'  => 1,
					   'make_html_usg'  => 1, 'make_html_src'  => 1, 'make_html_address' => 1, 'make_html_phone'  => 1
					  );
	function tag2field_xml($fieldname, $ar = array())
	{
		$fieldname = strtolower($fieldname);
		$funcname = 'make_xml_' . $fieldname;
		if (!isset($this->arEl[$fieldname]))
		{
			$this->arEl[$fieldname] = array('value' => '');
		}
		if (isset($this->arFuncList[$funcname]) && $this->arFuncList[$funcname])
		{
			return $this->$funcname($fieldname, $ar);
		}
	}
	function tag2field_html($fieldname, $ar = array())
	{
		$fieldname = strtolower($fieldname);
		$funcname = 'make_html_' . $fieldname;
		if (isset($this->arFuncList_html[$funcname]) && $this->arFuncList_html[$funcname])
		{
			return $this->$funcname($fieldname, $ar);
		}
	}

	//
	function make_xml_term($fieldname, $ar = array())
	{
		#return '<term>'. $this->objDom->get_content($this->arEl[$fieldname][0]) . '</term>';
	}
	function make_html_term($fieldname, $ar = array())
	{
		return '';
	}
	//
	function make_xml_trsp($fieldname, $ar = array(), $tag = 'trsp')
	{
		if (empty($ar))
		{
			$ar['elK'] = 0;
		}
		if (!isset($this->arEl[$fieldname]))
		{
			$this->arEl[$fieldname] = array();
		}
		$tmp['strxml'] = '';
		for (reset($this->arEl[$fieldname]); list($elK, $elV) = each($this->arEl[$fieldname]);)
		{
			if (isset($elV['value']))
			{
				$elV = explode(CRLF, $elV['value']);
				while (list($k, $v) = each($elV))
				{
					if ($v != '')
					{
						$tmp['strxml'] .= '<'.$tag.'>'. $v .'</'.$tag.'>';
					}
				}
			}
		}
		return $tmp['strxml'];
	}
	//
	function make_html_trsp($fieldname, $ar = array(), $tag = "trsp")
	{
		if (empty($ar))
		{
			$ar['elK'] = 0;
		}
		if (!isset($this->arEl[$fieldname]))
		{
			$this->arEl[$fieldname] = array();
		}
		$tmp['strhtml'] = '';
		$tmp['ar_compiled'] = array();
		$prepend_name = 'prepend_'.$tag;
		$split_name = 'split_'.$tag;
		$append_name = 'append_'.$tag;
		//
		$i = 0;
		//
		for (reset($this->arEl[$fieldname]); list($elK, $elV) = each($this->arEl[$fieldname]);)
		{
			if (isset($elV['value']))
			{
				$elV = explode(CRLF, $elV['value']);
				while (list($k, $v) = each($elV))
				{
					if ($v != '')
					{
						$tmp['ar_compiled'][$i] = $v;
						$i++;
					}
				}
			}
		}
		if (!empty($tmp['ar_compiled']))
		{
			$tmp['strhtml'] .= $this->$prepend_name .
							   implode($this->$split_name, $tmp['ar_compiled']) .
							   $this->$append_name;
		}
		return $tmp['strhtml'];
	}

	function make_xml_defn($fieldname, $ar = array())
	{
		global $oFunc;
		$tmp['strxml'] = '';
		// for each definition
		while(list($elK, $elV) = each($this->arEl[$fieldname]))
		{
			// get definition content
			$tmp['strxml'] .= '<defn>';
			//
			// Parse subtags, abbr + trns
			//
			$arTmp['elK'] = $elK;
			if ($this->arDictParam['is_abbr'])
			{
				$tmp['strxml'] .= $this->tag2field_xml('abbr', $arTmp);
			}
			if ($this->arDictParam['is_trns'])
			{
				$tmp['strxml'] .= $this->tag2field_xml('trns', $arTmp);
			}
			//
			$tmp['strxml'] .= $elV['value'];
			//
			// Parse subtags
			//
			for (reset($this->arFields); list($fK, $fV) = each($this->arFields);)
			{
				// not root elements only
				if ((!isset($fV[4]) || !$fV[4]) && $this->arDictParam['is_'.$fV[0]]
					&& ($fV[0] != 'abbr' && $fV[0] != 'trns'))
				{
					$tmp['strxml'] .= $this->tag2field_xml($fV[0], $arTmp);
				}
			}
			$tmp['strxml'] .= '</defn>';
			$tmp['strxml'] = str_replace('<defn></defn>', '', $tmp['strxml']);
		}
		return $tmp['strxml'];
	}
	function make_html_defn($fieldname, $ar = array())
	{
		global $oFunc;
		$tmp['strhtml'] = $tmp['br'] = '';
		//
		// more than one definition
		$this->is_plural = 0;
		if (!isset($this->arEl[$fieldname][0])) { $this->arEl[$fieldname][0] = ''; }
		if (sizeof($this->arEl[$fieldname]) > 1) { $this->is_plural = 1; }
		$tmp['strhtml'] .= $this->is_plural ? '<ol class="defnblock">' : '<div class="defnblock">';
		//
		// for each definition
		if (!is_array($this->arEl[$fieldname][0]))
		{
			$this->arEl[$fieldname][0] = array('value' => '');
		}
		//
		while (list($elK, $elV) = each($this->arEl[$fieldname]))
		{
			// get definition content
			$arTmp['elK'] = $elK;
			$tmp['strhtml'] .= CRLF . ($this->is_plural ? '<li>' : '');
			//
			// Parse subtags, abbr + trns
			$tmp['str_abbr'] = $tmp['str_trns'] = '';
			//
			// add <br> tag before definitions
			$this->Set('is_br_begin', 0);
			//
			if ($this->arDictParam['is_abbr'])
			{
				$tmp['str_abbr'] = $this->tag2field_html('abbr', $arTmp);
				$tmp['strhtml'] .= $tmp['str_abbr'];
			}
			if ($this->arDictParam['is_trns'])
			{
				$tmp['str_trns'] = $this->tag2field_html('trns', $arTmp);
				if (($tmp['str_abbr'] != '') && ($tmp['str_trns'] != ''))
				{
#					$tmp['strhtml'] .= '<br />';
				}
				$tmp['strhtml'] .= $tmp['str_trns'];
			}
			//
			$tmp['br'] = ($this->is_br_begin || $this->is_plural) ? '<br />' : '';
			//
			$tmp['strhtml'] .= ($elV['value'] != '') ? $tmp['br'] . '<div class="defn">'.$elV['value'].'</div>' : '';
			//
			// Parse subtags
			//
			for (reset($this->arFields); list($fK, $fV) = each($this->arFields);)
			{
				// not root elements only
				if ((!isset($fV[4]) || !$fV[4]) && $this->arDictParam['is_'.$fV[0]]
					&& ($fV[0] != 'abbr' && $fV[0] != 'trns'))
				{
					$tmp['strhtml'] .= $this->tag2field_html($fV[0], $arTmp);
				}
			}
			$tmp['strhtml'] .= CRLF . ($this->is_plural ? '</li>' : '');
		}
		$tmp['strhtml'] .= $this->is_plural ? '</ol>' : '</div>';
		return $tmp['strhtml'];
	}

	function make_xml_abbr($fieldname, $ar = array(), $tag = 'abbr')
	{
		$tmp['strxml'] = '';
		$tmp['arEl'] = isset($this->arEl[$fieldname][$ar['elK']]) ? $this->arEl[$fieldname][$ar['elK']] : array();
		//
		// do auto fill
		//
		if (empty($tmp['arEl']))
		{
			$tmp['arEl'][0] = array('value' => '', 'attributes' => array('lang' => '--'));
		}
		//
		while (list($chK, $chV) = each($tmp['arEl']))
		{
			//
			$tmp['attributes'] = $this->objDom->get_attribute('lang', '', $chV);
			$tmp['str'] = $this->objDom->get_content( $chV );
			//
			if (($tmp['str'] != '') || $tmp['attributes'] != '--')
			{
				$tmp['strxml'] .= '<'.$tag;
				$tmp['strxml'] .= ($tmp['attributes'] == '--') ? '' : ' lang="'.$tmp['attributes'].'"';
				$tmp['strxml'] .= '>';
				$tmp['strxml'] .= $tmp['str'];
				$tmp['strxml'] .= '</'.$tag.'>';
			}
		}
		return $tmp['strxml'];
	}
	function make_xml_trns($fieldname, $ar = array(), $tag = 'trns')
	{
		$tmp['strxml'] = '';
		$tmp['arEl'] = isset($this->arEl[$fieldname][$ar['elK']]) ? $this->arEl[$fieldname][$ar['elK']] : array();
		//
		// do auto fill
		//
		if (empty($tmp['arEl']))
		{
			$tmp['arEl'][0] = array('value' => '', 'attributes' => array('lang' => '--'));
		}
		//
		while (list($chK, $chV) = each($tmp['arEl']))
		{
			//
			$tmp['attributes'] = $this->objDom->get_attribute('lang', '', $chV);
			$tmp['str'] = $this->objDom->get_content( $chV );
			//
			if (($tmp['str'] != '') || $tmp['attributes'] != '--')
			{
				$tmp['strxml'] .= '<'.$tag;
				$tmp['strxml'] .= ($tmp['attributes'] == '--') ? '' : ' lang="'.$tmp['attributes'].'"';
				$tmp['strxml'] .= '>';
				$tmp['strxml'] .= $tmp['str'];
				$tmp['strxml'] .= '</'.$tag.'>';
			}
		}
		return $tmp['strxml'];
	}
	function make_html_abbr($fieldname, $ar = array(), $tag = 'abbr')
	{
		$tmp['strhtml'] = '';
		$tmp['arEl'] = isset($this->arEl[$fieldname][$ar['elK']]) ? $this->arEl[$fieldname][$ar['elK']] : array();
		$prepend_name = 'prepend_'.$tag;
		$split_name = 'split_'.$tag;
		$append_name = 'append_'.$tag;
		$postlang_name = 'postlang_'.$tag;
		//
		// Load abbreviation/translation list
		if ($tag == 'abbr')
		{
			$tmp['cur_element'] = 'arAbbr'.$this->abbr_element_web;
			$tmp['arTags_1'] = $this->$tmp['cur_element'];
			$tmp['arTags_0'] = $this->arAbbr0;
		}
		elseif ($tag == 'trns')
		{
			$tmp['cur_element'] = 'arTrns'.$this->abbr_element_web;
			$tmp['arTags_1'] = $this->$tmp['cur_element'];
			$tmp['arTags_0'] = $this->arTrns0;
		}
		//
		// do auto fill
		//
		if (empty($tmp['arEl']))
		{
			$tmp['arEl'][0] = array('value' => '', 'attributes' => array('lang' => '--'));
		}
		//
		$tmp['ar_compiled'] = array();
		$i = 0;
		while (list($chK, $chV) = each($tmp['arEl']))
		{
			$i++;
			//
			$tmp['str_title'] = $tmp['str_acronym'] = '';
			$tmp['attributes'] = $this->objDom->get_attribute('lang', '', $chV);
			$tmp['str'] = $this->objDom->get_content( $chV );
			//
			// append new value for abbr
			if (isset($tmp['arTags_0'][$tmp['attributes']]) && isset($tmp['arTags_1'][$tmp['attributes']]))
			{
				$tmp['str_title'] = ' title="' . $tmp['arTags_0'][$tmp['attributes']] . '"';
				$tmp['str_acronym'] = $tmp['arTags_1'][$tmp['attributes']];
			}
			elseif ($tmp['attributes'] != '')
			{
				$tmp['str_title'] = ' title="' . $tmp['attributes'] . '"';
				$tmp['str_acronym'] = $tmp['attributes'];
			}
			//
			if (($tmp['str'] != '') || $tmp['attributes'] != '--')
			{

				#$this->Set('is_br_begin', 1);
				$tagname = 'tag_'.$tag;
				$tmp['ar_compiled'][$i] = ($tmp['str_title'] != '') ? '<'.$this->$tagname.$tmp['str_title'].'>' : '';
				$tmp['ar_compiled'][$i] .= $tmp['str_acronym'];
				$tmp['ar_compiled'][$i] .= ($tmp['str_title'] != '') ? '</'.$this->$tagname.'>' : '';
				$tmp['ar_compiled'][$i] .= ($tmp['str'] != '') ? $this->$postlang_name. '<span class="'.$tag.'">' . $tmp['str'] . '</span>' : '';
			}
		}
		if (!empty($tmp['ar_compiled']))
		{
			$tmp['strhtml'] .= $this->$prepend_name . implode($this->$split_name, $tmp['ar_compiled']) . $this->$append_name;
		}
		return $tmp['strhtml'];
	}
	function make_html_trns($fieldname, $ar = array(), $tag = 'trns')
	{
		return $this->make_html_abbr($fieldname, $ar, $tag);
	}
	//
	//
	//
	function make_xml_usg($fieldname, $ar = array())
	{
		return $this->make_xml_set_array2textarea($fieldname, $ar, 'usg');
	}
	//
	function make_html_usg($fieldname, $ar = array())
	{
		return $this->make_html_set_array2textarea($fieldname, $ar, 'usg');
	}
	//
	function make_xml_src($fieldname, $ar = array())
	{
		return $this->make_xml_set_textarea($fieldname, $ar, 'src');
	}
	//
	function make_html_src($fieldname, $ar = array())
	{
		return $this->make_html_set_textarea($fieldname, $ar, 'src');
	}
	//
	function make_xml_address($fieldname, $ar = array())
	{
		return $this->make_xml_set_textarea($fieldname, $ar, 'address');
	}
	function make_html_address($fieldname, $ar = array())
	{
		return $this->make_html_set_textarea($fieldname, $ar, 'address');
	}
	//
	function make_xml_phone($fieldname, $ar = array())
	{
		return $this->make_xml_set_textarea($fieldname, $ar, 'phone');
	}
	//
	function make_html_phone($fieldname, $ar = array())
	{
		return $this->make_html_set_textarea($fieldname, $ar, 'phone');
	}
	//
	function make_xml_see($fieldname, $ar = array(), $tag = 'see')
	{
		global $oFunc;
		$tmp['strxml'] = $tmp['str'] = '';
		$tmp['arEl'] = isset($this->arEl[$fieldname][$ar['elK']]) ? $this->arEl[$fieldname][$ar['elK']] : array();
		//
		// do auto fill
		//
		if (empty($tmp['arEl']))
		{
			$tmp['arEl'][0] = array('value' => '', 'attributes' => array('link' => ''));
		}
		//
		while (list($elK, $elV) = each($tmp['arEl']))
		{
			$tmp['isLinkSee'] = 0;
			if (isset($elV['attributes']['is_link']))
			{
				$tmp['isLinkSee'] = 1;
			}
			$elV = explode(CRLF, $elV['value']);
			while (list($k, $v) = each($elV))
			{
				$tmp['str_text'] = '';
				if ($v != '')
				{
					preg_match_all("/(.*)\[\[(.*?)\]\]/", $v, $pregV );
					$v = isset($pregV[1][0]) ? trim($pregV[1][0]) : trim($v);
					$tmp['str_text'] = isset($pregV[2][0]) ? $pregV[2][0] : '';
					$tmp['strxml'] .= '<'.$tag;
					$tmp['strxml'] .= ($tmp['isLinkSee'] ? ' link="'. strip_tags($v) . '"' : '');
					$tmp['strxml'] .= (($tmp['str_text'] != '') ? ' text="'. $tmp['str_text'] .'"' : '');
					$tmp['strxml'] .= '>';
					$tmp['strxml'] .= $v;
					$tmp['strxml'] .= '</'.$tag.'>';
				}
			}
		}
		return $tmp['strxml'];
	}
	function make_xml_syn($fieldname, $ar = array())
	{
		return $this->make_xml_see($fieldname, $ar, 'syn');
	}

	function make_html_see($fieldname, $ar = array(), $tag = 'see')
	{
		global $oFunc, $oHtml;
		$tmp['strxml'] = $tmp['str'] = '';
		$tmp['arEl'] = isset($this->arEl[$fieldname][$ar['elK']]) ? $this->arEl[$fieldname][$ar['elK']] : array();
		//
		// do auto fill
		//
		if (empty($tmp['arEl']))
		{
			$tmp['arEl'][0] = array('value' => '', 'attributes' => array('link' => ''));
		}
		$tmp['ar_compiled'] = array();
		$i = 0;
		//
		while (list($elK, $elV) = each($tmp['arEl']))
		{
			$tmp['isLink'] = isset($elV['attributes']['is_link']) ? 1 : 0;
			$elV['attributes']['text'] = isset($elV['attributes']['text']) ? $elV['attributes']['text'] : '';
			$tmp['str_text'] = '';
			$v = $elV['value'];
			if ($v != '')
			{
				$i++;
				$delimeter = ($i == 1) ? '' : $this->split_see;
				preg_match_all("/\[\[(.*?)\]\]/", $elV['attributes']['text'], $pregV );
				$tmp['str_text'] = isset($pregV[1][0]) ? $pregV[1][0] : '';
				$urlXref = GW_IS_BROWSE_WEB ? $oHtml->url_normalize($this->Gtmp['xref'] . urlencode(strip_tags($v))) : chmGetFilename(strip_tags($v));
#				$v = ($tmp['isLink'] ? $oHtml->a($this->Gtmp['xref'] . $urlXref, '<span class="linkmark">&#x25BA;</span>' . $v . '</a>') : $v);
				$v = ($tmp['isLink'] ? '<a href="' . $urlXref . '">' . '<span class="linkmark">&#x25BA;</span>' . $v . '</a>' : $v);
				$v .= (($tmp['str_text'] != '') ? '&#032;'.$tmp['str_text'] : '');
				$tmp['strxml'] .= $delimeter . $v;
			}
		}
		if ($tmp['strxml'] != '')
		{
			$tmp['strxml'] = '<br /><span class="gw'.$fieldname.'"><span class="f">' . $this->L->m($tag) . ':</span>&#32;' . $this->prepend_see . $tmp['strxml'] . $this->append_see.'</span>';
		}
		return $tmp['strxml'];
	}
	function make_html_syn($fieldname, $ar = array(), $tag = 'syn')
	{
		return $this->make_html_see($fieldname, $ar, $tag);
	}

	//
	function make_xml_set_textarea($fieldname, $ar = array(), $tag)
	{
		$tmp['strxml'] = $tmp['str'] = '';
		$tmp['arEl'] = isset($this->arEl[$fieldname][$ar['elK']]) ? $this->arEl[$fieldname][$ar['elK']] : array();
		//
		//
		$this->unsetTag('textarea'); // reset settings for <textarea>
		//
		while (list($elK, $elV) = each($tmp['arEl']))
		{
			$elV = explode(CRLF, $elV['value']);
			while (list($k, $v) = each($elV))
			{
				if ($v != '')
				{
					$tmp['str'] .= '<'.$tag.'>' . $v . '</'.$tag.'>';
				}
			}
		}
		$tmp['strxml'] .= $tmp['str'];
		return $tmp['strxml'];
	}
	//
	function make_html_set_textarea($fieldname, $ar = array(), $tag)
	{
		$tmp['str'] = '';
		$tmp['arEl'] = isset($this->arEl[$fieldname][$ar['elK']]) ? $this->arEl[$fieldname][$ar['elK']] : array();
		//
		//
		$tmp['ar_compiled'] = array();
		$i = 0;
		$split_name = 'split_'.$tag;
		//
		if (!empty($tmp['arEl']))
		{
			$tmp['str'] .= '<br /><span class="gw'.$fieldname.'"><span class="f">' . $this->L->m($tag) . ':</span>&#032; ';
			while (list($elK, $elV) = each($tmp['arEl']))
			{
				$elV = explode(CRLF, $elV['value']);
				while (list($k, $v) = each($elV))
				{
					if ($v != '')
					{
						$tmp['ar_compiled'][$i] = $v;
						$i++;
					}
				}
			}
			$tmp['str'] .= implode($this->$split_name, $tmp['ar_compiled']);
			$tmp['str'] .= '</span>';
		}
		return $tmp['str'];
	}


	//
	function make_xml_set_array2textarea($fieldname, $ar = array(), $tag)
	{
		global $oFunc;
		//
		$tmp['strform'] = $tmp['str'] = '';
		//
		for (reset($this->arEl[$fieldname]); list($elK, $elV) = each($this->arEl[$fieldname]);)
		{
			if (isset($elV['value']) && (intval($ar['elK']) == intval($elK)))
			{
				$elV = explode(CRLF, $elV['value']);
				while (list($k, $v) = each($elV))
				{
					if ($v != '')
					{
						$tmp['str'] .= '<'.$tag.'>' . $v . '</'.$tag.'>';
					}
				}
			}
			elseif (intval($ar['elK']) == intval($elK)) // multiarray
			{
				while (list($k, $v) = each($elV))
				{
					$tmp['str'] .= '<'.$tag.'>';
					$tmp['str'] .= $v['value'];
					$tmp['str'] .= '</'.$tag.'>';
				}
				#print '<br />' . $ar['elK'] . ' == ' . $elK;
			}
		}
		return $tmp['str'];
	}

	function make_html_set_array2textarea($fieldname, $ar = array(), $tag)
	{
		global $oFunc;
		//
		$tmp['str'] = '';
		//
		$tmp['ar_compiled'] = array();
		$i = 0;
		$split_name = 'split_'.$tag;
		$append_name = 'append_'.$tag;
		$prepend_name = 'prepend_'.$tag;

		if (!empty($this->arEl) && isset($this->arEl[$fieldname]))
		{
			//
			for (reset($this->arEl[$fieldname]); list($elK, $elV) = each($this->arEl[$fieldname]);)
			{
				if (isset($elV['value']) && (intval($ar['elK']) == intval($elK)))
				{
					$elV = explode(CRLF, $elV['value']);
					while (list($k, $v) = each($elV))
					{
						if ($v != '')
						{
							$tmp['str'] .= '['.$tag.']' . $v . '[/'.$tag.']';
						}
					}
				}
				elseif (intval($ar['elK']) == intval($elK)) // multiarray
				{
					$tmp['str'] .= '&#160;'; // IE: required for correct rendering <li>
					$tmp['str'] .= '<div class="gw'.$fieldname.'" title="'.$this->L->m($tag).'">';
					while (list($k, $v) = each($elV))
					{
						$tmp['ar_compiled'][$i] = $v['value'];
						$i++;
					}
					$tmp['str'] .= $this->$prepend_name . implode($this->$split_name, $tmp['ar_compiled']) . $this->$append_name;
					$tmp['str'] .= '</div>';
				}
			}
		}
		return $tmp['str'];
	}

	/**
	 * Parses structured array and converts it into XML-code
	 *
	 * @param   array  $arPre Fields content structure
	 * @return  string   XML-code (for database)
	 */
	function array_to_xml($arPre)
	{
		$strXml = '';
		$t = new gw_timer;
		// Go for each configured root field.
		for (reset($this->arFields); list($fK, $fV) = each($this->arFields);)
		{
			if (isset($fV[4]) && $fV[4]) // select root elements only here
			{
				//
				if ($fV[0] == 'term') // terms always presents
				{
					$strXml .= $this->tag2field_xml($fV[0]);
				}
				else
				{
					// other tags are switchable, parsed inside $form class
					if ($this->arDictParam['is_'.$fV[0]])
					{
						$strXml .= $this->tag2field_xml($fV[0]);
					}
				}
			} // end of root elements
		} // end of per-field process
		#$strXml = '<line>'.$strXml.'</line>';
		return $strXml;
	} // end of array_to_xml();
	/**
	 * Parses structured array and converts it into HTML-code
	 *
	 * @param   array  $arPre Fields content structure
	 * @return  string   HTML-code (for website)
	 */
	function array_to_html($arPre)
	{
		global $oHtml;
		$strHtml = '';
		#$t = new gw_timer;
		// Go for each configured root field.
		for (reset($this->arFields); list($fK, $fV) = each($this->arFields);)
		{
			if (isset($fV[4]) && $fV[4]) // select root elements only here
			{
				//
				if ($fV[0] == 'term') // terms always presents
				{
					$strHtml .= $this->tag2field_html($fV[0]);
				}
				else
				{
					// other tags are switchable, parsed inside $form class
					if ($this->arDictParam['is_'.$fV[0]])
					{
						$strHtml .= $this->tag2field_html($fV[0]);
					}
				}
			} // end of root elements
		} // end of per-field process
		// parse additinal tags
		// -------------------------------------------------
		// $tag_stress_rule
		$ar_pairs_src = explode("|", $this->tag_stress_rule);
		$strHtml = str_replace('<stress>', $ar_pairs_src[0], $strHtml);
		$strHtml = str_replace('</stress>', $ar_pairs_src[1], $strHtml);
		// -------------------------------------------------
		// additional tags in whole definition
		$tagsA = array('xref');
		$tagsAttrA = array('link');
		for (;list($kt, $vt) = each($tagsA);)
		{
			for (;list($ka, $va) = each($tagsAttrA);)
			{
				preg_match_all("/<$vt( $va=\"(.*?)\")*\>([^<]*?)\<\/$vt\>/", $strHtml, $strTmpA);
				if( isset($strTmpA[0]) )
				{
					for (;list($kd, $vd) = each($strTmpA[0]);)
					{
						$urlXref = GW_IS_BROWSE_WEB ? $oHtml->url_normalize($this->Gtmp['xref'] . urlencode(strip_tags($strTmpA[2][$kd]))) : chmGetFilename(strip_tags($strTmpA[2][$kd]));
						$tagXref = '<a href="' . $urlXref . '">' . '<span class="linkmark">&#x25BA;</span>' . $strTmpA[3][$kd] . '</a>';
#						$tagXref = $oHtml->a($this->Gtmp['xref'] . $urlXref, '<span class="linkmark">&#x25BA;</span>' . $strTmpA[3][$kd]);
						$strHtml = str_replace( $strTmpA[0][$kd], $tagXref, $strHtml);
					}
				} // end of isset
			} // end of each attribute
		}// end of each tag
		// -------------------------------------------------
		return $strHtml;
	} // end of array_to_xml();



} // end of class

?>