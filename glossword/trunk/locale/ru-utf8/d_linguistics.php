<?php
/**
 *  DO NOT EDIT. Alteration of this file may cause the Glossword translations to malfunction.
 *  For any changes, please, contact translators.
 *  Translation file generated by Translation Kit (http://glossword.info/tkit/)
 *  © 2002-2004 Dmitry Shilnikov <dev at glossword.info>
 *  ------------------------------------------------------
 *  Project  : Glossword translations
 *  Topic    : Linguistics
 *  Language : Russian
 *  Charset  : UTF-8
 *  Phrases  : 42
 *  Location : locale/ru-utf8/d_linguistics.php
 *  Created  : 09 Jul 2004, 16:55
 *  ------------------------------------------------------
 *  Translation created by:
 *      - Dmitry <dev at glossword.ru>
 */
$lang['190'] = array('имя собственное','собств.');
$lang['143'] = array('ироническое употребление','ирон.');
$lang['141'] = array('дополнение','доп.');
$lang['140'] = array('шутливо, шутливое выражение','шутл.');
$lang['127'] = array('средний род','ср.');
$lang['099'] = array('жаргонное слово, сленг','жарг.');
$lang['083'] = array('перфект','перф.');
$lang['081'] = array('повелительное наклонение глагола','повел.');
$lang['078'] = array('по слогам','по слогам');
$lang['050'] = array('устаревшее слово или выражение','устар.');
$lang['044'] = array('сокращение','сокр.');
$lang['042'] = array('редко употребляемое выражение или термин; редко употребляется в этом значении','редк.');
$lang['041'] = array('в распространенном, неточном значении','распр.');
$lang['040'] = array('разговорное слово или выражение','разг.');
$lang['035'] = array('в переносном значении','перен.');
$lang['033'] = array('народное','народн.');
$lang['029'] = array('множественное число','мн. ч.');
$lang['025'] = array('мужской род','м. р.');
$lang['015'] = array('женский род','ж. р.');
$lang['014'] = array('единственное число','ед. ч.');
$lang['008'] = array('буквально, в буквальном смысле','букв.');
/**
 * The end of translation file.
 */
?>