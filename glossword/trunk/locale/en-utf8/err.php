<?php
/**
 *  DO NOT EDIT. Alteration of this file may cause the Glossword to malfunction.
 *  For any changes, please, contact translators.
 *  Translation file generated by Translation Kit (http://glossword.info/tkit/)
 *  © 2002-2004 Dmitry Shilnikov <dev at glossword.info>
 *  ------------------------------------------------------
 *  Project  : Glossword
 *  Topic    : Error message
 *  Language : English
 *  Charset  : UTF-8
 *  Phrases  : 26
 *  Location : locale/en-utf8/err.php
 *  Created  : 14 Nov 2004, 14:11
 *  ------------------------------------------------------
 *  Translation created by:
 *      - Dmitry <dev at glossword.info>
 *      - Лхагвасүрэнгийн Хүрэлбаатар <hujii247 at yahoo.com>
 */
$lang['reason_26'] = 'Term does not exist.';
$lang['reason_25'] = 'Term already exists';
$lang['reason_24'] = 'Your account has been activated! 
Please check your email for new password.';
$lang['reason_23'] = 'Wrong activation key.';
$lang['reason_22'] = 'A new password has been created, please check your E-mail for details on how to activate it.';
$lang['reason_21'] = 'Sorry, but your account is currently inactive. Please contact the site administrator for more information.';
$lang['reason_20'] = 'You have specified an incorrect username or an invalid password.';
$lang['reason_19'] = 'Specified password is too long.';
$lang['reason_18'] = 'That E-mail address cannot be used as it already exists.';
$lang['reason_17'] = 'Can\'t send mail to %s.';
$lang['reason_16'] = 'Current account disabled or awaiting for activation.';
$lang['reason_15'] = 'Selected login already exists. Try another one.';
$lang['reason_14'] = 'The new passwords doesn\'t match. Try again.';
$lang['reason_13'] = 'Permission denied';
$lang['reason_12'] = 'No such topic';
$lang['reason_11'] = 'Can\'t remove';
$lang['reason_10'] = 'File `%s\' not found.';
$lang['reason_9'] = 'Incorrect';
$lang['reason_8'] = 'Function %s not installed.';
$lang['reason_6'] = 'Unable to load `iconv\' or `recode\' PHP-extension.';
$lang['reason_5'] = 'No matches.';
$lang['reason_4'] = 'No dictionaries found.';
$lang['reason_3'] = 'Dictionaries found in this topic.';
$lang['reason_2'] = 'More topics inside.';
$lang['reason_1'] = 'Last topic. At least one topic should exist.';
$lang['reason_7'] = 'No dictionaries found.';
/**
 * The end of translation file.
 */
?>