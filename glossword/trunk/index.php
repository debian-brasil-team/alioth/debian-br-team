<?php
/**
 *  $Id: index.php,v 1.20 2004/11/13 12:31:42 yrtimd Exp $
 */
/**
 *  Glossword - glossary compiler (http://glossword.info/dev/)
 *  � 2002-2004 Dmitry N. Shilnikov <dev at glossword.info>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  (see `glossword/support/license.html' for details)
 */
/* ------------------------------------------------------- */
/**
 *  Website
 */
/* ------------------------------------------------------- */
if (!defined('IN_GW'))
{
	define('IN_GW', true);
}
/* ------------------------------------------------------- */
/* Load configuration */
$sys['path_include'] = 'inc';
$sys['is_prepend'] = 1;
include_once('/etc/glossword/db_config.php');
include_once($sys['path_include'] . '/config.inc.php');

/* ------------------------------------------------------- */
/* Append system settings */
$sys = array_merge($sys, getSettings());

/* --------------------------------------------------------
 * mod_rewrite configuration
 * ------------------------------------------------------- */
$oHtml = new gw_html;
$oHtml->setVar('ar_url_append', $sys['ar_url_append']);
$oHtml->setVar('is_htmlspecialchars', 0);
$oHtml->setVar('is_mod_rewrite', $sys['is_mod_rewrite']);
$oHtml->setVar('mod_rewrite_suffix', ',xhtml');
$oHtml->setVar('server_dir', $sys['server_dir']);
$oHtml->setVar('mod_rewrite_rule',
	array('a=t' => '/a/d/q', 'a=term' => '/a/r/t,d', 'a=list' => '/a/d/p,w1,w2', 'a='.A_PRINT => '/a/q,t,d', 'a=srch' => '', '/a/t')
);
if (($sys['is_mod_rewrite']) && (!preg_match("/\.php/", REQUEST_URI)))
{
	parse_str($oHtml->url_dir2str(REQUEST_URI), $_GET);
}
/* ------------------------------------------------------- */
/* Auth class */
define('PERMLEVEL',     0);
define('GW_IS_BROWSE_WEB',    1);
define('GW_IS_BROWSE_ADMIN',  0);
/* --------------------------------------------------------
 * Templates
 * ----------------------------------------------------- */
$tpl = new gwTemplate();
$arTplVars['dict'] = $arTplVars['srch'] = array();
$tpl->addVal( "PATH_SERVER_URL", $sys['server_url'] );
$tpl->addVal( "PATH_SERVER_DIR", $sys['server_dir'] );
$tpl->addVal( "PATH_IMG",    $sys['path_img'] );
$tpl->is_tpl_show_names = $sys['is_tpl_show_names'];
/* --------------------------------------------------------
 * Register global variables
 * ----------------------------------------------------- */
$gw_this['vars'] = $oGlobals->register(array(
	GW_ACTION,'d','p','r','t','q','u','w1','w2','w','in','ie','srch','arPost','strict',
	'id_srch', 'post','voteID','name','email','url','cookie','is_gzip','cookie',
	GW_T_LANGUAGE, G_DLANGUAGE
));
$oGlobals->do_default($gw_this['vars']['is_gzip'], $sys['is_use_gzip']);
for (reset($gw_this['vars']); list($k1, $v1) = each($gw_this['vars']);)
{
	$$k1 = $v1;
}
$gw_this['vars']['class_render'] = 'gw_render';
/* switch between `/index.php?a=action' and `/action/' (Apache mod_rewrite) */
if ($sys['is_mod_rewrite'])
{
	if (preg_match("/index\.php/", REQUEST_URI))
	{
		// redirect to a directory
		if( (REQUEST_URI != $sys['page_index'])
			&& !preg_match("/a=preferences/", REQUEST_URI)
			&& !preg_match("/a=srch/", REQUEST_URI)
			&& !preg_match("/q=/", REQUEST_URI)
			&& !preg_match("/a=fb/", REQUEST_URI) ) // convert for mod_rewrite
		{
			gwtk_header('http://' . HTTP_HOST . $oHtml->url_normalize(REQUEST_URI), 0);
		}
	}
}
unset($arPostVars);

## ---------------------------------------------------------
## printable version
$isPrint = 0;
if (${GW_ACTION} == A_PRINT)
{
	${GW_ACTION} = 'term';
	$isPrint = 1;
}
##
## ---------------------------------------------------------

//
// Get the list of dictionaries
//
$gw_this['arDictList'] = getDictArray();
//

## ---------------------------------------------------------
## encrypted url
if (($t != '') && preg_match("/[a-f]/", $t))
{
	$u = $t;
	$udecoded = url_decrypt($sys['hideurl'], $u);
	if (!preg_match("/&d=/", $udecoded)){ $udecoded = url_decrypt(2, $u); }
	if (!preg_match("/&d=/", $udecoded)){ $udecoded = url_decrypt(1, $u); }
	if (!preg_match("/&d=/", $udecoded)){ $udecoded = url_decrypt(0, $u); }
	$u = $udecoded;
	if ($u != '')
	{
		$ar1 = split("&", $u);
		while(is_array($ar1) && list($k, $v) = each($ar1))
		{
			@list($k2, $v2) = split('=', $v);
			$$k2 = $v2;
		}
		$dictID = $d;
	}
	else
	{
		$layout = 'catalog';
	}
}
/* Not installed */
if (!isset($sys['version']))
{
	if (file_exists('./install.php'))
	{
		gwtk_header('http://' . HTTP_HOST . $sys['dirname'] . '/install.php', 0);
	}
	$sys['y_email'] = $sys['y_name'] = $sys['site_name'] = $sys['site_desc'] =
	$sys['is_polls'] = '';
	$sys['path_img'] = 'img';
	$sys['hideurl'] = $sys['time_new'] = $sys['time_upd'] = 1;
	$sys['version'] = 'not installed';
	$sys['locale_name'] = 'en-utf8';
}
if ($a != "poll"){ if ($sys['hideurl'] > 0){ $t = $t; } }

// filtering GET vars...
#$t      = str_replace('.xhtml', '', $t); // compatibility for 1.4.x links
$in = ($in == '') ? 'both' : $in;
$p = ($p == '') ? 1 : $p;
$dictID = 0;
if ($oFunc->is_num($d) && ($d > 0))
{
	$dictID = $d;
}
$layout = ( !empty($a) ) ? $a : 'title';
// Strict search conditions:
// Dictionary ID > 0 and current page is not Search
$strict = ($strict == '') ? 0 : $strict;
// default value for $strict is 1 for dictionary pages
$strict = ((${GW_ACTION} != SEARCH) && ($dictID > 0)) ? 1 : $strict;
$strict = ((${GW_ACTION} == SEARCH) && ($dictID > 0) && $post != '') ? 1 : $strict;
$dictID = ((${GW_ACTION} == SEARCH) && ($dictID > 0) && !$strict && ($id_srch == '')) ? 0 : $dictID;
## ---------------------------------------------------------

// --------------------------------------------------------
// Load extensions: 27 aug 2003
// --------------------------------------------------------
$gw_this['vars']['funcnames'][GW_T_TERM] = isset($gw_this['vars']['funcnames'][GW_T_TERM]) ? $gw_this['vars']['funcnames'][GW_T_TERM] : array();
$gw_this['vars']['funcnames'][GW_T_DICT] = isset($gw_this['vars']['funcnames'][GW_T_DICT]) ? $gw_this['vars']['funcnames'][GW_T_DICT] : array();

// --------------------------------------------------------
// Load visual theme, last modified: 07 aug 2003
// --------------------------------------------------------
// Conditions:
// 1) set theme name to 'gw_silver' if no any settings found
// 2) set theme name to system theme for non-dictionary pages (title, top10, feedback, search)
// 3) set theme name to system theme for non-strict search (all dictionaries)
// 4) set theme name from dictionary settings if a dictionary ID was specified, search page also included

$gw_this['vars']['themename'] = $sys['themename'] = isset($sys['themename']) ? $sys['themename'] : 'gw_silver';

$isThemeLoaded = 0;
if ($dictID > 0) // dictionary selected
{
	// Get dictionary settings
	$arDictParam = getDictParam($dictID);
	// Wrong dictionary ID number found, redirect
	if (empty($arDictParam))
	{
		gwtk_header($sys['server_proto'].$sys['server_host'].$sys['page_index']);
	}
	//
	$gw_this['vars']['themename'] = $arDictParam['themename'];
	$gw_this['vars']['themename'] = (${GW_ACTION} == SEARCH) ? $sys['themename'] : $gw_this['vars']['themename'];
	$gw_this['vars']['themename'] = ($strict == 1) ? $arDictParam['themename'] : $gw_this['vars']['themename'];
}

/* -------------------------------------------------------- */
/* Replace main page by title page */
if (($d == '') && ($a == ''))
{
	for (reset($gw_this['arDictList']); list($kDict, $vDict) = each($gw_this['arDictList']);)
	{
		$arDictParam = getDictParam($vDict['id']);
		if (isset($arDictParam['is_dict_as_index']) && $arDictParam['is_dict_as_index'])
		{
			$d = $dictID = $vDict['id'];
			$layout = 'list';
			/* Change theme name */
			$gw_this['vars']['themename'] = $arDictParam['themename'];
			break;
		}
		else
		{
			unset($arDictParam);
		}
	}
}


if (preg_match("/^[0-9a-z_-]+$/", $gw_this['vars']['themename']))
{
	$themefile = $sys['path_tpl'] . '/' . $gw_this['vars']['themename'] . '/theme.inc.' . $sys['phpEx'];
	if (file_exists($themefile))
	{
		$isThemeLoaded = 1;
		$sys['path_theme'] = $gw_this['vars']['themename'];
	}
}
if (!$isThemeLoaded)
{
	$sys['path_theme'] = $sys['themename'];
	$themefile = $sys['path_tpl'] . '/' . $sys['path_theme'] . '/theme.inc.' . $sys['phpEx'];
}
if (file_exists($themefile))
{
	include_once( $themefile );
}
else
{
	die('Can\'t find ' . $themefile);
}
$tpl->addVal( "V_THEME_NAME", $gw_this['vars']['themename']);
$tpl->addVal( "PATH_CSS",    $sys['server_dir'] . '/' . $sys['path_tpl'] . '/' . $gw_this['vars']['themename'] );
$tpl->addVal( "TABLEWIDTH",  $sys['tblwidth'] );
unset($themefile);
unset($isThemeLoaded);
/* --------------------------------------------------------
 * Set interface language, last modified: 10 mar 2004
 * ----------------------------------------------------- */
/* Get default system language */
$gw_this['vars'][GW_T_LANGUAGE] = preg_replace("/-([a-z0-9])+$/", '', $sys['locale_name']);
$gw_this['vars']['lang_enc'] = preg_replace("/^([a-z0-9])+-/", '', $sys['locale_name']);

/* Get dictionary language */
if (($dictID > 0) && isset($arDictParam['lang']))
{
	$gw_this['vars'][GW_T_LANGUAGE] = ((${GW_ACTION} == SEARCH) && !$strict)
									? $gw_this['vars'][GW_T_LANGUAGE]
									: preg_replace("/-([a-z0-9])+$/", '', $arDictParam['lang']);
	$gw_this['vars']['lang_enc'] = preg_replace("/^([a-z0-9])+-/", '', $arDictParam['lang']);
}

/* Cookie overwrites dictionary language */
if (isset($gw_this['cookie'][GW_T_LANGUAGE]) && $gw_this['cookie'][GW_T_LANGUAGE] == '')
{
	$gw_this['cookie'][GW_T_LANGUAGE] = $gw_this['vars'][GW_T_LANGUAGE];
}
else if (isset($gw_this['cookie'][GW_T_LANGUAGE]))
{
	$gw_this['vars'][GW_T_LANGUAGE] = $gw_this['cookie'][GW_T_LANGUAGE];
}
/* Custom language overwrites cookie */
if (${GW_T_LANGUAGE} != '')
{
	$gw_this['vars'][GW_T_LANGUAGE] = ${GW_T_LANGUAGE};
}
/* 25 sep 2002, Translation Kit */
$L = new gwtk;
$L->setHomeDir('locale');
$L->setLocale($gw_this['vars'][GW_T_LANGUAGE].'-'.$gw_this['vars']['lang_enc']);
/* Language files */
$L->getCustom('actions', $gw_this['vars'][GW_T_LANGUAGE].'-'.$gw_this['vars']['lang_enc'], 'join');
$L->getCustom('err',     $gw_this['vars'][GW_T_LANGUAGE].'-'.$gw_this['vars']['lang_enc'], 'join');
$L->getCustom('options', $gw_this['vars'][GW_T_LANGUAGE].'-'.$gw_this['vars']['lang_enc'], 'join');
$L->getCustom('status',  $gw_this['vars'][GW_T_LANGUAGE].'-'.$gw_this['vars']['lang_enc'], 'join');
$L->getCustom('tdb',     $gw_this['vars'][GW_T_LANGUAGE].'-'.$gw_this['vars']['lang_enc'], 'join');
$L->getCustom('tht',     $gw_this['vars'][GW_T_LANGUAGE].'-'.$gw_this['vars']['lang_enc'], 'join');
$L->getCustom('tol',     $gw_this['vars'][GW_T_LANGUAGE].'-'.$gw_this['vars']['lang_enc'], 'join');

// ---------------------------------------------------------
// Search engine, last modified 1 aug 2003
// ---------------------------------------------------------
// Set common variables for templates
$arTplVars['srch'] = array();

$arTplVars['srch'][] = array(
	'L_SRCH_TERM' => $L->m('srch_1'),
	'L_SRCH_DEFN' => $L->m('srch_0'),
	'L_SRCH_BOTH' => $L->m('srch_-1'),
	'L_SRCH_PHRASE' => $L->m('srch_phrase'),
	'L_SRCH_SUBMIT' => $L->m('3_srch_submit'),
);
//
// Feature list of dictionary ID
$gw_this['arDictListSrch'] = array();
//
// Search query, default values
if (!is_array($srch)) { $srch = array(); }
if (!isset($srch['by'])) { $srch['by'] = 'd'; } // search by dictionary
if (!isset($srch['in'])) { $srch['in'] = -1; }  // search in all elements
if (!isset($srch['adv'])) { $srch['adv'] = 'all'; } // seach all words
//
// Set switcher for HTML
$arTplVars['srch'][] = array('chk_srch_strict' => '' ); // 'this dictionary only'
$arTplVars['srch'][] = array('chk_srch_all' => '' );
$arTplVars['srch'][] = array('chk_srch_exact' => '' );
$arTplVars['srch'][] = array('chk_srch_any' => '' );
$arTplVars['srch'][] = array('chk_srch_in_both' => '' );

// Web parameters conversion
$srch['in'] = ($in == 'both') ? -1 : intval($srch['in']);
$srch['in'] = ($in == 'defn') ? 0 : intval($srch['in']);
$srch['in'] = ($in == 'term') ? 1 : intval($srch['in']);
$srch['in'] = ($in == 'syn') ? 7 : intval($srch['in']);
$srch['in'] = ($in == 'address') ? 9 : intval($srch['in']);
$srch['in'] = ($in == 'phone') ? 10 : intval($srch['in']);
//
// Go for each search option
if (isset($srch['adv']) && $srch['adv'] == 'all')
{
	$arTplVars['srch'][] = array('chk_srch_all' => ' checked="checked"' );
}
elseif (isset($srch['adv']) && $srch['adv'] == 'exact')
{
	$arTplVars['srch'][] = array('chk_srch_exact' => ' checked="checked"' );
}
elseif (isset($srch['adv']) && $srch['adv'] == 'any')
{
	$arTplVars['srch'][] = array('chk_srch_any' => ' checked="checked"' );
}
else // default
{
	$arTplVars['srch'][] = array('chk_srch_all' => ' checked="checked"' );
}
// Set switcher for HTML
// Where to search
if ($srch['in'] == '1')
{
	// search for term only
	$arTplVars['srch'][] = array('chk_srch_in_term' => ' checked="checked"' );
}
elseif (intval($srch['in']) == 0) // search in definitions
{
	$arTplVars['srch'][] = array('chk_srch_in_defn' => ' checked="checked"' );
}
elseif (intval($srch['in']) == -1) // search in terms and definitions, default
{
	$arTplVars['srch'][] = array('chk_srch_in_both' => ' checked="checked"' );
}
else // search for all fields
{
	$srch['in'] = array(0);
	$arTplVars['srch'][] = array('chk_srch_in_term' => ' checked="checked"' );
}
//
if (isset($srch['by']) && $srch['by'] == 'd')
{
	// Search by dictionary
	//
	// Set switcher for HTML
	$arTplVars['srch'][] = array('chk_srch_by_dict' => ' checked="checked"' );
	for (reset($gw_this['arDictList']); list($kDict, $vDict) = each($gw_this['arDictList']);)
	{
		if ($dictID == 0)
		{
			$gw_this['arDictListSrch'][] = $vDict['id'];
		}
		else
		{
			// TODO: search for multiple dictionaries
			$gw_this['arDictListSrch'][] = $dictID;
			break;
		}
	}
}
elseif (isset($srch['by']) && $srch['by'] == 'tp')
{
	// Search by topics
	//
	// Set switcher for HTML
	$arTplVars['srch'][] = array('chk_srch_by_topic' => ' checked="checked"' );
	// Get topic's tree ID
	$gw_this['arTreeId'] = ctlgGetTree($gw_this['arTopics'], intval($arPost['id_tp']));
	for (reset($gw_this['arDictList']); list($kDict, $vDict) = each($gw_this['arDictList']);)
	{
		// check if dictionary presents in selected topic
		if (isset($gw_this['arTreeId'][$vDict['p']]))
		{
			$gw_this['arDictListSrch'][] = $vDict['id'];
		}
	}
}
else /* default */
{
	$arTplVars['srch'][] = array('chk_srch_by_dict' => ' checked="checked"' );
}
/* */
if ($strict) // check box for 'this dictionary only'
{
	$arTplVars['srch'][] = array('chk_srch_strict' => ' checked="checked"' );
}
// --------------------------------------------------------
// Load custom variables.
// Allows to alter default settings and keep sources clear
if (@file_exists('custom_vars.' . $sys['phpEx']))
{
	include_once('custom_vars.' . $sys['phpEx']);
}
/* ------------------------------------------------------- */
/* Load custom page constructor */
if (@file_exists('./'. 'custom_construct.' .$sys['phpEx']))
{
	include_once('./'. 'custom_construct.' .$sys['phpEx']);
}
else
{
	include_once($sys['path_include']. '/constructor.inc.' .$sys['phpEx']);
}
/* ------------------------------------------------------- */
/* Referer log */
if ($sys['is_LogRef'])
{
	if (!isset($oLog))
	{
		$oLog = new gw_logwriter($sys['path_logs']);
	}
	if ( ($HTTP_REF != '') && (!preg_match("/" . HTTP_HOST . '/', $HTTP_REF) ) )
	{
		$oLog->remote_ua = REMOTE_UA;
		$oLog->remote_ip = $oFunc->ip2int(REMOTE_IP);
		$oFunc->file_put_contents($oLog->get_filename('ref'), $oLog->get_str($HTTP_REF), 'a');
	}
}
/* ------------------------------------------------------- */
/* Custom constructor may change locale settings */
$L->setLocale($gw_this['vars'][GW_T_LANGUAGE].'-'.$gw_this['vars']['lang_enc']);
/* Last header */
$oHdr->add('Content-Type: '.$sys['content_type'].'; charset='.$L->languagelist('2'));
/* ------------------------------------------------------- */
/* Debug information */
if (DEBUG)
{
	include($sys['path_include'] . '/page.footer.' . $sys['phpEx']);
}
/* ------------------------------------------------------- */
/* Compile HTML-template */
$tpl->parse();
$str_output = $tpl->output();
/* Process text filters */
while (!$sys['is_debug_output']
		&& is_array($sys['filters_output'])
		&& list($k, $v) = each($sys['filters_output']) )
{
	$str_output = $v($str_output);
}


/* --------------------------------------------------------
 * GZip compression
 * ----------------------------------------------------- */
if ($sys['is_use_gzip'])
{
	/* Start time */
	if ($sys['is_LogGZip'])
	{
		$arLogGzip = array();
		if (!isset($oLog))
		{
			$oLog = new gw_logwriter($sys['path_logs']);
		}
		$arLogGzip[1] = strlen($str_output);
		$arLogGzip[3] = REMOTE_UA;
		$oTimer = new gw_timer('gzip');
	}
	/* requires $oHdr */
	$str_output = $oFunc->text_gzip($str_output, 9);
	/* End time */
	if ($sys['is_LogGZip'])
	{
		$arLogGzip[0] = sprintf("%1.5f", $oTimer->end());
		$arLogGzip[2] = strlen($str_output);
		if ($arLogGzip[1] != $arLogGzip[2])
		{
			$oFunc->file_put_contents($oLog->get_filename('gzip'), $oLog->make_str($arLogGzip), 'a');
		}
	}
}
/* --------------------------------------------------------
 * Final output
 * ----------------------------------------------------- */
$oHdr->output();
print $str_output;

?>