<?php
/*
Settings for Glossword 
*/
//
// USER ADMIN IDENTIFICATION
//
// user name of admin
define('GW_DEB_ADMIN_NAME', 'Marcio Teixeira');
// email of admin
define('GW_DEB_ADMIN_EMAIL', 'marciotex'.'@'.'pop.com.br');
// login name of admin
define('GW_DEB_ADMIN_LOGIN', 'gwdebadmin');
// password of admin
define('GW_DEB_ADMIN_PASSWD', 'passwd');
//
// $server_url CONFIG: don't touch, except if you know
// the implications
//
// $server_url will be
// GW_DEB_SERVER_PROTO + GW_DEB_SERVER_HOST + GW_DEB_SERVER_DIR
// e.g. http://127.0.0.1/glossword
//
// http:// ou https:// (e.g., apache-ssl)
define('GW_DEB_SERVER_PROTO', 'http://');
define('GW_DEB_SERVER_HOST', '127.0.0.1');
define('GW_DEB_SERVER_DIR', '/glossword');
//
// db_config
//
// Where's running mysql server?
// localhost? 127.0.0.1? 192.168.1.2? 200.123.45.23?
define('GW_DB_HOST', 'localhost');
// What's database name?
define('GW_DB_DATABASE', '`glossword`');
// What's table prefix?
define('GW_TBL_PREFIX', 'gw_');
// Database owner: who?
// Remember: user needs permissions to access and create
// databases on server
define('GW_DB_USER', 'root');
// password database owner: CONFIG ME!
define('GW_DB_PASSWORD', '<passdb>');
?>
