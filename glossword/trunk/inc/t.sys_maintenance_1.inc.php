<?php
if (!defined('IN_GW'))
{
	die('<!-- $Id: t.sys_maintenance_1.inc.php,v 1.1 2004/11/13 12:30:09 yrtimd Exp $ -->');
}
/**
 *  Glossword - glossary compiler (http://glossword.info/dev/)
 *  � 2002-2004 Dmitry N. Shilnikov <dev at glossword.info>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  (see `glossword/support/license.html' for details)
 */
/* Check permission */
if (!$auth->have_perm('admin', PERMLEVEL))
{
	return;
}
/* Script variables below */
global $name, $email, $id_topic;
$ar_msg_topics = array(
	1 => $oL->m('1029'),
	2 => $oL->m('1030'),
	3 => $oL->m('1031'),
	4 => $oL->m('1032'),
	5 => $oL->m('1033'),
	6 => $oL->m('1034'),
);
/* Script functions below */
function gw_get_support_form_send($vars)
{
	global $oL;
	$str = '';
	$v_mailto = 'dev@glossword.info';
	$str_mail_body = '<xml version="1.0">';
	for (; list($k, $v) = each($vars);)
	{
		$str_mail_body .= '<'.$k .'><![CDATA[ '. $v . ']]></'.$k .'>';
	}
	$str_mail_body .= '</xml>';
	$str_mail_body = htmlspecialchars($str_mail_body);
	if (get_magic_quotes_gpc()){ $str_mail_body = gw_stripslashes($str_mail_body); }
	/* to e-mail, from e-mail, subject, text, isDebug */
	if (kMailTo($v_mailto, $vars['email'],
				'Glossword report',
				$str_mail_body, 0))
	{
		$str = '<br/>'.$oL->m('fb_complete');
	}
	else
	{
		$str .= sprintf('<br/><span class="red u">'.$oL->m('reason_17').'</span>', $v_mailto);
		$str .= '<br/><br/><div class="t">';
		$str .= $str_mail_body;
		$str .= '</div>';
	}
	return $str;
}
function gw_get_support_form_confirm($vars)
{
	global $oL, $oDb, $user, $sys, ${GW_ACTION}, ${GW_TARGET}, ${GW_SID}, $tid, $oFunc, $gw_this, $ar_msg_topics;

	$str = '';

	$vars['message'] = nl2br($vars['message']);

	$form = new gwForms();
	$form->Set('action', $sys['page_admin']);
	$form->Set('submitok', $oL->m('1036'));
	$form->Set('submitcancel', $oL->m('3_cancel'));
	$form->Set('formbgcolor', $GLOBALS['theme']['color_2']);
	$form->Set('formbordercolor', $GLOBALS['theme']['color_4']);
	$form->Set('formbordercolorL', $GLOBALS['theme']['color_1']);
	$form->Set('align_buttons', $GLOBALS['sys']['css_align_right']);
	$form->Set('charset', $sys['internal_encoding']);

	$trClass = 't';
	$strForm = '';
	$strForm .= '<table cellspacing="3" cellpadding="0" border="0" width="100%"><tbody>';
	$strForm .= '<tr class="'.$trClass.'"><td>'.$oL->m('y_name').'</td><td>'.$vars['name'].'</td></tr>';
	$strForm .= '<tr class="'.$trClass.'"><td>'.$oL->m('y_email').'</td><td>'.$vars['email'].'</td></tr>';
	$strForm .= '<tr class="'.$trClass.'"><td>'.$oL->m('1025').'</td><td>'.$vars['gw_version'].'</td></tr>';
	$strForm .= '<tr class="'.$trClass.'"><td>'.$oL->m('1026').'</td><td>'.$vars['php_version'].'</td></tr>';
	$strForm .= '<tr class="'.$trClass.'"><td>'.$oL->m('1027').'</td><td>'.$vars['php_os'].'</td></tr>';
	$strForm .= '<tr class="'.$trClass.'"><td>'.$oL->m('1028').'</td><td>'.$vars['db_version'].'</td></tr>';
	$strForm .= '<tr class="'.$trClass.'"><td>'.$oL->m('topic').'</td><td>'.$ar_msg_topics[$vars['id_topic']].'</td></tr>';
	$strForm .= '<tr style="vertical-align:top"><td class="'.$trClass.'">'.$oL->m('message').'</td><td class="u">'. $vars['message'] .'</td></tr>';
	if ($vars['is_attach'])
	{
		$strForm .= '<tr class="'.$trClass.'"><td>'.$oL->m('options').'</td><td>'.$oL->m(1024).'</td></tr>';
	}
	$strForm .= '<tr><td style="width:20%"></td><td>';
		for (; list($k, $v) = each($vars);)
		{
			if (!$vars['is_attach'] && ($k == 'sys_info'))
			{
				continue;
			}
			$strForm .= $form->field('hidden', 'arPost['. $k .']', htmlspecialchars($v));
		}
		$strForm .= $form->field('hidden', GW_ACTION, ${GW_ACTION});
		$strForm .= $form->field('hidden', 'tid', $tid);
		$strForm .= $form->field('hidden', GW_TARGET, ${GW_TARGET});
		$strForm .= $form->field('hidden', GW_SID, ${GW_SID});
		$strForm .= $form->field('hidden', 'is', 2);
	$strForm .= '</td></tr>';

	$strForm .= '</tbody></table>';
	return $form->Output($strForm);
	return $str;
}
function gw_get_support_form($vars)
{
	global $oL, $oDb, $user, $sys, ${GW_ACTION}, ${GW_TARGET}, ${GW_SID}, $tid, $oFunc, $gw_this, $ar_msg_topics;

	$arSql = $oDb->sqlExec('SELECT version() as v');

	$form = new gwForms();
	$form->Set('action', $sys['page_admin']);
	$form->Set('submitok', $oL->m('1035'));
	$form->Set('submitcancel', $oL->m('3_cancel'));
	$form->Set('formbgcolor', $GLOBALS['theme']['color_2']);
	$form->Set('formbordercolor', $GLOBALS['theme']['color_4']);
	$form->Set('formbordercolorL', $GLOBALS['theme']['color_1']);
	$form->Set('align_buttons', $GLOBALS['sys']['css_align_right']);
	$form->Set('charset', $sys['internal_encoding']);

	$trClass = 't';
	$strForm = '';

	$strForm .= '<table cellspacing="3" cellpadding="0" border="0" width="100%"><tbody>';
	$strForm .= '<tr><td style="width:20%" class="'.$trClass.'">'.$oL->m('y_name').'</td>';
	$strForm .= '<td>'.
				$form->field('input', 'name', $user->get("user_name")).
				'</td></tr>';
	$strForm .= '<tr><td class="'.$trClass.'">'.$oL->m('y_email').'</td>';
	$strForm .= '<td>'.
				$form->field('input', 'email', $user->get("user_email")).
				'</td></tr>';
	$strForm .= '<tr><td class="'.$trClass.'">'.$oL->m('1025').'</td>';
	$strForm .= '<td class="disabled">'.
				$sys['version'].
				$form->field('hidden', 'arPost[gw_version]', $sys['version']).
				'</td></tr>';
	$strForm .= '<tr><td class="'.$trClass.'">'.$oL->m('1026').'</td>';
	$strForm .= '<td class="disabled">'.
				PHP_VERSION.
				$form->field('hidden', 'arPost[php_version]', PHP_VERSION).
				'</td></tr>';
	$strForm .= '<tr><td class="'.$trClass.'">'.$oL->m('1027').'</td>';
	$strForm .= '<td class="disabled">'.
				PHP_OS.
				$form->field('hidden', 'arPost[php_os]', PHP_OS).
				'</td></tr>';
	$strForm .= '<tr><td class="'.$trClass.'">'.$oL->m('1028').'</td>';
	$strForm .= '<td class="disabled">'.
				(isset($arSql[0]['v']) ? $arSql[0]['v'] : '').
				$form->field('hidden', 'arPost[db_version]', (isset($arSql[0]['v']) ? $arSql[0]['v'] : '')).
				'</td></tr>';
	$strForm .= '<tr><td class="'.$trClass.'">'.$oL->m('topic').'</td>';
	$strForm .= '<td>'.
				htmlFormsSelect($ar_msg_topics, $vars['id_topic'], 'id_topic', '', 'width:20em', $oL->languagelist("1")).
				'</td></tr>';
	$strForm .= '<tr style="vertical-align:top"><td class="'.$trClass.'">'.$oL->m('message').'</td>';
	$strForm .= '<td>';
	$strForm .= '<textarea name="arPost[message]" style="height:15em;border:1px solid #BCC8E2;overflow:auto;width:100%">'.
				htmlspecialchars($vars['message']).
				'</textarea>';
	$strForm .= '</td></tr>';
	$strForm .= '<tr><td colspan="2">';
	/* */
	$strForm .= '<table cellspacing="1" cellpadding="0" border="0" width="100%">';
	$strForm .= '<col width="1%"/><col width="99%"/><tbody>';
	$strForm .= '<tr>'.
				'<td class="' . $trClass . '">' .
				$form->field('checkbox', "arPost[is_attach]", $vars['is_attach']) .
				'</td><td class="' . $trClass . '">'.
				'<label for="arPost_is_attach_">'.
				$oL->m(1024) .
				'</label></td>'.
				'</tr>';
	$strForm .= $form->field('hidden', GW_ACTION, ${GW_ACTION});
	$strForm .= $form->field('hidden', 'tid', $tid);
	$strForm .= $form->field('hidden', GW_TARGET, ${GW_TARGET});
	$strForm .= $form->field('hidden', GW_SID, ${GW_SID});
	$strForm .= $form->field('hidden', 'is', 1);
	$strForm .= '</tbody></table>';
	/* */
	$strForm .= '</td></tr>';
	$strForm .= '<tr><td colspan="2">';
	$strForm .= '<div style="height:10em;border:1px solid #BCC8E2;overflow:auto;width:100%;color:#777;background:#FFF;font:70% verdana,arial,sans-serif">'.
				htmlspecialchars(gw_get_cfg()).
				$form->field('hidden', 'arPost[sys_info]', htmlspecialchars(gw_get_cfg())).
				'</div>';
	$strForm .= '</td></tr>';

	$strForm .= '</tbody></table>';
	return $form->Output($strForm);
}
/* */
function gw_get_cfg()
{
	global $sys, $oDb;
	/* */
	$arSql = $oDb->sqlExec('SELECT version() as v');
	$arInfoA = array(
	'{API}'                  => php_sapi_name(),
	'{DOCUMENT_ROOT}'        => getenv('DOCUMENT_ROOT'),
	'{SCRIPT_FILENAME}'      => getenv('SCRIPT_FILENAME'),
	'{SERVER_SOFTWARE}'      => getenv('SERVER_SOFTWARE'),
	'{REQUEST_URI}'          => preg_replace('/[0-9a-f]{32}/', '', REQUEST_URI),
	'{sys_server}'           => $sys['server_proto'].$sys['server_host'].$sys['server_dir'],
	);
	$arInfoT = array(
	'{expose_php}'           => (int) ini_get("expose_php"),
	'{magic_quotes_gpc}'     => (int) ini_get("magic_quotes_gpc"),
	'{magic_quotes_runtime}' => (int) ini_get("magic_quotes_runtime"),
	'{magic_quotes_sybase}'  => (int) ini_get("magic_quotes_sybase"),
	'{max_execution_time}'   => (int) ini_get("max_execution_time"),
	'{post_max_size}'        => (int) ini_get("post_max_size"),
	'{register_globals}'     => (int) ini_get("register_globals"),
	'{safe_mode}'            => (int) ini_get("safe_mode"),
	'{short_open_tag}'       => (int) ini_get("short_open_tag"),
	'{mbstring.internal_encoding}' => ini_get('mbstring.internal_encoding')
	);
	$str = '';
	$str .= '<line>'.CRLF;
	$str .= '<term>'.$sys['server_host'].'</term>'.CRLF;
	$str .= '<defn>'.CRLF;
	while (list($k, $v) = each($arInfoA))
	{
		$str .= '<abbr lang="'.$k.'">'.$v.'</abbr>'.CRLF;
	}
	while (list($k, $v) = each($arInfoT))
	{
		$str .= '<trns lang="'.$k.'">'.$v.'</trns>'.CRLF;
	}
	if (function_exists('get_loaded_extensions'))
	{
		$ar = get_loaded_extensions();
		sort($ar);
		while(list($k, $v) = each($ar))
		{
			$str .= '<see>'.$v.'</see>'.CRLF;
		}
	}
	$str .= '</defn>'.CRLF;
	$str .= '</line>'.CRLF;
	return $str;
}
/* Script action below */
$strR .= getFormTitleNav($oL->m(1001));

$arPost['is_attach'] = isset($arPost['is_attach']) ? $arPost['is_attach'] : 0;

if ($is == '1')
{
	$arPost['name'] = $name;
	$arPost['email'] = $email;
	$arPost['id_topic'] = $id_topic;
	$strR .= gw_get_support_form_confirm($arPost);
}
elseif ($is == '2')
{
	$strR .= gw_get_support_form_send($arPost);
}
else
{
	$arPost['id_topic'] = 1;
	$arPost['is_attach'] = 1;
	$arPost['message'] = '';
	$strR .= gw_get_support_form($arPost);
}


?>