<?php
if (!defined('IN_GW'))
{
	die('<!-- $Id: func.catalog.inc.php,v 1.12 2004/11/16 13:26:30 yrtimd Exp $ -->');
}
/**
 *  Glossword - glossary compiler (http://glossword.info/dev/)
 *  � 2002-2004 Dmitry N. Shilnikov <dev at glossword.info>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  (see `glossword/support/license.html' for details)
 */
// --------------------------------------------------------
/**
 *  Catalog functions.
 *
 *  @package gw_admin
 */
// --------------------------------------------------------


/**
 * Main function for listing terms
 *
 * @param    string  $w1     First search string
 * @param    string  $w2     Second search string
 * @param    int     $id_dict Dictionary ID
 * @param    int     $p      Current page number
 * @param    bool    $descr  True: display description
 * @param    bool    $full   True: display full mode
 * @param    bool    $kb     True: display size in kbytes
 * @return   array   [0] -> string, term items, [1] -> integer, total terms
 */
function getDictWordList($w1, $w2, $id_dict, $p, $is_descr = true, $is_full = false)
{
	global $oDb, $oSqlQ, $oHtml, $oFunc;
	global $L, $sys, $arDictParam;

	$strA = array(0 => '', 1 => '', 2 => '', 3=> '');

	$page_index = $sys['page_index'];
	
	// 0-Z
	if (($w1 != '') && ($w2 == ''))
	{
		$sql = $oSqlQ->getQ('cnt-term-by-t1', $arDictParam['tablename'], gw_text_sql($w1));
	}
	elseif (($w1 != '') && ($w2 != ''))
	{
		$sql = $oSqlQ->getQ('cnt-term-by-t1t2', $arDictParam['tablename'], gw_text_sql($w1), gw_text_sql($w2));
	}
	else
	{
		return $strA;
	}
	$arSql = $oDb->sqlExec($sql, sprintf("%05d", $id_dict), 0);

	$strA[1] = isset($arSql[0]['n']) ? $arSql[0]['n'] : 0;
	$strA[3] = $oFunc->math_divisor($strA[1], $sys['page_limit']);
	if ( ( $p < 1 ) || ( $p > $strA[3]) ){ $p = 1; }
	$limit = $oDb->prn_limit($strA[1], $p, $sys['page_limit']);
	/* */
	$sql_defn = 'SUBSTRING(defn, 1, 2048) AS defn';
	if ($is_full)
	{
		$sql_defn = 'defn';
	}
	if (($w1 != '') && ($w2 == ''))
	{
		$sql = $oSqlQ->getQ('get-term-by-t1', $sql_defn, $arDictParam['tablename'], gw_text_sql($w1)) . $limit;
	}
	else if (($w1 != '') && ($w2 != ''))
	{
		$sql = $oSqlQ->getQ('get-term-by-t1t2', $sql_defn, $arDictParam['tablename'], gw_text_sql($w1), gw_text_sql($w2)) . $limit;
	}

	$arSql = $oDb->sqlExec($sql, sprintf("%05d", $id_dict), 0);

	$arA = $xmlVals = array();
	$cnt = 0;
	$delmtr = " ";

	if ($is_full)
	{
		global $gw_this, $arFields;
		
		$tmp['cssTrClass'] = 't';
		$tmp['xref'] = $sys['page_index'] . '?a=term&amp;d=' . $id_dict . '&amp;q=';
		$tmp['href_srch_term'] = $sys['page_index'] . '?a=srch&amp;in=term&amp;d=%d&amp;q=%s&amp;strict=1';
		$tmp['href_link_term'] = $sys['page_index'] . '?a=term&amp;d=%d&amp;q=%s';
		$arDictParam['lang'] = $gw_this['vars'][GW_T_LANGUAGE].'-utf8';

		$oRender = new $gw_this['vars']['class_render'];
		$oRender->Set('Gsys', $sys );
		$oRender->Set('L', $L );
		$oRender->Set('arDictParam', $arDictParam );
		$oRender->Set('arFields', $arFields );
		$oRender->load_abbr_trns();
			
		for (; list($arK, $arV) = each($arSql);)
		{
			$arA[$arK]['defn'] = $arA[$arK]['term'] = '';
			// Render HTML page, 25 apr 2003
			//
			$arPre = gw_Xml2Array($arV['defn']);
			$tmp['term'] = $arV['term'];
			$tmp['t1'] = '';
			$tmp['t2'] = '';
			$tmp['tid'] = $arV['t_id'];
			//
			$objDom = new gw_domxml;
			$objDom->setCustomArray($arPre);
			$oRender->Set('Gtmp', $tmp );
			$oRender->Set('objDom', $objDom );
			$oRender->Set('arEl', $arPre );
			//
			$tmp['str_defn'] = $oRender->array_to_html($arPre);
			/* Process text filters */
			while (!$sys['is_debug_output'] 
					&& is_array($sys['filters_defn']) 
					&& list($k, $v) = each($sys['filters_defn']) )
			{
				$tmp['str_defn'] = $v($tmp['str_defn']);
			}
			//
			$arA[$arK]['term'] =& $arV['term'];
			$arA[$arK]['defn'] = $tmp['str_defn'];
			$arA[$arK]['t_id'] =& $arV['t_id'];
			$arA[$arK]['d_id'] =& $id_dict;

			/* $tag_stress_rule */
			$ar_pairs_src = explode("|", $oRender->tag_stress_rule);
			$arA[$arK]['term'] = str_replace('<stress>', $ar_pairs_src[0], $arA[$arK]['term']);
			$arA[$arK]['term'] = str_replace('</stress>', $ar_pairs_src[1], $arA[$arK]['term']);
			$arA[$arK]['defn'] = str_replace('<stress>', $ar_pairs_src[0], $arA[$arK]['defn']);
			$arA[$arK]['defn'] = str_replace('</stress>', $ar_pairs_src[1], $arA[$arK]['defn']);

		}
	}
	else
	{
		
	for (; list($arK, $arV) = each($arSql);)
	{
		//
		$arV["term"] = str_replace('<br />', ' - ', $arV['term']);
		//
		$arA[$arK]['len']  = $arV["len"];
		// -------------------------------------------------
		// $tag_stress_rule
		$ar_pairs_src = explode('|', '<span class="stress">|</span>');
		$arV['term'] = str_replace('<stress>', $ar_pairs_src[0], $arV['term']);
		$arV['term'] = str_replace('</stress>', $ar_pairs_src[1], $arV['term']);
		// --------------------------------------------------
		$arA[$arK]['term'] = strip_tags($arV['term'], '<span>');
		// removes {TEMPLATES} from term
		$arA[$arK]['term'] = preg_replace("/\{([A-Za-z0-9:\-_]+)\}/", '', $arA[$arK]['term']);

		$arA[$arK]['t_id'] =& $arV['t_id'];
		$arA[$arK]['d_id'] =& $id_dict;

		if ($arV['defn'] != '')
		{
			// Definitions list
			$arA[$arK]['defn'] = $arV['defn'];
			// removes {TEMPLATES} and {TEMPLATES}:
			$arA[$arK]['defn'] = preg_replace("/\{([A-Za-z0-9:\-_]+)\}([:])*/", '-', $arA[$arK]['defn']);

			// prepare &#nnn(nn) for xml_parse();
			$arA[$arK]['defn'] = preg_replace('/&#[x0-9a-f]+;/', ' ', $arA[$arK]['defn']); // fix
			$arA[$arK]['defn'] = preg_replace('/&[a-z]+;/', ' ', $arA[$arK]['defn']);  // fix

			$arA[$arK]['defn'] = str_replace('><', '> <', $arA[$arK]['defn']);
			$arA[$arK]['defn'] = str_replace('</defn> <defn>', ' &amp;#9674; ', $arA[$arK]['defn']);

			$encoding = (strtoupper($sys['internal_encoding']) == 'UTF-8') ? 'UTF-8' : 'ISO-8859-1';
			$xmlP = xml_parser_create($encoding);
			xml_parse_into_struct($xmlP, '<xml_parse>'.$arA[$arK]['defn'].'</xml_parse>', $xmlVals, $xmlIndex);
			xml_parser_free($xmlP);

			$arA[$arK]['defn'] = '';
			$cntAbbr = $cntTrns = $cntSee = $cntSyn = $cntTrsp = 1;
			$cnt_usg = 1;
			$strSee = $strSyn = '';
			for (;list($k1, $v1) = each($xmlVals);)
			{
				if ($v1['tag'] == "TRSP")
				{
					if (isset($v1['value']))
					{
						if ($cntTrsp == 1)
						{
							$arA[$arK]['defn'] .= '<span class="trsp">[' . $v1['value'] . ']</span>';
						}
						$arA[$arK]['defn'] .= '&#32;';
					}
					$cntTrsp++;
				}
				elseif ($v1['tag'] == "ABBR")
				{
					if (isset($v1['value']))
					{
						if ($cntAbbr == 1)
						{
							$arA[$arK]['defn'] .= '<span class="f">' . $v1['value'] . '</span>';
						}
						elseif($v1['value'] != "")
						{
							$arA[$arK]['defn'] .= $v1['value'];
						}
						$arA[$arK]['defn'] .= "&#160;";
					}
					$cntAbbr++;
				}
				elseif ($v1['tag'] == "TRNS")
				{
					if ( isset($v1['type']) && (($v1['type'] == 'open')||($v1['type'] == 'complete')))
					{
						$arA[$arK]['defn'] .= '&#32;';
					}
					if (isset($v1['value']))
					{
						if ($cntTrns == 1)
						{
							$arA[$arK]['defn'] .= '<span class="f">' . $v1['value'] . '</span> ';
						}
						elseif($v1['value'] != '')
						{
							$arA[$arK]['defn'] .= $v1['value'];
						}
					}
					if ( isset($v1['type']) && ($v1['type'] == 'close'))
					{
						$arA[$arK]['defn'] .= '&#32;';
					}
					$cntTrns++;
				}
				elseif ($v1['tag'] == "LI")
				{
					if (isset($v1['value']))
					{
						$arA[$arK]['defn'] .= '-' . $v1['value'] . $delmtr;
					}
				}
				elseif ($v1['tag'] == "STRESS")
				{
					if (isset($v1['value']))
					{
						$arA[$arK]['defn'] .= '<span class="stress">'.$v1['value'].'</span>';
					}
				}
				elseif ($v1['tag'] == "USG")
				{
					if ( isset($v1['type']) && (($v1['type'] == 'open')||($v1['type'] == 'complete')))
					{
						$arA[$arK]['defn'] .= ' ';
					}
					if (isset($v1['value']))
					{
						$arA[$arK]['defn'] .= $v1['value'];
					}
				}
				elseif ($v1['tag'] == "SEE")
				{
					if (isset($v1['value']) && (trim($v1['value']) != '') )
					{
						if ($cntSee == 1)
						{
							$strSee .= '&#32;<span class="f">' . $GLOBALS['L']->m('see') . ':</span> ' . $v1['value'];
						}
						else
						{
							$strSee .= '; '. $v1['value'];
						}
					}
					$cntSee++;
				}
				elseif ($v1['tag'] == "SYN")
				{
					if (isset($v1['value']))
					{
						if ($cntSyn == 1)
						{
							$strSyn .= '<span class="f">' . $GLOBALS['L']->m('syn') . ':</span> ' . $v1['value'];
						}
						else
						{
							$strSyn .= "; ". $v1['value'];
						}
					}
					$cntSyn++;
				}
				elseif ($v1['tag'] == "SRC")
				{
				}
				else
				{
					if (isset($v1['value']))
					{
						$arA[$arK]['defn'] .= $v1['value'];
					}
				}
			} // $xmlVals
			// 1.3.6
			$arA[$cnt]['defn'] = preg_replace("/&#160;([^<])/", " \\1", $arA[$cnt]['defn']);
			$defn_len1 = ($arA[$cnt]['len'] + $oFunc->mb_strlen($strSee . $strSyn));
			$defn_len2 = $oFunc->mb_strlen(strip_tags(str_replace("&#160;", " ", $arA[$arK]['defn'] . $strSee . $strSyn))) - 7;
			$defn_len3 = $oFunc->mb_strlen($strSee . $strSyn);

			if ($defn_len2 > $GLOBALS['sys']['defn_cut'] )
			{
				$arA[$arK]['kb']  = $oFunc->number_format($arA[$cnt]['len'] / 1024, 1, $L->languagelist('4')) . "&#160;" . $L->m('kb');

				$posSafe = ($GLOBALS['sys']['defn_cut'] + $defn_len3);

				// isset $strSee or $strSyn
				if ($defn_len3 > 1)
				{
					$posSpan = strpos($arA[$arK]['defn'], '</span>');
					if (!$posSpan){ $posSpan = $GLOBALS['sys']['defn_cut']; }
					if (($posSafe - $defn_len3) < $posSpan)
					{
						$posSafe = $posSpan + 7; ## strlen("</span>");
					}
					else
					{
						$posSafe = $posSafe - $defn_len3;
					}
					$arA[$arK]['defn'] = $oFunc->mb_substr($arA[$arK]['defn'], 0, $posSafe) . '&#8230; ' . $strSee . "&#32;" . $strSyn;
				}
				else
				{
# print ' ' . $posSafe;
				   $arA[$arK]['defn'] = $oFunc->mb_substr($arA[$arK]['defn'], 0, $posSafe) . '&#8230;';
				}
			}
			else
			{
				$arA[$arK]['defn'] .= $strSee . '&#32;' . $strSyn;
			}
#			$arA[$arK]['defn'] = preg_replace("/&#160;$/", '', $arA[$arK]['defn']);
			$arA[$arK]['defn'] = str_replace('&#32;&#32;', '&#32;', $arA[$arK]['defn']);
			$arA[$arK]['defn'] = preg_replace("/&#32;$/", '', $arA[$arK]['defn']);
		} // definition preview
		$cnt++;
	}
	}
	unset($arSql);
	//
	$tpl_srch = new gwTemplate();
	$tpl_srch->unknownTags = 'keep';
	$tpl_srch->unknownTagsT = 'keep';
	$tpl_srch->setHandle( 0 , $GLOBALS['sys']['path_tpl'] . '/' . $GLOBALS['sys']['path_theme']. '/tpl_term_list.html' );
	$tpl_srch->addVal("KB", '');
	$tpl_srch->dplayout = $GLOBALS["sys"]["dplayout"];
	$tpl_srch->addVal("PATH_IMG_DICT",   '');

	/* Prepare parsed data for template */
	$odd = 1;
	$intAr = sizeof($arA);
	$intRowStep = $sys['dplayout'] ? ceil($intAr / $sys['dplayout']) : 0;
	$intColStep = 1;
	$tmp['href_term'] = array();
	for (reset($arA); list($k1, $v1) = each($arA);)
	{
		/* Collect data for template */
		if (GW_IS_BROWSE_WEB)
		{
			$tmp['href_term']['a'] = 'term';
			$tmp['href_term']['d'] = $v1['d_id'];
			$tmp['href_term']['t'] = $v1['t_id'];
#            $tmp['href_term']['p'] = 1;
			$pairsA[$k1]['TERM'] = $oHtml->a( append_url($page_index . '?' . $oHtml->paramValue($tmp['href_term'], '&', '')), $v1['term']);
			# 'onmouseover="popLayer(\''.$v1['term'].'\')" onmouseout="hideLayer()"'
		}
		if (isset($v1['defn']))
		{
			$pairsA[$k1]['DEFN'] = $v1['defn'];
		}
		if (isset($v1['kb']))
		{
			$pairsA[$k1]['KB'] = $v1['kb'];
		}
		if ( isset($v1['syn']) )
		{
			$pairsA[$k1]['DEFN'] = $v1['syn'];
		}
		if ( isset($v1['syn']) && isset($v1['defn']) )
		{
			$pairsA[$k1]['DEFN'] = $v1['defn'] . '<br/><i>' . $v1['syn'] . '</i>';
		}
		if (GW_IS_BROWSE_WEB && (!isset($pairsA[$k1]['DEFN']) || trim($pairsA[$k1]['DEFN']) == '') || $is_full)
		{
			$pairsA[$k1]['TERM'] = $v1['term'];
		}
		// Sets even, odd colors
		$pairsA[$k1]['intListNum'] = ((($p - 1) * $GLOBALS['sys']['page_limit']) + $k1 + 1);
		if((($k1+1) % $intRowStep) == 1)
		{
			$odd = 0;
		}
		else
		{
			$odd++;
		}
		// One row
		if ($intRowStep == 1){ $odd = 0; }
		//
		if ($odd % 2)
		{
			$pairsA[$k1]['th_odd'] = $GLOBALS['theme']['color_2'];
			$pairsA[$k1]['th_even'] = $GLOBALS['theme']['color_1'];
		}
		else
		{
			$pairsA[$k1]['th_odd'] = $GLOBALS['theme']['color_1'];
			$pairsA[$k1]['th_even'] = $GLOBALS['theme']['color_2'];
		}
	} // for each parsed data
		

	$tpl_srch->addVal( 'INT_ONPAGE',        $strA[1]);
	$tpl_srch->addVal( 'L_ONPAGE',          $L->m('str_on_page'));
	$tpl_srch->addVal( 'L_PAGEOFPAGE',      sprintf($L->m('str_page_of_page'), $p, $strA[3]));
	$tpl_srch->addVal( 'PAGE_SELECTION',  '');
	$tpl_srch->addVal( "CSS_ALIGN_RIGHT", $GLOBALS['sys']['css_align_right']);
	$tpl_srch->addVal( "CSS_ALIGN_LEFT",  $GLOBALS['sys']['css_align_left']);


	$strA[0] = "";
	// put prepared data to template
	if (isset($pairsA))
	{
		$tpl_srch->setBlock( 0 ,"list_item", $pairsA );
#        $tpl_srch->addVal( "intListStart", (($p - 1) * $GLOBALS['sys']['page_limit']) + 1 );
		$tpl_srch->addVal( "TABLEWIDTH", $GLOBALS['sys']['tblwidth']);
		$tpl_srch->parse();
		$strA[0] = $tpl_srch->output();
		// 15 july 2002
		$strA[0] = str_replace('<span class="f"></span>', '', $strA[0]);
	}
	return $strA;
} // end of func getDictWordList

/**
 *
 * @return  array   All dictionaries with ID as key and NAME as value
 */
function getDictArray()
{
	global $oSqlQ, $oDb;
	if (PERMLEVEL > 1)
	{
		// not guest, get all dictionaries.
		// disable caching for admin mode
		$arSql = $oDb->sqlExec($oSqlQ->getQ('get-dicts-admin'), 'st', 0);
	}
	else
	{
		// guest: show only active dictionaries
		// use cache
		$arSql = $oDb->sqlRun($oSqlQ->getQ('get-dicts-web'), 'st');
	}
	return $arSql;
}
/**
 * Constructs search form with all dictionaries
 *
 * @return  string Complete HTML-code
 */
function getDictSrch($LANG = '', $x = 1, $y = 99, $qStrOrder = '', $is_form_only = 0, $id_dict = 0)
{
	global $gw_this, $L, $sys;
	$str = '';
	$arSql = $gw_this['arDictList'];
	$arDictMap = array();
	if (sizeof($arSql) > 0)
	{
		$GLOBALS['curDate'] = date("YmdHis");
		$GLOBALS['curDateMk'] = mktime(date("H,i,s,Y,m,d"));
		$strGroupBy = 'tpname';
		for (; list($arK, $arV) = each($arSql);)
		{
			$p = $arV['p'];
			unset($arV['p']);
			unset($arV[$strGroupBy]);
			$arDictMap[$arV['id']] = strip_tags($arV['title']);
		}
		asort($arDictMap);
		if (!$is_form_only)
		{
			$arDictMap = array_merge_clobber(array(0 => $L->m('srch_all')), $arDictMap);
		}
	}
	else
	{
		$arDictMap[0] = $GLOBALS['L']->m('reason_4');
		$str .= '<b>' . $GLOBALS['L']->m('reason_4') . '</b>';
	}
	$intRand = 0;
	$strRandValue = '';
#	$randSet[0][] = '';
#	$randarray = array("0");
#	$intRand = $randarray[rand(0, (sizeof($randarray)-1))];
#	if (isset($randSet[$intRand]))
#	{
#		$strRandValue = $randSet[$intRand][rand(0, (sizeof($randSet[$intRand])-1) )];
#	}
	if ($is_form_only)
	{
		return htmlFormsSelect($arDictMap, $id_dict, "d", 't', 'width:15em', $L->languagelist("1"));
	}
	global $oTplDictSrch;
	$oTplDictSrch = new gwTemplate();
	$oTplDictSrch->unknownTags = 'keep';
	$oTplDictSrch->unknownTagsT = 'keep';
	$oTplDictSrch->setHandle( 0 , $sys['path_tpl'].'/'.$sys['path_theme'].'/tpl_search_select.html' );
	/* allow multilingual_vars */
	gw_addon_multilingual_vars('', 'oTplDictSrch');
	/* */
	$oTplDictSrch->AddVal( 'block:SearchSelect', htmlFormsSelect($arDictMap, $intRand, "d", 't', 'width:100%', $L->languagelist("1")) );
	$oTplDictSrch->AddVal( 'FORM_ACTION',   $sys['page_index']);
	$oTplDictSrch->AddVal( 'L_SELECT_DICT', $L->m('srch_selectdict'));
	$oTplDictSrch->AddVal( 'L_TERM',        $L->m('term'));
	$oTplDictSrch->AddVal( 'L_SRCH_SUBMIT', $L->m('3_srch_submit'));
	$oTplDictSrch->AddVal( 'STR_RND',       $strRandValue);
	$oTplDictSrch->parse();
	return $oTplDictSrch->output();
}


/**
 * Get the list of dictionaries
 *
 * @param    string $LANG   Language
 * @param    int $dict_nmax  links per category
 * @param    int $x  X
 * @param    int $y  Y
 * @param    string  $qStrOrder   Order type for SQL-query
 * @return   string  html-code
 * @global   int    $tid
 * @global   int    $auth
 * @global   int    $sys
 */
function getDictList($LANG = "", $dict_nmax = 5, $x = 1, $y = 99, $qStrOrder = "")
{
	global $tid, $auth, $sys, $gw_this, $L;
	global $oHtml, $oFunc;

	$str = '';
	$arSql = $gw_this['arDictList'];
	$languagelist = $L->languagelist();
	if (sizeof($arSql) == 0) // no dictionaries
	{
		$str .= '<b>' . $GLOBALS['L']->m('reason_4') . '</b>';
	}
	else
	{
		$cnt = 0;
		$GLOBALS['curDate'] = date("YmdHis");
		$GLOBALS['curDateMk'] = mktime(date("H,i,s,Y,m,d"));
		$strGroupBy = "tpname";

		for (reset($arSql); list($arK, $arV) = each($arSql);)
		{
			$p = $arV['p'];
			unset($arV['p']);
			unset($arV[$strGroupBy]);
			$arDictMap[$p][$arK] = $arV;
		}

		// get topics map
		$ar = ctlgGetTopics();

		// display catalog in web mode
		if (GW_IS_BROWSE_WEB)
		{
			$page_index = $sys['page_index'];
			$str = getCatalogTitle($ar, $arDictMap, 0, 1, $dict_nmax);
		}
		if (GW_IS_BROWSE_ADMIN)
		{
			$page_index = $sys['page_admin'];
			$strSubtopics = "";
			$arData = array();
			if(isset($ar[0]["ch"])) // Root ->
			{
				$cnt0 = sizeof($ar[0]["ch"]);
				for ($i0 = 1; $i0 <= $cnt0; $i0++) // Root -> Topic
				{
				// number of dictionaries
				$intDictNumber = 0;
				// keys for Root -> Topic
				$k = key($ar[0]["ch"]);
				$arLevel2 = array();
				// if Root -> Topic -> Subtopic
				if (isset($ar[$k]['ch']))
				{
					$cnt_sub = 0; // count subtopics
					while(is_array($ar[$k]['ch']) && list($k2, $v2) = each($ar[$k]['ch']))
					{
						if (($cnt_sub < $dict_nmax) || ($dict_nmax == 0)) // read a few subtopics
						{
							$arLevel2[$cnt_sub] =  $oHtml->a(append_url($sys['page_admin'] . '?'.GW_ACTION.'='.BRWS.'&t=' . GW_T_DICT.'&tid=' . $ar[$k2]['id']), $ar[$k2]['tpname']);
						}
						else // then exit from while
						{
							continue;
						}
						$cnt_sub++;
					} // end with childs
					$strSubtopics = implode(', ',  $arLevel2);
					if (($cnt_sub > $dict_nmax) && ($dict_nmax != 0))
					{
						$strSubtopics .= '&#8230;';
					}
				} // end of subtopics
				// now count number of dictionairies in each topic
				$GLOBALS['arId'] = array();
				$arTreeId = ctlgGetTree($ar, $k);
				$arTreeId[$k] = $k;
				while(is_array($arTreeId) && list($kn, $vn) = each($arTreeId))
				{
					if (isset($arDictMap[$kn]))
					{
						$intDictNumber += sizeof( $arDictMap[$kn] );
					}
				}
				// do not include topics without dictionaries
				if ($intDictNumber > 0)
				{
					$arData[$cnt]['SUBTOPIC'] = $strSubtopics;
					$arData[$cnt]['SUM'] = $intDictNumber;
					$arData[$cnt]['TOPIC'] = $oHtml->a(append_url($sys['page_admin'] . '?'.GW_ACTION.'='.BRWS.'&t=' . GW_T_DICT.'&tid=' . $k), $ar[$k]['tpname']);
					$cnt++;
				}
				$strSubtopics = '';
				//
				next($ar[0]["ch"]);
			} // for
		} // end of parsing childs for root level
		$cnt = 0;

		global $tpl_c;
		$tpl_c = new gwTemplate();
		$tpl_c->setHandle( 0 , $sys['path_admin'] . '/' . $sys['path_tpl'] . "/catalog_title.html" );
		$tpl_c->addVal( "TABLEWIDTH", "100%");
		$tpl_c->dplayout = 2;
		$tpl_c->setBlock(0, "catalog_item", $arData);
		gw_addon_multilingual_vars(0, 'tpl_c');

		$tpl_c->parse();

		$str = $tpl_c->output();

		$GLOBALS['arId'] = array();
		if ($oFunc->is_num($tid))
		{
			$arAlltopics = ctlgGetTree($ar, $tid);
		}

		while( isset($arAlltopics) && is_array($arAlltopics) && list($kp, $tp) = each ($arAlltopics))
		{
			if (isset($arDictMap[$tp]) && is_array($arDictMap[$tp])) // topic selected
			{
				foreach( $arDictMap[$tp] as $k => $arV )
				{
					$bgc = $GLOBALS['theme']['color_2'];
					$menu = array();
					$str_is = $arV['is_active'] ? '<span class="green">%s</span>' : '<span class="red">%s</span>';
					$str_is = sprintf($str_is, $L->m('is_'.$arV['is_active']));

					$str .= '<table border="0" width="100%" cellpadding="3" cellspacing="1">';
					$str .= '<tr style="background:'.$bgc.';text-align:'.$GLOBALS['sys']['css_align_left'].';vertical-align:top">';
					$str .= '<td style="width:75%" class="m">';
					$str .= $arV['title'];
					$str .= '</td>';
					$str .= '<td class="t" style="width:14%;text-align:'.$sys['css_align_right'].'"><span class="gray">'. $oFunc->number_format($arV['int_terms'], 0, $L->languagelist('4')) . "</span></td>";
					$str .= '<td class="q" style="width:10%;text-align:center"><span class="gray">' . $languagelist[$arV['lang']] . "</span></td>";
					$str .= '<td class="q" style="width:1%;text-align:center">' . $str_is . '</td>';
					$str .= '</tr>';
					$str .= '</table>';
					if ($auth->is_allow('term', $arV["id"]))
					{
					$str .= '<table border="0" width="100%" cellpadding="3" cellspacing="1">';
					$str .= '<tr style="background:'.$bgc.';text-align:'.$GLOBALS['sys']['css_align_left'].';vertical-align:top">';
					$str .= '<td style="width:1%" class="t"><span class="f">'.$arV["id"].'</span></td>';
					$str .= '<td style="" class="t" colspan="2"><span class="f">';
					$auth->is_allow('dict', $arV["id"]) ? $menu[] = $oHtml->a($page_index . '?'.GW_ACTION.'='.GW_A_EDIT.'&'.GW_TARGET.'=' . GW_T_DICT.'&id=' . $arV["id"], $GLOBALS['L']->m('3_edit')) : '';
					$auth->is_allow('term', $arV["id"]) ? $menu[] = $oHtml->a($page_index . '?'.GW_ACTION.'='.GW_A_ADD.'&'.GW_TARGET.'=' . GW_T_TERM.'&id=' . $arV["id"], "+ " . $GLOBALS['L']->m('3_add_term')) : '';
					$auth->is_allow('export', $arV["id"]) ? $menu[] =  $oHtml->a($page_index . '?'.GW_ACTION.'='.EXPORT.'&'.GW_TARGET.'=' . GW_T_DICT.'&id=' . $arV["id"], $GLOBALS['L']->m('3_export')) : '';
					$auth->is_allow('dict', $arV["id"]) ? $menu[] = $oHtml->a($page_index . '?'.GW_ACTION.'='.IMPORT.'&'.GW_TARGET.'=' . GW_T_DICT.'&id=' . $arV["id"], $GLOBALS['L']->m('3_import')) : '';
					$str .= '['.implode("] [", $menu).']';
					$str .= '<br />'.$arV['announce'].'</span>';
					$str .= '</td>';
					$str .= '</tr>';
					$str .= '</table>';
					}
					$str .= '<table border="0" width="100%" cellpadding="2" cellspacing="1">';
					$str .= '<tr style="background:'.$GLOBALS['theme']['color_1'].'">';
					$str .= '<td><table cellspacing="2"><tr><td></td></tr></table>';
					$str .= '</td>';
					$str .= '</tr>';
					$str .= '</table>';
				} // foreach $arDictMap
			} // isset parent
		} // while
		unset($ar);
		unset($arDictMap);
		}
	} // dictionaries found

	return $str;
} // end of func getDictList



/**
 * Recursive function.
 * Returns title page for catalog.
 *
 * @return  array
 */
function getCatalogTitle($ar, $arDictMap, $p = 0, $depth = 1, $dict_nmax, $runtime = 0)
{
	global $curDateMk, $curDate, $a, $L, $sys, $oFunc, $oHtml, $gw_this;
	
	/* Other settings */
	$str = '';
	$tpcs_depth = 0;
	$tpcs_nmax = 0;
	$runtime++;
	$arLanguages =& $L->languagelist();
	if (isset($ar[$p]['ch'])) /* a child from Root found */
	{
		$cntTopic = 0;
		// removes limit for catalog page
		$tpcs_nmax = $dict_nmax;
		//
		$str .= CRLF . '<dl class="f">';
		while (is_array($ar[$p]['ch']) && list($k, $v) = each($ar[$p]['ch'])) // (Root or Topic) -> Topic
		{
			/* Reserved for dictionary parmeters */
#			prn_r( $k );
			if ($cntTopic > -1) // unlimit topics
			{
				// topic code, term
				$str .= '<dt>';
				// custom icons
				if ($sys['is_list_images'])
				{
					$file_icon = $sys['path_tpl'] . '/' . $gw_this['vars']['themename'] . '/icon_16_topic.gif';
					if (file_exists($file_icon))
					{
						$str .= '<img style="vertical-align:top" src="' . $sys['server_dir'] . '/' . $file_icon . '" ' .
						' width="16" height="16" alt="" />&#160;';
					}
				}
				$str .= '<span class="r">' . $ar[$k]['tpname'] . '</span></dt>';
				// topic code, definition
				$str .= '<dd>';
				//
				if (isset($arDictMap[$k])) // dictionary found
				{
					$cntDict = 0;
					$str .= CRLF . '<dl>';
					while (is_array($arDictMap[$k]) && list($k2, $v2) = each($arDictMap[$k]))
					{
						$strMark = '';
						$idcolor = '#999';
						$mktCreated = $oFunc->date_Ts14toTime($v2["date_created"]);
						$mktUpd = $oFunc->date_Ts14toTime($v2["date_modified"]);
						// define "UPDATED" mark
						if ( ($curDateMk - $mktUpd) < ($GLOBALS['sys']['time_upd'] * 86400) )
						{
							$strMark = '&#160;' . $L->m('mrk_upd');
							$idcolor = 'green';
						}
						// define "NEW" mark
						if ( ($curDateMk - $mktCreated) < ($GLOBALS['sys']['time_new'] * 86400) )
						{
							$strMark = '&#160;' . $L->m('mrk_new');
							$idcolor = "red";
						}
						if ($cntDict < $dict_nmax) // dictionaries limit
						{
							$strIcon = '';
							if ($sys['is_list_images'])
							{
								$file_icon = $sys['path_tpl'] . '/' . $gw_this['vars']['themename'] . '/icon_16_dict.gif';
								if (file_exists($file_icon))
								{
									$strIcon = '<img style="margin:1px;vertical-align:middle" src="' . $sys['server_dir'] . '/' . $file_icon . '" ' .
									'width="16" height="16" alt="" />&#160;';
								}
							}
							$str .= '<dt class="t">' . $oHtml->a( $sys['page_index']  . "?a=list&d=" . $v2['id'],
									($sys['is_list_images'] ? $strIcon : '') . $v2['title']);
							if ($sys['is_list_numbers'])
							{
								$str .= '&#32;(' . $oFunc->number_format($v2['int_terms'], 0, $L->languagelist('4')) . ')';
							}
							if ($v2['lang'] != $gw_this['vars'][GW_T_LANGUAGE].'-utf8') // mark as foreign language
							{
								$str .= isset($arLanguages[$v2['lang']]) ? ' ' . $arLanguages[$v2['lang']] . '' : '';
							}
							//
							if ($strMark != '') // show previously defined mark with previously defined color
							{
								$str .= '<span class="' . $idcolor . '">' . $strMark . '</span>';
							}
							$str .= '</dt>';
							// topic ends
							// Short description
							if ($sys['is_list_announce']) // show announce only for catalog
							{
								// announce
								if (!empty($v2['announce']) && $v2['announce'] != '-')
								{
									$str .= '<dd><span class="t">' . $v2['announce'] . '</span></dd>';
								}
								else
								{
									$str .= '<dd></dd>';
								}
							} // catalog mode
						} // end of dictionaries limit
						elseif ($cntDict == $dict_nmax)
						{
							$str .= '<dt class="t">' . htmlTagsA( $sys['page_index'] . '?a=catalog', $L->m('more') . "...") . '</dt>';
						}
						else
						{
							continue;
						} // end of $dict_nmax limit
						$cntDict++;
					} // while
					$str .= '</dl>';
					//
					if (!isset($ar[$k]['ch']))
					{
						$str .= '</dd>';
					}
				} // end of (isset($arDictMap[$k]) = if dictionary in topics exists)
				else
				{
					if (!isset($ar[$k]['ch']))
					{
						$str .= '</dd>';
					}
				}
				if (GW_IS_BROWSE_ADMIN || $a == '' || $a == 'catalog') // catalog page
				{
					$str .= getCatalogTitle($ar, $arDictMap, $k, $depth + 1, $dict_nmax, $runtime);
				}
				if (isset($ar[$k]['ch']))
				{
					$str .= '</dd>';
				}
			} // topic limit
			elseif ($cntTopic == $tpcs_nmax)
			{
				$str .= '<dt class="r">'
					 .'<img src="'.$GLOBALS['sys']['path_img'] . '/16_folder.gif" width="16" height="16" alt="" />&#160;'
					 . htmlTagsA( $sys['page_index'] . '?a=catalog', $L->m('more') . "...") . '</dt>';
				$cntDict++;
			}
			else
			{
				continue;
			} // end of $tpcs_nmax limit
			$cntTopic++;
		} // while childs in root
		$str .= '</dl>';
	} // childs
	return $str;
} // end of getCatalogTitle();

/**
 * Puts catalog structure into array (thread model).
 *
 * @param   int     $id record id
 * @return  array   catalog id, catalog name
 */
function ctlgGetTopics($id = 0)
{
	global $oSqlQ, $oDb;
	if (GW_IS_BROWSE_ADMIN) // disable caching for admin mode
	{
		$arSql = $oDb->sqlExec($oSqlQ->getQ('get-topics'), 'st', 0);
	}
	if (GW_IS_BROWSE_WEB)
	{
		$arSql = $oDb->sqlRun($oSqlQ->getQ('get-topics'), 'st');
	}
	$arStr = array(array());
	$arStr2 = array();
	for (reset($arSql); list($arK, $arV) = each($arSql);)
	{
		$i = $arV['id'];
		$p = $arV['p'];
		$arStr2[$i] = $arV;
		$arStr[$p]["ch"][$i] = $i;
		$arStr[$p]["max"] = $i;
		if(!isset($arStr[$p]["max"])) $arStr[$p]["max"] = $i;
		if(!isset($arStr[$p]["min"])) $arStr[$p]["min"] = $i;
	} // num_rows
	// merge
	while(is_array($arStr2) && list($key, $val) = each($arStr2) )
	{
		if (isset($arStr[$key]))
		{
			while(is_array($val) && list($k2, $v2) = each($val) )
			{
				$arStr[$key][$k2] = $v2;
			}
		}
		else
		{
			$arStr[$key] = $arStr2[$key];
		}
	}
	return $arStr;
} // end of ctlgGetTopics();



/**
 * Build tree, recursive:
 *
 * @param   array   $ar tree structure
 * @param   int     $startId parent id
 * @param   int     $cntRow number of row (counter)
 * @return compplete HTML-code
 * @globals array   $arImgTread images for branches
 * @globals int     $cntRow counter
 * @globals int     $tid
 * @globals array   $arParents
 * @globals string  $a
 * @globals int     $t
 * @globals object  $auth
 */
function ctlgGetTopicsRow($ar = array(), $startId = 0, $cntRow = 1)
{
	global $arImgTread, $cntRow, $tid, $arParents, $a, $t, $auth, $sys;
	$str = $strT = $image = "";
	$isDn = $isUp = 1;
	$isReset = 0;
	$page_index = $sys['page_index'];
	if (GW_IS_BROWSE_ADMIN)
	{
		$page_index = $sys['page_admin'];
	}
	
	// parents with selected tid (same for delete)
	if ($cntRow == 1)
	{
		$arParents = ctlgGetTree($ar, $tid);
	}

	if (sizeof($ar) > 0)
	{
		if ($startId != 0)
		{
			$p = $ar[$startId]["p"];
			if ($p != 0)
			{
				if (!isset($ar[$p]["img"]))
				{
					$ar[$p]["img"] = "";
				}
				$image = $ar[$p]["img"];
				if ($ar[$p]["max"] == $ar[$startId]["id"])
				{
					$image .= ($GLOBALS['topic_mode'] == "html") ? $arImgTread['l'] : "&#160;'-";
				}
				else
				{
					$image .= ($GLOBALS['topic_mode'] == "html") ? $arImgTread['t'] : "&#160;|-";
				}
			} // if $p != 0;

			if (isset($ar[$startId]["ch"]) && is_array($ar[$startId]["ch"]))
			{
				$IsDn = $IsUp = 1;
				if (isset($ar[$p]["img"]))
				{
					$ar[$startId]["img"] = $ar[$p]["img"];
					if($startId == $ar[$p]["max"])
					{
						$ar[$startId]["img"] .= ($GLOBALS['topic_mode'] == "html") ? $arImgTread['trans'] : "&#160;&#160;&#160;";
					}
					else
					{
						$ar[$startId]["img"] .= ($GLOBALS['topic_mode'] == "html") ? $arImgTread['i'] : "&#160;|&#160;";
					}
				}
				$image .= ($GLOBALS['topic_mode'] == "html") ? $arImgTread['m'] : "[-]";
			}
			else
			{
				if($ar[$startId]["p"] != 0)
				{
					$image .= ($GLOBALS['topic_mode'] == "html") ? $arImgTread['c'] : "-";
				}
				else
				{
					$image .= ($GLOBALS['topic_mode'] == "html") ? $arImgTread['space'] : "[ ]";
				}
			} // isset($ar[$startId]["ch"])

			if ( isset($ar[$p]['max']) && ($ar[$p]['max'] == $startId)){ $isDn = 0; }
			if ( isset($ar[$p]['min']) && ($ar[$p]['min'] == $startId)){ $isUp = 0; $isReset = 1; }
			if (!$isUp && !$isDn) { $isReset = 0; }

			$cntRow % 2 ? ($bgcolor = $GLOBALS['theme']['color_3']) : ($bgcolor = $GLOBALS['theme']['color_1']);
			if ($GLOBALS['topic_mode'] == "form")
			{
				$selected = "";
				if (isset($ar[$tid]["p"]))
				{
					$selected = ($ar[$tid]["p"] == $ar[$startId]["id"]) ? 'selected="selected" ' : '';
				}
				$isBuild = 0;
				// do not allow to place topic under the same topic and parent under the same parent
				// Edit mode
#
# !isset($arParents[$ar[$startId]["p"]])

				if (!isset($arParents[$startId]) && ($ar[$startId]["id"] != $tid))
				{
					$isBuild = 1;
				}
				// exclusion for Add mode

				if ($a == "add" || ($t == "dict"))
				{
					$isBuild = 1;
					// rule for auto-selection in Add mode
					if (isset($ar[$tid]["p"]))
					{
						$selected = ($ar[$tid]["id"] == $ar[$startId]["id"]) ? 'selected="selected" ' : '';
					}
				}
				if ($isBuild)
				{
					$str .= '<option ' . $selected . 'style="background:'.$bgcolor.'" value="'.$ar[$startId]["id"].'">';
					$str .= '&#160;' . $image . '&#160;' . $ar[$startId]["tpname"];
					$str .= '</option>';
				}
			} // form
			elseif ($GLOBALS['topic_mode'] == "html")
			{
				$is_allow_topic = $auth->is_allow('topic');

				$str .= '<tr style="color:'.$GLOBALS['theme']['color_5'].';background:'.$bgcolor.'">';
				$str .= '<td class="f" style="text-align:'.$sys['css_align_right'].'"><span class="t">&#160;' .  $cntRow . '&#160;</span></td>';
				$str .= '<td>';
				$str .= '<table cellspacing="0" cellpadding="0" border="0"><tr class="t">';
				$str .= '<td>' . $arImgTread['space'] . $image . '</td>';
				$str .= '<td>';
				$str .= ($auth->is_allow('topic')
						? htmlTagsA( $page_index . '?'.GW_ACTION.'='.GW_A_EDIT.'&'.GW_TARGET.'='.TPCS.'&tid=' . $ar[$startId]["id"],
							'&#160;'. $ar[$startId]["tpname"], '', '', $GLOBALS['L']->m('3_edit') )
						: $ar[$startId]["tpname"] );
				$str .= '</td>';
				$str .= '</tr>';
				$str .= '</table>';
				$str .= '</td>';
				$str .= '<td class="t" style="text-align:center">[';
				$str .= ($isUp && $is_allow_topic) ? htmlTagsA( $page_index . '?a='.UPDATE.'&t='.TPCS.'&mode=up&tid=' . $ar[$startId]["id"], $GLOBALS['L']->m('3_up')) : $GLOBALS['L']->m('3_up');
				$str .= '] [';
				$str .= ($isDn && $is_allow_topic) ? htmlTagsA( $page_index . '?a='.UPDATE.'&t='.TPCS.'&mode=dn&tid=' . $ar[$startId]["id"], $GLOBALS['L']->m('3_down')) : $GLOBALS['L']->m('3_down');
				$str .= '] [';
				$str .= ($isReset && $is_allow_topic) ? htmlTagsA( $page_index . '?a='.UPDATE.'&t='.TPCS.'&mode=reset&tid=' . $ar[$startId]["id"], $GLOBALS['L']->m('3_reset')) : $GLOBALS['L']->m('3_reset');
				$str .= ']</td>';
				$str .= '<td style="text-align:center" class="t">[&#160;';
				$str .= ($is_allow_topic) ? htmlTagsA( $page_index . '?a='.GW_A_ADD.'&t='.TPCS.'&tid=' . $ar[$startId]["id"], $GLOBALS['L']->m('3_add_subtopic')) : $GLOBALS['L']->m('3_add_subtopic');
				$str .= '] [';
				$str .=  htmlTagsA( $page_index . '?'.GW_ACTION.'='.GW_A_EDIT.'&'.GW_TARGET.'='.TPCS.'&tid=' . $ar[$startId]["id"], $GLOBALS['L']->m('3_edit'));
				$str .= '] [';
				$str .= ($is_allow_topic) ? htmlTagsA( $page_index . '?a='.REMOVE.'&t='.TPCS.'&tid=' . $ar[$startId]["id"], $GLOBALS['L']->m('3_remove')) : $GLOBALS['L']->m('3_remove');
				$str .= ']</td>';
				$str .= '</tr>';
			} // html
		} // end of $startId != 0

		if (isset($ar[$startId]["ch"]) && is_array($ar[$startId]["ch"]))
		{
			$cnt = sizeof($ar[$startId]["ch"]);
			for ($i = 1; $i <= $cnt; $i++)
			{
				$k = key($ar[$startId]["ch"]);
				$cntRow++;
				$strT .= ctlgGetTopicsRow($ar, $k);
				next($ar[$startId]["ch"]);
			}
		}
	} // count > 1
	 return $str . $strT;
}

/**
 * Get branch from any tree. Recursive.
 * @param   array with a tree structure
 * @param   chunk id
 * @return  array
 */
function ctlgGetTree($ar, $id)
{
	global $arId;
	if (isset($ar[$id]['ch']))
	{
		while(is_array($ar[$id]['ch']) && list($k, $v) = each($ar[$id]['ch']) )
		{
			if (isset($ar[$k]['ch']))
			{
				ctlgGetTree($ar, $k);
			}
			$arId[$k] = $k;
		}
	}
	$arId[$id] = $id;
	return $arId;
} // end of ctlgGetTree();





/* end of file */
?>