<?php
if (!defined('IN_GW'))
{
	die('<!-- $Id: t.sys_maintenance_2.inc.php,v 1.1 2004/11/13 12:30:09 yrtimd Exp $ -->');
}
/**
 *  Glossword - glossary compiler (http://glossword.info/dev/)
 *  � 2002-2004 Dmitry N. Shilnikov <dev at glossword.info>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  (see `glossword/support/license.html' for details)
 */
/* Check permission */
if (!$auth->have_perm('admin', PERMLEVEL))
{
	return;
}
/* Script variables below */

/* Script functions below */
function gw_html_contents()
{
	global $is, $arStatus, $oL, $strR, $sys, $tid;

	$arKeyUnused = getUnusedKeywords();
	$arWordlist = getTableInfo(TBL_WORDLIST);
	$intKeyUnused = sizeof($arKeyUnused);
	$arStatus[] = array($oL->m('1010'), sprintf('<b>%s</b>', number_format($arWordlist['Rows'], 0, '', ' ')));
	$arStatus[] = array($oL->m('1011'), sprintf('<b class="red">%s</b>', number_format($intKeyUnused, 0, '', ' ')));
	$arStatus[] = array($oL->m('1012'), sprintf('<b class="green">%s</b>', number_format(($arWordlist['Rows']-$intKeyUnused), 0, '', ' ')));
	$intKbWordlist = ($arWordlist['Data_free'] + $arWordlist['Data_length'] + $arWordlist['Index_length']);
	$arStatus[] = array($oL->m('1013'), sprintf('<b>%s</b>', number_format($intKbWordlist, 0, '', ' ')));
	if ($intKeyUnused > 0)
	{
		$arStatus[] = array($oL->m('1014'), sprintf('<b class="red">%s</b>',
			number_format((($intKbWordlist / $arWordlist['Rows']) * $intKeyUnused), 0, '', ' '))
		);
		$arStatus[] = array($oL->m('1015'), sprintf('<b class="green">%s</b>',
			number_format((($intKbWordlist / $arWordlist['Rows']) * ($arWordlist['Rows']-$intKeyUnused)), 0, '', ' '))
		);
		/* Link to confirm */
		if ($is == '')
		{
			$arStatus[] = array('&#160;');
			$arStatus[] = array(sprintf('<b>%s</b>', $oL->m('1016')), sprintf('<a href="%s">%s</a>',
			append_url($sys['page_admin'].'?'.GW_ACTION.'='.A_MAINTENANCE.'&tid='.$tid.'&'.GW_TARGET.'='.SYS.'&is=1'),
			$oL->m('3_yes')));
			$arStatus[] = array('&#160;');
		}
	}
	else
	{
		$arStatus[] = array('&#160;');
		$arStatus[] = array($oL->m('1017'), '');
		$arStatus[] = array('&#160;');
	}
	$strR .= '<div class="contents u">';
	$strR .= $oL->m('1009');
	$strR .= html_array_to_table_multi($arStatus);
	$strR .= '</div>';
}
/* */
function gw_optimize_keywords()
{
	global $id, $arStatus, $oDb, $oL;
	$arKeyUnused = getUnusedKeywords();
	$sql_o = 'DELETE FROM '.TBL_WORDLIST.' WHERE word_id IN (\'%s\')';
	$arKeyId = array();
	for (reset($arKeyUnused); list($k, $v) = each($arKeyUnused);)
	{
		$arKeyId[] = $v['word_id'];
	}
	if (!empty($arKeyId))
	{
		$sql = sprintf($sql_o, implode("', '", $arKeyId));
		if ($oDb->sqlExec($sql, '', 0))
		{
			$arStatus[] = array(sprintf('<b class="red">%s</b>', $oL->m('1018')), '');
		}
	}
	$oDb->sqlExec('CHECK TABLE '.TBL_WORDMAP);
	$oDb->sqlExec('OPTIMIZE TABLE '.TBL_WORDMAP);
	$oDb->sqlExec('CHECK TABLE '.TBL_WORDLIST);
	$oDb->sqlExec('OPTIMIZE TABLE '.TBL_WORDLIST);
	$arStatus[] = array(sprintf('<b class="red">%s</b>', $oL->m('1019')), '');
}
function getUnusedKeywords()
{
  	global $oDb;
	$sql = 'SELECT '.TBL_WORDLIST.'.word_id
			FROM '.TBL_WORDLIST.'
			LEFT JOIN '.TBL_WORDMAP.'
			ON '.TBL_WORDLIST.'.word_id = '.TBL_WORDMAP.'.word_id
			WHERE '.TBL_WORDMAP.'.word_id IS NULL
			ORDER BY '.TBL_WORDLIST.'.word_text';
	$arSql = $oDb->sqlExec($sql, '', 0);
	return $arSql;
}
/* Script action below */

$strR .= getFormTitleNav($oL->m(1008));
if ($is == '1')
{
	gw_optimize_keywords();
}
gw_html_contents();

?>