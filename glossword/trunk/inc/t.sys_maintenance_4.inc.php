<?php
if (!defined('IN_GW'))
{
	die('<!-- $Id: t.sys_maintenance_4.inc.php,v 1.1 2004/11/13 12:30:09 yrtimd Exp $ -->');
}
/**
 *  Glossword - glossary compiler (http://glossword.info/dev/)
 *  � 2002-2004 Dmitry N. Shilnikov <dev at glossword.info>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  (see `glossword/support/license.html' for details)
 */
/* Check permission */
if (!$auth->have_perm('admin', PERMLEVEL))
{
	return;
}
/* Script variables below */

/* Script functions below */
function gw_get_sql_cache()
{
	global $oDb, $oFunc, $oL, $is, $sys, $tid;

	if (($is == '2') && $oDb->sqlExec(sprintf('DELETE FROM %s', TBL_SRCH_RESULTS)))
	{
		$arStatus[] = array('<span class="t red">'.$oL->m('2_success').'</span>');
	}
	$arTableInfo = $oDb->table_info(TBL_SRCH_RESULTS);
	$int_size = ($arTableInfo['Data_length'] > 1) ? $arTableInfo['Data_length']+$arTableInfo['Index_length'] : 0;
	$arStatus[] = array($oL->m('1020'), $oFunc->number_format($int_size, 0, $oL->languagelist('4')));
	$arStatus[] = array($oL->m('1021'), $oFunc->number_format($arTableInfo['Rows'], 0, $oL->languagelist('4')));
	/* Link to confirm */
	if (($int_size > 1) && ($is != 2))
	{
		$arStatus[] = array(sprintf('<b>%s</b>', $oL->m('1023')), sprintf('<a href="%s">%s</a>',
		append_url($sys['page_admin'].'?'.GW_ACTION.'='.A_MAINTENANCE.'&tid='.$tid.'&'.GW_TARGET.'='.SYS.'&is=2'),
		$oL->m('3_clean')));
		$arStatus[] = array('&#160;');
	}
	return $arStatus;
}
/* */
function gw_get_file_cache()
{
	global $oFunc, $oL, $is, $sys, $tid;

	$ar_tree = gw_parse_tree($sys['path_cache_sql'], ($is == 1) ? 1 : 0);
	$arStatus = $ar_tree['names'];
	$arStatus[] = array($oL->m('1022'), $oFunc->number_format($ar_tree['bytes'], 0, $oL->languagelist('4')));
	$arStatus[] = array($oL->m('1021'), $oFunc->number_format($ar_tree['files'], 0, $oL->languagelist('4')));
	/* Link to confirm */
	if (($ar_tree['bytes'] > 1) && ($is != '1'))
	{
		$arStatus[] = array(sprintf('<b>%s</b>', $oL->m('1023')), sprintf('<a href="%s">%s</a>',
		append_url($sys['page_admin'].'?'.GW_ACTION.'='.A_MAINTENANCE.'&tid='.$tid.'&'.GW_TARGET.'='.SYS.'&is=1'),
		$oL->m('3_clean')));
		$arStatus[] = array('&#160;');
	}
	return $arStatus;
}
/* */
function gw_parse_tree($path, $is_clean = 0)
{
	$ar = array('files' => 0, 'bytes' => 0, 'names' => array());
	if (!file_exists($path))
	{
		return $ar;
	}
	if (substr($path, -1) != '/')
	{
		$path = $path.'/';
	}
	$h_all = opendir($path);
	while (($filename = readdir($h_all)) !== false)
	{
		if ($filename != '.' && $filename != '..' && !is_dir($filename))
		{
			if ($is_clean)
			{
				$ar['names'][] = '<span class="t red">'.$path.$filename.'</span>';
				unlink($path.$filename);
			}
			else
			{
				$ar['files']++;
				$ar['bytes'] += filesize($path.$filename);
			}
		}
	}
	return $ar;
}
/* Script action below */
$strR .= getFormTitleNav($oL->m(1004));
$arStatus = array();
$arStatus = array_merge($arStatus, gw_get_sql_cache());
$arStatus = array_merge($arStatus, gw_get_file_cache());

$strR .= '<div class="contents u">';
$strR .= html_array_to_table_multi($arStatus);
$strR .= '</div>';

?>