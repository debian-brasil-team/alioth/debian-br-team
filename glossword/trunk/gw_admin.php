<?php
/* protects script from wrong virtual host configuration */
if (defined('IN_GW'))
{
	die("<!-- $Id: gw_admin.php,v 1.8 2004/11/13 12:31:42 yrtimd Exp $ -->");
}
/**
 *  Glossword - glossary compiler (http://glossword.info/dev/)
 *  � 2002-2004 Dmitry N. Shilnikov <dev at glossword.info>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  (see `glossword/support/license.html' for details)
 */
// --------------------------------------------------------
/**
 *  Main administrative events.
 */
// --------------------------------------------------------
// time counter
$mtime = explode(" ", microtime());
$starttime = $mtime[1] + $mtime[0];

define('IN_GW', TRUE);
define('THIS_SCRIPT', 'gw_admin.php');

// maximum error control when in admin mode
error_reporting(E_ALL);

/* ------------------------------------------------------- */
/* Load configuration */
$sys['path_include'] = 'inc';
$sys['is_prepend'] = 1;
include_once('/etc/glossword/db_config.php');
include_once( $sys['path_include'] . '/config.inc.php' );
include_once( $sys['path_include'] . '/lib.auth.'      . $sys['phpEx'] );
include_once( $sys['path_include'] . '/lib.ct_sql.'    . $sys['phpEx'] );
include_once( $sys['path_include'] . '/lib.page.'      . $sys['phpEx'] );
include_once( $sys['path_include'] . '/lib.session.'   . $sys['phpEx'] );
$path['theme'] = 'gw_silver';
/* ------------------------------------------------------- */
/* Append system settings */
$sys = array_merge($sys, getSettings());
/* --------------------------------------------------------
 * mod_rewrite configuration
 * ------------------------------------------------------- */
$sys['is_mod_rewrite'] = 0;
$oHtml = new gw_html;
$oHtml->setVar('is_htmlspecialchars', 0);
$oHtml->setVar('is_mod_rewrite', 0);
$oHtml->is_append_sid = 1;
$oHtml->id_sess_name = 'sid';

/* --------------------------------------------------------
 * Register global variables
 * ----------------------------------------------------- */
$gw_this['vars'] = $oGlobals->register(array(
	'arControl','arPost','arPre','d','in','file_location',
	GW_ACTION,GW_SID,GW_TARGET,GW_T_LANGUAGE,
	'id','isConfirm','srch','id_srch','is','name','email','id_topic',
	'isUpdate','mode','p','post','q','strict','submit','tid'
));
for (reset($gw_this['vars']); list($k1, $v1) = each($gw_this['vars']);)
{
	$$k1 = $v1;
}
$arPostVars = array('file_location');
//
for (reset($arPostVars); list($k, $v) = each($arPostVars);)
{
	if (isset($_FILES[$v]) && ($_FILES[$v] != '')) // get values from FILES
	{
		$$v = $_FILES[$v];
	}
	elseif (isset($HTTP_POST_FILES[$v]) && ($HTTP_POST_FILES[$v] != '')) // compatibility FILES
	{
		$$v = $HTTP_POST_FILES[$v];
	}
}
unset($arPostVars);
$oHtml->id_sess = $sid;
// --------------------------------------------------------
// 28 Oct 2002, The authentication code
page_open(array("sess" => "gwtkSession", "auth" => "gwAuth", "user" => "gwUser"));
//
define('PERMLEVEL',     $user->get('auth_level'));
define('GW_IS_BROWSE_WEB',    0);
define('GW_IS_BROWSE_ADMIN',  1);
if(!isset($sess)){ exit('Can\'t load $sess'); }
if(!isset($auth)){ exit('Can\'t load $auth'); }
if(!isset($user)){ exit('Can\'t load $user'); }

if ($a == 'logout') // unauth
{
	$auth->logout();
	page_close();
	// redirect to login page
	$url_login = $sys['server_proto'] . $sys['server_host'] . $sys['page_login'];
	gwtk_header($url_login);
}
// --------------------------------------------------------

//
// Get dictinary list
//
$gw_this['arDictList'] = getDictArray();
//

##------------------------------------------------
## Admin-only included files
##
## isDebugQ         Show insert/update/delete queries
## path_img_admin   Path to images for administrative interface
## path_export      Path to directory where import/export files will be readed/saved (chmod 0777 required)
##------------------------------------------------

$sys['isDebugQ']    = 0;
$sys['path_img_admin'] = $sys['path_admin'] . '/images';
include_once( $sys['path_include'] . '/class.forms.' .        $sys['phpEx'] );
include_once( $sys['path_include'] . '/class.gw_htmlforms.' . $sys['phpEx'] );
include_once( $sys['path_include'] . '/func.admin.inc.' .     $sys['phpEx'] );
include_once( $sys['path_include'] . '/class.confirm.' .      $sys['phpEx'] );

// --------------------------------------------------------
// Set interface language, last modified: 06 aug 2003
// --------------------------------------------------------
// get default system language
$gw_this['vars'][GW_T_LANGUAGE] = preg_replace("/-([a-z0-9])+$/", '', $sys['locale_name']);
$gw_this['vars']['lang_enc'] = preg_replace("/^([a-z0-9])+-/", '', $sys['locale_name']);
// get dictionary language
if (isset($arDictParam['lang']))
{
	$gw_this['vars'][GW_T_LANGUAGE] = preg_replace("/-([a-z0-9])+$/", '', $arDictParam['lang']);
}
// get custom language
if (${GW_T_LANGUAGE} != '')
{
	$gw_this['vars'][GW_T_LANGUAGE] = ${GW_T_LANGUAGE};
}
$L = new gwtk;
$L->setHomeDir('locale'); // path to ./your_project/locale
$L->setLocale($gw_this['vars'][GW_T_LANGUAGE].'-'.$gw_this['vars']['lang_enc']); // path to ./your_project/locale/ru-ru/
//
// Language files
//
$L->getCustom('actions', $gw_this['vars'][GW_T_LANGUAGE].'-'.$gw_this['vars']['lang_enc'], 'join');
$L->getCustom('admin',   $gw_this['vars'][GW_T_LANGUAGE].'-'.$gw_this['vars']['lang_enc'], 'join');
$L->getCustom('err',     $gw_this['vars'][GW_T_LANGUAGE].'-'.$gw_this['vars']['lang_enc'], 'join');
$L->getCustom('options', $gw_this['vars'][GW_T_LANGUAGE].'-'.$gw_this['vars']['lang_enc'], 'join');
$L->getCustom('status',  $gw_this['vars'][GW_T_LANGUAGE].'-'.$gw_this['vars']['lang_enc'], 'join');
$L->getCustom('tdb',     $gw_this['vars'][GW_T_LANGUAGE].'-'.$gw_this['vars']['lang_enc'], 'join');
$L->getCustom('tht',     $gw_this['vars'][GW_T_LANGUAGE].'-'.$gw_this['vars']['lang_enc'], 'join');
$L->getCustom('tol',     $gw_this['vars'][GW_T_LANGUAGE].'-'.$gw_this['vars']['lang_enc'], 'join');
//
// Treads for topic editor
//
$arImgTread = array();
$arImgTread['c'] = '<img src="' . $sys['path_img_admin'] . '/p_c.gif" width="9" height="21" alt=""/>';
$arImgTread['i'] = '<img src="' . $sys['path_img_admin'] . '/p_i.gif" width="12" height="21" alt=""/>';
$arImgTread['l'] = '<img src="' . $sys['path_img_admin'] . '/p_l.gif" width="12" height="21" alt=""/>';
$arImgTread['m'] = '<img src="' . $sys['path_img_admin'] . '/p_m.gif" width="9" height="21" alt=""/>';
$arImgTread['n'] = '<img src="' . $sys['path_img_admin'] . '/p_n.gif" width="9" height="21" alt=""/>';
$arImgTread['p'] = '<img src="' . $sys['path_img_admin'] . '/p_p.gif" width="9" height="21" alt=""/>';
$arImgTread['t'] = '<img src="' . $sys['path_img_admin'] . '/p_t.gif" width="12" height="21" alt=""/>';
$arImgTread['trans'] = '<img src="' . $sys['path_img'] . '/0x0.gif" width="12" height="21" alt=""/>';
$arImgTread['space'] = '<img src="' . $sys['path_img'] . '/0x0.gif" width="5" height="21" alt=""/>';
##
##------------------------------------------------
//
// Load visual theme
//
$themefile = $sys['path_tpl'] . '/' . $sys['themename'] . '/theme.inc.' . $sys['phpEx'];
if (file_exists($themefile))
{
	include_once( $themefile );
}
else
{
	die("Can't find " . $themefile);
}
//
// Init. strings for template
//
$strR = $strL = $strM = '';
//
// Start new template for admin page
//
$tpl = new gwTemplate();
$tpl->isTextSmooth = 0;
$tpl->unknownTagsT = 'keep';

$menu_info = array();
$menu_info[] = '<span class="green"><b>' . $L->m('online') . '</b></span>: <b>' . $user->get("user_name") . '</b>';
$menu_info[] = $oFunc->date_SecToTime( time() - $oFunc->date_Ts14toTime($user->get('date_login')) );
$menu_info[] = $oHtml->a($sys['page_admin'].'?a=logout', $L->m('3_logout'));

// --------------------------------------------------------
// Load Add-ons
// --------------------------------------------------------
include_once('gw_addon/multilingual_vars.php');

//
// Apply variables for HTML-templates
//
$tpl->addVal( 'LANG',           $L->languagelist("0") );
$tpl->addVal( 'TEXT_DIRECTION', $L->languagelist("1") );
$tpl->addVal( 'CHARSET',        $L->languagelist("2") );
// 11 december 2002, r-t-l with l-t-r
$sys['css_align_right'] = 'right';
$sys['css_align_left'] = 'left';
if ($L->languagelist("1") == 'rtl')
{
	$sys['css_align_right'] = 'left';
	$sys['css_align_left'] = 'right';
}
$tpl->addVal( "CSS_ALIGN_RIGHT", $sys['css_align_right'] );
$tpl->addVal( "CSS_ALIGN_LEFT",  $sys['css_align_left'] );
$tpl->addVal( "PATH_IMG",        $sys['path_img'] );
$tpl->addVal( "PATH_SERVER_URL", $sys['server_url'] );
$tpl->addVal( "PATH_SERVER_DIR", $sys['server_dir'] );
$tpl->addVal( "V_THEME_NAME",    $sys['themename']);
$tpl->addVal( "PATH_IMG",        $sys['path_img'] );
$tpl->addVal( "TITLE",           $L->m('2_page_1') );
$tpl->addVal( "name_dict",       $L->m('2_page_1') );
$tpl->addVal( "PATH_CSS",        $sys['path_tpl'] . '/' . $sys['themename'] );
$tpl->addVal( "PATH_ADMIN",      $sys['path_admin'] );
$tpl->addVal( "TABLEWIDTH",      $sys['tblwidth'] );
$tpl->addVal( "FORM_ACTION",     $sys['page_admin'] );
$tpl->addVal( "CURRENT_ACTION",  '' );
$tpl->addVal( "q",               '' );
$tpl->addVal( "U_TO_WEB",        $sys['server_proto'] . $sys['server_host'] . $sys['page_index'] );
$tpl->addVal( "U_TO_MAIN",       append_url($sys['page_admin']) );
$tpl->addVal( "L_TO_MAIN",       $L->m('3_tomain'));
$tpl->addVal( "L_TOP_OF_PAGE",   $L->m('3_top'));
$tpl->addVal( "non:menuinfo",    implode(" | ", $menu_info));
$tpl->addVal( "GLOSSWORDVERSION",$sys['version'] );
$tpl->addVal( "termsamount",     $L->m('termsamount'));
$tpl->addVal( "sitename",        $L->m('sitename'));
$tpl->addVal( "URL_MAILTO",      $oFunc->text_mailto('<a href="mailto:'.$sys['y_email'].'">'. $sys['y_name'] . '</a>'));
$tplConstruct = array( $sys['path_admin'] . "/" . $sys['path_tpl'] . "/header.html",
					   $sys['path_admin'] . "/" . $sys['path_tpl'] . "/dictname.html");

$tpl->addVal( 'sid_name', GW_SID);
$tpl->addVal( 'sid', ${GW_SID});
$tpl->addVal( 'L_ADD_A_TERM', $L->m('3_add_term'));
$tpl->addVal( 'A_SEARCH', SEARCH);

$tpl->addVal( 'L_TERM', $L->m('term'));
$tpl->addVal( 'L_DICT', $L->m('dict'));
$tpl->addVal( 'L_DEFN', $L->m('defn'));
	
		
// Possible variables for admin mode:
// $p  is page number   [ $p > 1 ]
// $id is dictionary ID [ $id > 0 ]

// ---------------------------------------------------------
// Search engine, last modified 1 aug 2003
// ---------------------------------------------------------
// Add variables used in search form into common templates
$arTplVars['srch'] = array();

$arTplVars['srch'][] = array(
	'L_SRCH_TERM' => $L->m('srch_1'),
	'L_SRCH_DEFN' => $L->m('srch_0'),
	'L_SRCH_BOTH' => $L->m('srch_-1'),
	'L_SRCH_PHRASE_EDIT' => $L->m('srch_phrase_edit'),
	'L_SRCH_SUBMIT' => $L->m('3_srch_submit'),
);

//
// Feature list of dictionary ID
$gw_this['arDictListSrch'] = array();
//
// Search query, default values
if (!is_array($srch)) { $srch = array(); }
if (!isset($srch['by'])) { $srch['by'] = 'd'; } // search by dictionary
if (!isset($srch['in'])) { $srch['in'] = 1; }  // search in terms only for admin mode
if (!isset($srch['adv'])) { $srch['adv'] = 'all'; } // seach all words
//
// Set switcher for HTML
$arTplVars['srch'][] = array('chk_srch_all' => '' );
$arTplVars['srch'][] = array('chk_srch_exact' => '' );
$arTplVars['srch'][] = array('chk_srch_any' => '' );
$arTplVars['srch'][] = array('chk_srch_in_both' => '' );

// Web parameters conversion
$srch['in'] = ($in == 'both') ? -1 : intval($srch['in']);
$srch['in'] = ($in == 'defn') ? 0 : intval($srch['in']);
$srch['in'] = ($in == 'term') ? 1 : intval($srch['in']);
$srch['in'] = ($in == 'syn') ? 7 : intval($srch['in']);
$srch['in'] = ($in == 'address') ? 9 : intval($srch['in']);
$srch['in'] = ($in == 'phone') ? 10 : intval($srch['in']);



// Where to search
if ($srch['in'] == '1')
{
	// search for term only
	$arTplVars['srch'][] = array('chk_srch_in_term' => ' checked="checked"' );
}
elseif (intval($srch['in']) == 0) // search in terms and definitions, default
{
	$arTplVars['srch'][] = array('chk_srch_in_defn' => ' checked="checked"' );
}
elseif (intval($srch['in']) == -1) // search in terms and definitions, default
{
	$arTplVars['srch'][] = array('chk_srch_in_both' => ' checked="checked"' );
}
else // search for all fields
{
	$srch['in'] = array(0);
	$arTplVars['srch'][] = array('chk_srch_in_term' => ' checked="checked"' );
}
if (isset($srch['by']) && $srch['by'] == 'd')
{
	// Search by dictionary
	//
	// Set switcher for HTML
	$arTplVars['srch'][] = array('chk_srch_by_dict' => ' checked="checked"' );
	for (reset($gw_this['arDictList']); list($kDict, $vDict) = each($gw_this['arDictList']);)
	{
		if ($d == 0)
		{
			$gw_this['arDictListSrch'][] = $vDict['id'];
		}
		else
		{
			// TODO: search for multiple dictionaries
			$gw_this['arDictListSrch'][] = $d;
			break;
		}
	}
}


// ---------------------------------------------------------

// Fix page number
$p = ( !empty($p) ) ? $p : 1;
// Fix dictionary ID
$d = ( $id != '' ) ? $id : $d;
$id = ( $d != '' ) ? $d : $id;
//
// Set required field names for HTML-forms
//
$arReq['dict'] = array("title", "id_tp", "uid", "tablename", "lang", "cfg", "themename");
$arReq['term'] = array("arPre[term][0][value]");
$arReq['topic'] = array("name", "parent");
//
$strDictDetails = '';

// --------------------------------------------------------
// Load extensions: 27 aug 2003
// --------------------------------------------------------
$gw_this['vars']['funcnames'][UPDATE . GW_T_TERM] = isset($gw_this['vars']['funcnames'][UPDATE . GW_T_TERM]) ? $gw_this['vars']['funcnames'][UPDATE . GW_T_TERM] : array();
$gw_this['vars']['funcnames'][UPDATE . GW_T_DICT] = isset($gw_this['vars']['funcnames'][UPDATE . GW_T_DICT]) ? $gw_this['vars']['funcnames'][UPDATE . GW_T_DICT] : array();

## --------------------------------------------------------
## Get dictionary description
## Very important
	if (!empty($id) && preg_match("/^[0-9]{1,5}$/", $id) )
	{
		// Get dictionary settings
		$arDictParam = getDictParam($id);
		//
		// No any settings, return to the dictionary listing
		if (empty($arDictParam))
		{
			gwtk_header(append_url($sys['server_proto'].$sys['server_host'].$sys['page_admin'].'?'.GW_ACTION.'='.BRWS.'&'.GW_TARGET.'=' . GW_T_DICT));
		}
		//
		$DBTABLE = $arDictParam['tablename'];
		// add dictionary ID into HTML-template
		$tpl->addVal( 'd', $d );
		//
		$languagelist = $L->languagelist();
		$str_is = $arDictParam['is_active'] ? '<span class="green">%s</span>' : '<span class="red">%s</span>';
		$str_is = sprintf($str_is, $L->m('is_'.$arDictParam['is_active']));

		//
		$strDictDetails .= '<table cellspacing="0" cellpadding="2" border="0" width="100%">';
		$strDictDetails .= '<tr class="t" style="vertical-align:top">';
		$strDictDetails .= '<td style="width:45%"><span class="f">' . $L->m('dict') . ':</span><br />'. '<a href="'.$sys['page_index'].'?a=list&amp;d='.$id.'" onclick="window.open(this.href);return false;">' . $arDictParam['title'] . '</a></td>';
		$strDictDetails .= '<td style="width:25%"><span class="f">' . $L->m('termsamount') . ':</span><br />'. $oFunc->number_format($arDictParam['int_terms'], 0, $L->languagelist('4')) .'</td>';
		$strDictDetails .= '<td style="width:10%"><span class="f">' . $L->m('status') . ':</span><br />'. $str_is . '</td>';
		$strDictDetails .= '<td style="width:10%"><span class="f">' . $L->m('lang') . ':</span><br />'. $languagelist[$arDictParam["lang"]] . '</td>';
		$strDictDetails .= '<td style="width:10%"><span class="f">' . $L->m('size') . ':</span><br />'. $oFunc->number_format($arDictParam["int_bytes"]/1024, 1, $L->languagelist('4')) . '&#160;'. $L->m('kb') . '</td>';
		$strDictDetails .= '</tr>';
		$strDictDetails .= '</table>';
		$tpl->addVal( "DICT_DETAILS", $strDictDetails );
		//
		// Get a term parameters
		//
		if ($tid != '')
		{
			$arTermParam = getTermParam($tid);
		}
	} // is dictionary known

	// Display navigation tool bar
	$strL .= admGetNavbar($arNav, ${GW_ACTION}, ${GW_TARGET});

	if (empty($arDictParam)){ $arDictParam = array(); }

	$q = trim($oFunc->mb_substr($q, 0, 254));
	// Search for terms
	if ($a == 'srch')
	{
		if ($id_srch == '') // 1st search query
		{
			$gw_this['arSrchResults'] = gw_search($q, $gw_this['arDictListSrch'], $srch);
		}
		else // list seach results
		{
			$gw_this['arSrchResults'] = gw_search_results($id_srch, $p);
		}
		$tpl->addVal( 'q', textcodetoform($q) );

		$intSumPages = 1;
		if ( isset($gw_this['arSrchResults']['pages']) )
		{
			$intSumPages = $gw_this['arSrchResults']['pages'];
		}
		// fix page number
		if ( ( $p < 1 ) || ( $p > $intSumPages) ){ $p = 1; }
		if ($intSumPages > 1)
		{
			$tpl->addVal("L_pages", $L->m('L_pages'));
		}
		$sys['total'] = $gw_this['arSrchResults']['found'];
		$tpl->addVal( 'q', textcodetoform($gw_this['arSrchResults']['q']) );
		$tpl->addVal( 'PAGE_SELECTION', '');
		/* enable page navigation */
		if ($intSumPages > 1)
		{
			$tpl->addVal( 'PAGE_SELECTION',
					getNavToolbar($intSumPages, $p, $sys['page_admin'] . '?'.GW_ACTION.'='.SEARCH.'&id_srch='.$id_srch.'&d='.$d.'&p=')
					);
		}
		if ($gw_this['arSrchResults']['found'] == 0) // nothing was found
		{
			$gw_this['arSrchResults']['html'] =
			'<div style="font:75% verdana, helvetica">'.
			'<p>'. $L->m('reason_5') . '</p>'.
			'<p>' .$oHtml->a(append_url($sys['page_admin'] . '?'. GW_ACTION . '='.SEARCH.'&amp;d='.$d."&amp;q=".urlencode($gw_this['arSrchResults']['q']).'&amp;in=both'),
								$L->m('srch_-1')).
			'</p>'.
			'</div>';
		}
		$strR .= $gw_this['arSrchResults']['html'];

	}

	// Display search form
	if (($id != '') && ((${GW_TARGET} == GW_T_DICT) || (${GW_TARGET} == GW_T_TERM)) || !empty($q) || !empty($id_srch))
	{
		$tpl->addVal( 'srch_a_name', SEARCH);
		$tplConstruct[] = $sys['path_admin'].'/'.$sys['path_tpl'].'/searchform.html';
	}
	else
	{
		$tplConstruct[] = $sys['path_admin'].'/'.$sys['path_tpl'].'/searchform_empty.html';
	}
##
## --------------------------------------------------------


$strForm = $strToMain = '';
$isPostError = 1;

## --------------------------------------------------------
## Preview items (translation, transcription etc.)
if (is_array($arPre))
{
	if (${GW_ACTION} != GW_A_ADD){ ${GW_ACTION} = GW_A_EDIT; }
	if (isset($arPre[$t]['remove'])){ ${GW_ACTION} = REMOVE; }
	${GW_TARGET} = GW_T_TERM;
}
##
## --------------------------------------------------------

// constructs 'at.{GW_ACTION}_{GW_TARGET}.php' files
$pathAction = $sys['path_include'] . '/' . GW_TARGET . '.' . ${GW_TARGET} . '.inc.' . $sys['phpEx'];

$gw_this['vars']['cur_funcname'] = $gw_this['vars'][GW_TARGET] . '_' . $gw_this['vars'][GW_ACTION];
// array index for $L->m()
$strPageIndex = isset($arPageNumbers[${GW_TARGET} . "_" . ${GW_ACTION}])
				? "2_page_" . $arPageNumbers[${GW_TARGET} . "_" . ${GW_ACTION}]
				: "2_page_1";
if (${GW_TARGET} != '')
{
	file_exists($pathAction) ? include_once($pathAction) : printf($L->m("reason_10"), $pathAction);
}
file_exists($sys['path_include'].'/t.'.$gw_this['vars']['cur_funcname'].'.inc.' . $sys['phpEx']) 
	? include_once($sys['path_include'].'/t.'.$gw_this['vars']['cur_funcname'].'.inc.' . $sys['phpEx']) 
	: '';

// set current action name (page numbers)
$tpl->addVal( "CURRENT_ACTION", $L->m($strPageIndex));

// create dictionaries list
$tpl->AddVal( 'block:SearchSelect', getDictSrch('', 1, 99,'', 1, $d));

if ($strPageIndex == "2_page_1") // Control panel home
{
	$tpl->addVal( 'A_ADD', GW_A_ADD);
	$tpl->addVal( 'L_ADD', $L->m('3_add'));

	$arHelpMap = array(
					'tip016' => 'tip019',
					'tip017' => 'tip020',
					'tip018' => 'tip021'
				 );
	$strHelp = '';
	$strHelp .= '<dl>';
	for(; list($k, $v) = each($arHelpMap);)
	{
		$strHelp .= '<dt><b>' . $L->m($k) . '</b></dt>';
		$strHelp .= '<dd>' . $L->m($v) . '</dd>';
	}
	$strHelp .= '</dl>';
	$strR .= '<br/>'.kTbHelp($L->m('2_tip'), $strHelp);
}
else
{
   $tpl->addVal( "TITLE", $L->m('2_page_1') . ' - ' . $L->m($strPageIndex) );
}


// Last elements for page
$tplConstruct[] = $sys['path_admin']. '/' . $sys['path_tpl'] . '/page_admin.html';
$tplConstruct[] = $sys['path_admin']. '/' . $sys['path_tpl'] . '/footer.html';

// Add previously defined template variables
for (reset($arTplVars['srch']); list($k, $v) = each($arTplVars['srch']);)
{
	while (is_array($v) && list($k2, $v2) = each($v))
	{
		$tpl->AddVal($k2, $v2);
	}
}
// Add previously defined template files
if ( isset($tplConstruct) && is_array($tplConstruct) )
{
	for (reset($tplConstruct); list($k, $v) = each($tplConstruct); )
	{
		$tpl->setHandle( $k , $v );
	}
}
/* close sessions */
page_close();

// --------------------------------------------------------
// Debug information
if (DEBUG)
{
	include($sys['path_include'] . '/page.footer.' . $sys['phpEx']);
}
// --------------------------------------------------------
// last header
$oHdr->add('Content-Type: text/html; charset='.$L->languagelist('2'));
$oHdr->output();

# $strR .= '<br />';
# $strR .= getTop10('R_DICT_EFFIC', 100);

$tpl->addVal( "TOMAIN", $strToMain );
$tpl->addVal( "ADMIN_RIGHT_SIDE", $strR );
$tpl->addVal( "ADMIN_LEFT_SIDE", $strL );
$tpl->parse();

print $tpl->output();

#print_r($_POST);


?>