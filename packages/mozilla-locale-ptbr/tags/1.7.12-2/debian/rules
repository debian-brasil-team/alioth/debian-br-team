#!/usr/bin/make -f

# Mostly taken from mozilla-locale-ja and mozilla-locale-fr. 
# Thanks to Takuo Kitame <kitame@northeye.org> and 
# Aurelien Jarno <aurel32@debian.org>.

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

VER = 1.7.8

FRPTEMP = frp-temp
FLPTEMP = flp-temp

configure: configure-stamp
configure-stamp:
	dh_testdir
	touch configure-stamp

build: configure-stamp build-stamp
build-stamp:
	dh_testdir

	rm -rf build-tree
	unzip -d build-dir langptbr*.xpi

	cd build-dir/bin/chrome/ && \
	unzip pt-BR.jar && \
	sed -e 's/{&buildId.label;}/($(VER))/g' \
	locale/pt-BR/navigator/navigator.dtd > locale/pt-BR/navigator/navigator.dtd.$(VER) && \
	mv locale/pt-BR/navigator/navigator.dtd.$(VER) \
	   locale/pt-BR/navigator/navigator.dtd && \
	for patch in `ls ../../../debian/patches/flp/*` ; do \
		patch -p0 < $$patch ; \
	done && \
	rm -r pt-BR.jar && \
	zip -r pt-BR.jar locale && \
	rm -rf locale

	cd build-dir/bin/chrome/ && \
	unzip BR.jar && \
	sed -e 's!homePageDefault.*!homePageDefault=http://www.debian.org/!g' \
	    -e 's!browser\.startup\.homepage.*!browser.startup.homepage=file:///usr/share/doc/mozilla-locale-ptbr/localstart.html!g' \
	    -e 's!startup.homepage_override_url.*!startup.homepage_override_url=file:///usr/share/doc/mozilla-locale-ptbr/localstart.html!g' \
	locale/BR/navigator-region/region.properties > \
	locale/BR/navigator-region/region.properties.ptbr && \
	mv locale/BR/navigator-region/region.properties.ptbr \
	   locale/BR/navigator-region/region.properties && \
	rm -r BR.jar && \
	zip -r BR.jar locale && \
	rm -rf locale

	touch build-stamp

clean:
	dh_testdir
	dh_testroot

	# code based on gnome-pkg-tools to auto create Uploaders field from 
	# metainfo available on the Debian Brasil's Team SVN repo
	TEAM_LIST=../../metainfo/debian-br.team; \
	if test -f $${TEAM_LIST}; then \
		MAINTAINER=$$(sed -n 's/^Maintainer: //p' debian/control.in); \
		grep -vF "$${MAINTAINER}" $${TEAM_LIST} | tr '\n' ' ' > debian/uploaders.tmp; \
		cat debian/uploaders.tmp; \
		if diff -q debian/uploaders debian/uploaders.tmp; then \
			rm debian/uploaders.tmp; \
		else \
			mv debian/uploaders.tmp debian/uploaders; \
		fi; \
	fi
	UPLOADERS=$$(cat debian/uploaders); \
	sed "s/@DEBIAN_BR_TEAM@/$${UPLOADERS}/g" < debian/control.in > debian/control.tmp; \
	if diff -q debian/control debian/control.tmp; then \
		rm debian/control.tmp; \
	else \
		mv debian/control.tmp debian/control; \
	fi
	# end of Uploaders generating code

	rm -f build-stamp configure-stamp
	-rm -rf build-dir

	dh_clean

install: build
	dh_testdir
	dh_testroot
	dh_clean -k
	dh_installdirs

	install -d  $(CURDIR)/debian/mozilla-locale-ptbr/usr/lib/mozilla/chrome/

	install -m 644 build-dir/bin/chrome/pt-unix.jar \
	        $(CURDIR)/debian/mozilla-locale-ptbr/usr/lib/mozilla/chrome/
	install -m 644 build-dir/bin/chrome/pt-BR.jar \
	        $(CURDIR)/debian/mozilla-locale-ptbr/usr/lib/mozilla/chrome/

	cd build-dir && tar chf - bin/chrome/ | \
	tar -C $(CURDIR)/debian/mozilla-locale-ptbr/usr/lib/mozilla/ -xhf - 
	cd $(CURDIR)/debian/mozilla-locale-ptbr/usr/lib/mozilla/ && mv bin/chrome/* chrome/ && \
		rm -r bin/

	install -m 644 debian/50ptbr-locale debian/mozilla-locale-ptbr/var/lib/mozilla/chrome.d

	echo "Upstream-Version=$(VER)" > debian/mozilla-locale-ptbr.substvars

# Build architecture-independent files here.
binary-indep: build install
	dh_testdir
	dh_testroot
	dh_installdocs -i debian/localstart.html
	dh_installchangelogs -i
	dh_link -i
	dh_compress -i
	dh_fixperms -i
	dh_installdeb -i
	dh_gencontrol -i
	dh_gencontrol -i
	dh_md5sums -i
	dh_builddeb -i

# Build architecture-dependent files here.
binary-arch: build install
# We have nothing to do by default.

binary: binary-indep binary-arch
.PHONY: build clean binary-indep binary-arch binary install configure
