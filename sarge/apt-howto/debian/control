Source: apt-howto
Section: doc
Priority: optional
Maintainer: Gustavo Noronha Silva <kov@debian.org>
Uploaders: Osamu Aoki <osamu@debian.org>, Debian-BR Team Maintainers <debian-devel-portuguese@lists.debian.org>, Eduardo Marcel Macan <macan@debian.org>, Goedson Teixeira Paixao <goedson@debian.org>, Gustavo Franco <stratus@acm.org>, Marcio Roberto Teixeira <marciotex@pop.com.br>, Otavio Salvador <otavio@debian.org> 
Build-Depends-Indep: debhelper (>> 3.0.0), debiandoc-sgml(>= 1.1.86), tetex-bin, tetex-extra, w3m (>= 0.3) | w3m-ssl (>= 0.3) | w3mmee (>= 0.3), cjk-latex, freetype1-tools, tfm-arphic-bsmi00lp, tfm-arphic-bkai00mp, tfm-arphic-gbsn00lp, tfm-arphic-gkai00mp, hbf-jfs56, hbf-cns40-b5, hbf-kanji48, gs, cjk-latex, hlatex, hlatex-fonts-base
Standards-Version: 3.6.1

Package: apt-howto
Architecture: all
Depends: apt-howto-pt-br, apt-howto-ca, apt-howto-el, apt-howto-en, apt-howto-de, apt-howto-fr, apt-howto-pl, apt-howto-ko, apt-howto-es, apt-howto-it, apt-howto-ru, apt-howto-tr, apt-howto-ja
Description: example-based guide to APT
 This manual tries to be a quick but complete source of
 information about the APT system and its features.
 . 
 It documents the main uses of APT with many examples.
 .
 This package depends on all translations of the manual.
 Its purpose is to provide a smooth upgrade for those
 who had apt-howto installed before the package was split.
 It may then be removed.

Package: apt-howto-common
Architecture: all
Suggests: apt-howto-en, apt-howto-pt-br, apt-howto-de, apt-howto-fr, apt-howto-pl, apt-howto-ko, apt-howto-es, apt-howto-it, apt-howto-ru, apt-howto-tr
Description: example-based guide to APT
 This manual tries to be a quick but complete source of
 information about the APT system and its features.
 . 
 It documents the main uses of APT with many examples.
 .
 This package contains common files for all the manuals.

Package: apt-howto-pt-br
Architecture: all
Conflicts: apt-howto (<= 1.7.6-1)
Replaces: apt-howto (<= 1.7.6-1)
Depends:  apt-howto-common
Description: example-based guide to APT (Portuguese)
 This manual tries to be a quick but complete source of
 information about the APT system and its features.
 . 
 It documents the main uses of APT with many examples.
 .
 This package holds the Portuguese version, which is the 
 primary version.

Package: apt-howto-ca
Architecture: all
Conflicts: apt-howto (<= 1.7.6-1)
Replaces: apt-howto (<= 1.7.6-1)
Depends:  apt-howto-common
Description: example-based guide to APT (Catalan)
 This manual tries to be a quick but complete source of
 information about the APT system and its features.
 . 
 It documents the main uses of APT with many examples.
 .
 This package contains the Catalan version.

Package: apt-howto-en
Architecture: all
Conflicts: apt-howto (<= 1.7.6-1)
Replaces: apt-howto (<= 1.7.6-1)
Depends:  apt-howto-common
Description: example-based guide to APT (English)
 This manual tries to be a quick but complete source of
 information about the APT system and its features.
 . 
 It documents the main uses of APT with many examples.
 .
 This package contains the English version.

Package: apt-howto-el
Architecture: all
Conflicts: apt-howto (<= 1.7.6-1)
Replaces: apt-howto (<= 1.7.6-1)
Depends:  apt-howto-common
Description: example-based guide to APT (Greek)
 This manual tries to be a quick but complete source of
 information about the APT system and its features.
 . 
 It documents the main uses of APT with many examples.
 .
 This package contains the Greek version.

Package: apt-howto-de
Architecture: all
Conflicts: apt-howto (<= 1.7.6-1)
Replaces: apt-howto (<= 1.7.6-1)
Depends:  apt-howto-common
Description: example-based guide to APT (German)
 This manual tries to be a quick but complete source of
 information about the APT system and its features.
 . 
 It documents the main uses of APT with many examples.
 .
 This package contains the German version.

Package: apt-howto-fr
Architecture: all
Conflicts: apt-howto (<= 1.7.6-1)
Replaces: apt-howto (<= 1.7.6-1)
Depends:  apt-howto-common
Description: example-based guide to APT (French)
 This manual tries to be a quick but complete source of
 information about the APT system and its features.
 . 
 It documents the main uses of APT with many examples.
 .
 This package contains the French version.

Package: apt-howto-pl
Architecture: all
Conflicts: apt-howto (<= 1.7.6-1)
Replaces: apt-howto (<= 1.7.6-1)
Depends:  apt-howto-common
Description: example-based guide to APT (Polish)
 This manual tries to be a quick but complete source of
 information about the APT system and its features.
 . 
 It documents the main uses of APT with many examples.
 .
 This package contains the Polish translation.

Package: apt-howto-ko
Architecture: all
Conflicts: apt-howto (<= 1.7.6-1)
Replaces: apt-howto (<= 1.7.6-1)
Depends:  apt-howto-common
Description: example-based guide to APT (Korean)
 This manual tries to be a quick but complete source of
 information about the APT system and its features.
 . 
 It documents the main uses of APT with many examples.
 .
 This package contains the Korean translation.

Package: apt-howto-es
Architecture: all
Conflicts: apt-howto (<= 1.7.6-1)
Replaces: apt-howto (<= 1.7.6-1)
Depends:  apt-howto-common
Description: example-based guide to APT (Spanish)
 This manual tries to be a quick but complete source of
 information about the APT system and its features.
 . 
 It documents the main uses of APT with many examples.
 .
 This package contains the Spanish version.

Package: apt-howto-it
Architecture: all
Conflicts: apt-howto (<= 1.7.6-1)
Replaces: apt-howto (<= 1.7.6-1)
Depends:  apt-howto-common
Description: example-based guide to APT (Italian)
 This manual tries to be a quick but complete source of
 information about the APT system and its features.
 . 
 It documents the main uses of APT with many examples.
 .
 This package contains the Italian version.

Package: apt-howto-ru
Architecture: all
Conflicts: apt-howto (<= 1.7.6-1)
Replaces: apt-howto (<= 1.7.6-1)
Depends:  apt-howto-common
Description: example-based guide to APT (Russian)
 This manual tries to be a quick but complete source of
 information about the APT system and its features.
 . 
 It documents the main uses of APT with many examples.
 .
 This package contains the Russian version.

Package: apt-howto-ja
Architecture: all
Conflicts: apt-howto (<= 1.7.6-1)
Replaces: apt-howto (<= 1.7.6-1)
Depends:  apt-howto-common
Description: example-based guide to APT (Japanese)
 This manual tries to be a quick but complete source of
 information about the APT system and its features.
 . 
 It documents the main uses of APT with many examples.
 .
 This package contains the Japanese version.

Package: apt-howto-tr
Architecture: all
Conflicts: apt-howto (<= 1.7.6-1)
Replaces: apt-howto (<= 1.7.6-1)
Depends:  apt-howto-common
Description: example guide to APT (Turkish)
 This manual tries to be a quick but complete source of
 information about the APT system and its features.
 . 
 It documents the main uses of APT with many examples.
 .
 This package contains the Turkish version.
