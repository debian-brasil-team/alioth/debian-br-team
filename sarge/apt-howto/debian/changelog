apt-howto (1.8.10.3-1) unstable; urgency=low

  * debian/control.in:
  - added cjk-latex, hlatex, and hlatex-fonts-base to Build-Depends
    and increased minimun version for debiandoc-sgml to 1.1.86 so
    that PDF files for Korean, Japanese and Chinese are possible

 -- Gustavo Noronha Silva <kov@debian.org>  Sun,  6 Mar 2005 11:11:12 -0300

apt-howto (1.8.10.2-1) unstable; urgency=medium

  * apt-howto.it.sgml:
  - updated to 1.8.10, thanks to Luca Monducci!
  * urgency set to medium as the changes are simple and this
    new translation should get into sarge soon.
  * debian/control.in, debian/rules:
  - updated to use information from the Debian Brasil's Team
    SVN repo to auto-update the Uploaders field, if available.
  * debian/uploaders:
  - added to provide a source which always has the names of uploaders
    even if not building from the repo - one might need to mess with
    debian/control.in outside the repo.
  * debian/control.in:
  - modified short description to make it perhaps more informative
    and to make lintian happy

 -- Gustavo Noronha Silva <kov@debian.org>  Wed, 12 Jan 2005 21:36:07 -0200

apt-howto (1.8.10.1-2) unstable; urgency=high

  * debian/control:
  - added -ca and -el to Depends on apt-howto package
  - moved apt-howto-en from Recommends to Suggests, the -common
    package should simply not be requested for instalation itself
  * debian/patches/00_special_case_el_in_fixhtml.diff:
  - added to also special-case .el in bin/fixhtml, as it was 
    generating *.en.html files
  * debian/rules:
  - remove *.ent on clean:
  - added build stuff for ca and el (Closes: #285394)
  - apply the special case patch on configure and deapply on clean
  * urgency set to high because it is a quite nasty bug not
    providing the actual documentation for two languages

 -- Gustavo Noronha Silva <kov@debian.org>  Mon, 13 Dec 2004 22:15:06 -0200

apt-howto (1.8.10.1-1) unstable; urgency=medium

  * New "incremental" release
  * apt-howto.en.sgml, apt-howto.pt-br.sgml:
  - small syntactic fixes
  * apt-howto.fr.sgml:
  - updated translation to version 1.8.10, by Julien Louis
  * Makefile:
  - applied idea from Frank Lichtenheld <djpig@debian.org>
    to fix build of el (Closes: #284606)

 -- Gustavo Noronha Silva <kov@debian.org>  Tue,  7 Dec 2004 23:24:42 -0200

apt-howto (1.8.10-1) unstable; urgency=low

  * apt-howto.en.sgml, apt-howto.pt-br.sgml:
  - relicensed to GPL, some other translations still missing
    after all of them are updated, this will close bug 280673.

 -- Gustavo Noronha Silva <kov@debian.org>  Mon, 15 Nov 2004 14:00:58 -0200

apt-howto (1.8.9) UNRELEASED; urgency=low

  * apt-howto.en.sgml:
  - small correction best->better, thanks to Tom Fernandes 
    <anyaddress@gmx.net>
  - applied patch from J. Bruce Fields <bfields@fieldses.org> to
    make the pin-priority discussion clearer (Closes: #278100)
  - there modifications make 1.8.9
  * apt-howto.pt-br.sgml:
  - updated to 1.8.9

 -- Gustavo Noronha Silva <kov@debian.org>  Sun, 31 Oct 2004 20:49:54 -0300

apt-howto (1.8.8.1) unstable; urgency=high

  * Build with newer debiandoc-sgml to take advantage of the
    bugfixes done in the latest version
  * apt-howto.fr.sgml:
  - translation update to 1.8.8 by Julien Louis
  * Severity high because there was no real change and this
    should follow debiandoc-sgml into testing soon

 -- Gustavo Noronha Silva <kov@debian.org>  Wed, 20 Oct 2004 22:35:32 -0300

apt-howto (1.8.8) unstable; urgency=low

  * New upstream release
  * Updated from DDP's CVS:
  - apt-howto.en.sgml (1.8.6 -> 1.8.8)
   + clarifies debconf configuration stuff for apt-listchanges
     (Closes: #203443)
   + applied patch by "Guilherme de S. Pastore" <gpastore@colband.com.br>
     to fix some typos (Closes: #257349)
   + fixes the apt-show-versions example (Closes: #245158)
  - apt-howto.fr.sgml (1.8.5 -> 1.8.7)
  - apt-howto.pt-br.sgml (1.8.5 -> 1.8.8)

 -- Gustavo Noronha Silva <kov@debian.org>  Sun, 12 Sep 2004 12:32:16 -0300

apt-howto (1.8.6-3) unstable; urgency=low

  * Gustavo Noronha Silva:
   - (el):
    + translation updates by George Papamichelakis <george@step.gr>
   - *.sgml:
    + updated all files from the DDP CVS repository
   - debian/changelog converted to UTF-8
  * Osamu Aoki:
   - Fix build script to cope with new tetex-bin 2.0.2-18 (Closes: #266063)
     This fix is based on Atsuhito Kohda's patch to debian-reference.
   - Add gs to build-dep

 -- Gustavo Noronha Silva <kov@debian.org>  Sun, 15 Feb 2004 16:15:17 -0300

apt-howto (1.8.6-2) unstable; urgency=low

  * Added Greek translation, by George Papamichelakis <george@step.gr>
    and Catalan translation by Antoni Bella Perez <vasten@telefonica.net>
    (Closes: #221871)
  
 -- Gustavo Noronha Silva <kov@debian.org>  Sun, 15 Feb 2004 12:22:05 -0300

apt-howto (1.8.6-1) unstable; urgency=low

  * The source package is repackaged by Osamu Aoki and Jens Seidel
    for the better hyphenation and CJK support platform.
  * Japanese translation now contains real PS and PDF files.
  * The document contents are taken from DDP CVS.
  * Added apt-howto-common package to handle manuals etc.
  * Added menu entry.
  * NMU with the maintainer approval.

 -- Osamu Aoki <osamu@debian.org>  Sun, 31 Aug 2003 20:54:02 +0200

apt-howto (1.8.5-3) unstable; urgency=low

  * (it, fr):
  - updated to 1.8.5
  * (en):
  - fixed small typo (bellow)

 -- Gustavo Noronha Silva <kov@debian.org>  Tue, 22 Jul 2003 01:29:41 -0300

apt-howto (1.8.5-2) unstable; urgency=low

  * *.sgml:
  - patch to undo overchanging of 'Priority' to 'Pin-Priority'
    thanks to Ingo Saitz <Ingo.Saitz@stud.uni-hannover.de>

 -- Gustavo Noronha Silva <kov@debian.org>  Thu,  3 Jul 2003 01:13:33 -0300

apt-howto (1.8.5-1) unstable; urgency=low

  * New upstream release (en, pt_BR)
  * *.sgml:
  * apt-howto.[pt_BR,en].sgml:
    - Moved some text from the "install" section to the
      "default-version" section, where it fits better.
      Also clarify the specific version installation and
      change some text that implied testing was better than
      unstable. (Closes: #180534)
  * *.sgml
    Changed the prompt symbol used on the examples
    of the section about source packages to show
    '$' instead of '#' where possible on all translations
    (Closes: #181277)
  * *.sgml
    Change 'Priority' to 'Pin-Priority' on all the
    versions. (Closes: #195270)
  * debian/README.Debian:
  - fix typo 'english'->'English', 'portuguese'->'Portuguese'

 -- Gustavo Noronha Silva <kov@debian.org>  Wed,  2 Jul 2003 18:39:39 -0300

apt-howto (1.8.3-5) unstable; urgency=low

  * debian/rules:
  - ugh, forgot to add tr as a language that should be packaged

 -- Gustavo Noronha Silva <kov@debian.org>  Sun, 13 Apr 2003 23:09:50 -0300

apt-howto (1.8.3-4) unstable; urgency=low

  * debian/control, debian/apt-howto-tr.doc-base:
  - new translation package - apt-howto-tr, thanks
    to Murat Demirten
  * apt-howto.it.sgml:
  - Luca fixed some typo bugs and reworded 1 section
  * apt-howto.es.sgml:
  - Thomas Bliesener updated to 1.8.3
  * apt-howto.fr.sgml:
  - typo fix

 -- Gustavo Noronha Silva <kov@debian.org>  Sat,  5 Apr 2003 02:10:30 -0300

apt-howto (1.8.3-3) unstable; urgency=low

  * apt-howto.es.sgml:
  - updated to 1.8.2, thanks to Thomas Bliesener <bli@melix.com.mx>

 -- Gustavo Noronha Silva <kov@debian.org>  Wed, 22 Jan 2003 23:34:57 -0200

apt-howto (1.8.3-2) unstable; urgency=low

  * apt-howto.it.sgml:
  - updated to 1.8.3, thanks to Luca
  * debian/control:
  - updated Standards-Version
  * debian/README.Debian:
  - fixed minor spelling mistakes, fixed Debian-BR's URL

 -- Gustavo Noronha Silva <kov@debian.org>  Tue, 14 Jan 2003 01:06:26 -0200

apt-howto (1.8.3-1) unstable; urgency=low

  * New release (1.8.3):
  Up-to-date:
  - apt-howo.pt_BR.sgml
  - apt-howo.en.sgml
  - apt-howo.fr.sgml
  * changes for 1.8.3:
  - applied patch from Luca which replaces all > and < with
    &gt; and &lt;
  - made more paragraphs around examples, to get po-debiandoc
    to work better
  - made all fixes about # for root, etc as in pt_BR version
  - added explanation about the showsrc arg for apt-cache
  * apt-howto.it.sgml:
  - applied patch from Luca to update the file to 1.8.2
    with the &lt; and &gt; corrections in place

 -- Gustavo Noronha Silva <kov@debian.org>  Sun, 22 Dec 2002 00:19:20 -0200

apt-howto (1.8.2-2) unstable; urgency=low

  * apt-howto.{de,fr,pl,ru}.sgml:
  - updated to 1.8.2

 -- Gustavo Noronha Silva <kov@debian.org>  Fri, 22 Nov 2002 13:17:30 -0200

apt-howto (1.8.2-1) unstable; urgency=low

  * apt-howto.{en,pt_BR}.sgml:
  - new version
  - reworded section about using APT localy (dpkg-scanpackages)
  * apt-howto.{en,it}.sgml:
  - accepted patch from Luca Monducci <luca.mo@tiscali.it>
    fixing small problem
  * apt-howto.it.sgml:
  - updated to version 1.8.1
  * apt-howto.de.sgml, debian/control, debian/apt-howto-de.doc-base:
  - added, thanks to David Spreen <netzwurm@debian.org>
  - version 1.8.0
  * debian/rules:
  - changed the names of the files *-$lang.* to *.$lang.*

 -- Gustavo Noronha Silva <kov@debian.org>  Fri,  8 Nov 2002 22:07:10 -0200

apt-howto (1.8.0-1) unstable; urgency=low

  * apt-howto-{en,pt_BR}.sgml:
  - new version
  - removed section about downgrading a Debian distribution,
    that's misleading (Closes: #159126)
  - added links to more distributions that are supporting APT
    (from apt4rpm's site)
  - applied patch from Luca to make apt-howto po-debiandoc-friendly
  * apt-howo-ru.sgml:
  - updated to version 1.8.0, thanks to ilgiz 
    kalmetev <ilgiz@bashtelecom.ru>
  * apt-howto-pl.sgml:
  - updated to version 1.8.0, thanks to "Pawe³ Têcza" 
    <pawel.tecza@poczta.fm>
  * debian/control:
  - accepted patch from jeremy@jsbygott.fsnet.co.uk to improve
    the description (Closes: #160241) 
  * apt-howto-it.sgml:
  - updated to 1.8.0, thanks to Luca Monducci <luca.mo@tiscali.it>
  * apt-howto-fr.sgml:
  - updated to 1.8.0, thanks to Julien Louis

 -- Gustavo Noronha Silva <kov@debian.org>  Fri, 20 Sep 2002 00:41:02 -0300

apt-howto (1.7.9-3) unstable; urgency=low

  * apt-howto-fr.sgml:
  - updated to 1.7.9, thanks to Julien Louis

 -- Gustavo Noronha Silva <kov@debian.org>  Mon,  9 Sep 2002 03:11:38 -0300

apt-howto (1.7.9-2) unstable; urgency=low

  * apt-howto-pl.sgml,apt-howto-it.sgml:
  - updated translation to 1.7.9 thanks to Pawel Tecza
    and Luca Monducci <luca.mo@tiscali.it>
  * apt-howto.spec:
  - spec file to generate apt-howto.rpm, thanks to
    Richard Bos <allabos@freeler.nl>
  * debian/README.Debian:
  - files are now located on /usr/share/doc/Debian
    (Closes: #158442)

 -- Gustavo Noronha Silva <kov@debian.org>  Tue, 27 Aug 2002 17:35:35 -0300

apt-howto (1.7.9-1) unstable; urgency=low

  * New version, fixes misinformation about the --reinstall
    switch (pt_BR, en)
  * apt-howto-fr.sgml:
  - proof reading by Julien Louis, thanks (1.7.8)
  * apt-howto-ru.sgml:
  - translatio update (1.7.9), thanks to 
    ilgiz kalmetev <ilgiz@bashtelecom.ru>

 -- Gustavo Noronha Silva <kov@debian.org>  Thu, 15 Aug 2002 10:38:37 -0300

apt-howto (1.7.8-2) unstable; urgency=low

  * apt-howto-{fr,it}:
  - corrections to fr by Philippe Batailler and translation update to 
    it by Luca Monducci <luca.mo@tiscali.it>

 -- Gustavo Noronha Silva <kov@debian.org>  Wed, 31 Jul 2002 17:24:53 -0300

apt-howto (1.7.8-1) unstable; urgency=low

  * New version, including a section about apt-file
  * apt-howto-fr.sgml, debian/control, debian/apt-howto-fr.doc-base:
  - added new package apt-howto-fr, thanks to Julien Louis
  * apt-howto-pl.sgml:
  - updated by Pawel Tecza, thanks

 -- Gustavo Noronha Silva <kov@debian.org>  Wed, 24 Jul 2002 04:12:14 -0300

apt-howto (1.7.7-7) unstable; urgency=low

  * Accepted a patch from Osamu Aoki <debian@aokiconsulting.com>
    to change the layout of installed documents.
  * debian/control:
  - splited -> split (Closes: #150873)
  * apt-howto-ja.sgml:
  - added missing <p>  tag (Closes: #148447)

 -- Gustavo Noronha Silva <kov@debian.org>  Thu, 27 Jun 2002 10:57:11 -0300

apt-howto (1.7.7-6) unstable; urgency=low

  * apt-howto-en.sgml:
  - fixed dupped words, thanks to Luca Monducci <luca.mo@tiscali.it>

 -- Gustavo Noronha Silva <kov@debian.org>  Sat,  1 Jun 2002 03:44:08 -0300

apt-howto (1.7.7-5) unstable; urgency=low

  * apt-howto-ja.sgml, debian/control, debian/rules:
  - added new translation, thanks to Tomohiro KUBOTA <kubota@debian.org>

 -- Gustavo Noronha Silva <kov@debian.org>  Tue, 14 May 2002 01:11:56 -0300

apt-howto (1.7.7-4) unstable; urgency=low

  * apt-howto-it.sgml:
  - updated to match 1.7.7, thanks to Luca Monducci <luca.mo@tiscali.it>
  * apt-howto-en.sgml:
  - removed double <p>, thanks to Luca, also.

 -- Gustavo Noronha Silva <kov@debian.org>  Mon,  6 May 2002 23:50:16 +0000

apt-howto (1.7.7-3) unstable; urgency=high

  * apt-howto-pl.sgml:
  - updated to match 1.7.7, thanks to "Pawe" <pawel.tecza@poczta.fm>
  * urgency high because 1.7.6 misses important information, so 1.7.7
    is better suited to woody... also, there are no "dangerous"
    packaging changes since the latest releases, just contents to the
    sgml

 -- Gustavo Noronha Silva <kov@debian.org>  Sat, 20 Apr 2002 17:13:01 -0300

apt-howto (1.7.7-2) unstable; urgency=low

  * apt-howto-en.sgml,ml, apt-howto-pt_BR.sgml:
  - noted the need to include lines of both distributions
    when using 'mixed'systems (Closes: #142574)

 -- Gustavo Noronha Silva <kov@debian.org>  Sun, 14 Apr 2002 02:16:02 -0300

apt-howto (1.7.7-1) unstable; urgency=low

  * debian/control, debian/rules:
  - added new packages apt-howto-{it,ru}
  - added tetex-extra to Build-Depends-Indep, to fix the
    building of the package (Closes: #140131)
  * Adds new Russian translation, thanks to ilgiz kalmetev 
    <ilgiz@bashtelecom.ru> and Italian translation thanks
    to Luca Monducci <luca.mo@tiscali.it>
  * Typo fixes and new versions, with more information on
    mixing releases

 -- Gustavo Noronha Silva <kov@debian.org>  Sun, 31 Mar 2002 19:25:28 -0300

apt-howto (1.7.6-9) unstable; urgency=low

  * debian/control:
  - added new package apt-howto-es
  * adds new spanish translation, thanks to Thomas Bliesener 
    <bli@melix.com.mx>
  * Fixes problems with the naming of the text files in all
    doc-base files. (Closes: #137517)
  * Fixes typo on 'useful', thanks to Matt Kraai <kraai@debian.org>
  * Applied a patch with more typos fixed, thanks to
    Matt Kraai <kraai@debian.org>
  * Added Matt Kraai to the Thanks section of all languages.
    I copied the "thanks" sentences from Bryan Stillwell in
    all languages and fixed the bad position of the &lt; thing
    on apt-howto-es.sgml on Bryan's entry

 -- Gustavo Noronha Silva <kov@debian.org>  Tue, 12 Mar 2002 21:49:33 -0300

apt-howto (1.7.6-8) unstable; urgency=low

  * debian/control:
  - added tetex-bin to Build-Depends-Indep (Closes: #133076)

 -- Gustavo Noronha Silva <kov@debian.org>  Sat,  9 Feb 2002 14:06:43 -0200

apt-howto (1.7.6-7) unstable; urgency=low

  * debian/control:
  - small typo fixed (versio->version) (Closes: #132302)

 -- Gustavo Noronha Silva <kov@debian.org>  Wed,  6 Feb 2002 01:40:21 -0200

apt-howto (1.7.6-6) unstable; urgency=low

  * apt-howto-ko.sgml:
  - updated translation, thanks to Yooseong Yang <yooseong@debian.org>
  * debian/control:
  - no need for debhelper >> 3.0.0.
  * debian/rules:
  - DH_COMPAT=2

 -- Gustavo Noronha Silva <kov@debian.org>  Fri,  1 Feb 2002 02:43:48 -0200

apt-howto (1.7.6-5) unstable; urgency=low

  * fixed the location of &lt; on Credits section
    (Closes: #128792)
  * debian/control:
  - changed description to tell the user that the apt-howto
    is there and depends on all others to provide a smooth
    upgrade from non-splitted version (Closes: #131246)

 -- Gustavo Noronha Silva <kov@debian.org>  Tue, 29 Jan 2002 17:51:40 -0200

apt-howto (1.7.6-4) unstable; urgency=low

  * spelling corrections
  * updated Polish translation
  * debian/apt-howto-pl.doc-base.apt-howto-pl:
  - changed the description to a description written in Polish
  (thanks to "Pawel Tecza" <pawel.tecza@poczta.fm> for all entries
  above)

 -- Gustavo Noronha Silva <kov@debian.org>  Wed,  9 Jan 2002 13:27:13 -0200

apt-howto (1.7.6-3) unstable; urgency=low

  * debian/README.Debian:
  - fixed the Debian-BR's URL
  * debian/*:
  - fixed the names of the doc-base files

 -- Gustavo Noronha Silva <kov@debian.org>  Mon,  7 Jan 2002 12:48:25 -0200

apt-howto (1.7.6-2) unstable; urgency=low

  * Split the package in several ones, one for each translations.
    I received some informal (non-BTS) messages asking for that
    and I really think that it is better
  * made all lang-specific packages Conflict and Replace 
    apt-howto <= 1.7.6-1

 -- Gustavo Noronha Silva <kov@debian.org>  Sun,  6 Jan 2002 00:41:22 -0200

apt-howto (1.7.6-1) unstable; urgency=low

  * 1.7.6:
  - added a hint for using the -o Debug::pkgProblemResolver=yes
    switch to solve dist-upgrade problems (Closes: #127349)
    thanks to Ross Boylan <RossBoylan@stanfordalumni.org>
  - added APT4RPM as one of the apt-supporting "distributions"
    thanks to Richard Bos <allabos@freeler.nl>
  - wrote a section about the equivs program, this will solve
    some of the problems reported by Ian Bicking <ianb@colorstudy.com>
    the rest of the problems are design decisions of APT and dpkg,
    and were already reported as wishlists, etc (Closes: #120099)
  - wrote a section about the localepurge program, in the new chapter
    I created for equivs and localepurge.

 -- Gustavo Noronha Silva <kov@debian.org>  Fri,  4 Jan 2002 21:05:25 -0200

apt-howto (1.7.5-2) unstable; urgency=low

  * Makefile:
  - removed -c on debiandoc2html call to generate index.html files
  instead of index.$lang.html

 -- Gustavo Noronha Silva <kov@debian.org>  Thu,  3 Jan 2002 21:44:18 -0200

apt-howto (1.7.5-1) unstable; urgency=low

  * small typo fixed (Closes: #127348), thanks to
  Ross Boylan <RossBoylan@stanfordalumni.org>

 -- Gustavo Noronha Silva <kov@debian.org>  Wed,  2 Jan 2002 03:18:03 -0200

apt-howto (1.7.5) unstable; urgency=low

  * Some minor fixes
  * Polish version updated to 1.7.5 too

 -- Gustavo Noronha Silva <kov@debian.org>  Sat, 15 Dec 2001 22:57:43 -0200

apt-howto (1.7.4-1) unstable; urgency=low

  * Updating Polish version, thanks to Pawel Tecza.

 -- Gustavo Noronha Silva <kov@debian.org>  Sun,  2 Dec 2001 00:44:53 -0200

apt-howto (1.7.4) unstable; urgency=low

  * Adding Korean version
  * Updating Polish version
  * debian/rules: creates apt-howto-ko.ps with a message, I cannot
    understand what's happening with korean version when compiling
    with debiandoc2latexps, but I'll contact the translator about this.
  * Added doc-base entries for korean and polish translations
  * Changed Build-Depends to Build-Depends-Indep

 -- Gustavo Noronha Silva <kov@debian.org>  Thu, 15 Nov 2001 01:53:55 -0200

apt-howto (1.7.3) unstable; urgency=low

  * Minor fixes, adding polish translation (a little older than
  than the english and portuguese version, but Pawel is going to
  fix this =)).

 -- Gustavo Noronha Silva <kov@debian.org>  Wed, 26 Sep 2001 16:44:57 -0300

apt-howto (1.7.2) unstable; urgency=low

  * Wrote a section about apt-listchanges.

 -- Gustavo Noronha Silva <kov@debian.org>  Mon, 10 Sep 2001 15:08:15 -0300

apt-howto (1.7) unstable; urgency=low

  * Wrote a section about APT "pinning" support.
  * Fixed this section with the suggestions of Branden Robinson
    in the debian-l10n-english list.

 -- Gustavo Noronha Silva <kov@debian.org>  Mon,  3 Sep 2001 21:14:03 -0300

apt-howto (1.6) unstable; urgency=low

  * Applied lots of patches correcting typos, mainly in the english
  version. Thanks to Bryan Stillwell <bryan@bokeoa.com> and
  Tecza Pawel <pawel.tecza@poland.com>

 -- Gustavo Noronha Silva <kov@debian.org>  Tue, 21 Aug 2001 19:19:16 -0300

apt-howto (1.5) unstable; urgency=low

  * Fixed gramatical error in package description (Closes: #107217)
    thanks to Viral <viral@debian.org>
  * Fixed some gramatical errors in doc-base files too, thanks to
    Viral <viral@debian.org>
  * Removed uneeded debian/dhelp file
  * Fixed some typos, thanks to Alexey Vyskubov <alexey.vyskubov@nokia.com>
  * Changed date from <date> to a static date (Closes: #107080)
    The other bugs are not apt-howto's bugs
  * Changed doc-base's section to Debian instead of debian 
    (Closes: #107887)
  * Changed -pt to -pt_BR

 -- Gustavo Noronha Silva <kov@debian.org>  Tue,  7 Aug 2001 16:14:31 -0300

apt-howto (1.4) unstable; urgency=low

  * New upstream release
  * Makes this package a debian-native package
  * Closing ITP: (Closes: #106566) I always forget this! 

 -- Gustavo Noronha Silva <kov@debian.org>  Sun, 29 Jul 2001 04:43:41 -0300

apt-howto (1.2-1) unstable; urgency=low

  * Initial Release.

 -- Gustavo Noronha Silva <kov@debian.org>  Wed, 25 Jul 2001 03:31:19 -0300


